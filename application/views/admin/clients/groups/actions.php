<?php if(isset($client)){ ?>
<h4 class="customer-profile-group-heading"><?php echo _l('actions'); ?></h4>
<?php if(has_permission('actions','','create')){ ?>
<a href="<?php echo admin_url('actions/action?rel_type=customer&rel_id='.$client->userid); ?>" class="btn btn-info mbot25<?php if($client->active == 0){echo ' disabled';} ?>"><?php echo _l('new_action'); ?></a>
<?php } ?>
<?php if(total_rows('tblactions',array('rel_type'=>'customer','rel_id'=>$client->userid))> 0 && (has_permission('actions','','create') || has_permission('actions','','edit'))){ ?>
<!--<a href="#" class="btn btn-info mbot25" data-toggle="modal" data-target="#sync_data_action_data"><?php // echo _l('sync_data'); ?></a>-->
<?php $this->load->view('admin/actions/sync_data',array('related'=>$client,'rel_id'=>$client->userid,'rel_type'=>'customer')); ?>
<?php } ?>
<?php
$table_data = array(
 _l('action') . ' #',
 _l('action_subject'),
// _l('action_total'),
 _l('action_date'),
// _l('action_open_till'),
// _l('tags'),
// _l('action_date_created'),
 _l('action_status'));
$custom_fields = get_custom_fields('action',array('show_on_table'=>1));
foreach($custom_fields as $field){
 array_push($table_data,$field['name']);
}
$table_data = do_action('actions_relation_table_columns',$table_data);
render_datatable($table_data,'actions-client-profile',[],[
    'data-last-order-identifier' => 'actions-relation',
    'data-default-order'         => get_table_last_order('actions-relation'),
]);
?>
<?php } ?>
