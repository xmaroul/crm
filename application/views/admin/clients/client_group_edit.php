<div class="modal fade" id="manage_customer_groups_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('customer_group_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($groups as $group) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $group['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
                                        echo icon_btn('clients/delete_group/' . $group['id'], 'remove', 'btn-danger _delete');
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<?php // $this->load->view('admin/clients/client_group'); ?>
<?php // init_tail(); ?>
<script>
//        jQuery(document).ready(function ($) {
//            initDataTable('.table-customer-groups', window.location.href, [1], [1]);
//            initDataTable('.table-customer-groups', window.location.href, [1], [1]);
//                 $('#manage_customer_groups_modal').on('shown.bs.modal', function(e) {
//                    initDataTable('.table-customer-groups', window.location.href, [1], [1]);
//                  var invoker = $(e.relatedTarget);
//                  var group_id = $(invoker).data('id');
          //        $('#customer_group_modal .add-title').removeClass('hide');
          //        $('#customer_group_modal .edit-title').addClass('hide');
          //        $('#customer_group_modal input[name="id"]').val('');
          //        $('#customer_group_modal input[name="name"]').val('');
          //        // is from the edit button
          //        if (typeof(group_id) !== 'undefined') {
          //            $('#customer_group_modal input[name="id"]').val(group_id);
          //            $('#customer_group_modal .add-title').addClass('hide');
          //            $('#customer_group_modal .edit-title').removeClass('hide');
          //            $('#customer_group_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
          //        }
//              });
//       });
  
//   });
//    function manage_customer_groups(form) {
//        var data = $(form).serialize();
//        var url = form.action;
//        $.post(url, data).done(function(response) {
//            response = JSON.parse(response);
//            if (response.success == true) {
//                if($.fn.DataTable.isDataTable('.table-customer-groups')){
//                    $('.table-customer-groups').DataTable().ajax.reload();
//                }
//                if($('body').hasClass('dynamic-create-groups') && typeof(response.id) != 'undefined') {
//                    var groups = $('select[name="groups_in[]"]');
//                    groups.prepend('<option value="'+response.id+'">'+response.name+'</option>');
//                    groups.selectpicker('refresh');
//                }
//                alert_float('success', response.message);
//            }
//            $('#customer_group_modal').modal('hide');
//        });
//        return false;
//    }

</script>
