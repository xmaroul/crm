<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="_filters _hidden_inputs">
            <?php
               foreach($statuses as $_status){
                $val = '';
                if($_status == $this->input->get('status')){
                  $val = $_status;
                }
                echo form_hidden('actions_'.$_status,$val);
               }
               foreach($years as $year){
                echo form_hidden('year_'.$year['year'],$year['year']);
               }
               foreach($actions_sale_agents as $agent){
                echo form_hidden('sale_agent_'.$agent['sale_agent']);
               }
               echo form_hidden('leads_related');
               echo form_hidden('customers_related');
               echo form_hidden('expired');
               ?>
         </div>
         <div class="col-md-12">
			 <!-- Start of filters -->
            <div class="panel_s mbot10 hidden">
               <div class="panel-body _buttons">
                  <?php if(has_permission('actions','','create')){ ?>
                  <a href="<?php echo admin_url('actions/action'); ?>" class="btn btn-info pull-left display-block">
                  <?php echo _l('new_action'); ?>
                  </a>
                  <?php } ?>
                  <!--<a href="<?php // echo admin_url('actions/pipeline/'.$switch_pipeline); ?>" class="btn btn-default mleft5 pull-left hidden-xs"><?php // echo _l('switch_to_pipeline'); ?></a>-->
                  <div class="display-block text-right">
                     <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu width300">
                           <li>
                              <a href="#" data-cview="all" onclick="dt_custom_view('','.table-suppliers-actions',''); return false;">
                              <?php echo _l('actions_list_all'); ?>
                              </a>
                           </li>
                           <li class="divider"></li>
                           <?php foreach($statuses as $status){ ?>
                           <li class="<?php if($this->input->get('status') == $status){echo 'active';} ?>">
                              <a href="#" data-cview="actions_<?php echo $status; ?>" onclick="dt_custom_view('actions_<?php echo $status; ?>','.table-suppliers-actions','actions_<?php echo $status; ?>'); return false;">
                              <?php echo format_action_status($status,'',false); ?>
                              </a>
                           </li>
                           <?php } ?>
                           <?php if(count($years) > 0){ ?>
                           <li class="divider"></li>
                           <?php foreach($years as $year){ ?>
                           <li class="active">
                              <a href="#" data-cview="year_<?php echo $year['year']; ?>" onclick="dt_custom_view(<?php echo $year['year']; ?>,'.table-suppliers-actions','year_<?php echo $year['year']; ?>'); return false;"><?php echo $year['year']; ?>
                              </a>
                           </li>
                           <?php } ?>
                           <?php } ?>
                           <?php if(count($actions_sale_agents) > 0){ ?>
                           <div class="clearfix"></div>
                           <li class="divider"></li>
                           <li class="dropdown-submenu pull-left">
                              <a href="#" tabindex="-1"><?php echo _l('sale_agent_string'); ?></a>
                              <ul class="dropdown-menu dropdown-menu-left">
                                 <?php foreach($actions_sale_agents as $agent){ ?>
                                 <li>
                                    <a href="#" data-cview="sale_agent_<?php echo $agent['sale_agent']; ?>" onclick="dt_custom_view('sale_agent_<?php echo $agent['sale_agent']; ?>','.table-suppliers-actions','sale_agent_<?php echo $agent['sale_agent']; ?>'); return false;"><?php echo get_staff_full_name($agent['sale_agent']); ?>
                                    </a>
                                 </li>
                                 <?php } ?>
                              </ul>
                           </li>
                           <?php } ?>
                           <div class="clearfix"></div>
                           <li class="divider"></li>
                           <li>
                              <a href="#" data-cview="expired" onclick="dt_custom_view('expired','.table-suppliers-actions','expired'); return false;">
                              <?php echo _l('action_expired'); ?>
                              </a>
                           </li>
                           <li>
                              <a href="#" data-cview="leads_related" onclick="dt_custom_view('leads_related','.table-suppliers-actions','leads_related'); return false;">
                              <?php echo _l('actions_leads_related'); ?>
                              </a>
                           </li>
                           <li>
                              <a href="#" data-cview="customers_related" onclick="dt_custom_view('customers_related','.table-suppliers-actions','customers_related'); return false;">
                              <?php echo _l('actions_customers_related'); ?>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <a href="#" class="btn btn-default btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-suppliers-actions','#action'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a>
                  </div>
               </div>
            </div>
			 <!-- End of filters -->
            <div class="row">
				
               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
					  <h3 class="no-mtop"><?php echo _l('suppliers_actions'); ?></h3>
                     <div class="panel-body">
                        <!-- if invoiceid found in url -->
                        <?php echo form_hidden('action_id',$action_id); ?>
                        <?php
                           $table_data = array(
                              _l('action') . ' #',
                              _l('action_subject'),
//							   _l('action_status'),
                              _l('action_to'),
//                              _l('action_total'),
                              _l('action_date'),
//                              _l('action_open_till'),
//                              _l('tags'),
//                              _l('action_date_created'),
                            );

                             $custom_fields = get_suppliers_action_custom_fields('action', "slug IN ('action_chreosi_supplier', 'action_pistosi_supplier')");
                             foreach($custom_fields as $field){
                                array_push($table_data,$field['name']);
                             }

                             $table_data = do_action('actions_table_columns',$table_data);
                             render_datatable($table_data,'suppliers-actions',[],[
                                 'data-last-order-identifier' => 'actions',
                                 'data-default-order'         => get_table_last_order('actions'),
                             ]);
                           ?>
                     </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="action" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('admin/includes/modals/sales_attach_file'); ?>
<script>var hidden_columns = [4,5,6];</script>
<?php init_tail(); ?>
<div id="convert_helper"></div>
<script>
   var action_id;
   $(function(){
     var Actions_ServerParams = {};
     $.each($('._hidden_inputs._filters input'),function(){
       Actions_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
     });
//     initDataTable('.table-actions', admin_url+'actions/table', ['undefined'], ['undefined'], Actions_ServerParams, [7, 'desc']);
	 initDataTable('.table-suppliers-actions', admin_url+'actions/suppliers_table', ['undefined'], ['undefined'], Actions_ServerParams);
     init_action();
   });
</script>
<?php echo app_script('assets/js','actions.js'); ?>
</body>
</html>
