<?php init_head(); ?>
<div id="wrapper">
   <div class="content accounting-template action">
      <div class="row">
         <?php
			$staff_user_id = get_staff_user_id();
			$staff_is_admin = is_admin($staff_user_id);
            if(isset($action)){
                echo form_hidden('isedit',$action->id);
                $status_id = $action->status;
            }
            $rel_type = '';
            $rel_id = '';
            $status_id = '';
            if(isset($action) || ($this->input->get('rel_id') && $this->input->get('rel_type'))){
				if($this->input->get('rel_id')){
					$rel_id = $this->input->get('rel_id');
					$rel_type = $this->input->get('rel_type');
				} else {
					$rel_id = $action->rel_id;
					$rel_type = $action->rel_type;
				}
            }
            ?>
         <?php echo form_open($this->uri->uri_string(),array('id'=>'action-form','class'=>'_transaction_form action-form')); ?>
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="row">
                     <?php if(isset($action)){ ?>
                     <div class="col-md-12">
                        <?php echo format_action_status($action->status); ?>
                     </div>
                     <div class="clearfix"></div>
                     <hr />
                     <?php } ?>
                     <div class="col-md-6 border-right">
                         <div class="row">
                          <div class="col-md-12">
                              <?php $value = (isset($action) ? _d($action->date) : _d(date('Y-m-d'))) ?>
                              <?php echo render_date_input('date','action_date',$value); ?>
                          </div>
							<div class="col-md-12 <?php  if($rel_type == 'supplier') echo 'hidden'; ?>">
                          <?php
                              $eidos_kinisis_value = isset($action) ? get_custom_field_value($action->id, 26, 'action') : NULL;
//                              echo render_s('custom_fields[action][23]','action_kind',$eidos_kinisis_value, $attrs); ?>
						   <?php $fc_rel_id = (isset($action) ? $action->id : false); ?>
						   <?php  echo render_custom_fields('action',$fc_rel_id, "slug = 'action_idos_kinisis' "); ?>
                              
							</div>
                        </div>
                        <?php $value = (isset($action) ? $action->subject : ''); ?>
                        <?php $attrs = (!isset($action) ? array('label_columns' => 12, 'text_columns' => 12) : array('autofocus'=>true, 'rows' => 15, 'label_columns' => 12, 'text_columns' => 12)); ?>
                        <?php 
                            echo render_textarea('subject','action_subject',$value,$attrs,array(),'','tinymce'); 
                        ?>
						<div class="row <?php  if($rel_type != 'supplier') echo 'hidden'; ?>">
                           <div class="col-md-12">
                              <?php
                                $supplier_xreosi_value = isset($action) ? get_custom_field_value($action->id, 49, 'action') : NULL;
                                echo render_input('custom_fields[action][49]','action_supplier_xreosi_value',$supplier_xreosi_value, 'number');
                              ?>
                           </div>
							<div class="col-md-12">
                              <?php
                                $supplier_pistosi_value = isset($action) ? get_custom_field_value($action->id, 50, 'action') : NULL;
                                echo render_input('custom_fields[action][50]','action_supplier_pistosi_value',$supplier_pistosi_value, 'number');
                              ?>
                           </div>
                       </div>
						<div class=" <?php  if($rel_type == 'supplier') echo 'hidden'; ?>">
                        <h4 class="no-mtop">Κοστολόγηση</h4>
                        <div class="row">
                           <div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?> ">
                              <?php
                                $timi_katalogou_value = isset($action) ? get_custom_field_value($action->id, 14, 'action') : NULL;
                                echo render_input('custom_fields[action][14]','action_catalogue_value',$timi_katalogou_value, 'number');
                              ?>
                           </div>
                           <div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                              <?php
                                $ekptwsi_rate_value = isset($action) ? get_custom_field_value($action->id, 15, 'action') : NULL;
                                echo render_input('custom_fields[action][15]','action_sales_rate',$ekptwsi_rate_value, 'number');
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                              <?php
                                $xrewsi_value = isset($action) ? get_custom_field_value($action->id, 16, 'action') : NULL;
                                echo render_input('custom_fields[action][16]','action_charge',$xrewsi_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                           <div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                               <?php
                                $metaforika_value = isset($action) ? get_custom_field_value($action->id, 17, 'action') : NULL;
                                echo render_input('custom_fields[action][17]','action_delivery_cost',$metaforika_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                               <?php
                                $maketa_value = isset($action) ? get_custom_field_value($action->id, 31, 'action') : NULL;
                                echo render_input('custom_fields[action][31]','action_maketa_cost',$maketa_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<div class="col-md-6 to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                               <?php
                                $aeras_value = isset($action) ? get_custom_field_value($action->id, 32, 'action') : NULL;
                                echo render_input('custom_fields[action][32]','action_aeras_cost',$aeras_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                       </div>
                        <div class="row to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                           <div class="col-md-12">
                              <?php
                                $synolo_value = isset($action) ? get_custom_field_value($action->id, 19, 'action') : NULL;
//                              echo render_input('custom_fields[action][19]','action_total',$synolo_value,'number',array('disabled' => 'disabled'));
							   echo render_input('custom_fields[action][19]','action_total',$synolo_value,'number');
                              ?>
                           </div>
                       </div>
                        <hr>
                        <div class="row to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                           <div class="col-md-12" id="extra_sale_value_container">
                               <?php
                              $extra_sale_value = isset($action) ? get_custom_field_value($action->id, 18, 'action') : NULL;
                              echo render_input('custom_fields[action][18]','action_extra_sales_rate',$extra_sale_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<?php $sxolio_ekptwsis_value = isset($action) ? get_custom_field_value($action->id, 33, 'action') : NULL; ?>
							
							<div class="col-md-12" id="extra_sale_comment_container" class="<?php if(!empty($sxolio_ekptwsis_value)) echo 'hidden'; ?> ">
                               <?php
                              echo render_input('custom_fields[action][33]','action_extra_sales_comment',$sxolio_ekptwsis_value, '', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                        </div>
                        <div class="row to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                           <div class="col-md-12">
                               <?php
                              $synolo_geniko_value = isset($action) ? get_custom_field_value($action->id, 20, 'action') : NULL;
//                              echo render_input('custom_fields[action][20]','action_full_total',$synolo_geniko_value,'number',array('disabled' => 'disabled'));
							  echo render_input('custom_fields[action][20]','action_full_total',$synolo_geniko_value,'number');
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-12">
                               <?php
                              $pistwsi_value = isset($action) ? get_custom_field_value($action->id, 21, 'action') : NULL;
                              echo render_input('custom_fields[action][21]','action_credit',$pistwsi_value, 'number', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                       </div>
                        <div class="row to-hide-of-idos <?php  if($eidos_kinisis_value == 'Είσπραξη') echo 'hidden'; ?>">
                           <div class="col-md-12">
                               <?php
                              $ypoloipo_value = isset($action) ? get_custom_field_value($action->id, 22, 'action') : NULL;
//                              echo render_input('custom_fields[action][22]','action_balance',$ypoloipo_value,'number',array('disabled' => 'disabled'));
							  echo render_input('custom_fields[action][22]','action_balance',$ypoloipo_value,'number');
                              ?>
                           </div>
                       </div>
                     </div>
					</div>
                     <div class="col-md-6">
                                <?php
								  if($rel_type != 'supplier'){
									$action_id = !isset($action) ? FALSE : $action->id;
									$priority_value = isset($action) ? get_custom_field_value($action->id, 28, 'action') : NULL;
									$priority_options = get_select_custom_fields_options("slug = 'action_proteraiotita'");
									$priority_default_value = NULL;
									foreach($priority_options as $id_name){
										if (trim($id_name['name']) == trim($priority_value)){
											$priority_default_value = $id_name['id'];
											break;
										}
									}
									echo render_select('custom_fields[action][28]', $priority_options, array('id','name'), 'action_proteraiotita', $priority_default_value);
								  }
                              ?>
                              <div class="form-group row select-placeholder <?php  if($rel_type == 'supplier') echo 'hidden'; ?>">
                                 <label for="status" class="control-label col-md-4"><?php echo _l('action_status'); ?></label>
                                 <?php
                                    $disabled = '';
                                    if(isset($action)){
                                     if($action->estimate_id != NULL || $action->invoice_id != NULL){
                                       $disabled = 'disabled';
                                     }
                                    }
                                    ?>
								 <div class="col-md-8">
									<select name="status" id="status" class="selectpicker" data-width="100%" <?php echo $disabled; ?> data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
									   <?php foreach($statuses as $status){ ?>
									   <option value="<?php echo $status; ?>" <?php if((isset($action) && $action->status == $status) || (!isset($action) && $status == 0)){echo 'selected';} ?>><?php echo format_action_status($status,'',false); ?></option>
									   <?php } ?>
									</select>
								</div>
                              </div>
						 
							 <div class=" <?php if(isset($status_id) && $status_id != 1){echo ' hide';} ?>" id="action_delay_comment_wrapper">
                             <?php
                              $anamoni_comment_value = isset($action) ? get_custom_field_value($action->id, 27, 'action') : NULL;
                              echo render_input('custom_fields[action][27]','action_delay_comment',$anamoni_comment_value);
                              ?>
                           </div>
						 
							<?php
							  if($rel_type != 'supplier'){
									$selected = array();
									if(isset($action->assigned)){
									foreach($action->assigned as $member){
											array_push($selected,$member['staff_id']);
									}
									} else {
											array_push($selected,get_staff_user_id());
									}
									echo render_select('assigned[]',$staff,array('staffid',array('firstname','lastname')),'action_assigned',$selected, array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
							  }
                               
							?>
                        <div class="form-group row select-placeholder">
                           <label for="rel_type" class="control-label  col-md-4"><?php echo _l('action_related'); ?></label>
						   <div class="col-md-8">
								<select <?php // if(!empty($rel_id)){echo 'disabled';} ?> name="rel_type" id="rel_type" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
								   <option value=""></option>
								   <option value="lead" <?php if((isset($action) && $action->rel_type == 'lead') || $this->input->get('rel_type')){if($rel_type == 'lead'){echo 'selected';}} ?>><?php echo _l('action_for_lead'); ?></option>
								   <option value="customer" <?php if((isset($action) &&  $action->rel_type == 'customer') || $this->input->get('rel_type')){if($rel_type == 'customer'){echo 'selected';}} ?>><?php echo _l('action_for_customer'); ?></option>
								   <option value="supplier" <?php if((isset($action) &&  $action->rel_type == 'supplier') || $this->input->get('rel_type')){if($rel_type == 'supplier'){echo 'selected';}} ?>><?php echo _l('action_for_supplier'); ?></option>
								</select>
						   </div>
                        </div>
                        <div class="form-group row select-placeholder <?php if($rel_id == ''){echo ' hide';} ?> " id="rel_id_wrapper">
                           <label for="rel_id" class=" control-label col-md-4">
							   <span class="rel_id_label"></span>
						   </label>
								<div id="rel_id_select">
									<div class="col-md-8">
								   <select <?php // if(!empty($rel_id)){echo 'disabled';} ?> name="rel_id" id="rel_id" class="ajax-search" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
								   <?php 
									if($rel_id != '' && $rel_type != ''){
									  $rel_data = get_relation_data($rel_type,$rel_id);
									  $rel_val = get_relation_values($rel_data,$rel_type);
										 echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
									  } ?>
								   </select>
								</div>
							</div>
                        </div>
                        <?php 
							$value = (isset($action) ? $action->action_to : '');
							if (empty($value) && !empty($rel_val)){
								$value = $rel_val['name'];
							}
						?>
                        <?php echo render_input('action_to','action_to',$value); ?>
                        <?php // $value = (isset($action) ? $action->address : ''); ?>
                        <?php // echo render_textarea('address','action_address',$value); ?>
						 <?php $attrs = array('rows' => 8); ?>
                        <div class="row <?php  if($rel_type == 'supplier') echo 'hidden'; ?>">
							<div class="col-md-12">
                             <?php
                              $xrewsis_value = isset($action) ? get_custom_field_value($action->id, 23, 'action') : NULL;
                              echo render_textarea('custom_fields[action][23]','action_charge_comment',$xrewsis_value, $attrs);
                              ?>
                           </div>
							<div class="col-md-12">
                             <?php
                              $paragwgis_value = isset($action) ? get_custom_field_value($action->id, 24, 'action') : NULL;
                              echo render_textarea('custom_fields[action][24]','action_production_comment',$paragwgis_value, $attrs);
                              ?>
                           </div>
							<div class="col-md-12">
                             <?php
                              $deliver_value = isset($action) ? get_custom_field_value($action->id, 25, 'action') : NULL;
                              echo render_textarea('custom_fields[action][25]','action_deliver_comment',$deliver_value, $attrs);
                              ?>
                           </div>
                        </div>
						<div class="form-group no-mbot">
                           <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                           <input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo (isset($action) ? prep_tags_input(get_tags_in($action->id,'action')) : ''); ?>" data-role="tagsinput">
                        </div>
                     </div>
					 <div class="clearfix"></div>
					<hr>
					
					<?php
					if($rel_type != 'supplier'){
						$user_can_see_extra_fields = $staff_is_admin || ($staff_user_id == 4);
					?>
					<div class="col-md-6 border-right fast-container <?php if ($priority_value != 'Γρήγορα' || !$user_can_see_extra_fields ) echo 'hidden'?>">
						<?php 
							$perigrafi_value = isset($action) ? get_custom_field_value($action->id, 46, 'action') : NULL;
							$attrs = (!isset($action) ? array('label_columns' => 12, 'text_columns' => 12) : array('autofocus'=>true, 'rows' => 15, 'label_columns' => 12, 'text_columns' => 12));
							echo render_textarea('custom_fields[action][46]', 'Περιγραφή', $perigrafi_value, $attrs,array(),'','tinymce');
						?>
						<h4 class="no-mtop">Κοστολόγηση</h4>
                        <div class="row">
                           <div class="col-md-6">
                              <?php
                                $timi_katalogou_f_value = isset($action) ? get_custom_field_value($action->id, 34, 'action') : NULL;
                                echo render_input('custom_fields[action][34]','action_catalogue_value',$timi_katalogou_f_value, 'number');
                              ?>
                           </div>
                           <div class="col-md-6">
                              <?php
                                $ekptwsi_rate_f_value = isset($action) ? get_custom_field_value($action->id, 35, 'action') : NULL;
                                echo render_input('custom_fields[action][35]','action_sales_rate',$ekptwsi_rate_f_value, 'number');
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-6">
                              <?php
                                $xrewsi_f_value = isset($action) ? get_custom_field_value($action->id, 36, 'action') : NULL;
                                echo render_input('custom_fields[action][36]','action_charge',$xrewsi_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                           <div class="col-md-6">
                               <?php
                                $metaforika_f_value = isset($action) ? get_custom_field_value($action->id, 37, 'action') : NULL;
                                echo render_input('custom_fields[action][37]','action_delivery_cost',$metaforika_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<div class="col-md-6">
                               <?php
                                $maketa_f_value = isset($action) ? get_custom_field_value($action->id, 38, 'action') : NULL;
                                echo render_input('custom_fields[action][38]','action_maketa_cost',$maketa_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<div class="col-md-6">
                               <?php
                                $aeras_f_value = isset($action) ? get_custom_field_value($action->id, 39, 'action') : NULL;
                                echo render_input('custom_fields[action][39]','action_aeras_cost',$aeras_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-12">
                              <?php
                                $synolo_f_value = isset($action) ? get_custom_field_value($action->id, 40, 'action') : NULL;
//                              echo render_input('custom_fields[action][19]','action_total',$synolo_value,'number',array('disabled' => 'disabled'));
							   echo render_input('custom_fields[action][40]','action_total',$synolo_f_value,'number');
                              ?>
                           </div>
                       </div>
                        <hr>
                        <div class="row">
                           <div class="col-md-12" id="extra_sale_value_container">
                               <?php
                              $extra_sale_f_value = isset($action) ? get_custom_field_value($action->id, 41, 'action') : NULL;
                              echo render_input('custom_fields[action][41]','action_extra_sales_rate',$extra_sale_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
							<?php $sxolio_ekptwsis_f_value = isset($action) ? get_custom_field_value($action->id, 42, 'action') : NULL; ?>
							
							<div class="col-md-12" id="extra_sale_comment_container_f" class="<?php if(!empty($sxolio_ekptwsis_value)) echo 'hidden'; ?> ">
                               <?php
                              echo render_input('custom_fields[action][42]','action_extra_sales_comment',$sxolio_ekptwsis_f_value, '', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                               <?php
                              $synolo_geniko_f_value = isset($action) ? get_custom_field_value($action->id, 43, 'action') : NULL;
//                              echo render_input('custom_fields[action][20]','action_full_total',$synolo_geniko_value,'number',array('disabled' => 'disabled'));
							  echo render_input('custom_fields[action][43]','action_full_total',$synolo_geniko_f_value,'number');
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-12">
                               <?php
                              $pistwsi_f_value = isset($action) ? get_custom_field_value($action->id, 44, 'action') : NULL;
                              echo render_input('custom_fields[action][44]','action_credit',$pistwsi_f_value, 'number', array('onFocus' => 'startCalcF();', 'onBlur' => 'stopCalc();'));
                              ?>
                           </div>
                       </div>
                        <div class="row">
                           <div class="col-md-12">
                               <?php
                              $ypoloipo_f_value = isset($action) ? get_custom_field_value($action->id, 45, 'action') : NULL;
//                              echo render_input('custom_fields[action][22]','action_balance',$ypoloipo_value,'number',array('disabled' => 'disabled'));
							  echo render_input('custom_fields[action][45]','action_balance',$ypoloipo_f_value,'number');
                              ?>
                           </div>
                       </div>
					</div>
					<?php } ?>
                  </div>
                  <div class="btn-bottom-toolbar bottom-transaction text-right">
                  <p class="no-mbot pull-left mtop5 btn-toolbar-notice"><?php echo _l('include_action_items_merge_field_help','<b>{action_items}</b>'); ?></p>
<!--                    <button type="button" class="btn btn-info mleft10 action-form-submit save-and-send transaction-submit">
                        <?php // echo _l('save_and_send'); ?>
                    </button>-->
                    <button class="btn btn-info mleft5 action-form-submit transaction-submit" type="button">
                      <?php echo _l('submit'); ?>
                    </button>
               </div>
               </div>
            </div>
         </div>
         <?php echo form_close(); ?>
         <?php $this->load->view('admin/invoice_items/item'); ?>
      </div>
      <div class="btn-bottom-pusher"></div>
   </div>
</div>
<?php // $this->load->view('admin/includes/modals/sales_attach_file'); ?>

<?php init_tail(); ?>
<script>
   var _rel_id = $('#rel_id'),
   _rel_type = $('#rel_type'),
   _rel_id_wrapper = $('#rel_id_wrapper'),
   data = {};
	if(_rel_id.val() !== null){
		_rel_type.prop('disabled', true);
		_rel_id.prop('disabled', true);
	}
	var _action_status = $('#status'),
	_action_action_delay_comment_wrapper = $('#action_delay_comment_wrapper');


   $(function(){
	   
		$('#action-form').on('submit', function() {
			_rel_type.prop('disabled', false);
			_rel_id.prop('disabled', false);
		});
	   $(".selectpicker[name='custom_fields[action][28]']").on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
			var selected = $(e.currentTarget).val();
			if (selected == 'Γρήγορα'){
				$("div.fast-container").removeClass('hidden');
			}else{
				$("div.fast-container").addClass('hidden');
			}
		});
		$(".selectpicker[name='custom_fields[action][26]']").on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
			var selected = $(e.currentTarget).val();
			if (selected == 'Είσπραξη'){
				$( "div.to-hide-of-idos" ).each(function( index ) {
					$(this).addClass('hidden');
				});
			}else{
				$( "div.to-hide-of-idos" ).each(function( index ) {
					$(this).removeClass('hidden');
				});
			}
		});
	     
    init_currency_symbol();
    // Maybe items ajax search
    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');
    validate_action_form();
    $('body').on('change','#rel_id', function() {
     if($(this).val() != ''){
      $.get(admin_url + 'actions/get_relation_data_values/' + $(this).val() + '/' + _rel_type.val(), function(response) {
        $('input[name="action_to"]').val(response.to);
        $('textarea[name="address"]').val(response.address);
        $('input[name="email"]').val(response.email);
        $('input[name="phone"]').val(response.phone);
        $('input[name="city"]').val(response.city);
        $('input[name="state"]').val(response.state);
        $('input[name="zip"]').val(response.zip);
        $('select[name="country"]').selectpicker('val',response.country);
        var currency_selector = $('#currency');
        if(_rel_type.val() == 'customer'){
          if(typeof(currency_selector.attr('multi-currency')) == 'undefined'){
            currency_selector.attr('disabled',true);
          }

         } else {
           currency_selector.attr('disabled',false);
        }
        var action_to_wrapper = $('[app-field-wrapper="action_to"]');
        if(response.is_using_company == false && !empty(response.company)) {
          action_to_wrapper.find('#use_company_name').remove();
          action_to_wrapper.find('#use_company_help').remove();
          action_to_wrapper.append('<div id="use_company_help" class="hide">'+response.company+'</div>');
          action_to_wrapper.find('label')
          .prepend("<a href=\"#\" id=\"use_company_name\" data-toggle=\"tooltip\" data-title=\"<?php echo _l('use_company_name_instead'); ?>\" onclick='document.getElementById(\"action_to\").value = document.getElementById(\"use_company_help\").innerHTML.trim(); this.remove();'><i class=\"fa fa-building-o\"></i></a> ");
        } else {
          action_to_wrapper.find('label #use_company_name').remove();
          action_to_wrapper.find('label #use_company_help').remove();
        }
       /* Check if customer default currency is passed */
       if(response.currency){
         currency_selector.selectpicker('val',response.currency);
       } else {
        /* Revert back to base currency */
        currency_selector.selectpicker('val',currency_selector.data('base'));
      }
      currency_selector.selectpicker('refresh');
      currency_selector.change();
    }, 'json');
    }
   });
    $('.rel_id_label').html(_rel_type.find('option:selected').text());
    _rel_type.on('change', function() {
      var clonedSelect = _rel_id.html('').clone();
      _rel_id.selectpicker('destroy').remove();
      _rel_id = clonedSelect;
      $('#rel_id_select').append(clonedSelect);
      action_rel_id_select();
      if($(this).val() != ''){
        _rel_id_wrapper.removeClass('hide');
      } else {
        _rel_id_wrapper.addClass('hide');
      }
      $('.rel_id_label').html(_rel_type.find('option:selected').text());
    });
    action_rel_id_select();
    <?php if(!isset($action) && $rel_id != ''){ ?>
      _rel_id.change();
      <?php } ?>
    });
	_action_status.on('change', function() {
      if($(this).val() == 1){
        _action_action_delay_comment_wrapper.removeClass('hide');
      } else {
        _action_action_delay_comment_wrapper.addClass('hide');
      }
    });
   function action_rel_id_select(){
      var serverData = {};
      serverData.rel_id = _rel_id.val();
      data.type = _rel_type.val();
      <?php if(isset($action)){ ?>
        serverData.connection_type = 'action';
        serverData.connection_id = '<?php echo $action->id; ?>';
      <?php } ?>
      init_ajax_search(_rel_type.val(),_rel_id,serverData);
   }
   function validate_action_form(){
      _validate_form($('#action-form'), {
//        subject : 'required',
        action_to : 'required',
        rel_type: 'required',
        rel_id : 'required',
        date : 'required',
        email: {
         email:true,
         required:true
       },
//       currency : 'required',
     });
   }
   
	function startCalc(){
		interval = setInterval("calc()",1);
		 showComment();
	}
	function startCalcF(){
		interval = setInterval("calcF()",1);
		 showCommentF();
	}
	function showComment(){
		$('#extra_sale_comment_container').removeClass('hidden');
	}
	function showCommentF(){
		$('#extra_sale_comment_container_f').removeClass('hidden');
	}
	function calc(){
		charge = document.getElementsByName("custom_fields[action][16]")[0].value;
		transfer = document.getElementsByName("custom_fields[action][17]")[0].value;
		maketa = document.getElementsByName("custom_fields[action][31]")[0].value;
		aeras = document.getElementsByName("custom_fields[action][32]")[0].value;
		extra_ekptwsi = document.getElementsByName("custom_fields[action][18]")[0].value;
		pistwsi = document.getElementsByName("custom_fields[action][21]")[0].value;
		var synolo = document.getElementsByName("custom_fields[action][19]")[0];
		var geniko_synolo = document.getElementsByName("custom_fields[action][20]")[0];
		var ypoloipo = document.getElementsByName("custom_fields[action][22]")[0];
//		custom_fields[action][18]
		synolo.value = geniko_synolo.value = ypoloipo.value = (charge * 1) + (transfer * 1) + (maketa * 1) + (aeras * 1);
		if (extra_ekptwsi){
			geniko_synolo.value = ypoloipo.value = (charge * 1) + (transfer * 1) + (maketa * 1) + (aeras * 1) - (extra_ekptwsi *1);
		}
		if (pistwsi){
			ypoloipo.value = (charge * 1) + (transfer * 1) + (maketa * 1) + (aeras * 1) - (extra_ekptwsi *1) - pistwsi;
		}
	}
	function calcF(){
		chargeF = document.getElementsByName("custom_fields[action][36]")[0].value;
		transferF = document.getElementsByName("custom_fields[action][37]")[0].value;
		maketaF = document.getElementsByName("custom_fields[action][38]")[0].value;
		aerasF = document.getElementsByName("custom_fields[action][39]")[0].value;
		extra_ekptwsiF = document.getElementsByName("custom_fields[action][41]")[0].value;
		pistwsiF = document.getElementsByName("custom_fields[action][44]")[0].value;
		var synoloF = document.getElementsByName("custom_fields[action][40]")[0];
		var geniko_synoloF = document.getElementsByName("custom_fields[action][43]")[0];
		var ypoloipoF = document.getElementsByName("custom_fields[action][45]")[0];
//		custom_fields[action][18]
		synoloF.value = geniko_synoloF.value = ypoloipoF.value = (chargeF * 1) + (transferF * 1) + (maketaF * 1) + (aerasF * 1);
		if (extra_ekptwsiF){
			geniko_synoloF.value = ypoloipoF.value = (chargeF * 1) + (transferF * 1) + (maketaF * 1) + (aerasF * 1) - (extra_ekptwsiF *1);
		}
		if (pistwsiF){
			ypoloipoF.value = (chargeF * 1) + (transferF * 1) + (maketaF * 1) + (aerasF * 1) - (extra_ekptwsiF * 1) - pistwsiF;
		}
	}
	function stopCalc(){
		clearInterval(interval);
		checkToHideComment();
	}
	
	function checkToHideComment(){
		sale = $('#extra_sale_value_container input').val();
		if(sale == '' || sale == 0 ) {
          $('#extra_sale_comment_container').addClass('hidden');
		}
		salef = $('#extra_sale_value_container_f input').val();
		if(salef == '' || salef == 0 ) {
          $('#extra_sale_comment_container_f').addClass('hidden');
		}
	}
</script>
<?php // echo app_script('assets/js','proposals.js'); ?>
</body>
</html>
