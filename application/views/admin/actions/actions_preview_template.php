<?php echo form_hidden('_attachment_sale_id',$action->id); ?>
<?php echo form_hidden('_attachment_sale_type','action'); ?>
<div class="panel_s">
   <div class="panel-body">
      <div class="horizontal-scrollable-tabs preview-tabs-top">
         <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
         <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
         <div class="horizontal-tabs">
            <ul class="nav nav-tabs nav-tabs-horizontal mbot15" role="tablist">
               <li role="presentation" class="active">
                  <a href="#tab_action" aria-controls="tab_action" role="tab" data-toggle="tab">
                  <?php echo _l('action'); ?>
                  </a>
               </li>
               <?php if(isset($action)){ ?>
               <li role="presentation">
                  <a href="#tab_comments" onclick="get_action_comments(); return false;" aria-controls="tab_comments" role="tab" data-toggle="tab">
                  <?php echo _l('action_comments'); ?>
                  </a>
               </li>
               <li role="presentation">
                  <a href="#tab_reminders" onclick="initDataTable('.table-reminders', admin_url + 'misc/get_reminders/' + <?php echo $action->id ;?> + '/' + 'action', undefined, undefined, undefined,[1,'asc']); return false;" aria-controls="tab_reminders" role="tab" data-toggle="tab">
                  <?php echo _l('estimate_reminders'); ?>
                  <?php
                     $total_reminders = total_rows('tblreminders',
                      array(
                       'isnotified'=>0,
                       'staff'=>get_staff_user_id(),
                       'rel_type'=>'action',
                       'rel_id'=>$action->id
                       )
                      );
                     if($total_reminders > 0){
                      echo '<span class="badge">'.$total_reminders.'</span>';
                     }
                     ?>
                  </a>
               </li>
<!--               <li role="presentation" class="tab-separator">
                  <a href="#tab_tasks" onclick="init_rel_tasks_table(<?php // echo $action->id; ?>,'action'); return false;" aria-controls="tab_tasks" role="tab" data-toggle="tab">
                  <?php // echo _l('tasks'); ?>
                  </a>
               </li>-->
				
                <li role="presentation">
                  <a href="#" data-target="#sales_attach_file" aria-controls="tab_tasks" role="tab" data-toggle="modal">
                  <?php echo _l('attach_file'); ?>
                  </a>
                </li>
                 <li role="presentation" class="tab-separator">
                     <a href="#tab_notes" onclick="get_sales_notes(<?php echo $action->id; ?>,'actions'); return false" aria-controls="tab_notes" role="tab" data-toggle="tab">
                     <?php echo _l('estimate_notes'); ?>
                     <span class="notes-total">
                        <?php if($totalNotes > 0){ ?>
                           <span class="badge"><?php echo $totalNotes; ?></span>
                        <?php } ?>
                     </span>
                     </a>
                  </li>
<!--               <li role="presentation" data-toggle="tooltip" title="<?php // echo _l('emails_tracking'); ?>" class="tab-separator">
                  <a href="#tab_emails_tracking" aria-controls="tab_emails_tracking" role="tab" data-toggle="tab">
                    <?php // if(!is_mobile()){ ?>
                     <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                     <?php // } else { ?>
                     <?php // echo _l('emails_tracking'); ?>
                     <?php // } ?>
                  </a>
               </li>
               <li role="presentation" data-toggle="tooltip" data-title="<?php // echo _l('view_tracking'); ?>" class="tab-separator">
                  <a href="#tab_views" aria-controls="tab_views" role="tab" data-toggle="tab">
                    <?php // if(!is_mobile()){ ?>
                     <i class="fa fa-eye"></i>
                     <?php // } else { ?>
                     <?php // echo _l('view_tracking'); ?>
                     <?php // } ?>
                  </a>
               </li>-->
               <li role="presentation" data-toggle="tooltip" data-title="<?php echo _l('toggle_full_view'); ?>" class="tab-separator toggle_view">
                  <a href="#" onclick="small_table_full_view(); return false;">
                  <i class="fa fa-expand"></i></a>
               </li>
               <?php } ?>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3">
            <?php echo format_action_status($action->status,'pull-left mright5 mtop5'); ?>
         </div>
         <div class="col-md-9 text-right _buttons action_buttons">
            <?php if(has_permission('actions','','edit')){ ?>
            <a href="<?php echo admin_url('actions/action/'.$action->id); ?>" data-placement="left" data-toggle="tooltip" title="<?php echo _l('action_edit'); ?>" class="btn btn-default btn-with-tooltip" data-placement="bottom"><i class="fa fa-pencil-square-o"></i></a>
            <?php } ?>
            <div class="btn-group">
               <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-pdf-o"></i><?php if(is_mobile()){echo ' PDF';} ?> <span class="caret"></span></a>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li class="hidden-xs"><a href="<?php echo admin_url('actions/pdf/'.$action->id.'?output_type=I'); ?>"><?php echo _l('view_pdf'); ?></a></li>
                  <li class="hidden-xs"><a href="<?php echo admin_url('actions/pdf/'.$action->id.'?output_type=I'); ?>" target="_blank"><?php echo _l('view_pdf_in_new_window'); ?></a></li>
                  <li><a href="<?php echo admin_url('actions/pdf/'.$action->id); ?>"><?php echo _l('download'); ?></a></li>
                  <li>
                     <a href="<?php echo admin_url('actions/pdf/'.$action->id.'?print=true'); ?>" target="_blank">
                     <?php echo _l('print'); ?>
                     </a>
                  </li>
               </ul>
            </div>
            <a href="#" class="btn btn-default btn-with-tooltip" data-target="#action_send_to_customer" data-toggle="modal"><span data-toggle="tooltip" class="btn-with-tooltip" data-title="<?php echo _l('action_send_to_email'); ?>" data-placement="bottom"><i class="fa fa-envelope"></i></span></a>
            <div class="btn-group ">
               <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <?php echo _l('more'); ?> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                     <a href="<?php echo site_url('action/'.$action->id .'/'.$action->hash); ?>" target="_blank"><?php echo _l('action_view'); ?></a>
                  </li>
                  <?php if(!empty($action->open_till) && date('Y-m-d') < $action->open_till && ($action->status == 4 || $action->status == 1) && is_actions_expiry_reminders_enabled()) { ?>
                  <li>
                     <a href="<?php echo admin_url('actions/send_expiry_reminder/'.$action->id); ?>"><?php echo _l('send_expiry_reminder'); ?></a>
                  </li>
                  <?php } ?>
<!--                  <li>
                     <a href="#" data-toggle="modal" data-target="#sales_attach_file"><?php // echo _l('attach_file'); ?></a>
                  </li>-->
                  <?php if(has_permission('actions','','create')){ ?>
                  <li>
                     <a href="<?php echo admin_url() . 'actions/copy/'.$action->id; ?>"><?php echo _l('action_copy'); ?></a>
                  </li>
                  <?php } ?>
                  <?php if($action->estimate_id == NULL && $action->invoice_id == NULL){ ?>
                  <?php foreach($action_statuses as $status){
                     if(has_permission('actions','','edit')){
                      if($action->status != $status){ ?>
                  <li>
                     <a href="<?php echo admin_url() . 'actions/mark_action_status/'.$status.'/'.$action->id; ?>"><?php echo _l('action_mark_as',format_action_status($status,'',false)); ?></a>
                  </li>
                  <?php
                     } } } ?>
                  <?php } ?>
                  <?php if(!empty($action->signature) && has_permission('actions','','delete')){ ?>
                  <li>
                     <a href="<?php echo admin_url('actions/clear_signature/'.$action->id); ?>" class="_delete">
                     <?php echo _l('clear_signature'); ?>
                     </a>
                  </li>
                  <?php } ?>
                  <?php if(has_permission('actions','','delete')){ ?>
                  <li>
                     <a href="<?php echo admin_url() . 'actions/delete/'.$action->id; ?>" class="text-danger delete-text _delete"><?php echo _l('action_delete'); ?></a>
                  </li>
                  <?php } ?>
               </ul>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <hr class="hr-panel-heading" />
      <div class="row">
         <div class="col-md-12">
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="tab_action">
                  <div class="row mtop10">
                     <?php if($action->status == 3 && !empty($action->acceptance_firstname) && !empty($action->acceptance_lastname) && !empty($action->acceptance_email)){ ?>
                     <div class="col-md-12">
                        <div class="alert alert-info">
                           <?php echo _l('accepted_identity_info',array(
                              _l('action_lowercase'),
                              '<b>'.$action->acceptance_firstname . ' ' . $action->acceptance_lastname . '</b> (<a href="mailto:'.$action->acceptance_email.'">'.$action->acceptance_email.'</a>)',
                              '<b>'. _dt($action->acceptance_date).'</b>',
                              '<b>'.$action->acceptance_ip.'</b>'.(is_admin() ? '&nbsp;<a href="'.admin_url('actions/clear_acceptance_info/'.$action->id).'" class="_delete text-muted" data-toggle="tooltip" data-title="'._l('clear_this_information').'"><i class="fa fa-remove"></i></a>' : '')
                              )); ?>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="col-md-6">
                        <h4 class="bold">
                           <?php
                              $tags = get_tags_in($action->id,'action');
                              if(count($tags) > 0){
                               echo '<i class="fa fa-tag" aria-hidden="true" data-toggle="tooltip" data-title="'.implode(', ',$tags).'"></i>';
                              }
                              ?>
                           <a href="<?php echo admin_url('actions/action/'.$action->id); ?>">
                           <span id="action-number">
                           <?php echo format_action_number($action->id); ?>
                           </span>
                           </a>
                        </h4>
                        <p class="bold mbot15">
                            <a href="<?php echo admin_url('actions/action/'.$action->id); ?>"><?php echo $action->subject; ?></a>
                        </p>
                        <p><?php echo get_custom_field_value($action->id, 12, 'action')?></p>
                        
                        
                        <address>
                           <?php echo format_organization_info(); ?>
                        </address>
                     </div>
                     <div class="col-md-6 text-right">
                        <address>
                           <span class="bold"><?php echo _l('action_to'); ?>:</span><br />
                           <?php echo format_action_info($action,'admin'); ?>
                        </address>
                        <div class="col-md-12">
                            <div class="col-md-8">
                            <?php $timi_katalogou_value = isset($action) ? get_custom_field_value($action->id, 14, 'action') : NULL; ?>
                            <span class="bold"><?php echo _l('action_catalogue_value'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                            <?php 
								if ($timi_katalogou_value){
									echo format_money($timi_katalogou_value, '€');
								}else{
									echo '- €';
								}?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $ekptwsi_rate_value = isset($action) ? get_custom_field_value($action->id, 15, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_sales_rate'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                            <?php echo $ekptwsi_rate_value; ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $xrewsi_value = isset($action) ? get_custom_field_value($action->id, 16, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_charge'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                                <?php
									if ($xrewsi_value){
										echo format_money($xrewsi_value,'€'); 
									}else{
										echo '- €';
									}
								?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $metaforika_value = isset($action) ? get_custom_field_value($action->id, 17, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_delivery_cost'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                                <?php 
									if ($metaforika_value){
										echo format_money($metaforika_value,'€'); 
									}else{
										echo '- €';
									}
								?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $synolo_value = isset($action) ? get_custom_field_value($action->id, 19, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_total'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                                <?php 
									if ($synolo_value){
										echo format_money($synolo_value,'€'); 
									}else{
										echo '- €';
									}
								?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $extra_sale_value = isset($action) ? get_custom_field_value($action->id, 18, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_extra_sales_rate'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                                <?php 
									if ($extra_sale_value){
										echo format_money($extra_sale_value,'€');
									}else{
										echo '- €';
									}
								?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <?php $synolo_geniko_value = isset($action) ? get_custom_field_value($action->id, 20, 'action') : NULL; ?>
                                <span class="bold"><?php echo _l('action_full_total'); ?>:</span>
                            </div>
                            <div class="col-md-4">
                                <?php
								if ($synolo_geniko_value){
									echo format_money($synolo_geniko_value,'€');
								}else{
									echo '- €';
								}
								?>
                            </div>
                        </div>
                     </div>
                  </div>
                  <hr class="hr-panel-heading" />
                  <?php
                     if(count($action->attachments) > 0){ ?>
                  <p class="bold"><?php echo _l('action_files'); ?></p>
                  <?php foreach($action->attachments as $attachment){
                     $attachment_url = site_url('download/file/sales_attachment/'.$attachment['attachment_key']);
                     if(!empty($attachment['external'])){
                        $attachment_url = $attachment['external_link'];
                     }
                     ?>
                  <div class="mbot15 row" data-attachment-id="<?php echo $attachment['id']; ?>">
                     <div class="col-md-8">
                        <div class="pull-left"><i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i></div>
                        <a href="<?php echo $attachment_url; ?>" target="_blank"><?php echo $attachment['file_name']; ?></a>
                        <br />
                        <small class="text-muted"> <?php echo $attachment['filetype']; ?></small>
                     </div>
                     <div class="col-md-4 text-right">
                        <?php if($attachment['visible_to_customer'] == 0){
                           $icon = 'fa-toggle-off';
                           $tooltip = _l('show_to_customer');
                           } else {
                           $icon = 'fa-toggle-on';
                           $tooltip = _l('hide_from_customer');
                           }
                           ?>
                        <a href="#" data-toggle="tooltip" onclick="toggle_file_visibility(<?php echo $attachment['id']; ?>,<?php echo $action->id; ?>,this); return false;" data-title="<?php echo $tooltip; ?>"><i class="fa <?php echo $icon; ?>" aria-hidden="true"></i></a>
                        <?php if($attachment['staffid'] == get_staff_user_id() || is_admin()){ ?>
                        <a href="#" class="text-danger" onclick="delete_action_attachment(<?php echo $attachment['id']; ?>); return false;"><i class="fa fa-times"></i></a>
                        <?php } ?>
                     </div>
                  </div>
                  <?php } ?>
                  <?php } ?>
                  <div class="clearfix"></div>
                  <?php // if(isset($action_merge_fields)){ ?>
<!--                  <p class="bold text-right"><a href="#" onclick="slideToggle('.avilable_merge_fields'); return false;">//<?php // echo _l('available_merge_fields'); ?></a></p>
                  <hr class="hr-panel-heading" />
                  <div class="hide avilable_merge_fields mtop15">
                     <div class="row">
                        <div class="col-md-12">
                           <ul class="list-group">
                              //<?php // foreach($action_merge_fields as $field){
//                                 foreach($field as $f){
//                                 if($f['key'] != '{email_signature}'){
//                                   echo '<li class="list-group-item"><b>'.$f['name'].'</b> <a href="#" class="pull-right" onclick="insert_action_merge_field(this); return false;">'.$f['key'].'</a></li>';
//                                 }
//                                 }
//                                 } ?>
                           </ul>
                        </div>
                     </div>
                  </div>-->
                  <?php // } ?>
<!--                  <div class="editable action tc-content" id="action_content_area" style="border:1px solid #d2d2d2;min-height:70px;border-radius:4px;">
                     <?php // if(empty($action->content)){
//                        echo '<span class="text-danger text-uppercase mtop15 editor-add-content-notice"> ' . _l('click_to_add_content') . '</span>';
//                        } else {
//                        echo $action->content;
//                        }
                        ?>
                  </div>-->
               </div>
               <div role="tabpanel" class="tab-pane" id="tab_comments">
                  <div class="row action-comments mtop15">
                     <div class="col-md-12">
                        <div id="action-comments"></div>
                        <div class="clearfix"></div>
                        <textarea name="content" id="comment" rows="4" class="form-control mtop15 action-comment"></textarea>
                        <button type="button" class="btn btn-info mtop10 pull-right" onclick="add_action_comment();"><?php echo _l('action_add_comment'); ?></button>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="tab_notes">
                  <?php echo form_open(admin_url('actions/add_note/'.$action->id),array('id'=>'sales-notes','class'=>'action-notes-form')); ?>
                  <?php echo render_textarea('description'); ?>
                  <div class="text-right">
                     <button type="submit" class="btn btn-info mtop15 mbot15"><?php echo _l('estimate_add_note'); ?></button>
                  </div>
                  <?php echo form_close(); ?>
                  <hr />
                  <div class="panel_s mtop20 no-shadow" id="sales_notes_area">
                  </div>
               </div>
<!--               <div role="tabpanel" class="tab-pane" id="tab_emails_tracking">
                  <?php
//                     $this->load->view('admin/includes/emails_tracking',array(
//                       'tracked_emails'=>
//                       get_tracked_emails($action->id, 'action'))
//                       );
                     ?>
               </div>-->
<!--               <div role="tabpanel" class="tab-pane" id="tab_tasks">
                  <?php // init_relation_tasks_table(array( 'data-new-rel-id'=>$action->id,'data-new-rel-type'=>'action')); ?>
               </div>-->
               <div role="tabpanel" class="tab-pane" id="tab_reminders">
                  <a href="#" data-toggle="modal" class="btn btn-info" data-target=".reminder-modal-action-<?php  echo $action->id; ?>"><i class="fa fa-bell-o"></i> <?php echo _l('action_set_reminder_title'); ?></a>
                  <hr />
                  <?php  render_datatable(array( _l( 'reminder_description'), _l( 'reminder_date'), _l( 'reminder_staff'), _l( 'reminder_is_notified')), 'reminders'); ?>
                  <?php  $this->load->view('admin/includes/modals/reminder',array('id'=>$action->id,'name'=>'action','members'=>$members,'reminder_title'=>_l('action_set_reminder_title'))); ?>
               </div>
               <div role="tabpanel" class="tab-pane ptop10" id="tab_views">
                  <?php
                     $views_activity = get_views_tracking('action',$action->id);
                       if(count($views_activity) === 0) {
                     echo '<h4 class="no-margin">'._l('not_viewed_yet',_l('action_lowercase')).'</h4>';
                     }
                     foreach($views_activity as $activity){ ?>
                  <p class="text-success no-margin">
                     <?php echo _l('view_date') . ': ' . _dt($activity['date']); ?>
                  </p>
                  <p class="text-muted">
                     <?php echo _l('view_ip') . ': ' . $activity['view_ip']; ?>
                  </p>
                  <hr />
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php // $this->load->view('admin/actions/send_action_to_email_template'); ?>
<script>
   init_btn_with_tooltips();
   init_datepicker();
   init_selectpicker();
   init_form_reminder();
   init_tabs_scrollable();
     // defined in manage actions
     action_id = '<?php echo $action->id; ?>';
     init_action_editor();
</script>
