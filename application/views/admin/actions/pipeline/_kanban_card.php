<?php if ($action['status'] == $status) { ?>
<li data-action-id="<?php echo $action['id']; ?>" class="<?php if($action['invoice_id'] != NULL || $action['estimate_id'] != NULL){echo 'not-sortable';} ?>">
   <div class="panel-body">
      <div class="row">
         <div class="col-md-12">
            <h4 class="bold pipeline-heading">
               <a href="<?php echo admin_url('actions/list_actions/'.$action['id']); ?>" data-toggle="tooltip" data-title="<?php echo $action['subject']; ?>" onclick="action_pipeline_open(<?php echo $action['id']; ?>); return false;"><?php echo format_action_number($action['id']); ?></a>
               <?php if(has_permission('estimates','','edit')){ ?>
               <a href="<?php echo admin_url('actions/action/'.$action['id']); ?>" target="_blank" class="pull-right"><small><i class="fa fa-pencil-square-o" aria-hidden="true"></i></small></a>
               <?php } ?>
            </h4>
            <span class="mbot10 inline-block full-width">
            <?php
               if($action['rel_type'] == 'lead'){
                 echo '<a href="'.admin_url('leads/index/'.$action['rel_id']).'" onclick="init_lead('.$action['rel_id'].'); return false;" data-toggle="tooltip" data-title="'._l('lead').'">' .$action['action_to'].'</a><br />';
               } else if($action['rel_type'] == 'customer'){
                 echo '<a href="'.admin_url('clients/client/'.$action['rel_id']).'" data-toggle="tooltip" data-title="'._l('client').'">' .$action['action_to'].'</a><br />';
               }
               ?>
            </span>
         </div>
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-8">
                  <?php if($action['total'] != 0){ ?>
                  <span class="bold"><?php echo _l('action_total'); ?>: <?php echo format_money($action['total'],$this->currencies_model->get($action['currency'])->symbol); ?></span>
                  <br />
                  <?php } ?>
                  <?php echo _l('action_date'); ?>: <?php echo _d($action['date']); ?>
                  <?php if(is_date($action['open_till'])){ ?>
                  <br />
                  <?php echo _l('action_open_till'); ?>: <?php echo _d($action['open_till']); ?>
                  <?php } ?>
                  <br />
               </div>
               <div class="col-md-4 text-right">
                  <small><i class="fa fa-comments" aria-hidden="true"></i> <?php echo _l('action_comments'); ?>: <?php echo total_rows('tblactioncomments', array(
                     'actionid' => $action['id']
                     )); ?></small>
               </div>
               <?php $tags = get_tags_in($action['id'],'action');
                  if(count($tags) > 0){ ?>
               <div class="col-md-12">
                  <div class="mtop5 kanban-tags">
                     <?php echo render_tags($tags); ?>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</li>
<?php } ?>
