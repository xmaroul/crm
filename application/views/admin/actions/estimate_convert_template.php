<div class="modal fade action-convert-modal" id="convert_to_estimate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xxl" role="document">
        <?php echo form_open('admin/actions/convert_to_estimate/'.$action->id,array('id'=>'action_convert_to_estimate_form','class'=>'_transaction_form disable-on-submit')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close dismiss-action-convert-modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('action_convert_to_estimate'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php $this->load->view('admin/estimates/estimate_template'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default dismiss-action-convert-modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/invoice_items/item'); ?>
<script>
    init_ajax_search('customer','#clientid.ajax-search');
    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');
    custom_fields_hyperlink();
    init_selectpicker();
    init_datepicker();
    init_color_pickers();
    init_items_sortable();
    init_tags_inputs();
    validate_estimate_form('#action_convert_to_estimate_form');
    <?php if($action->assigned != 0){ ?>
    $('#convert_to_estimate #sale_agent').selectpicker('val',<?php echo $action->assigned; ?>);
    <?php } ?>
    $('select[name="discount_type"]').selectpicker('val','<?php echo $action->discount_type; ?>');
    $('input[name="discount_percent"]').val('<?php echo $action->discount_percent; ?>');
    $('input[name="discount_total"]').val('<?php echo $action->discount_total; ?>');
    <?php if(is_sale_discount($action,'fixed')) { ?>
        $('.discount-total-type.discount-type-fixed').click();
    <?php } ?>
    $('input[name="adjustment"]').val('<?php echo $action->adjustment; ?>');
    $('input[name="show_quantity_as"][value="<?php echo $action->show_quantity_as; ?>"]').prop('checked',true).change();
    $('#convert_to_estimate #clientid').change();
</script>
