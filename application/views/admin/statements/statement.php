<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <?php echo form_open($this->uri->uri_string(),array('id'=>'statement_form')); ?>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <h4 class="no-margin">
                            <?php echo $title; ?>
                        </h4>
                        <hr class="hr-panel-heading" />
                        <?php
                        $disable_type_edit = '';
                        if(isset($statement)){
                            if($statement->billing_type != 1){
                                if(total_rows('tblstafftasks',array('rel_id'=>$statement->id,'rel_type'=>'statement','billable'=>1,'billed'=>1)) > 0){
                                    $disable_type_edit = 'disabled';
                                }
                            }
                        }
                        ?>
                        <?php $value = (isset($statement) ? $statement->name : ''); ?>
                        <?php echo render_input('name','statement_name',$value); ?>
                        <div class="form-group select-placeholder row">
                            <label for="clientid" class="control-label col-md-4"><?php echo _l('statement_customer'); ?></label>
							<div class="col-md-8">
								<select id="clientid" name="clientid" data-live-search="true" data-width="100%" class="ajax-search" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
								   <?php $selected = (isset($statement) ? $statement->clientid : '');
								   if($selected == ''){
									   $selected = (isset($customer_id) ? $customer_id: '');
								   }
								   if($selected != ''){
									$rel_data = get_relation_data('customer',$selected);
									$rel_val = get_relation_values($rel_data,'customer');
									echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
								} ?>
								</select>	
							</div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-6 hidden">
                            <div class="form-group select-placeholder">
                                <label for="billing_type"><?php echo _l('statement_billing_type'); ?></label>
                                <div class="clearfix"></div>
                                <select name="billing_type" class="selectpicker" id="billing_type" data-width="100%" <?php echo $disable_type_edit ; ?> data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                    <option value=""></option>
                                    <option value="1" <?php if(isset($statement) && $statement->billing_type == 1 || !isset($statement) && $auto_select_billing_type && $auto_select_billing_type->billing_type == 1){echo 'selected'; } ?>><?php echo _l('statement_billing_type_fixed_cost'); ?></option>
                                    <option value="2" <?php if(isset($statement) && $statement->billing_type == 2 || !isset($statement) && $auto_select_billing_type && $auto_select_billing_type->billing_type == 2){echo 'selected'; } ?>><?php echo _l('statement_billing_type_statement_hours'); ?></option>
                                    <option value="3" data-subtext="<?php echo _l('statement_billing_type_statement_task_hours_hourly_rate'); ?>" <?php if(isset($statement) && $statement->billing_type == 3 || !isset($statement) && $auto_select_billing_type && $auto_select_billing_type->billing_type == 3){echo 'selected'; } ?>><?php echo _l('statement_billing_type_statement_task_hours'); ?></option>
                                </select>
                                <?php if($disable_type_edit != ''){
                                    echo '<p class="text-danger">'._l('cant_change_billing_type_billed_tasks_found').'</p>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row select-placeholder">
                                <label for="status" class="control-label col-md-4"><?php echo _l('statement_status'); ?></label>
                                <div class="col-md-8">
									<select name="status" id="status" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
										<?php foreach($statuses as $status){ ?>
										<option value="<?php echo $status['id']; ?>" <?php if(!isset($statement) && $status['id'] == 2 || (isset($statement) && $statement->status == $status['id'])){echo 'selected';} ?>><?php echo $status['name']; ?></option>
										<?php } ?>
									</select>
								</div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($statement) && statement_has_recurring_tasks($statement->id)) { ?>
                    <div class="alert alert-warning recurring-tasks-notice hide">

                    </div>
                    <?php } ?>
                    <?php if(total_rows('tblemailtemplates',array('slug'=>'statement-finished-to-customer','active'=>0)) == 0){ ?>
                    <div class="form-group statement_marked_as_finished hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="statement_marked_as_finished_email_to_contacts" id="statement_marked_as_finished_email_to_contacts">
                            <label for="statement_marked_as_finished_email_to_contacts"><?php echo _l('statement_marked_as_finished_to_contacts'); ?></label>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($statement)){ ?>
                    <div class="form-group mark_all_tasks_as_completed hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="mark_all_tasks_as_completed" id="mark_all_tasks_as_completed">
                            <label for="mark_all_tasks_as_completed"><?php echo _l('statement_mark_all_tasks_as_completed'); ?></label>
                        </div>
                    </div>
                    <div class="notify_statement_members_status_change hide">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="notify_statement_members_status_change" id="notify_statement_members_status_change">
                            <label for="notify_statement_members_status_change"><?php echo _l('notify_statement_members_status_change'); ?></label>
                        </div>
                        <hr />
                    </div>
                    <?php } ?>
                    <?php
                    $input_field_hide_class_total_cost = '';
                    if(!isset($statement)){
                        if($auto_select_billing_type && $auto_select_billing_type->billing_type != 1 || !$auto_select_billing_type){
                            $input_field_hide_class_total_cost = 'hide';
                        }
                    } else if(isset($statement) && $statement->billing_type != 1){
                        $input_field_hide_class_total_cost = 'hide';
                    }
                    ?>
                    <div id="statement_cost" class="<?php echo $input_field_hide_class_total_cost; ?>">
                        <?php $value = (isset($statement) ? $statement->statement_cost : ''); ?>
                        <?php echo render_input('statement_cost','statement_total_cost',$value,'number'); ?>
                    </div>
                    <?php
                    $input_field_hide_class_rate_per_hour = '';
                    if(!isset($statement)){
                        if($auto_select_billing_type && $auto_select_billing_type->billing_type != 2 || !$auto_select_billing_type){
                            $input_field_hide_class_rate_per_hour = 'hide';
                        }
                    } else if(isset($statement) && $statement->billing_type != 2){
                        $input_field_hide_class_rate_per_hour = 'hide';
                    }
                    ?>
                    <div id="statement_rate_per_hour" class="<?php echo $input_field_hide_class_rate_per_hour; ?>">
                        <?php $value = (isset($statement) ? $statement->statement_rate_per_hour : ''); ?>
                        <?php
                        $input_disable = array();
                        if($disable_type_edit != ''){
                            $input_disable['disabled'] = true;
                        }
                        ?>
                        <?php echo render_input('statement_rate_per_hour','statement_rate_per_hour',$value,'number',$input_disable); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo render_input('estimated_hours','estimated_hours',isset($statement) ? $statement->estimated_hours : '','number'); ?>
                        </div>
                        <div class="col-md-6">
                           <?php
                           $selected = array();
                           if(isset($statement_members)){
                            foreach($statement_members as $member){
                                array_push($selected,$member['staff_id']);
                            }
                        } else {
                            array_push($selected,get_staff_user_id());
                        }
                        echo render_select('statement_members[]',$staff,array('staffid',array('firstname','lastname')),'statement_members',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php $value = (isset($statement) ? _d($statement->start_date) : _d(date('Y-m-d'))); ?>
                        <?php echo render_date_input('start_date','statement_start_date',$value); ?>
                    </div>
                    <div class="col-md-6">
                        <?php $value = (isset($statement) ? _d($statement->deadline) : ''); ?>
                        <?php echo render_date_input('deadline','statement_deadline',$value); ?>
                    </div>
                </div>
                <?php if(isset($statement) && $statement->date_finished != null && $statement->status == 4) { ?>
                    <?php echo render_datetime_input('date_finished','statement_completed_date',_dt($statement->date_finished)); ?>
                <?php } ?>
                
                <?php $rel_id_custom_field = (isset($statement) ? $statement->id : false); ?>
                <?php echo render_custom_fields('statements',$rel_id_custom_field); ?>
				<div class="row">
					<p class="bold"><?php echo _l('statement_description'); ?></p>
					<?php $contents = ''; if(isset($statement)){$contents = $statement->description;} ?>
					<?php echo render_textarea('description','',$contents,array('rows' => 25, 'text_columns' => 12),array(),''); ?>
					<?php if(total_rows('tblemailtemplates',array('slug'=>'assigned-to-statement','active'=>0)) == 0){ ?>
				</div>
				<div class="form-group">
				<label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
				<input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo (isset($statement) ? prep_tags_input(get_tags_in($statement->id,'statement')) : ''); ?>" data-role="tagsinput">
                </div>
				
				<div class="form-group hidden">
					<div class="checkbox checkbox-success">
						<input type="checkbox" <?php if((isset($statement) && $statement->progress_from_tasks == 1) || !isset($statement)){echo 'checked';} ?> name="progress_from_tasks" id="progress_from_tasks">
						<label for="progress_from_tasks"><?php echo _l('calculate_progress_through_tasks'); ?></label>
					</div>
				</div>
				<?php
				if(isset($statement) && $statement->progress_from_tasks == 1){
					$value = $this->statements_model->calc_progress_by_tasks($statement->id);
				} else if(isset($statement) && $statement->progress_from_tasks == 0){
					$value = $statement->progress;
				} else {
					$value = 0;
				}
				?>
				<div class="form-group hidden">
					<label for=""><?php echo _l('statement_progress'); ?> <span class="label_progress"><?php echo $value; ?>%</span></label>
					<?php echo form_hidden('progress',$value); ?>
					<div class="statement_progress_slider statement_progress_slider_horizontal mbot15"></div>
				</div>
                <div class="checkbox checkbox-primary hidden">
                   <input type="checkbox" name="send_created_email" id="send_created_email">
                   <label for="send_created_email"><?php echo _l('statement_send_created_email'); ?></label>
               </div>
               <?php } ?>
               <div class="btn-bottom-toolbar text-right">
                   <button type="submit" data-form="#statement_form" class="btn btn-info" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('submit'); ?></button>
               </div>
           </div>
       </div>
   </div>
<?php echo form_close(); ?>
</div>
<div class="btn-bottom-pusher"></div>
</div>
</div>
<?php init_tail(); ?>
<script>
    <?php if(isset($statement)){ ?>
        var original_statement_status = '<?php echo $statement->status; ?>';
        <?php } ?>
        $(function(){

            $('select[name="billing_type"]').on('change',function(){
                var type = $(this).val();
                if(type == 1){
                    $('#statement_cost').removeClass('hide');
                    $('#statement_rate_per_hour').addClass('hide');
                } else if(type == 2){
                    $('#statement_cost').addClass('hide');
                    $('#statement_rate_per_hour').removeClass('hide');
                } else {
                    $('#statement_cost').addClass('hide');
                    $('#statement_rate_per_hour').addClass('hide');
                }
            });

            _validate_form($('form'),{name:'required',clientid:'required',start_date:'required',billing_type:'required'});

            $('select[name="status"]').on('change',function(){
                var status = $(this).val();
                var mark_all_tasks_completed = $('.mark_all_tasks_as_completed');
                var notify_statement_members_status_change = $('.notify_statement_members_status_change');
                mark_all_tasks_completed.removeClass('hide');
                if(typeof(original_statement_status) != 'undefined'){
                    if(original_statement_status != status){

                        mark_all_tasks_completed.removeClass('hide');
                        notify_statement_members_status_change.removeClass('hide');

                        if(status == 4 || status == 5 || status == 3) {
                            $('.recurring-tasks-notice').removeClass('hide');
                            var notice = "<?php echo _l('statement_changing_status_recurring_tasks_notice'); ?>";
                            notice = notice.replace('{0}', $(this).find('option[value="'+status+'"]').text().trim());
                            $('.recurring-tasks-notice').html(notice);
                            $('.recurring-tasks-notice').append('<input type="hidden" name="cancel_recurring_tasks" value="true">');
                            mark_all_tasks_completed.find('input').prop('checked',true);
                        } else {
                            $('.recurring-tasks-notice').html('').addClass('hide');
                            mark_all_tasks_completed.find('input').prop('checked',false);
                        }
                    } else {
                        mark_all_tasks_completed.addClass('hide');
                        mark_all_tasks_completed.find('input').prop('checked',false);
                        notify_statement_members_status_change.addClass('hide');
                        $('.recurring-tasks-notice').html('').addClass('hide');
                    }
                }

                if(status == 4){
                    $('.statement_marked_as_finished').removeClass('hide');
                } else {
                    $('.statement_marked_as_finished').addClass('hide');
                    $('.statement_marked_as_finished').prop('checked',false);
                }
            });

            $('form').on('submit',function(){
                $('select[name="billing_type"]').prop('disabled',false);
                $('#available_features,#available_features option').prop('disabled',false);
                $('input[name="statement_rate_per_hour"]').prop('disabled',false);
            });

            var progress_input = $('input[name="progress"]');
            var progress_from_tasks = $('#progress_from_tasks');
            var progress = progress_input.val();

            $('.statement_progress_slider').slider({
                min:0,
                max:100,
                value:progress,
                disabled:progress_from_tasks.prop('checked'),
                slide: function( event, ui ) {
                    progress_input.val( ui.value );
                    $('.label_progress').html(ui.value+'%');
                }
            });

            progress_from_tasks.on('change',function(){
                var _checked = $(this).prop('checked');
                $('.statement_progress_slider').slider({disabled:_checked});
            });

            $('#statement-settings-area input').on('change',function(){
                if($(this).attr('id') == 'view_tasks' && $(this).prop('checked') == false){
                    $('#create_tasks').prop('checked',false).prop('disabled',true);
                    $('#edit_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_comments').prop('checked',false).prop('disabled',true);
                    $('#comment_on_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_attachments').prop('checked',false).prop('disabled',true);
                    $('#view_task_checklist_items').prop('checked',false).prop('disabled',true);
                    $('#upload_on_tasks').prop('checked',false).prop('disabled',true);
                    $('#view_task_total_logged_time').prop('checked',false).prop('disabled',true);
                } else if($(this).attr('id') == 'view_tasks' && $(this).prop('checked') == true){
                    $('#create_tasks').prop('disabled',false);
                    $('#edit_tasks').prop('disabled',false);
                    $('#view_task_comments').prop('disabled',false);
                    $('#comment_on_tasks').prop('disabled',false);
                    $('#view_task_attachments').prop('disabled',false);
                    $('#view_task_checklist_items').prop('disabled',false);
                    $('#upload_on_tasks').prop('disabled',false);
                    $('#view_task_total_logged_time').prop('disabled',false);
                }
            });

            // Auto adjust customer permissions based on selected statement visible tabs
            // Eq Project creator disable TASKS tab, then this function will auto turn off customer statement option Allow customer to view tasks

            $('#available_features').on('change',function(){
                $("#available_features option").each(function(){
                   if($(this).data('linked-customer-option') && !$(this).is(':selected')) {
                        var opts = $(this).data('linked-customer-option').split(',');
                        for(var i = 0; i<opts.length;i++) {
                            var statement_option = $('#'+opts[i]);
                            statement_option.prop('checked',false);
                            if(opts[i] == 'view_tasks') {
                                statement_option.trigger('change');
                            }
                        }
                   }
               });
            });
            $("#view_tasks").trigger('change');
            <?php if(!isset($statement)) { ?>
                $('#available_features').trigger('change');
            <?php } ?>
        });
    </script>
</body>
</html>
