<?php

$table_data = [
   _l('the_number_sign'),
   _l('statement_name'),
    [
         'name'     => _l('statement_customer'),
         'th_attrs' => ['class' => isset($client) ? 'not_visible' : ''],
    ],
   _l('tags'),
   _l('statement_start_date'),
   _l('statement_deadline'),
   _l('statement_members'),
   _l('statement_status'),
];

$custom_fields = get_custom_fields('statements', ['show_on_table' => 1]);
foreach ($custom_fields as $field) {
    array_push($table_data, $field['name']);
}

$table_data = do_action('statements_table_columns', $table_data);

render_datatable($table_data, isset($class) ?  $class : 'statements', [], [
  'data-last-order-identifier' => 'statements',
  'data-default-order'  => get_table_last_order('statements'),
]);
