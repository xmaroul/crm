<div class="horizontal-scrollable-tabs">
  <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
  <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
  <div class="horizontal-tabs">
    <ul class="nav nav-tabs no-margin statement-tabs nav-tabs-horizontal" role="tablist">
        <?php foreach(get_statement_tabs_admin($statement->id) as $tab){

            $dropdown = isset($tab['dropdown']) ? true : false;
            if($dropdown){

                $total_hidden = 0;
                foreach($tab['dropdown'] as $d){
                    if((isset($d['visible']) && $d['visible'] == false) || (isset($statement->settings->available_features[$d['name']]) && $statement->settings->available_features[$d['name']] == 0)) {
                        $total_hidden++;
                    }
                }
                if($total_hidden == count($tab['dropdown'])) {
                    continue;
                }
            }
            if((isset($tab['visible']) && $tab['visible'] == true) || !isset($tab['visible'])){
                if(isset($statement->settings->available_features[$tab['name']]) && $statement->settings->available_features[$tab['name']] == 0){
                    continue;
                }
                ?>
                <li class="<?php if($tab['name'] == 'statement_overview'){echo 'active ';} ?>statement_tab_<?php echo $tab['name']; ?><?php if($dropdown){echo ' nav-tabs-submenu-parent';} ?>">
                    <a data-group="<?php echo $tab['name']; ?>" href="<?php echo $tab['url']; ?>" role="tab"<?php if($dropdown){ ?> data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-toggle" id="dropdown_<?php echo $tab['name']; ?>"<?php } ?>><i class="<?php echo $tab['icon']; ?>" aria-hidden="true"></i> <?php echo $tab['lang']; ?>
                    <?php if($dropdown){ ?> <span class="caret"></span> <?php } ?>
                </a>
                <?php if($dropdown){ ?>
                    <?php if(!is_rtl()){ ?>
                    <div class="tabs-submenu-wrapper">
                    <?php } ?>
                       <ul class="dropdown-menu" aria-labelledby="dropdown_<?php echo $tab['name']; ?>">
                        <?php
                        usort($tab['dropdown'], function($a, $b) {
                            return $a['order'] - $b['order'];
                        });
                        foreach($tab['dropdown'] as $d){
                            if((isset($d['visible']) && $d['visible'] == true) || !isset($d['visible'])){
                                echo '<li class="'.(isset($statement->settings->available_features[$d['name']]) && $statement->settings->available_features[$d['name']] == 0 ? 'hide': '').' nav-tabs-submenu-child"><a href="'.$d['url'].'" data-group="'.$d['name'].'">'.$d['lang'].'</a></li>';
                            }
                        }
                        ?>
                    </ul>
                <?php if(!is_rtl()){ ?>
                </div>
                <?php } ?>
                <?php } ?>
            </li>
            <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>

