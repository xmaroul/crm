<?php init_head(); ?>
<div id="wrapper">
  <?php echo form_hidden('statement_id',$statement->id) ?>
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s statement-top-panel panel-full">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-7 statement-heading">
                <h3 class="hide statement-name"><?php echo $statement->name; ?></h3>
                <div id="statement_view_name" class="pull-left">
                 <select class="selectpicker" id="statement_top" data-width="fit"<?php if(count($other_statements) > 4){ ?> data-live-search="true" <?php } ?>>
                   <option value="<?php echo $statement->id; ?>" selected><?php echo $statement->name; ?></option>
                   <?php foreach($other_statements as $op){ ?>
                   <option value="<?php echo $op['id']; ?>" data-subtext="<?php echo $op['company']; ?>">#<?php echo $op['id']; ?> - <?php echo $op['name']; ?></option>
                   <?php } ?>
                 </select>
               </div>
               <div class="visible-xs">
                 <div class="clearfix"></div>
               </div>
               <?php echo '<div class="label pull-left mleft15 mtop5 p8 statement-status-label-'.$statement->status.'" style="background:'.$statement_status['color'].'">'.$statement_status['name'].'</div>'; ?>
             </div>
             <div class="col-md-5 text-right">
              <?php if(has_permission('tasks','','create')){ ?>
              <a href="#" onclick="new_task_from_relation(undefined,'statement',<?php echo $statement->id; ?>); return false;" class="btn btn-info"><?php echo _l('new_task'); ?></a>
              <?php } ?>
              <?php
              $invoice_func = 'pre_invoice_statement';
              ?>
              <?php if(has_permission('invoices','','create')){ ?>
              <a href="#" onclick="<?php echo $invoice_func; ?>(<?php echo $statement->id; ?>); return false;" class="invoice-statement btn btn-info<?php if($statement->client_data->active == 0){echo ' disabled';} ?>"><?php echo _l('invoice_statement'); ?></a>
              <?php } ?>
              <?php
              $statement_pin_tooltip = _l('pin_statement');
              if(total_rows('tblpinnedstatements',array('staff_id'=>get_staff_user_id(),'statement_id'=>$statement->id)) > 0){
                $statement_pin_tooltip = _l('unpin_statement');
              }
              ?>
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo _l('more'); ?> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right width200 statement-actions">
                  <li>
                   <a href="<?php echo admin_url('statements/pin_action/'.$statement->id); ?>">
                    <?php echo $statement_pin_tooltip; ?>
                  </a>
                </li>
                <?php if(has_permission('statements','','edit')){ ?>
                <li>
                  <a href="<?php echo admin_url('statements/statement/'.$statement->id); ?>">
                    <?php echo _l('edit_statement'); ?>
                  </a>
                </li>
                <?php } ?>
                <?php if(has_permission('statements','','create')){ ?>
                <li>
                  <a href="#" onclick="copy_statement(); return false;">
                    <?php echo _l('copy_statement'); ?>
                  </a>
                </li>
                <?php } ?>
                <?php if(has_permission('statements','','create') || has_permission('statements','','edit')){ ?>
                <li class="divider"></li>
                <?php foreach($statuses as $status){
                  if($status['id'] == $statement->status){continue;}
                  ?>
                  <li>
                    <a href="#" data-name="<?php echo _l('statement_status_'.$status['id']); ?>" onclick="statement_mark_as_modal(<?php echo $status['id']; ?>,<?php echo $statement->id; ?>, this); return false;"><?php echo _l('statement_mark_as',$status['name']); ?></a>
                  </li>
                  <?php } ?>
                  <?php } ?>
                  <li class="divider"></li>
                  <?php if(has_permission('statements','','create')){ ?>
                  <li>
                   <a href="<?php echo admin_url('statements/export_statement_data/'.$statement->id); ?>" target="_blank"><?php echo _l('export_statement_data'); ?></a>
                 </li>
                 <?php } ?>
                 <?php if(is_admin()){ ?>
                 <li>
                  <a href="<?php echo admin_url('statements/view_statement_as_client/'.$statement->id .'/'.$statement->clientid); ?>" target="_blank"><?php echo _l('statement_view_as_client'); ?></a>
                </li>
                <?php } ?>
                <?php if(has_permission('statements','','delete')){ ?>
                <li>
                  <a href="<?php echo admin_url('statements/delete/'.$statement->id); ?>" class="_delete">
                    <span class="text-danger"><?php echo _l('delete_statement'); ?></span>
                  </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="panel_s statement-menu-panel">
      <div class="panel-body">
        <?php do_action('before_render_statement_view',$statement->id); ?>
        <?php $this->load->view('admin/statements/statement_tabs'); ?>
      </div>
    </div>
    <?php if($view == 'statement_milestones') { ?>
    <a href="#" class="statement-tabs-and-opts-toggler screen-options-btn bold"><?php echo _l('show_tabs_and_options'); ?></a>
    <?php } else { ?>
    <?php if((has_permission('statements','','create') || has_permission('statements','','edit')) && $statement->status == 1 && $this->statements_model->timers_started_for_statement($statement->id)){ ?>
    <div class="alert alert-warning statement-no-started-timers-found mbot15">
      <?php echo _l('statement_not_started_status_tasks_timers_found'); ?>
    </div>
    <?php } ?>
    <?php if($statement->deadline && date('Y-m-d') > $statement->deadline && $statement->status == 2){ ?>
    <div class="alert alert-warning bold statement-due-notice mbot15">
      <?php echo _l('statement_due_notice',floor((abs(time() - strtotime($statement->deadline)))/(60*60*24))); ?>
    </div>
    <?php } ?>
    <?php if(!has_contact_permission('statements',get_primary_contact_user_id($statement->clientid)) && total_rows('tblcontacts',array('userid'=>$statement->clientid)) > 0){ ?>
    <div class="alert alert-warning statement-permissions-warning mbot15">
      <?php echo _l('statement_customer_permission_warning'); ?>
    </div>
    <?php } ?>
    <?php } ?>
    <div class="panel_s">
      <div class="panel-body">
        <?php echo $group_view; ?>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<?php if(isset($discussion)){
  echo form_hidden('discussion_id',$discussion->id);
  echo form_hidden('discussion_user_profile_image_url',$discussion_user_profile_image_url);
  echo form_hidden('current_user_is_admin',$current_user_is_admin);
}
echo form_hidden('statement_percent',$percent);
?>
<div id="invoice_statement"></div>
<div id="pre_invoice_statement"></div>
<?php $this->load->view('admin/statements/milestone'); ?>
<?php $this->load->view('admin/statements/copy_settings'); ?>
<?php $this->load->view('admin/statements/_mark_tasks_finished'); ?>
<?php init_tail(); ?>
<?php $discussion_lang = get_statement_discussions_language_array(); ?>
<?php echo app_script('assets/js','statements.js'); ?>
<!-- For invoices table -->
<script>
  taskid = '<?php echo $this->input->get('taskid'); ?>';
</script>
<script>
  var gantt_data = {};
  <?php if(isset($gantt_data)){ ?>
    gantt_data = <?php echo json_encode($gantt_data); ?>;
    <?php } ?>
    var discussion_id = $('input[name="discussion_id"]').val();
    var discussion_user_profile_image_url = $('input[name="discussion_user_profile_image_url"]').val();
    var current_user_is_admin = $('input[name="current_user_is_admin"]').val();
    var statement_id = $('input[name="statement_id"]').val();
    if(typeof(discussion_id) != 'undefined'){
      discussion_comments('#discussion-comments',discussion_id,'regular');
    }
    $(function(){
     var statement_progress_color = '<?php echo do_action('admin_statement_progress_color','#84c529'); ?>';
     var circle = $('.statement-progress').circleProgress({fill: {
      gradient: [statement_progress_color, statement_progress_color]
    }}).on('circle-animation-progress', function(event, progress, stepValue) {
      $(this).find('strong.statement-percent').html(parseInt(100 * stepValue) + '<i>%</i>');
    });
  });

    function discussion_comments(selector,discussion_id,discussion_type){
     $(selector).comments({
       roundProfilePictures: true,
       textareaRows: 4,
       textareaRowsOnFocus: 6,
       profilePictureURL:discussion_user_profile_image_url,
       enableUpvoting: false,
       enableAttachments:true,
       popularText:'',
       enableDeletingCommentWithReplies:false,
       textareaPlaceholderText:"<?php echo $discussion_lang['discussion_add_comment']; ?>",
       newestText:"<?php echo $discussion_lang['discussion_newest']; ?>",
       oldestText:"<?php echo $discussion_lang['discussion_oldest']; ?>",
       attachmentsText:"<?php echo $discussion_lang['discussion_attachments']; ?>",
       sendText:"<?php echo $discussion_lang['discussion_send']; ?>",
       replyText:"<?php echo $discussion_lang['discussion_reply']; ?>",
       editText:"<?php echo $discussion_lang['discussion_edit']; ?>",
       editedText:"<?php echo $discussion_lang['discussion_edited']; ?>",
       youText:"<?php echo $discussion_lang['discussion_you']; ?>",
       saveText:"<?php echo $discussion_lang['discussion_save']; ?>",
       deleteText:"<?php echo $discussion_lang['discussion_delete']; ?>",
       viewAllRepliesText:"<?php echo $discussion_lang['discussion_view_all_replies'] . ' (__replyCount__)'; ?>",
       hideRepliesText:"<?php echo $discussion_lang['discussion_hide_replies']; ?>",
       noCommentsText:"<?php echo $discussion_lang['discussion_no_comments']; ?>",
       noAttachmentsText:"<?php echo $discussion_lang['discussion_no_attachments']; ?>",
       attachmentDropText:"<?php echo $discussion_lang['discussion_attachments_drop']; ?>",
       currentUserIsAdmin:current_user_is_admin,
       getComments: function(success, error) {
         $.get(admin_url + 'statements/get_discussion_comments/'+discussion_id+'/'+discussion_type,function(response){
           success(response);
         },'json');
       },
       postComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'statements/add_discussion_comment/'+discussion_id+'/'+discussion_type,
           data: commentJSON,
           success: function(comment) {
             comment = JSON.parse(comment);
             success(comment)
           },
           error: error
         });
       },
       putComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'statements/update_discussion_comment',
           data: commentJSON,
           success: function(comment) {
             comment = JSON.parse(comment);
             success(comment)
           },
           error: error
         });
       },
       deleteComment: function(commentJSON, success, error) {
         $.ajax({
           type: 'post',
           url: admin_url + 'statements/delete_discussion_comment/'+commentJSON.id,
           success: success,
           error: error
         });
       },
       timeFormatter: function(time) {
         return moment(time).fromNow();
       },
       uploadAttachments: function(commentArray, success, error) {
         var responses = 0;
         var successfulUploads = [];
         var serverResponded = function() {
           responses++;
             // Check if all requests have finished
             if(responses == commentArray.length) {
                 // Case: all failed
                 if(successfulUploads.length == 0) {
                   error();
                 // Case: some succeeded
               } else {
                 successfulUploads = JSON.parse(successfulUploads);
                 success(successfulUploads)
               }
             }
           }
           $(commentArray).each(function(index, commentJSON) {
             // Create form data
             var formData = new FormData();
             if(commentJSON.file.size && commentJSON.file.size > max_php_ini_upload_size_bytes){
              alert_float('danger',"<?php echo _l("file_exceeds_max_filesize"); ?>");
              serverResponded();
            } else {
             $(Object.keys(commentJSON)).each(function(index, key) {
               var value = commentJSON[key];
               if(value) formData.append(key, value);
             });

             if (typeof(csrfData) !== 'undefined') {
                formData.append(csrfData['token_name'], csrfData['hash']);
             }
             $.ajax({
               url: admin_url + 'statements/add_discussion_comment/'+discussion_id+'/'+discussion_type,
               type: 'POST',
               data: formData,
               cache: false,
               contentType: false,
               processData: false,
               success: function(commentJSON) {
                 successfulUploads.push(commentJSON);
                 serverResponded();
               },
               error: function(data) {
                var error = JSON.parse(data.responseText);
                alert_float('danger',error.message);
                serverResponded();
              },
            });
           }
         });
         }
       });
}
</script>
</body>
</html>
