<p><?php echo _l('statement_note_private'); ?></p>
<hr />
<?php echo form_open(admin_url('statements/save_note/'.$statement->id)); ?>
<?php echo render_textarea('content','',$staff_notes,array(),array(),'','tinymce'); ?>
<button type="submit" class="btn btn-info"><?php echo _l('statement_save_note'); ?></button>
<?php echo form_close(); ?>
