<div class="modal fade" id="proposal_xronos_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_xronos_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('proposal_xronos_add_heading'); ?></span>
                </h4>
            </div>
            <?php // echo form_open('admin/clients/group',array('id'=>'customer-group-modal')); ?>
			<?php echo form_open('admin/proposals/xronos',array('id'=>'proposal-xronos-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','proposal_xronos_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-xronos-modal'), {
        name: 'required'
    }, manage_proposal_xronos);

       $('#proposal_xronos_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var xronos_id = $(invoker).data('id');
        $('#proposal_xronos_modal .add-title').removeClass('hide');
        $('#proposal_xronos_modal .edit-title').addClass('hide');
        $('#proposal_xronos_modal input[name="id"]').val('');
        $('#proposal_xronos_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(xronos_id) !== 'undefined') {
            $('#proposal_xronos_modal input[name="id"]').val(xronos_id);
            $('#proposal_xronos_modal .add-title').addClass('hide');
            $('#proposal_xronos_modal .edit-title').removeClass('hide');
            $('#proposal_xronos_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
	
	$('#manage_proposal_xronos_modal').on('show.bs.modal', function(e) {
		var row_element = $('#manage_proposal_xronos_modal div.modal-body div.row');
		row_element.empty();
		$('select[name="xronos_in[]"] > option').each(function() {
                    delete_callback = "'delete_xronos'";
                    var new_row = '<div class="row" id="row-'+this.value+'"><div class="col-md-12"><div class="col-md-6">'+this.text+'</div><div class="col-md-2"><a href="#" class="btn btn-danger _delete delete_xronos btn-icon" id="delete_xronos'+this.value+'" onclick="delete_proposal_xronos('+this.value+', '+delete_callback+')"><i class="fa fa-remove"></i></a></div></div>';
                    row_element.prepend(new_row);
		});
	});
   });
    function manage_proposal_xronos(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-xronos')){
                    $('.table-proposal-xronos').DataTable().ajax.reload();
                }
                if($('select[name="xronos_in[]"]').length && typeof(response.id) != 'undefined') {
                    var xronoi = $('select[name="xronos_in[]"]');
                    xronoi.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    xronoi.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_xronos_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>
