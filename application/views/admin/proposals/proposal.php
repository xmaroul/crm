<?php init_head(); ?>
<div id="wrapper">
   <div class="content accounting-template proposal">
      <div class="row">
         <?php
			$fc_rel_id = false;
            if(isset($proposal)){
				echo form_hidden('isedit',$proposal->id);
				$fc_rel_id = $proposal->id;
            }
            $rel_type = '';
            $rel_id = '';
            if(isset($proposal) || ($this->input->get('rel_id') && $this->input->get('rel_type'))){
				if($this->input->get('rel_id')){
					$rel_id = $this->input->get('rel_id');
					$rel_type = $this->input->get('rel_type');
				} else {
					$rel_id = $proposal->rel_id;
					$rel_type = $proposal->rel_type;
				}
            }
            ?>
         <?php echo form_open($this->uri->uri_string(),array('id'=>'proposal-form','class'=>'_transaction_form proposal-form')); ?>
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
				   <h4 class="customer-profile-group-heading">Προσφορά</h4>
                  <div class="row">
                     <?php if(isset($proposal)){ ?>
                     <div class="col-md-12">
                        <?php echo format_proposal_status($proposal->status); ?>
                     </div>
                     <div class="clearfix"></div>
                     <hr />
                     <?php } ?>
                     <div class="col-md-6 border-right">
                        <?php $value = (isset($proposal) ? $proposal->subject : ''); ?>
                        <?php $attrs = (isset($proposal) ? array() : array('autofocus'=>true)); ?>
                        <?php echo render_input('subject','proposal_subject',$value,'text',$attrs); ?>
						<div class="row">
                          <div class="col-md-6">
                              <?php $value = (isset($proposal) ? _d($proposal->date) : _d(date('Y-m-d'))) ?>
                              <?php echo render_date_input('date','proposal_date',$value); ?>
                          </div>
                          <div class="col-md-6">
                            <?php
                        $value = '';
                        if(isset($proposal)){
                          $value = _d($proposal->open_till);
                        } else {
                          if(get_option('proposal_due_after') != 0){
                              $value = _d(date('Y-m-d',strtotime('+'.get_option('proposal_due_after').' DAY',strtotime(date('Y-m-d')))));
                          }
                        }
                        echo render_date_input('open_till','proposal_open_till',$value); ?>
                          </div>
                        </div>

						<?php $attrs = (!isset($proposal) ? array('label_columns' => 12, 'text_columns' => 12) : array('data-fieldto' => 'proposal', 'data-fieldid' =>  12 , 'rows' => 12, 'label_columns' => 12, 'text_columns' => 12)); ?>
                        <?php 
							$add_prodiagrafes_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_prodiagrafes_modal"><i class="fa fa-plus"></i></a>';
							$edit_prodiagrafes_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_prodiagrafes_modal"><i class="fa fa-edit"></i></a>';
							$prodiagrafes_value = isset($proposal) ? get_custom_field_value($proposal->id, 12, 'proposal') : '';
							echo render_textarea('custom_fields[proposal][12]','Προδιαγραφές'."<div class='prodiagrafes-buttons'>".$add_prodiagrafes_icon_link.$edit_prodiagrafes_icon_link."</div>",$prodiagrafes_value,$attrs,array(),'','tinymce'); 
						?>
						 <div class="form-group">
                        <?php
						// Maketa
						$selected = array();
						if(isset($proposal_maketas)){
							foreach($proposal_maketas as $maketa){
								array_push($selected,$maketa['maketaid']);
							}
						}
						if(is_admin() || get_option('staff_members_create_inline_proposal_maketas') == '1'){
							$add_maketa_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_maketa_modal"><i class="fa fa-plus"></i></a>';
							$edit_maketa_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_maketa_modal"><i class="fa fa-remove"></i></a>';
							echo render_select_with_input_group('maketas_in[]',$maketas,array('id','name'),'proposal_maketas',$selected,$add_maketa_icon_link.$edit_maketa_icon_link,array(),array(),'','',true);
						} else {
							echo render_select('maketas_in[]',$maketas,array('id','name'),'proposal_maketas',$selected,array(),array(),'','',false);
						}
						$proposal_dimiourgiko_maketa_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 51, 'proposal') : NULL;
						echo render_input('custom_fields[proposal][51]','proposal_dimiourgiko_maketa_allo',$proposal_dimiourgiko_maketa_allo_value);
						?>
						</div>
						 <hr>
						<div class="form-group">
						<?php $selected = array();
						if(isset($proposal_taxes)){
							foreach($proposal_taxes as $tax){
								array_push($selected,$tax['taxid']);
							}
						}
						if(is_admin() || get_option('staff_members_create_inline_proposal_taxes') == '1'){
							$add_tax_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_tax_modal"><i class="fa fa-plus"></i></a>';
							$edit_tax_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_tax_modal"><i class="fa fa-remove"></i></a>';
							echo render_select_with_input_group('taxes_in[]',$taxes,array('id','name'),'proposal_taxes',$selected,$add_tax_icon_link.$edit_tax_icon_link,array(),array(),'','',true);
						} else {
							echo render_select('taxes_in[]',$taxes,array('id','name'),'proposal_taxes',$selected,array(),array(),'','',false);
						}
						$proposal_fori_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 52, 'proposal') : NULL;
						echo render_input('custom_fields[proposal][52]','proposal_fori_allo',$proposal_fori_allo_value);?>
						 </div>
						 <hr>
						<div class="form-group">
						<?php $selected = array();
						if(isset($proposal_metaforika)){
							foreach($proposal_metaforika as $metaforika_value){
								array_push($selected,$metaforika_value['metaforikaid']);
							}
						}
						if(is_admin() || get_option('staff_members_create_inline_proposal_metaforika') == '1'){
							$add_metaforika_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_metaforika_modal"><i class="fa fa-plus"></i></a>';
							$edit_metaforika_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_metaforika_modal"><i class="fa fa-remove"></i></a>';
							echo render_select_with_input_group('metaforika_in[]',$metaforika,array('id','name'),'proposal_metaforika',$selected,$add_metaforika_icon_link.$edit_metaforika_icon_link,array(),array(),'','',true);
						} else {
							echo render_select('metaforika_in[]',$metaforika,array('id','name'),'proposal_metaforika',$selected,array(),array(),'','',false);
						}
						$proposal_metaforika_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 53, 'proposal') : NULL;
						echo render_input('custom_fields[proposal][53]','proposal_metaforika_allo',$proposal_metaforika_allo_value);?>
						</div>
						 <hr>
						<div class="form-group">
							<?php $selected = array();
							if(isset($proposal_topostropos)){
								foreach($proposal_topostropos as $topostropos_value){
									array_push($selected,$topostropos_value['topostroposid']);
								}
							}
							if(is_admin() || get_option('staff_members_create_inline_proposal_topostropos') == '1'){
								$add_topostropos_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_topostropos_modal"><i class="fa fa-plus"></i></a>';
								$edit_topostropos_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_topostropos_modal"><i class="fa fa-remove"></i></a>';
								echo render_select_with_input_group('topostropos_in[]',$topostropos,array('id','name'),'proposal_topostropos',$selected,$add_topostropos_icon_link.$edit_topostropos_icon_link,array(),array(),'','',true);
							} else {
								echo render_select('topostropos_in[]',$topostropos,array('id','name'),'proposal_topostropos',$selected,array(),array(),'','',false);
							}
							$proposal_topos_tropos_paradosis_i_apostolis_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 54, 'proposal') : NULL;
							echo render_input('custom_fields[proposal][54]','proposal_topos_tropos_paradosis_i_apostolis_allo',$proposal_topos_tropos_paradosis_i_apostolis_allo_value);?>
						</div>
						 <hr>
						<div class="form-group">
							<?php $selected = array();
							if(isset($proposal_xronos)){
								foreach($proposal_xronos as $xronos_value){
									array_push($selected,$xronos_value['xronosid']);
								}
							}
							if(is_admin() || get_option('staff_members_create_inline_proposal_xronos') == '1'){
								$add_xronos_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_xronos_modal"><i class="fa fa-plus"></i></a>';
								$edit_xronos_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_xronos_modal"><i class="fa fa-remove"></i></a>';
								echo render_select_with_input_group('xronos_in[]',$xronos,array('id','name'),'proposal_xronos',$selected,$add_xronos_icon_link.$edit_xronos_icon_link,array(),array(),'','',true);
							} else {
								echo render_select('xronos_in[]',$xronos,array('id','name'),'proposal_xronos',$selected,array(),array(),'','',false);
							}
							$proposal_chronos_paradosis_i_apostolis_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 55, 'proposal') : NULL;
							echo render_input('custom_fields[proposal][55]','proposal_chronos_paradosis_i_apostolis_allo',$proposal_chronos_paradosis_i_apostolis_allo_value);?>
						</div>
						 <hr>
						<div class="form-group">
							<?php $selected = array();
							if(isset($proposal_pliromi)){
								foreach($proposal_pliromi as $pliromi_value){
									array_push($selected,$pliromi_value['pliromiid']);
								}
							}
							if(is_admin() || get_option('staff_members_create_inline_proposal_pliromi') == '1'){
								$add_pliromi_icon_link = '<a href="#" data-toggle="modal" data-target="#proposal_pliromi_modal"><i class="fa fa-plus"></i></a>';
								$edit_pliromi_icon_link = '<a href="#" data-toggle="modal" data-target="#manage_proposal_pliromi_modal"><i class="fa fa-remove"></i></a>';
								echo render_select_with_input_group('pliromi_in[]',$pliromi,array('id','name'),'proposal_pliromi',$selected,$add_pliromi_icon_link.$edit_pliromi_icon_link,array(),array(),'','',true);
							} else {
								echo render_select('pliromi_in[]',$pliromi,array('id','name'),'proposal_pliromi',$selected,array(),array(),'','',false);
							}
							$proposal_tropos_pliromis_allo_value = isset($proposal) ? get_custom_field_value($proposal->id, 56, 'proposal') : NULL;
							echo render_input('custom_fields[proposal][56]','proposal_tropos_pliromis_allo',$proposal_tropos_pliromis_allo_value);?>
							<?php  echo render_custom_fields('proposal',$fc_rel_id, "slug IN ('proposal_lipa')"); ?>
						</div>
						 <hr>
                     </div>
                     <div class="col-md-6">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group row select-placeholder">
                                 <label for="status" class="control-label col-md-4"><?php echo _l('proposal_status'); ?></label>
                                 <?php
                                    $disabled = '';
                                    if(isset($proposal)){
                                     if($proposal->estimate_id != NULL || $proposal->invoice_id != NULL){
                                       $disabled = 'disabled';
                                     }
                                    }
                                    ?>
								 <div class="col-md-8">
									<select name="status" class="selectpicker" data-width="100%" <?php echo $disabled; ?> data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
									   <?php foreach($statuses as $status){ ?>
									   <option value="<?php echo $status; ?>" <?php if((isset($proposal) && $proposal->status == $status) || (!isset($proposal) && $status == 0)){echo 'selected';} ?>><?php echo format_proposal_status($status,'',false); ?></option>
									   <?php } ?>
									</select>
								</div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <?php
                                 $i = 0;
                                 $selected = '';
                                 foreach($staff as $member){
                                  if(isset($proposal)){
                                    if($proposal->assigned == $member['staffid']) {
                                      $selected = $member['staffid'];
                                    }
                                  }
                                  $i++;
                                 }
                                 echo render_select('assigned',$staff,array('staffid',array('firstname','lastname')),'proposal_assigned',$selected);
                                 ?>
                           </div>
                        </div>
                        <div class="form-group row select-placeholder">
                           <label for="rel_type" class="control-label  col-md-4"><?php echo _l('proposal_related'); ?></label>
						   <div class="col-md-8">
								<select <?php if($this->input->get('rel_id')) {echo ' disabled';} ?> name="rel_type" id="rel_type" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
								   <option value=""></option>
								   <option value="lead" <?php if((isset($proposal) && $proposal->rel_type == 'lead') || $this->input->get('rel_type')){if($rel_type == 'lead'){echo 'selected';}} ?>><?php echo _l('proposal_for_lead'); ?></option>
								   <option value="customer" <?php if((isset($proposal) &&  $proposal->rel_type == 'customer') || $this->input->get('rel_type')){if($rel_type == 'customer'){echo 'selected';}} ?>><?php echo _l('proposal_for_customer'); ?></option>
								</select>
						   </div>
                        </div>
                        <div class="form-group row select-placeholder <?php if($rel_id == ''){echo ' hide';} ?> " id="rel_id_wrapper">
                           <label for="rel_id" class="col-md-4 control-label"><span class="rel_id_label"></span></label>
								<div id="rel_id_select">
									<div class="col-md-8">
								   <select <?php if($this->input->get('rel_id')) {echo ' disabled';} ?> name="rel_id" id="rel_id" class="ajax-search" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
								   <?php	if($rel_id != '' && $rel_type != ''){
												$rel_data = get_relation_data($rel_type,$rel_id);
												$rel_val = get_relation_values($rel_data,$rel_type);
												echo '<option value="'.$rel_val['id'].'" selected>'.$rel_val['name'].'</option>';
											} ?>
								   </select>
								</div>
							</div>
                        </div>
						<?php //	$proposal_to_options = array(); 
//								$selected = (isset($proposal) ? $proposal->proposal_to : ''); 
//								echo render_select('proposal_to',$proposal_to_options, array('proposal_to_id', 'name'),'proposal_to',$selected); ?>
                        <?php  $value = (isset($proposal) ? $proposal->proposal_to : ''); ?>
                        <?php echo render_input('proposal_to','proposal_to',$value); ?>
                        <?php // $value = (isset($proposal) ? $proposal->address : ''); ?>
                        <?php //  echo render_textarea('address','proposal_address',$value, array(), array(), NULL, 'hidden'); ?>
                        <div class="row">
<!--                           <div class="col-md-6">
                              <?php $value = (isset($proposal) ? $proposal->city : ''); ?>
                              <?php echo render_input('city','billing_city',$value, array('class' => 'hidden')); ?>
                           </div>
							<div class="col-md-6">
                              <?php  $value = (isset($proposal) ? $proposal->state : ''); ?>
                              <?php  echo render_input('state','billing_state',$value, array('class' => 'hidden')); ?>
                           </div>
                           <div class="col-md-6">
                              <?php // $countries = get_all_countries(); ?>
                              <?php // $selected = (isset($proposal) ? $proposal->country : ''); ?>
                              <?php // echo render_select('country',$countries,array('country_id',array('short_name'),'iso2'),'billing_country',$selected); ?>
                           </div>
                           <div class="col-md-6">
                              <?php // $value = (isset($proposal) ? $proposal->zip : ''); ?>
                              <?php // echo render_input('zip','billing_zip',$value); ?>
                           </div>-->
                           <div class="col-md-12">
							   <?php	
//								$proposal_email_options = array(); 
//								$selected = (isset($proposal) ? $proposal->email : ''); 
//								echo render_select('proposal_email',$proposal_to_options, array('proposal_email_id', 'email'),'proposal_email',$selected); ?>
                              <?php $value = (isset($proposal) ? $proposal->email : ''); ?>
                              <?php echo render_input('email','proposal_email',$value); ?>
                           </div>
                           <div class="col-md-12">
							   <?php 
//								$proposal_phone_options = array(); 
//								$selected = (isset($proposal) ? $proposal->phone : ''); 
//								echo render_select('proposal_phone',$proposal_phone_options, array('proposal_phone_id', 'phone'),'proposal_phone',$selected); ?>
                              <?php $value = (isset($proposal) ? $proposal->phone : ''); ?>
                              <?php echo render_input('phone','proposal_phone',$value); ?>
                           </div>
                        </div>
						 <?php $attrs = (!isset($proposal) ? array('rows' => 12, 'label_columns' => 12, 'text_columns' => 12) : array('data-fieldto' => 'proposal', 'data-fieldid' =>  12 , 'rows' => 12, 'label_columns' => 12, 'text_columns' => 12)); ?>
                        <?php 
							$paratiriseis_value = isset($proposal) ? get_custom_field_value($proposal->id, 11, 'proposal') : '';
							echo render_textarea('custom_fields[proposal][11]','Παρατηρήσεις',$paratiriseis_value,$attrs,array(),'','tinymce'); 
//							echo render_textarea('custom_fields[action][13]', 'Περιγραφή', $perigrafi_value, array('data-fieldto' => 'action', 'data-fieldid' =>  12 , 'rows' => 12)); 
						?>
                        <?php // echo render_custom_fields('proposal',$fc_rel_id, "slug = 'proposal_paratirisis'"); ?>
                       <div class="form-group no-mbot">
                           <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                           <input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo (isset($proposal) ? prep_tags_input(get_tags_in($proposal->id,'proposal')) : ''); ?>" data-role="tagsinput">
                        </div>
                     </div>
                  </div>
                  <div class="btn-bottom-toolbar bottom-transaction text-right">
                  <p class="no-mbot pull-left mtop5 btn-toolbar-notice"><?php echo _l('include_proposal_items_merge_field_help','<b>{proposal_items}</b>'); ?></p>
                    <button type="button" class="btn btn-info mleft10 proposal-form-submit save-and-send transaction-submit">
                        <?php echo _l('save_and_send'); ?>
                    </button>
                    <button class="btn btn-info mleft5 proposal-form-submit transaction-submit" type="button">
                      <?php echo _l('submit'); ?>
                    </button>
               </div>
               </div>
            </div>
         </div>
<!--         <div class="col-md-12">
            <div class="panel_s">
               <?php // $this->load->view('admin/estimates/_add_edit_items'); ?>
            </div>
         </div>-->
         <?php echo form_close(); ?>
         <?php $this->load->view('admin/invoice_items/item'); ?>
      </div>
      <div class="btn-bottom-pusher"></div>
   </div>
</div>
<?php // $this->load->view('admin/includes/modals/sales_attach_file'); ?>
<?php 
	$this->load->view('admin/proposals/proposal_maketa'); 
    $this->load->view('admin/proposals/proposal_maketa_edit'); 
	$this->load->view('admin/proposals/proposal_tax'); 
    $this->load->view('admin/proposals/proposal_tax_edit');
	$this->load->view('admin/proposals/proposal_metaforika');
    $this->load->view('admin/proposals/proposal_metaforika_edit');
	$this->load->view('admin/proposals/proposal_topostropos');
    $this->load->view('admin/proposals/proposal_topostropos_edit');
	$this->load->view('admin/proposals/proposal_xronos');
    $this->load->view('admin/proposals/proposal_xronos_edit');
	$this->load->view('admin/proposals/proposal_pliromi');
    $this->load->view('admin/proposals/proposal_pliromi_edit');
	$this->load->view('admin/proposals/proposal_prodiagrafes');
    $this->load->view('admin/proposals/proposal_prodiagrafes_edit');
?>
<?php init_tail(); ?>
<script>
   var _rel_id = $('#rel_id'),
   _rel_type = $('#rel_type'),
   _rel_id_wrapper = $('#rel_id_wrapper'),
   data = {};

   $(function(){
		$("#prodiagrafes_tags").on("change", function() {
			var value = $(this).val().toLowerCase();
			$("#prodiagrafes_list li").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});
    init_currency_symbol();
    // Maybe items ajax search
    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');
    validate_proposal_form();
    $('body').on('change','#rel_id', function() {
     if($(this).val() != ''){
      $.get(admin_url + 'proposals/get_relation_data_values/' + $(this).val() + '/' + _rel_type.val(), function(response) {
			
		var select_to = $('select#proposal_to');
		select_to.html('');
        $.each(response.to,function(index, value){
			select_to.append('<option id="' + value + '">' + value + '</option>');
		});
		select_to.selectpicker('refresh');
		
		var select_email = $('select#proposal_email');
		select_email.html('');
        $.each(response.email,function(index, value){
			select_email.append('<option id="' + value + '">' + value + '</option>');
		});
		select_email.selectpicker('refresh');
		
		
		$('input[name="proposal_to"]').val(response.to);
//        $('textarea[name="address"]').val(response.address);
        $('input[name="email"]').val(response.email);
        $('input[name="phone"]').val(response.phone);
//        $('input[name="city"]').val(response.city);
//        $('input[name="state"]').val(response.state);
//        $('input[name="zip"]').val(response.zip);
//        $('select[name="country"]').selectpicker('val',response.country);
        var currency_selector = $('#currency');
        if(_rel_type.val() == 'customer'){
          if(typeof(currency_selector.attr('multi-currency')) == 'undefined'){
            currency_selector.attr('disabled',true);
          }

         } else {
           currency_selector.attr('disabled',false);
        }
        var proposal_to_wrapper = $('[app-field-wrapper="proposal_to"]');
        if(response.is_using_company == false && !empty(response.company)) {
          proposal_to_wrapper.find('#use_company_name').remove();
          proposal_to_wrapper.find('#use_company_help').remove();
          proposal_to_wrapper.append('<div id="use_company_help" class="hide">'+response.company+'</div>');
          proposal_to_wrapper.find('label')
          .prepend("<a href=\"#\" id=\"use_company_name\" data-toggle=\"tooltip\" data-title=\"<?php echo _l('use_company_name_instead'); ?>\" onclick='document.getElementById(\"proposal_to\").value = document.getElementById(\"use_company_help\").innerHTML.trim(); this.remove();'><i class=\"fa fa-building-o\"></i></a> ");
        } else {
          proposal_to_wrapper.find('label #use_company_name').remove();
          proposal_to_wrapper.find('label #use_company_help').remove();
        }
       /* Check if customer default currency is passed */
       if(response.currency){
         currency_selector.selectpicker('val',response.currency);
       } else {
        /* Revert back to base currency */
        currency_selector.selectpicker('val',currency_selector.data('base'));
      }
      currency_selector.selectpicker('refresh');
      currency_selector.change();
    }, 'json');
    }
   });
    $('.rel_id_label').html(_rel_type.find('option:selected').text());
    _rel_type.on('change', function() {
      var clonedSelect = _rel_id.html('').clone();
      _rel_id.selectpicker('destroy').remove();
      _rel_id = clonedSelect;
      $('#rel_id_select div').append(clonedSelect);
      proposal_rel_id_select();
      if($(this).val() != ''){
        _rel_id_wrapper.removeClass('hide');
      } else {
        _rel_id_wrapper.addClass('hide');
      }
      $('.rel_id_label').html(_rel_type.find('option:selected').text());
    });
    proposal_rel_id_select();
    <?php if(!isset($proposal) && $rel_id != ''){ ?>
      _rel_id.change();
      <?php } ?>
    });
   function proposal_rel_id_select(){
      var serverData = {};
      serverData.rel_id = _rel_id.val();
      data.type = _rel_type.val();
      <?php if(isset($proposal)){ ?>
        serverData.connection_type = 'proposal';
        serverData.connection_id = '<?php echo $proposal->id; ?>';
      <?php } ?>
      init_ajax_search(_rel_type.val(),_rel_id,serverData);
   }
   function validate_proposal_form(){
      _validate_form($('#proposal-form'), {
        subject : 'required',
        proposal_to : 'required',
        rel_type: 'required',
        rel_id : 'required',
        date : 'required',
        email: {
//         email:true,
         required:true
       },
//       currency : 'required',
     });
   }
</script>
<?php // echo app_script('assets/js','proposals.js'); ?>
</body>
</html>
