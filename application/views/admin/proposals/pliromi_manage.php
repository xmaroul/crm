<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                     <div class="_buttons">
                        <a href="#" class="btn btn-info pull-left" data-toggle="modal" data-target="#proposal_pliromi_modal"><?php echo _l('new_proposal_pliromi'); ?></a>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr-panel-heading" />
                    <div class="clearfix"></div>
                    <?php render_datatable(array(
                        _l('proposal_pliromi_name'),
                        _l('options'),
                        ),'proposal-pliromi'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/proposals/proposal_pliromi'); ?>
<?php init_tail(); ?>
<script>
   $(function(){
        initDataTable('.table-proposal-pliromi', window.location.href, [1], [1]);
   });
</script>
</body>
</html>
