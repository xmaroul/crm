<div class="modal fade" id="manage_proposal_topostropos_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_topostropos_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($topostropos as $topostropos_value) { ?>
                        <div class="row" id="row-<?php echo $topostropos_value['id']; ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $topostropos_value['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
										echo icon_btn('#', 'remove', 'btn-danger _delete delete_topostropos', array('id' => 'delete_topostropos'.$topostropos_value['id'], 'onclick' => "delete_proposal_topostropos(".$topostropos_value['id'].", 'delete_topostropos')"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	function delete_proposal_topostropos(id, method){
	   var url = method+'/'+id;
	   $.post(url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-topostropos')){
                    $('.table-proposal-tax').DataTable().ajax.reload();
                }
                if($('select[name="topostropos_in[]"]').length) {
                    $('select[name="topostropos_in[]"] option[value="'+id+'"]').remove();
					var topostropos = $('select[name="topostropos_in[]"]');
                    topostropos.selectpicker('refresh');
                }
				$('#row-'+id).remove();
                alert_float('success', response.message);
			}
			$('#manage_proposal_topostropos_modal').modal('hide');
        });
        return false;
   }
	
</script>
