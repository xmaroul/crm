<div class="modal fade" id="manage_proposal_maketa_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_maketa_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($maketas as $maketa) { ?>
                        <div class="row" id="row-<?php echo $maketa['id']; ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $maketa['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
										echo icon_btn('#', 'remove', 'btn-danger _delete delete_maketa', array('id' => 'delete_maketa'.$maketa['id'], 'onclick' => "delete_proposal_maketa(".$maketa['id'].", 'delete_maketa')"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	
   function delete_proposal_maketa(id, method){
	   var url = method+'/'+id;
	   $.post(url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-maketas')){
                    $('.table-proposal-maketas').DataTable().ajax.reload();
                }
                if($('select[name="maketas_in[]"]').length && response.success === true) {
                    $('select[name="maketas_in[]"] option[value="'+id+'"]').remove();
					var maketas = $('select[name="maketas_in[]"]');
                    maketas.selectpicker('refresh');
                }
				$('#row-'+id).remove();
                alert_float('success', response.message);
            }
            $('#manage_proposal_maketa_modal').modal('hide');
//            location.reload();
        });
        return false;
   }
</script>
