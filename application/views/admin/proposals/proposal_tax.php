<div class="modal fade" id="proposal_tax_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_tax_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('proposal_tax_add_heading'); ?></span>
                </h4>
            </div>
			<?php echo form_open('admin/proposals/tax',array('id'=>'proposal-tax-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','proposal_tax_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-tax-modal'), {
        name: 'required'
    }, manage_proposal_tax);

	$('#proposal_tax_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var tax_id = $(invoker).data('id');
        $('#proposal_tax_modal .add-title').removeClass('hide');
        $('#proposal_tax_modal .edit-title').addClass('hide');
        $('#proposal_tax_modal input[name="id"]').val('');
        $('#proposal_tax_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(tax_id) !== 'undefined') {
            $('#proposal_tax_modal input[name="id"]').val(tax_id);
            $('#proposal_tax_modal .add-title').addClass('hide');
            $('#proposal_tax_modal .edit-title').removeClass('hide');
            $('#proposal_tax_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
	
	$('#manage_proposal_tax_modal').on('show.bs.modal', function(e) {
		var row_element = $('#manage_proposal_tax_modal div.modal-body div.row');
		row_element.empty();
		$('select[name="taxes_in[]"] > option').each(function() {
			delete_callback = "'delete_tax'";
			var new_row = '<div class="row" id="row-'+this.value+'"><div class="col-md-12"><div class="col-md-6">'+this.text+'</div><div class="col-md-2"><a href="#" class="btn btn-danger _delete delete_tax btn-icon" id="delete_tax'+this.value+'" onclick="delete_proposal_tax('+this.value+', '+delete_callback+')"><i class="fa fa-remove"></i></a></div></div>';
			row_element.prepend(new_row);
		});
	});
	
   });
    function manage_proposal_tax(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-taxes')){
                    $('.table-proposal-taxes').DataTable().ajax.reload();
                }
                if($('select[name="taxes_in[]"]').length && typeof(response.id) != 'undefined') {
                    var taxes = $('select[name="taxes_in[]"]');
                    taxes.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    taxes.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_tax_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>
