<div class="modal fade" id="manage_proposal_pliromi_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_pliromi_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($pliromi as $pliromi_value) { ?>
                        <div class="row" id="row-<?php echo $pliromi_value['id']; ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $pliromi_value['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
										echo icon_btn('#', 'remove', 'btn-danger _delete delete_pliromi', array('id' => 'delete_pliromi'.$pliromi_value['id'], 'onclick' => "delete_proposal_pliromi(".$pliromi_value['id'].", 'delete_pliromi')"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	function delete_proposal_pliromi(id, method){
	   var url = method+'/'+id;
	   $.post(url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-pliromi')){
                    $('.table-proposal-pliromi').DataTable().ajax.reload();
                }
                if($('select[name="pliromi_in[]"]').length) {
                    $('select[name="pliromi_in[]"] option[value="'+id+'"]').remove();
					var pliromi = $('select[name="pliromi_in[]"]');
                    pliromi.selectpicker('refresh');
                }
				$('#row-'+id).remove();
                alert_float('success', response.message);
			}
			$('#manage_proposal_pliromi_modal').modal('hide');
        });
        return false;
   }
</script>
