<div class="modal fade" id="manage_proposal_tax_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_tax_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($taxes as $tax) { ?>
                        <div class="row" id="row-<?php echo $tax['id']; ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $tax['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
										echo icon_btn('#', 'remove', 'btn-danger _delete delete_tax', array('id' => 'delete_tax'.$tax['id'], 'onclick' => "delete_proposal_tax(".$tax['id'].", 'delete_tax')"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	function delete_proposal_tax(id, method){
	   var url = method+'/'+id;
	   $.post(url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-tax')){
                    $('.table-proposal-tax').DataTable().ajax.reload();
                }
                if($('select[name="taxes_in[]"]').length) {
                    $('select[name="taxes_in[]"] option[value="'+id+'"]').remove();
					var tax = $('select[name="taxes_in[]"]');
                    tax.selectpicker('refresh');
                }
				$('#row-'+id).remove();
                alert_float('success', response.message);
			}
			$('#manage_proposal_tax_modal').modal('hide');
        });
        return false;
   }
</script>
