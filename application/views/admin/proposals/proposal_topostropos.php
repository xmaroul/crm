<div class="modal fade" id="proposal_topostropos_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_topostropos_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('proposal_topostropos_add_heading'); ?></span>
                </h4>
            </div>
            <?php // echo form_open('admin/clients/group',array('id'=>'customer-group-modal')); ?>
			<?php echo form_open('admin/proposals/topostropos',array('id'=>'proposal-topostropos-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','proposal_topostropos_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-topostropos-modal'), {
        name: 'required'
    }, manage_proposal_topostropos);

       $('#proposal_topostropos_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var topostropos_id = $(invoker).data('id');
        $('#proposal_topostropos_modal .add-title').removeClass('hide');
        $('#proposal_topostropos_modal .edit-title').addClass('hide');
        $('#proposal_topostropos_modal input[name="id"]').val('');
        $('#proposal_topostropos_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(topostropos_id) !== 'undefined') {
            $('#proposal_topostropos_modal input[name="id"]').val(topostropos_id);
            $('#proposal_topostropos_modal .add-title').addClass('hide');
            $('#proposal_topostropos_modal .edit-title').removeClass('hide');
            $('#proposal_topostropos_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
	$('#manage_proposal_topostropos_modal').on('show.bs.modal', function(e) {
		var row_element = $('#manage_proposal_topostropos_modal div.modal-body div.row');
		row_element.empty();
		$('select[name="topostropos_in[]"] > option').each(function() {
			delete_callback = "'delete_topostropos'";
			var new_row = '<div class="row" id="row-'+this.value+'"><div class="col-md-12"><div class="col-md-6">'+this.text+'</div><div class="col-md-2"><a href="#" class="btn btn-danger _delete delete_topostropos btn-icon" id="delete_topostropos'+this.value+'" onclick="delete_proposal_topostropos('+this.value+', '+delete_callback+')"><i class="fa fa-remove"></i></a></div></div>';
			row_element.prepend(new_row);
		});
	});
   });
    function manage_proposal_topostropos(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-topostropos')){
                    $('.table-proposal-topostropos').DataTable().ajax.reload();
                }
                if($('select[name="topostropos_in[]"]').length && typeof(response.id) != 'undefined') {
                    var tropoi = $('select[name="topostropos_in[]"]');
                    tropoi.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    tropoi.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_topostropos_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>
