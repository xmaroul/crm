<div class="modal fade" id="proposal_maketa_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_maketa_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('proposal_maketa_add_heading'); ?></span>
                </h4>
            </div>
            <?php // echo form_open('admin/clients/group',array('id'=>'customer-group-modal')); ?>
			<?php echo form_open('admin/proposals/maketa',array('id'=>'proposal-maketa-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','proposal_maketa_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-maketa-modal'), {
        name: 'required'
    }, manage_proposal_maketas);

       $('#proposal_maketa_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var maketa_id = $(invoker).data('id');
        $('#proposal_maketa_modal .add-title').removeClass('hide');
        $('#proposal_maketa_modal .edit-title').addClass('hide');
        $('#proposal_maketa_modal input[name="id"]').val('');
        $('#proposal_maketa_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(maketa_id) !== 'undefined') {
            $('#proposal_maketa_modal input[name="id"]').val(maketa_id);
            $('#proposal_maketa_modal .add-title').addClass('hide');
            $('#proposal_maketa_modal .edit-title').removeClass('hide');
            $('#proposal_maketa_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
	$('#manage_proposal_maketa_modal').on('show.bs.modal', function(e) {
            var row_element = $('#manage_proposal_maketa_modal div.modal-body div.row');
            row_element.empty();
            $('select[name="maketas_in[]"] > option').each(function() {
                delete_callback = "'delete_maketa'";
                var new_row = '<div class="row" id="row-'+this.value+'"><div class="col-md-12"><div class="col-md-6">'+this.text+'</div><div class="col-md-2"><a href="#" class="btn btn-danger _delete delete_maketa btn-icon" id="delete_maketa'+this.value+'" onclick="delete_proposal_maketa('+this.value+', '+delete_callback+')"><i class="fa fa-remove"></i></a></div></div>';
                row_element.prepend(new_row);
            });
	});
   });
    function manage_proposal_maketas(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-maketas')){
                    $('.table-proposal-maketas').DataTable().ajax.reload();
                }
                if($('select[name="maketas_in[]"]').length && typeof(response.id) != 'undefined') {
                    var maketas = $('select[name="maketas_in[]"]');
                    maketas.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    maketas.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_maketa_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>
