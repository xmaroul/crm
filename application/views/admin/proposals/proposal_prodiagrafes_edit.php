<div class="modal fade" id="manage_proposal_prodiagrafes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_prodiagrafes_edit_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
					<div class="col-md-12">
						<?php 
							echo render_select( 'prodiagrafes_tags',$prodiagrafes_tags, array('tag','tag'));
						?>
					</div>
					<ul id="prodiagrafes_list">
                    <?php
                        foreach ($prodiagrafes as $prodiagrafe) { ?>
						<li id="<?php echo $prodiagrafe['id']; ?>">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6" id="content-<?php echo $prodiagrafe['id']; ?>">
										<?php echo $prodiagrafe['name']; ?>
									</div>
									<div class="col-md-1">
										<?php 
											echo icon_btn('#', 'remove', 'btn-danger _delete delete_prodiagrafe', array('id' => 'delete_prodiagrafe'.$prodiagrafe['id'], 'onclick' => "delete_proposal_prodiagrafi(".$prodiagrafe['id'].", 'delete_prodiagrafe')"));
										?>
									</div>
									<div class="col-md-1">
										<!--<a href="#" onClick="copyToClipboard('content-<?php // echo $prodiagrafe['id']; ?>')" id="'.$prodiagrafe['id'].'"><i class="fa fa-copy"></i></a>-->
									</div>
								</div>
							</div>
						</li>
                    <?php } ?>
					</ul>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	function delete_proposal_prodiagrafi(id, method){
	   var url = admin_url + 'proposals/' +method+'/'+id;
	   $.post( url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($('ul#prodiagrafes_list li').length) {
					$("ul#prodiagrafes_list li").each(function(index){
						if(this.id == id){
							this.remove();
						}
					});
//					var prodiagrafes = $('select[name="maketas_in[]"]');
//                    maketas.selectpicker('refresh');
                }
//				$('#row-'+id).remove();
                alert_float('success', response.message);
            }
            $('#manage_proposal_maketa_modal').modal('hide');
//            location.reload();
        });
        return false;
   }
   function copy_proposal_prodiagrafi(id){
	   if (document.selection) {
			var range = document.body.createTextRange();
			range.moveToElementText(document.getElementById('prodiagrafi-content-'+id));
			range.select().createTextRange();
			document.execCommand("copy");

		} else if (window.getSelection) {
			var range = document.createRange();
			range.selectNode(document.getElementById('prodiagrafi-content-'+id));
			window.getSelection().addRange(range);
			document.execCommand("copy");
			alert("text copied, copy in the text-area");
		}
	   /* Get the text field */
//		var copyText = document.getElementById("prodiagrafi-content-"+id);
//
//		/* Select the text field */
//		copyText.select();
//
//		/* Copy the text inside the text field */
//		document.execCommand("copy");
//
//		/* Alert the copied text */
//		alert("Copied the text: " + copyText.value);
   }
</script>
