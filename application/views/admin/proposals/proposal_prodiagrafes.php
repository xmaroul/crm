<div class="modal fade" id="proposal_prodiagrafes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php // echo _l('proposal_prodiagrafes_edit_heading'); ?></span>
                    <span class="add-title"><?php  echo _l('proposal_prodiagrafes_add_heading'); ?></span>
                </h4>
            </div>
			<?php echo form_open('admin/proposals/prodiagrafe',array('id'=>'proposal-prodiagrafes-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_textarea('name','proposal_prodiagrafes_name'); ?>
						<?php echo render_input('tag','proposal_prodiagrafes_tag'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-prodiagrafes-modal'), {
        name: 'required'
    }, manage_proposal_prodiagrafes);

	$('#proposal_prodiagrafes_modal').on('show.bs.modal', function(e) {
		var invoker = $(e.relatedTarget);
		var prodiagrafes_id = $(invoker).data('id');
		$('#proposal_prodiagrafes_modal .add-title').removeClass('hide');
		$('#proposal_prodiagrafes_modal .edit-title').addClass('hide');
		$('#proposal_prodiagrafes_modal input[name="id"]').val('');
		$('#proposal_prodiagrafes_modal input[name="name"]').val('');
		// is from the edit button
		if (typeof(prodiagrafes_id) !== 'undefined') {
			$('#proposal_prodiagrafes_modal input[name="id"]').val(prodiagrafes_id);
			$('#proposal_prodiagrafes_modal .add-title').addClass('hide');
			$('#proposal_prodiagrafes_modal .edit-title').removeClass('hide');
			$('#proposal_prodiagrafes_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
		}
    });
	$('#manage_proposal_prodiagrafes_modal').on('show.bs.modal', function(e) {
		var ul_element = $('#manage_proposal_prodiagrafes_modal div.modal-body div.row ul#prodiagrafes_list');
		ul_element.empty();
		$.post(admin_url + 'proposals/get_prodiagrafes').done(function(data) {
			data = JSON.parse(data);
			for (var i = 0; i < data.length; i++) {
				 delete_callback = "'delete_prodiagrafe'";
				  var new_row = '<li id="content-item-'+data[i].id+'">\n\
									<div class="row">\n\
										<div class="col-md-12">\n\
											<div class="col-md-6" id="prodiagrafi-content-'+data[i].id+'">\n\
												'+data[i].name+'<span class="hidden">'+data[i].tag+'</span>\n\
											</div>\n\
											<div class="col-md-1">\n\
												<a href="#" class="btn btn-danger _delete delete_prodiagrafe btn-icon" id="delete_prodiagrafe'+data[i].id+'" onclick="delete_proposal_prodiagrafi('+data[i].id+', '+delete_callback+')"><i class="fa fa-remove"></i></a>\n\
											</div>\n\
										</div>\n\
								</li>';
				  ul_element.prepend(new_row);
			}
		});
		$.post(admin_url + 'proposals/get_prodiagrafes_tags').done(function(data) {
			data = JSON.parse(data);
			if($('select[name="prodiagrafes_tags"]').length) {
				var prodiagrafes_tags = $('select[name="prodiagrafes_tags"]');
				prodiagrafes_tags.empty();
				prodiagrafes_tags.append('<option value=""></option>');
				for (var i = 0; i < data.length; i++) {
					prodiagrafes_tags.append('<option value="'+data[i].tag+'">'+data[i].tag+'</option>');
				}
				prodiagrafes_tags.selectpicker('refresh');
			}
		});
		$("#prodiagrafes_tags").on("change", function() {
			var value = $(this).val().toLowerCase();
			$("#prodiagrafes_list li").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			});
		});
	});
   });
    function manage_proposal_prodiagrafes(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-prodiagrafes')){
                    $('.table-proposal-prodiagrafes').DataTable().ajax.reload();
                }
                if($('select[name="prodiagrafes_in[]"]').length && typeof(response.id) != 'undefined') {
                    var prodiagrafes = $('select[name="prodiagrafes_in[]"]');
                    prodiagrafes.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    prodiagrafes.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_prodiagrafes_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>


<!--<div class="col-md-1">\n\
<a href="#" class="btn btn-warning _copy copy_prodiagrafe btn-icon" id="copy_prodiagrafe'+data[i].id+'" onclick="copy_proposal_prodiagrafi('+data[i].id+')"><i class="fa fa-copy"></i></a>\n\
</div>\n\-->