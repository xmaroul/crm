<div class="modal fade" id="proposal_metaforika_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_metaforika_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('proposal_metaforika_add_heading'); ?></span>
                </h4>
            </div>
            <?php // echo form_open('admin/clients/group',array('id'=>'customer-group-modal')); ?>
			<?php echo form_open('admin/proposals/metaforika',array('id'=>'proposal-metaforika-modal')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('name','proposal_metaforika_name'); ?>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',function(){
       _validate_form($('#proposal-metaforika-modal'), {
        name: 'required'
    }, manage_proposal_metaforika);

       $('#proposal_metaforika_modal').on('show.bs.modal', function(e) {
        var invoker = $(e.relatedTarget);
        var metaforika_id = $(invoker).data('id');
        $('#proposal_metaforika_modal .add-title').removeClass('hide');
        $('#proposal_metaforika_modal .edit-title').addClass('hide');
        $('#proposal_metaforika_modal input[name="id"]').val('');
        $('#proposal_metaforika_modal input[name="name"]').val('');
        // is from the edit button
        if (typeof(maketa_id) !== 'undefined') {
            $('#proposal_metaforika_modal input[name="id"]').val(maketa_id);
            $('#proposal_metaforika_modal .add-title').addClass('hide');
            $('#proposal_metaforika_modal .edit-title').removeClass('hide');
            $('#proposal_metaforika_modal input[name="name"]').val($(invoker).parents('tr').find('td').eq(0).text());
        }
    });
	$('#manage_proposal_metaforika_modal').on('show.bs.modal', function(e) {
            var row_element = $('#manage_proposal_metaforika_modal div.modal-body div.row');
            row_element.empty();
            $('select[name="metaforika_in[]"] > option').each(function() {
                delete_callback = "'delete_metaforika'";
                var new_row = '<div class="row" id="row-'+this.value+'"><div class="col-md-12"><div class="col-md-6">'+this.text+'</div><div class="col-md-2"><a href="#" class="btn btn-danger _delete delete_metaforika btn-icon" id="delete_metaforika'+this.value+'" onclick="delete_proposal_metaforika('+this.value+', '+delete_callback+')"><i class="fa fa-remove"></i></a></div></div>';
                row_element.prepend(new_row);
            });
	});
   });
   
    function manage_proposal_metaforika(form) {
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-metaforika')){
                    $('.table-proposal-metaforika').DataTable().ajax.reload();
                }
                if($('select[name="metaforika_in[]"]').length && typeof(response.id) != 'undefined') {
                    var metaforika = $('select[name="metaforika_in[]"]');
                    metaforika.prepend('<option value="'+response.id+'">'+response.name+'</option>');
                    metaforika.selectpicker('refresh');
                }
                alert_float('success', response.message);
            }
            $('#proposal_metaforika_modal').modal('hide');
//            location.reload();
        });
        return false;
    }

</script>
