<div class="modal fade" id="manage_proposal_metaforika_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('proposal_metaforika_delete_heading'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php
                        foreach ($metaforika as $metaforika_value) { ?>
                        <div class="row" id="row-<?php echo $metaforika_value['id']; ?>">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <?php echo $metaforika_value['name']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php 
                                        echo icon_btn('#', 'remove', 'btn-danger _delete delete_metaforika', array('id' => 'delete_metaforika'.$metaforika_value['id'], 'onclick' => "delete_proposal_metaforika(".$metaforika_value['id'].", 'delete_metaforika')"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>  
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script>
	function delete_proposal_metaforika(id, method){
	   var url = method+'/'+id;
	   $.post(url, { id: id }).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                if($.fn.DataTable.isDataTable('.table-proposal-metaforika')){
                    $('.table-proposal-metaforika').DataTable().ajax.reload();
                }
                if($('select[name="metaforika_in[]"]').length) {
                    $('select[name="metaforika_in[]"] option[value="'+id+'"]').remove();
					var metaforika = $('select[name="metaforika_in[]"]');
                    metaforika.selectpicker('refresh');
                }
                $('#row-'+id).remove();
                alert_float('success', response.message);
            }
            $('#manage_proposal_metaforika_modal').modal('hide');
        });
        return false;
   }

</script>
