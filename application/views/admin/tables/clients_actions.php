<?php

defined('BASEPATH') or exit('No direct script access allowed');

$baseCurrencySymbol = $this->ci->currencies_model->get_base_currency()->symbol;

$aColumns = [
    'tblactions.id',
    'subject',
    'action_to',
//    'total',
    'date',
//    'open_till',
//    '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM tbltags_in JOIN tbltags ON tbltags_in.tag_id = tbltags.id WHERE rel_id = tblactions.id and rel_type="action" ORDER by tag_order ASC) as tags',
//    'datecreated',
    'status',
];

$sIndexColumn = 'id';
$sTable       = 'tblactions';

$where  = [];
array_push($where, "AND rel_type IN ('customer') ");
$filter = [];

if ($this->ci->input->post('leads_related')) {
    array_push($filter, 'OR rel_type="lead"');
}
if ($this->ci->input->post('customers_related')) {
    array_push($filter, 'OR rel_type="customer"');
}
if ($this->ci->input->post('suppliers_related')) {
    array_push($filter, 'OR rel_type="supplier"');
}
if ($this->ci->input->post('expired')) {
    array_push($filter, 'OR open_till IS NOT NULL AND open_till <"' . date('Y-m-d') . '" AND status NOT IN(2,3)');
}

$statuses  = $this->ci->actions_model->get_statuses();
$statusIds = [];

foreach ($statuses as $status) {
    if ($this->ci->input->post('action_' . $status)) {
        array_push($statusIds, $status);
    }
}
if (count($statusIds) > 0) {
    array_push($filter, 'AND status IN (' . implode(', ', $statusIds) . ')');
}

$agents    = $this->ci->actions_model->get_sale_agents();
$agentsIds = [];
foreach ($agents as $agent) {
    if ($this->ci->input->post('sale_agent_' . $agent['sale_agent'])) {
        array_push($agentsIds, $agent['sale_agent']);
    }
}
if (count($agentsIds) > 0) {
    array_push($filter, 'AND assigned IN (' . implode(', ', $agentsIds) . ')');
}

$years      = $this->ci->actions_model->get_actions_years();
$yearsArray = [];
foreach ($years as $year) {
    if ($this->ci->input->post('year_' . $year['year'])) {
        array_push($yearsArray, $year['year']);
    }
}
if (count($yearsArray) > 0) {
    array_push($filter, 'AND YEAR(date) IN (' . implode(', ', $yearsArray) . ')');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

if (!has_permission('actions', '', 'view')) {
    array_push($where, 'AND ' . get_action_sql_where_staff(get_staff_user_id()));
}

$join          = [];
$custom_fields = get_table_custom_fields('action');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);

    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblactions.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$aColumns = do_action('actions_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'currency',
    'rel_id',
    'rel_type',
    'invoice_id',
    'hash',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

//$table_data = array(
//								_l('action') . ' #',
//								_l('action_date'),
//								_l('action_kind'),
//								_l('action_subject'),
//								_l('action_catalogue_value'),
//								_l('action_charge'),
//								_l('action_delivery_cost'),
//								_l('action_total'),
//								_l('action_credit'),
//								_l('action_balance'),
//								_l('action_status'),
//								_l('action_to'),
//								);

$user_id = $_SESSION['staff_user_id'];
foreach ($rResult as $aRow) {
	$priority = get_custom_field_value($aRow['tblactions.id'], 28, 'action');
	if ($priority == 'Άμεσα' && !in_array($user_id, array(1,4))){
		continue;
	}
    $row = [];

    $numberOutput = '<a href="' . admin_url('action/list_action/' . $aRow['tblactions.id']) . '" onclick="init_action(' . $aRow['tblactions.id'] . '); return false;">' . format_action_number($aRow['tblactions.id']) . '</a>';

    $numberOutput .= '<div class="row-options">';

//    $numberOutput .= '<a href="' . site_url('action/' . $aRow['tblactions.id'] . '/' . $aRow['hash']) . '" target="_blank">' . _l('view') . '</a>';
    if (has_permission('actions', '', 'edit')) {
        $numberOutput .= '<a href="' . admin_url('actions/action/' . $aRow['tblactions.id']) . '">' . _l('edit') . '</a>';
    }
    $numberOutput .= '</div>';

    $row[] = $numberOutput;

    $row[] = '<a href="' . admin_url('action/list_action/' . $aRow['tblactions.id']) . '" onclick="init_action(' . $aRow['tblactions.id'] . '); return false;">' . $aRow['subject'] . '</a>';
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 14, 'action');
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 16, 'action');
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 17, 'action');
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 20, 'action');
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 16, 'action');
//	
//	$row[] = get_custom_field_value($aRow['tblactions.id'], 21, 'action');
//	
	$row[] = format_action_status($aRow['status']);
	
    if ($aRow['rel_type'] == 'lead') {
        $toOutput = '<a href="#" onclick="init_lead(' . $aRow['rel_id'] . ');return false;" target="_blank" data-toggle="tooltip" data-title="' . _l('lead') . '">' . $aRow['action_to'] . '</a>';
    } elseif ($aRow['rel_type'] == 'customer') {
        $toOutput = '<a href="' . admin_url('clients/client/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['action_to'] . '</a>';
    }

    $row[] = $toOutput;

//    $amount = format_money($aRow['total'], ($aRow['currency'] != 0 ? $this->ci->currencies_model->get_currency_symbol($aRow['currency']) : $baseCurrencySymbol));

//    if ($aRow['invoice_id']) {
//        $amount .= '<br /> <span class="hide"> - </span><span class="text-success">' . _l('estimate_invoiced') . '</span>';
//    }
//
//    $row[] = $amount;


    $row[] = _d($aRow['date']);

//    $row[] = _d($aRow['open_till']);

//    $row[] = render_tags($aRow['tags']);

//    $row[] = _d($aRow['datecreated']);

    

    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $hook_data = do_action('action_table_row_data', [
        'output' => $row,
        'row'    => $aRow,
    ]);

    $row = $hook_data['output'];

    $output['aaData'][] = $row;
}
