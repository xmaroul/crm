<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('suppliers', '', 'delete');

$custom_fields = get_table_custom_fields('suppliers');
$this->ci->db->query("SET sql_mode = ''");

$aColumns = [
    '1',
    'tblsuppliers.userid as userid',
    'company',
    'fullname',
//    'firstname',
//    'email',
    'tblsuppliers.phonenumber as phonenumber',
    'tblsuppliers.active',
//    'GROUP_CONCAT(DISTINCT(tblsuppliersgroups.name)) as supplierGroups',
//    'tblsuppliers.datecreated as datecreated',
];

$sIndexColumn = 'userid';
$sTable       = 'tblsuppliers';
$where        = [];
// Add blank where all filter can be stored
$filter = [];

$join = [
    'LEFT JOIN tblcontacts ON tblcontacts.userid=tblsuppliers.userid AND tblcontacts.is_primary=1',
    'LEFT JOIN tblsuppliergroups_in ON tblsuppliergroups_in.supplier_id = tblsuppliers.userid',
    'LEFT JOIN tblsuppliersgroups ON tblsuppliersgroups.id = tblsuppliergroups_in.groupid',
];

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblsuppliers.userid = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}
// Filter by custom groups
$groups   = $this->ci->suppliers_model->get_groups();
$groupIds = [];
foreach ($groups as $group) {
    if ($this->ci->input->post('supplier_group_' . $group['id'])) {
        array_push($groupIds, $group['id']);
    }
}
if (count($groupIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplier_id FROM tblsuppliergroups_in WHERE groupid IN (' . implode(', ', $groupIds) . '))');
}

$countries  = $this->ci->suppliers_model->get_suppliers_distinct_countries();
$countryIds = [];
foreach ($countries as $country) {
    if ($this->ci->input->post('country_' . $country['country_id'])) {
        array_push($countryIds, $country['country_id']);
    }
}
if (count($countryIds) > 0) {
    array_push($filter, 'AND country IN (' . implode(',', $countryIds) . ')');
}


$this->ci->load->model('invoices_model');
// Filter by invoices
$invoiceStatusIds = [];
foreach ($this->ci->invoices_model->get_statuses() as $status) {
    if ($this->ci->input->post('invoices_' . $status)) {
        array_push($invoiceStatusIds, $status);
    }
}
if (count($invoiceStatusIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplierid FROM tblinvoices WHERE status IN (' . implode(', ', $invoiceStatusIds) . '))');
}

// Filter by estimates
$estimateStatusIds = [];
$this->ci->load->model('estimates_model');
foreach ($this->ci->estimates_model->get_statuses() as $status) {
    if ($this->ci->input->post('estimates_' . $status)) {
        array_push($estimateStatusIds, $status);
    }
}
if (count($estimateStatusIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplierid FROM tblestimates WHERE status IN (' . implode(', ', $estimateStatusIds) . '))');
}

// Filter by projects
$projectStatusIds = [];
$this->ci->load->model('projects_model');
foreach ($this->ci->projects_model->get_project_statuses() as $status) {
    if ($this->ci->input->post('projects_' . $status['id'])) {
        array_push($projectStatusIds, $status['id']);
    }
}
if (count($projectStatusIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplierid FROM tblprojects WHERE status IN (' . implode(', ', $projectStatusIds) . '))');
}

// Filter by proposals
$proposalStatusIds = [];
$this->ci->load->model('proposals_model');
foreach ($this->ci->proposals_model->get_statuses() as $status) {
    if ($this->ci->input->post('proposals_' . $status)) {
        array_push($proposalStatusIds, $status);
    }
}
if (count($proposalStatusIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT rel_id FROM tblproposals WHERE status IN (' . implode(', ', $proposalStatusIds) . ') AND rel_type="supplier")');
}

// Filter by having contracts by type
$this->ci->load->model('contracts_model');
$contractTypesIds = [];
$contract_types   = $this->ci->contracts_model->get_contract_types();

foreach ($contract_types as $type) {
    if ($this->ci->input->post('contract_type_' . $type['id'])) {
        array_push($contractTypesIds, $type['id']);
    }
}
if (count($contractTypesIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplier FROM tblcontracts WHERE contract_type IN (' . implode(', ', $contractTypesIds) . '))');
}

// Filter by proposals
$customAdminIds = [];
foreach ($this->ci->suppliers_model->get_suppliers_admin_unique_ids() as $cadmin) {
    if ($this->ci->input->post('responsible_admin_' . $cadmin['staff_id'])) {
        array_push($customAdminIds, $cadmin['staff_id']);
    }
}

if (count($customAdminIds) > 0) {
    array_push($filter, 'AND tblsuppliers.userid IN (SELECT supplier_id FROM tblsupplieradmins WHERE staff_id IN (' . implode(', ', $customAdminIds) . '))');
}

if ($this->ci->input->post('requires_registration_confirmation')) {
    array_push($filter, 'AND tblsuppliers.registration_confirmed=0');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

if (!has_permission('suppliers', '', 'view')) {
    array_push($where, 'AND tblsuppliers.userid IN (SELECT supplier_id FROM tblsupplieradmins WHERE staff_id=' . get_staff_user_id() . ')');
}

if ($this->ci->input->post('exclude_inactive')) {
    array_push($where, 'AND (tblsuppliers.active = 1 OR tblsuppliers.active=0 AND registration_confirmed = 0)');
}

if ($this->ci->input->post('my_suppliers')) {
    array_push($where, 'AND tblsuppliers.userid IN (SELECT supplier_id FROM tblsupplieradmins WHERE staff_id=' . get_staff_user_id() . ')');
}

$aColumns = do_action('suppliers_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'tblcontacts.id as contact_id',
    'lastname',
    'tblsuppliers.zip as zip',
    'registration_confirmed',
], 'group by tblsuppliers.userid', [7 => 'name']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    // Bulk actions
    $row[] = '<div class="checkbox"><input type="checkbox" value="' . $aRow['userid'] . '"><label></label></div>';
    // User id
    $row[] = $aRow['userid'];

    // Company
    $company  = $aRow['company'];
    $isPerson = false;

    if ($company == '') {
        $company  = _l('no_company_view_profile');
        $isPerson = true;
    }

    $url = admin_url('suppliers/supplier/' . $aRow['userid']);

    if ($isPerson && $aRow['contact_id']) {
        $url .= '?contactid=' . $aRow['contact_id'];
    }

    $company = '<a href="' . $url . '">' . $company . '</a>';

    $company .= '<div class="row-options">';
    $company .= '<a href="' . $url . '">' . _l('view') . '</a>';

    if ($aRow['registration_confirmed'] == 0 && is_admin()) {
        $company .= ' | <a href="' . admin_url('suppliers/confirm_registration/' . $aRow['userid']) . '" class="text-success bold">' . _l('confirm_registration') . '</a>';
    }
    if (!$isPerson) {
        $company .= ' | <a href="' . admin_url('suppliers/supplier/' . $aRow['userid'] . '?group=contacts') . '">' . _l('supplier_contacts') . '</a>';
    }
    if ($hasPermissionDelete) {
        $company .= ' | <a href="' . admin_url('suppliers/delete/' . $aRow['userid']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
    }

    $company .= '</div>';

    $row[] = $company;

    // Fullname
    $row[] = $aRow['fullname'];
    
    
    // Primary contact
//    $row[] = ($aRow['contact_id'] ? '<a href="' . admin_url('suppliers/supplier/' . $aRow['userid'] . '?contactid=' . $aRow['contact_id']) . '" target="_blank">' . $aRow['firstname'] . ' ' . $aRow['lastname'] . '</a>' : '');

    // Primary contact email
//    $row[] = ($aRow['email'] ? '<a href="mailto:' . $aRow['email'] . '">' . $aRow['email'] . '</a>' : '');

    // Primary contact phone
    $row[] = ($aRow['phonenumber'] ? '<a href="tel:' . $aRow['phonenumber'] . '">' . $aRow['phonenumber'] . '</a>' : '');

    $supplier_email = get_custom_field_value($aRow['userid'], 27, 'suppliers');
    $row[] = ($supplier_email ? '<a href="mailto:' . $supplier_email . '">' . $supplier_email . '</a>' : '');
    
    // Toggle active/inactive supplier
    $toggleActive = '<div class="onoffswitch" data-toggle="tooltip" data-title="' . _l('supplier_active_inactive_help') . '">
    <input type="checkbox"' . ($aRow['registration_confirmed'] == 0 ? ' disabled' : '') . ' data-switch-url="' . admin_url() . 'suppliers/change_supplier_status" name="onoffswitch" class="onoffswitch-checkbox" id="' . $aRow['userid'] . '" data-id="' . $aRow['userid'] . '" ' . ($aRow['tblsuppliers.active'] == 1 ? 'checked' : '') . '>
    <label class="onoffswitch-label" for="' . $aRow['userid'] . '"></label>
    </div>';

    // For exporting
    $toggleActive .= '<span class="hide">' . ($aRow['tblsuppliers.active'] == 1 ? _l('is_active_export') : _l('is_not_active_export')) . '</span>';

    $row[] = $toggleActive;

    // Customer groups parsing
//    $groupsRow = '';
//    if ($aRow['supplierGroups']) {
//        $groups = explode(',', $aRow['supplierGroups']);
//        foreach ($groups as $group) {
//            $groupsRow .= '<span class="label label-default mleft5 inline-block supplier-group-list pointer">' . $group . '</span>';
//        }
//    }
//
//    $row[] = $groupsRow;

//    $row[] = _dt($aRow['datecreated']);

    // Custom fields add values
//    foreach ($customFieldsColumns as $customFieldColumn) {
//        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
//    }

    $hook = do_action('suppliers_table_row_data', [
        'output' => $row,
        'row'    => $aRow,
    ]);

    $row = $hook['output'];

    $row['DT_RowClass'] = 'has-row-options';
    if ($aRow['registration_confirmed'] == 0) {
        $row['DT_RowClass'] .= ' alert-info requires-confirmation';
        $row['Data_Title']  = _l('supplier_requires_registration_confirmation');
        $row['Data_Toggle'] = 'tooltip';
    }
    $output['aaData'][] = $row;
}
