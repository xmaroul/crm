<?php

defined('BASEPATH') or exit('No direct script access allowed');
$aColumns = ['name'];

$sIndexColumn = 'id';
$sTable       = 'tblproposalsmaketas';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0 ; $i < count($aColumns) ; $i++) {
        $_data = '<a href="#" data-toggle="modal" data-target="#proposal_maketa_modal" data-id="' . $aRow['id'] . '">' . $aRow[$aColumns[$i]] . '</a>';

        $row[] = $_data;
    }
    $options = icon_btn('#', 'pencil-square-o', 'btn-default', ['data-toggle' => 'modal', 'data-target' => '#proposal_maketa_modal', 'data-id' => $aRow['id']]);
    $row[]   = $options .= icon_btn('proposals/delete_maketa/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
