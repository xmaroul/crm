<?php

defined('BASEPATH') or exit('No direct script access allowed');

$baseCurrencySymbol = $this->ci->currencies_model->get_base_currency()->symbol;

$aColumns = [
    'tblactions.id',
    'subject',
    'action_to',
    'date',
];

$sIndexColumn = 'id';
$sTable       = 'tblactions';

$where  = [];
array_push($where, "AND rel_type IN ('supplier') ");
$filter = [];

if ($this->ci->input->post('leads_related')) {
    array_push($filter, 'OR rel_type="lead"');
}
if ($this->ci->input->post('customers_related')) {
    array_push($filter, 'OR rel_type="customer"');
}
if ($this->ci->input->post('suppliers_related')) {
    array_push($filter, 'OR rel_type="supplier"');
}
if ($this->ci->input->post('expired')) {
    array_push($filter, 'OR open_till IS NOT NULL AND open_till <"' . date('Y-m-d') . '" AND status NOT IN(2,3)');
}

$statuses  = $this->ci->actions_model->get_statuses();
$statusIds = [];

foreach ($statuses as $status) {
    if ($this->ci->input->post('action_' . $status)) {
        array_push($statusIds, $status);
    }
}
if (count($statusIds) > 0) {
    array_push($filter, 'AND status IN (' . implode(', ', $statusIds) . ')');
}

$agents    = $this->ci->actions_model->get_sale_agents();
$agentsIds = [];
foreach ($agents as $agent) {
    if ($this->ci->input->post('sale_agent_' . $agent['sale_agent'])) {
        array_push($agentsIds, $agent['sale_agent']);
    }
}
if (count($agentsIds) > 0) {
    array_push($filter, 'AND assigned IN (' . implode(', ', $agentsIds) . ')');
}

$years      = $this->ci->actions_model->get_actions_years();
$yearsArray = [];
foreach ($years as $year) {
    if ($this->ci->input->post('year_' . $year['year'])) {
        array_push($yearsArray, $year['year']);
    }
}
if (count($yearsArray) > 0) {
    array_push($filter, 'AND YEAR(date) IN (' . implode(', ', $yearsArray) . ')');
}

if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

if (!has_permission('actions', '', 'view')) {
    array_push($where, 'AND ' . get_action_sql_where_staff(get_staff_user_id()));
}

$join          = [];
$custom_fields = get_suppliers_action_custom_fields('action', "slug IN ('action_chreosi_supplier', 'action_pistosi_supplier')");

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);

    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblactions.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$aColumns = do_action('actions_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'currency',
    'rel_id',
    'rel_type',
    'invoice_id',
    'hash',
]);

$output  = $result['output'];
$rResult = $result['rResult'];

$user_id = $_SESSION['staff_user_id'];
foreach ($rResult as $aRow) {
	$priority = get_custom_field_value($aRow['tblactions.id'], 28, 'action');
	if ($priority == 'Άμεσα' && !in_array($user_id, array(1,4))){
		continue;
	}
    $row = [];

    $numberOutput = '<a href="' . admin_url('action/list_action/' . $aRow['tblactions.id']) . '" onclick="init_action(' . $aRow['tblactions.id'] . '); return false;">' . format_action_number($aRow['tblactions.id']) . '</a>';

    $numberOutput .= '<div class="row-options">';

//    $numberOutput .= '<a href="' . site_url('action/' . $aRow['tblactions.id'] . '/' . $aRow['hash']) . '" target="_blank">' . _l('view') . '</a>';
    if (has_permission('actions', '', 'edit')) {
        $numberOutput .= '<a href="' . admin_url('actions/action/' . $aRow['tblactions.id']) . '">' . _l('edit') . '</a>';
    }
    $numberOutput .= '</div>';

    $row[] = $numberOutput;

    $row[] = '<a href="' . admin_url('action/list_action/' . $aRow['tblactions.id']) . '" onclick="init_action(' . $aRow['tblactions.id'] . '); return false;">' . $aRow['subject'] . '</a>';
//	
//	$row[] = '';
	
    if ($aRow['rel_type'] == 'lead') {
        $toOutput = '<a href="#" onclick="init_lead(' . $aRow['rel_id'] . ');return false;" target="_blank" data-toggle="tooltip" data-title="' . _l('lead') . '">' . $aRow['action_to'] . '</a>';
    } elseif ($aRow['rel_type'] == 'customer') {
        $toOutput = '<a href="' . admin_url('clients/client/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['action_to'] . '</a>';
    }elseif ($aRow['rel_type'] == 'supplier') {
        $toOutput = '<a href="' . admin_url('suppliers/supplier/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('supplier') . '">' . $aRow['action_to'] . '</a>';
    }

    $row[] = $toOutput;

    $row[] = _d($aRow['date']);

    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $hook_data = do_action('action_table_row_data', [
        'output' => $row,
        'row'    => $aRow,
    ]);

    $row = $hook_data['output'];

    $output['aaData'][] = $row;
}
