<?php

defined('BASEPATH') or exit('No direct script access allowed');

$baseCurrencySymbol = $this->ci->currencies_model->get_base_currency()->symbol;

$aColumns = [
    'tblactions.id as id',
    'subject',
//    'total',
    'date',
//    'open_till',
//    '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM tbltags_in JOIN tbltags ON tbltags_in.tag_id = tbltags.id WHERE rel_id = tblactions.id and rel_type="action" ORDER by tag_order ASC) as tags',
//    'datecreated',
//    'status',
    ];

$sIndexColumn = 'id';
$sTable       = 'tblactions';
$join         = [];
if ($rel_type != 'supplier'){
	$aColumns[] = 'status';
	$custom_fields = get_table_custom_fields('action');
}else{
	$custom_fields = get_suppliers_action_custom_fields('action', "slug IN ('action_chreosi_supplier', 'action_pistosi_supplier')");
}


foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);

    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN tblcustomfieldsvalues as ctable_' . $key . ' ON tblactions.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$where = 'AND rel_id = ' . $rel_id . ' AND rel_type = "' . $rel_type . '"';

if ($rel_type == 'customer') {
    $this->ci->db->where('userid', $rel_id);
    $customer = $this->ci->db->get('tblclients')->row();
    if ($customer) {
        if (!is_null($customer->leadid)) {
            $where .= ' OR rel_type="lead" AND rel_id=' . $customer->leadid;
        }
    }
}

if ($rel_type == 'supplier') {
    $this->ci->db->where('userid', $rel_id);
    $supplier = $this->ci->db->get('tblsuppliers')->row();
    if ($supplier) {
        if (!is_null($supplier->leadid)) {
            $where .= ' OR rel_type="lead" AND rel_id=' . $supplier->leadid;
        }
    }
}

$where = [$where];

if (!has_permission('actions', '', 'view')) {
    array_push($where, 'AND ' . get_actions_sql_where_staff(get_staff_user_id()));
}

$aColumns = do_action('actions_relation_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    'currency',
    'invoice_id',
    'hash',
    ]);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $numberOutput = '<a href="' . admin_url('actions/list_actions/' . $aRow['id']) . '">' . format_action_number($aRow['id']) . '</a>';

    $numberOutput .= '<div class="row-options">';

    $numberOutput .= '<a href="' . site_url('action/' . $aRow['id'] . '/' . $aRow['hash']) . '" target="_blank">' . _l('view') . '</a>';
    if (has_permission('actions', '', 'edit')) {
        $numberOutput .= ' | <a href="' . admin_url('actions/action/' . $aRow['id']) . '">' . _l('edit') . '</a>';
    }
    $numberOutput .= '</div>';

    $row[] = $numberOutput;

    $row[] = '<a href="' . admin_url('actions/list_actions/' . $aRow['id']) . '">' . $aRow['subject'] . '</a>';

    $row[] = _d($aRow['date']);

	if ($rel_type != 'supplier'){
		$row[] = format_action_status($aRow['status']);
		// Custom fields add values
		foreach ($customFieldsColumns as $customFieldColumn) {
			$row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
		}
	}else{
		$row[] = get_custom_field_value($aRow['id'], 49, 'action');
		$row[] = get_custom_field_value($aRow['id'], 50, 'action');
	}
    

    $output['aaData'][] = $row;
}
