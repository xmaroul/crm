<?php if(isset($supplier)){ ?>
<h4 class="supplier-profile-group-heading"><?php echo _l('supplier_expenses_tab'); ?></h4>
<?php if(has_permission('expenses','','create')){ ?>
<a href="<?php echo admin_url('expenses/expense?supplier_id='.$supplier->userid); ?>" class="btn btn-info mbot15<?php if($supplier->active == 0){echo ' disabled';} ?>">
    <?php echo _l('new_expense'); ?>
</a>
<?php } ?>
<div id="expenses_total" class="mbot25"></div>
<?php $this->load->view('admin/expenses/table_html', array('class'=>'expenses-single-supplier')); ?>
<?php } ?>
