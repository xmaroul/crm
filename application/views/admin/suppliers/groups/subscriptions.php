<?php if(isset($supplier)){ ?>
<h4 class="supplier-profile-group-heading"><?php echo _l('subscriptions'); ?></h4>
<?php if(has_permission('subscriptions','','create')){ ?>
<a href="<?php echo admin_url('subscriptions/create?supplier_id='.$supplier->userid); ?>" class="btn btn-info mbot25<?php if($supplier->active == 0){echo ' disabled';} ?>"><?php echo _l('new_subscription'); ?></a>
<?php } ?>
<?php $this->load->view('admin/subscriptions/table_html',array('url'=>admin_url('subscriptions/table?supplier_id='.$supplier->userid))); ?>
<?php } ?>
