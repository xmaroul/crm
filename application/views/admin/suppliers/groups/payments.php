<?php if(isset($supplier)){ ?>
<h4 class="supplier-profile-group-heading"><?php echo _l('supplier_payments_tab'); ?></h4>
<a href="#" class="btn btn-info mbot25" data-toggle="modal" data-target="#supplier_zip_payments"><?php echo _l('zip_payments'); ?></a>
<?php $this->load->view('admin/payments/table_html', array('class'=>'payments-single-supplier')); ?>
<?php include_once(APPPATH . 'views/admin/suppliers/modals/zip_payments.php'); ?>
<?php } ?>
