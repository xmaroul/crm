<?php if(isset($supplier)){ ?>
<h4 class="supplier-profile-group-heading"><?php echo _l('estimates'); ?></h4>
<?php if(has_permission('estimates','','create')){ ?>
<a href="<?php echo admin_url('estimates/estimate?supplier_id='.$supplier->userid); ?>" class="btn btn-info mbot15<?php if($supplier->active == 0){echo ' disabled';} ?>"><?php echo _l('create_new_estimate'); ?></a>
<?php } ?>
<?php if(has_permission('estimates','','view') || has_permission('estimates','','view_own') || get_option('allow_staff_view_estimates_assigned') == '1'){ ?>
<a href="#" class="btn btn-info mbot15" data-toggle="modal" data-target="#supplier_zip_estimates"><?php echo _l('zip_estimates'); ?></a>
<?php } ?>
<div id="estimates_total"></div>
<?php
   $this->load->view('admin/estimates/table_html', array('class'=>'estimates-single-supplier'));
   include_once(APPPATH . 'views/admin/suppliers/modals/zip_estimates.php');
?>
<?php } ?>
