<?php if(isset($supplier)){ ?>
<h4 class="supplier-profile-group-heading"><?php echo _l('actions'); ?></h4>
<?php if(has_permission('actions','','create')){ ?>
<a href="<?php echo admin_url('actions/action?rel_type=supplier&rel_id='.$supplier->userid); ?>" class="btn btn-info mbot25<?php if($supplier->active == 0){echo ' disabled';} ?>"><?php echo _l('new_action'); ?></a>
<?php } ?>
<?php if(total_rows('tblactions',array('rel_type'=>'supplier','rel_id'=>$supplier->userid))> 0 && (has_permission('actions','','create') || has_permission('actions','','edit'))){ ?>
<!--<a href="#" class="btn btn-info mbot25" data-toggle="modal" data-target="#sync_data_action_data"><?php // echo _l('sync_data'); ?></a>-->
<?php // $this->load->view('admin/actions/sync_data',array('related'=>$supplier,'rel_id'=>$supplier->userid,'rel_type'=>'supplier')); ?>
<?php } ?>
<?php
$table_data = array(
 _l('action') . ' #',
 _l('action_subject'),
// _l('action_total'),
 _l('action_date'),
// _l('action_open_till'),
// _l('tags'),
 _l('action_chreosi_supplier'),
 _l('action_pistosi_supplier'));

render_datatable($table_data,'actions-supplier-profile',[],[
//    'data-last-order-identifier' => 'actions-relation',
//    'data-default-order'         => get_table_last_order('actions-relation'),
]);
?>
<?php } ?>
