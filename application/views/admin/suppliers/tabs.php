<?php
    $supplier_tabs = get_supplier_profile_tabs($supplier->userid);
?>
<ul class="nav navbar-pills navbar-pills-flat nav-tabs nav-stacked supplier-tabs" role="tablist">
   <?php
   $visible_supplier_profile_tabs = get_option('visible_supplier_profile_tabs');
   if($visible_supplier_profile_tabs != 'all') {
      $visible_supplier_profile_tabs = unserialize($visible_supplier_profile_tabs);
   }
   foreach($supplier_tabs as $tab){
      if((isset($tab['visible']) && $tab['visible'] == true) || !isset($tab['visible'])){

        // Check visibility from settings too
        if(is_array($visible_supplier_profile_tabs) && $tab['name'] != 'profile') {
          if(!in_array($tab['name'], $visible_supplier_profile_tabs)) {
            continue;
          }
        }
        ?>
      <li class="<?php if($tab['name'] == 'profile'){echo 'active ';} ?>supplier_tab_<?php echo $tab['name']; ?>">
        <a data-group="<?php echo $tab['name']; ?>" href="<?php echo $tab['url']; ?>"><i class="<?php echo $tab['icon']; ?> menu-icon" aria-hidden="true"></i><?php echo $tab['lang']; ?>
            <?php if(isset($tab['id']) && $tab['id'] == 'reminders'){
              $total_reminders = total_rows('tblreminders',
                  array(
                   'isnotified'=>0,
                   'staff'=>get_staff_user_id(),
                   'rel_type'=>'supplier',
                   'rel_id'=>$supplier->userid
                   )
                  );
              if($total_reminders > 0){
                echo '<span class="badge">'.$total_reminders.'</span>';
              }
          }
          ?>
      </a>
  </li>
  <?php } ?>
  <?php } ?>
</ul>
