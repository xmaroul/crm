<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Action extends Clients_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id, $hash)
    {
        check_action_restrictions($id, $hash);
        $action = $this->actions_model->get($id);

        if ($action->rel_type == 'customer' && !is_client_logged_in()) {
            load_client_language($action->rel_id);
        } else if($action->rel_type == 'lead') {
            load_lead_language($action->rel_id);
        }

        $identity_confirmation_enabled = get_option('action_accept_identity_confirmation');
        if ($this->input->post()) {
            $action = $this->input->post('action');
            switch ($action) {
                case 'action_pdf':

                    $action_number = format_action_number($id);
                    $companyname     = get_option('invoice_company_name');
                    if ($companyname != '') {
                        $action_number .= '-' . mb_strtoupper(slug_it($companyname), 'UTF-8');
                    }

                    try {
                        $pdf = action_pdf($action);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                        die;
                    }

                    $pdf->Output($action_number . '.pdf', 'D');

                    break;
                case 'action_comment':
                    // comment is blank
                    if (!$this->input->post('content')) {
                        redirect($this->uri->uri_string());
                    }
                    $data               = $this->input->post();
                    $data['actionid'] = $id;
                    $this->actions_model->add_comment($data, true);
                    redirect($this->uri->uri_string() . '?tab=discussion');

                    break;
                case 'accept_action':
                    $success = $this->actions_model->mark_action_status(3, $id, true);
                    if ($success) {
                        process_digital_signature_image($this->input->post('signature', false), PROPOSAL_ATTACHMENTS_FOLDER . $id);

                        $this->db->where('id', $id);
                        $this->db->update('tblactions', get_acceptance_info_array());
                        redirect($this->uri->uri_string(), 'refresh');
                    }

                    break;
                case 'decline_action':
                    $success = $this->actions_model->mark_action_status(2, $id, true);
                    if ($success) {
                        redirect($this->uri->uri_string(), 'refresh');
                    }

                    break;
            }
        }

        $number_word_lang_rel_id = 'unknown';
        if ($action->rel_type == 'customer') {
            $number_word_lang_rel_id = $action->rel_id;
        }
        $this->load->library('numberword', [
            'clientid' => $number_word_lang_rel_id,
        ]);

        $this->use_navigation = false;
        $this->use_submenu    = false;

        $data['title']     = $action->subject;
        $data['action']  = do_action('action_html_pdf_data', $action);
        $data['bodyclass'] = 'action action-view';

        $data['identity_confirmation_enabled'] = $identity_confirmation_enabled;
        if ($identity_confirmation_enabled == '1') {
            $data['bodyclass'] .= ' identity-confirmation';
        }

        $data['comments'] = $this->actions_model->get_comments($id);
        add_views_tracking('action', $id);
        do_action('action_html_viewed', $id);
        $data['exclude_reset_css'] = true;
        $data                      = do_action('action_customers_area_view_data', $data);
        no_index_customers_area();
        $this->data = $data;
        $this->view = 'viewaction';
        $this->layout();
    }
}
