<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Actions extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('actions_model');
        $this->load->model('currencies_model');
    }

    public function index($action_id = '')
    {
        $this->list_actions($action_id);
    }
	
	public function clients_actions($action_id = '')
    {
        $this->list_clients_actions($action_id);
    }
	
	public function suppliers_actions($action_id = '')
    {
        $this->list_suppliers_actions($action_id);
    }

	public function list_clients_actions($action_id = ''){
        close_setup_menu();

        if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('actions');
        }

        $isPipeline = $this->session->userdata('actions_pipeline') == 'true';

		// Pipeline was initiated but user click from home page and need to show table only to filter
		if ($this->input->get('status') && $isPipeline) {
			$this->pipeline(0, true);
		}

		$data['action_id']             = $action_id;
		$data['switch_pipeline']       = true;
		$data['title']                 = _l('clients_actions');
		$data['statuses']              = $this->actions_model->get_statuses();
		$data['actions_sale_agents']   = $this->actions_model->get_sale_agents();
		$data['years']                 = $this->actions_model->get_actions_years();
		$this->load->view('admin/actions/manage_clients_actions', $data);
    }
	
	public function list_suppliers_actions($action_id = ''){
        close_setup_menu();

        if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('actions');
        }

        $isPipeline = $this->session->userdata('actions_pipeline') == 'true';

		// Pipeline was initiated but user click from home page and need to show table only to filter
		if ($this->input->get('status') && $isPipeline) {
			$this->pipeline(0, true);
		}

		$data['action_id']             = $action_id;
		$data['switch_pipeline']       = true;
		$data['title']                 = _l('suppliers_actions');
		$data['statuses']              = $this->actions_model->get_statuses();
		$data['actions_sale_agents']   = $this->actions_model->get_sale_agents();
		$data['years']                 = $this->actions_model->get_actions_years();
		$this->load->view('admin/actions/manage_suppliers_actions', $data);
    }
	
    public function list_actions($action_id = '')
    {
        close_setup_menu();

        if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('actions');
        }

        $isPipeline = $this->session->userdata('actions_pipeline') == 'true';

        if ($isPipeline && !$this->input->get('status')) {
            $data['title']           = _l('actions_pipeline');
            $data['bodyclass']       = 'actions-pipeline';
            $data['switch_pipeline'] = false;
            // Direct access
            if (is_numeric($action_id)) {
                $data['actionid'] = $action_id;
            } else {
                $data['actionid'] = $this->session->flashdata('actionid');
            }

            $this->load->view('admin/actions/pipeline/manage', $data);
        } else {

            // Pipeline was initiated but user click from home page and need to show table only to filter
            if ($this->input->get('status') && $isPipeline) {
                $this->pipeline(0, true);
            }

            $data['action_id']             = $action_id;
            $data['switch_pipeline']       = true;
            $data['title']                 = _l('actions');
            $data['statuses']              = $this->actions_model->get_statuses();
            $data['actions_sale_agents']   = $this->actions_model->get_sale_agents();
            $data['years']                 = $this->actions_model->get_actions_years();
            $this->load->view('admin/actions/manage', $data);
        }
    }

    public function table()
    {
        if (!has_permission('actions', '', 'view')
            && !has_permission('actions', '', 'view_own')
            && get_option('allow_staff_view_actions_assigned') == 0) {
            ajax_access_denied();
        }

        $this->app->get_table_data('actions');
    }
	
	public function clients_table()
    {
        if (!has_permission('actions', '', 'view')
            && !has_permission('actions', '', 'view_own')
            && get_option('allow_staff_view_actions_assigned') == 0) {
            ajax_access_denied();
        }

        $this->app->get_table_data('clients_actions');
    }
	
	public function suppliers_table()
    {
        if (!has_permission('actions', '', 'view')
            && !has_permission('actions', '', 'view_own')
            && get_option('allow_staff_view_actions_assigned') == 0) {
            ajax_access_denied();
        }

        $this->app->get_table_data('suppliers_actions');
    }

    public function action_relations($rel_id, $rel_type)
    {
        $this->app->get_table_data('actions_relations', [
            'rel_id'   => $rel_id,
            'rel_type' => $rel_type,
        ]);
    }

    public function delete_attachment($id)
    {
        $file = $this->misc_model->get_file($id);
        if ($file->staffid == get_staff_user_id() || is_admin()) {
            echo $this->actions_model->delete_attachment($id);
        } else {
            ajax_access_denied();
        }
    }

    public function clear_signature($id)
    {
        if (has_permission('actions', '', 'delete')) {
            $this->actions_model->clear_signature($id);
        }

        redirect(admin_url('actions/list_actions/' . $id));
    }

    public function sync_data()
    {
        if (has_permission('actions', '', 'create') || has_permission('actions', '', 'edit')) {
            $has_permission_view = has_permission('actions', '', 'view');

            $this->db->where('rel_id', $this->input->post('rel_id'));
            $this->db->where('rel_type', $this->input->post('rel_type'));

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }

            $address = trim($this->input->post('address'));
            $address = nl2br($address);
            $this->db->update('tblactions', [
                'phone'   => $this->input->post('phone'),
                'zip'     => $this->input->post('zip'),
                'country' => $this->input->post('country'),
                'state'   => $this->input->post('state'),
                'address' => $address,
                'city'    => $this->input->post('city'),
            ]);

            if ($this->db->affected_rows() > 0) {
                echo json_encode([
                    'message' => _l('all_data_synced_successfully'),
                ]);
            } else {
                echo json_encode([
                    'message' => _l('sync_actions_up_to_date'),
                ]);
            }
        }
    }

    public function action($id = '')
    {
        if ($this->input->post()) {
            $action_data = $this->input->post();
            if ($id == '') {
                if (!has_permission('actions', '', 'create')) {
                    access_denied('actions');
                }
                $id = $this->actions_model->add($action_data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('action')));
                    if ($this->set_action_pipeline_autoload($id)) {
                        redirect(admin_url('actions'));
                    } else {
						if ($action_data['rel_type'] == 'supplier'){
							redirect(admin_url('suppliers/supplier/'.$action_data['rel_id'].'?group=actions'));
						}elseif($action_data['rel_type'] == 'customer'){
							redirect(admin_url('clients/client/'.$action_data['rel_id'].'?group=actions'));
						}else{
							redirect(admin_url('actions/list_actions/' . $id));
						}
                        
                    }
                }
            } else {
                if (!has_permission('actions', '', 'edit')) {
                    access_denied('actions');
                }
                $success = $this->actions_model->update($action_data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('action')));
                }
                if ($this->set_action_pipeline_autoload($id)) {
                    redirect(admin_url('actions'));
                } else {
                    if ($action_data['rel_type'] == 'supplier'){
						redirect(admin_url('suppliers/supplier/'.$action_data['rel_id'].'?group=actions'));
					}elseif($action_data['rel_type'] == 'customer'){
						redirect(admin_url('clients/client/'.$action_data['rel_id'].'?group=actions'));
					}else{
						redirect(admin_url('actions/list_actions/' . $id));
					}
                }
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('action_lowercase'));
        } else {
            $data['action'] = $this->actions_model->get($id);

            if (!$data['action'] || !user_can_view_action($id)) {
                blank_page(_l('action_not_found'));
            }

            $data['estimate']    = $data['action'];
            $data['is_action'] = true;
            $title               = _l('edit', _l('action_lowercase'));
        }

        $this->load->model('taxes_model');
        $data['taxes'] = $this->taxes_model->get();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['statuses']      = $this->actions_model->get_statuses();
        $data['staff']         = $this->staff_model->get('', ['active' => 1]);
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();

        $data['title'] = $title;
        $this->load->view('admin/actions/action', $data);
    }

    public function get_template()
    {
        $name = $this->input->get('name');
        echo $this->load->view('admin/actions/templates/' . $name, [], true);
    }

    public function send_expiry_reminder($id)
    {
        $canView = user_can_view_action($id);
        if (!$canView) {
            access_denied('actions');
        } else {
            if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && $canView == false) {
                access_denied('actions');
            }
        }

        $success = $this->actions_model->send_expiry_reminder($id);
        if ($success) {
            set_alert('success', _l('sent_expiry_reminder_success'));
        } else {
            set_alert('danger', _l('sent_expiry_reminder_fail'));
        }
        if ($this->set_action_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('actions/list_actions/' . $id));
        }
    }

    public function clear_acceptance_info($id)
    {
        if (is_admin()) {
            $this->db->where('id', $id);
            $this->db->update('tblactions', get_acceptance_info_array(true));
        }

        redirect(admin_url('actions/list_actions/' . $id));
    }

    public function pdf($id)
    {
        if (!$id) {
            redirect(admin_url('actions'));
        }

        $canView = user_can_view_action($id);
        if (!$canView) {
            access_denied('actions');
        } else {
            if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && $canView == false) {
                access_denied('actions');
            }
        }

        $action = $this->actions_model->get($id);

        try {
            $pdf = action_pdf($action);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';

        if ($this->input->get('output_type')) {
            $type = $this->input->get('output_type');
        }

        if ($this->input->get('print')) {
            $type = 'I';
        }

        $action_number = format_action_number($id);
        $pdf->Output($action_number . '.pdf', $type);
    }

    public function get_action_data_ajax($id, $to_return = false)
    {
        if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && get_option('allow_staff_view_actions_assigned') == 0) {
            echo _l('access_denied');
            die;
        }

        $action = $this->actions_model->get($id, [], true);

        if (!$action || !user_can_view_action($id)) {
            echo _l('action_not_found');
            die;
        }

        $template_name         = 'action-send-to-customer';
        $data['template_name'] = $template_name;

        $this->db->where('slug', $template_name);
        $this->db->where('language', 'english');
        $template_result = $this->db->get('tblemailtemplates')->row();
		if ($template_result){
			 $data['template_system_name'] = $template_result->name;
			$data['template_id']          = $template_result->emailtemplateid;
		}

        $data['template_disabled'] = false;
        if (total_rows('tblemailtemplates', ['slug' => $data['template_name'], 'active' => 0]) > 0) {
            $data['template_disabled'] = true;
        }

        define('EMAIL_TEMPLATE_ACTION_ID_HELP', $action->id);

        $data['template'] = get_email_template_for_sending($template_name, $action->email);

        $action_merge_fields  = get_available_merge_fields();
        $_action_merge_fields = [];
        array_push($_action_merge_fields, [
            [
                'name' => 'Items Table',
                'key'  => '{action_items}',
            ],
        ]);
        foreach ($action_merge_fields as $key => $val) {
            foreach ($val as $type => $f) {
                if ($type == 'actions') {
                    foreach ($f as $available) {
                        foreach ($available['available'] as $av) {
                            if ($av == 'actions') {
                                array_push($_action_merge_fields, $f);

                                break;
                            }
                        }

                        break;
                    }
                } elseif ($type == 'other') {
                    array_push($_action_merge_fields, $f);
                }
            }
        }
        $data['action_statuses']     = $this->actions_model->get_statuses();
        $data['members']               = $this->staff_model->get('', ['active' => 1]);
        $data['action_merge_fields'] = $_action_merge_fields;
        $data['action']              = $action;
        $data['totalNotes']            = total_rows('tblnotes', ['rel_id' => $id, 'rel_type' => 'action']);
        if ($to_return == false) {
            $this->load->view('admin/actions/actions_preview_template', $data);
        } else {
            return $this->load->view('admin/actions/actions_preview_template', $data, true);
        }
    }

    public function add_note($rel_id)
    {
        if ($this->input->post() && user_can_view_action($rel_id)) {
            $this->misc_model->add_note($this->input->post(), 'action', $rel_id);
            echo $rel_id;
        }
    }

    public function get_notes($id)
    {
        if (user_can_view_action($id)) {
            $data['notes'] = $this->misc_model->get_notes($id, 'action');
            $this->load->view('admin/includes/sales_notes_template', $data);
        }
    }

    public function convert_to_estimate($id)
    {
        if (!has_permission('estimates', '', 'create')) {
            access_denied('estimates');
        }
        if ($this->input->post()) {
            $this->load->model('estimates_model');
            $estimate_id = $this->estimates_model->add($this->input->post());
            if ($estimate_id) {
                set_alert('success', _l('action_converted_to_estimate_success'));
                $this->db->where('id', $id);
                $this->db->update('tblactions', [
                    'estimate_id' => $estimate_id,
                    'status'      => 3,
                ]);
                logActivity('Action Converted to Estimate [EstimateID: ' . $estimate_id . ', ActionID: ' . $id . ']');

                do_action('action_converted_to_estimate', ['action_id' => $id, 'estimate_id' => $estimate_id]);

                redirect(admin_url('estimates/estimate/' . $estimate_id));
            } else {
                set_alert('danger', _l('action_converted_to_estimate_fail'));
            }
            if ($this->set_action_pipeline_autoload($id)) {
                redirect(admin_url('actions'));
            } else {
                redirect(admin_url('actions/list_actions/' . $id));
            }
        }
    }

    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('invoices');
        }
        if ($this->input->post()) {
            $this->load->model('invoices_model');
            $invoice_id = $this->invoices_model->add($this->input->post());
            if ($invoice_id) {
                set_alert('success', _l('action_converted_to_invoice_success'));
                $this->db->where('id', $id);
                $this->db->update('tblactions', [
                    'invoice_id' => $invoice_id,
                    'status'     => 3,
                ]);
                logActivity('Action Converted to Invoice [InvoiceID: ' . $invoice_id . ', ActionID: ' . $id . ']');
                do_action('action_converted_to_invoice', ['action_id' => $id, 'invoice_id' => $invoice_id]);
                redirect(admin_url('invoices/invoice/' . $invoice_id));
            } else {
                set_alert('danger', _l('action_converted_to_invoice_fail'));
            }
            if ($this->set_action_pipeline_autoload($id)) {
                redirect(admin_url('actions'));
            } else {
                redirect(admin_url('actions/list_actions/' . $id));
            }
        }
    }

    public function get_invoice_convert_data($id)
    {
        $this->load->model('payment_modes_model');
        $data['payment_modes'] = $this->payment_modes_model->get('', [
            'expenses_only !=' => 1,
        ]);
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']          = $this->staff_model->get('', ['active' => 1]);
        $data['action']       = $this->actions_model->get($id);
        $data['billable_tasks'] = [];
        $data['add_items']      = $this->_parse_items($data['action']);

        if ($data['action']->rel_type == 'lead') {
            $this->db->where('leadid', $data['action']->rel_id);
            $data['customer_id'] = $this->db->get('tblclients')->row()->userid;
        } elseif ($data['action']->rel_type == 'supplier') {
			$this->db->where('leadid', $data['action']->rel_id);
            $data['supplier_id'] = $this->db->get('tblsuppliers')->row()->userid;
		}else {
            $data['customer_id'] = $data['action']->rel_id;
        }
        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'action',
            'rel_id'     => $id,
        ];
        $this->load->view('admin/actions/invoice_convert_template', $data);
    }

    public function get_estimate_convert_data($id)
    {
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']     = $this->staff_model->get('', ['active' => 1]);
        $data['action']  = $this->actions_model->get($id);
        $data['add_items'] = $this->_parse_items($data['action']);

        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();
        if ($data['action']->rel_type == 'lead') {
            $this->db->where('leadid', $data['action']->rel_id);
            $data['customer_id'] = $this->db->get('tblclients')->row()->userid;
        } else {
            $data['customer_id'] = $data['action']->rel_id;
        }

        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'action',
            'rel_id'     => $id,
        ];

        $this->load->view('admin/actions/estimate_convert_template', $data);
    }

    private function _parse_items($action)
    {
        $items = [];
        foreach ($action->items as $item) {
            $taxnames = [];
            $taxes    = get_action_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                array_push($taxnames, $tax['taxname']);
            }
            $item['taxname']        = $taxnames;
            $item['parent_item_id'] = $item['id'];
            $item['id']             = 0;
            $items[]                = $item;
        }

        return $items;
    }

    /* Send action to email */
    public function send_to_email($id)
    {
        $canView = user_can_view_action($id);
        if (!$canView) {
            access_denied('actions');
        } else {
            if (!has_permission('actions', '', 'view') && !has_permission('actions', '', 'view_own') && $canView == false) {
                access_denied('actions');
            }
        }

        if ($this->input->post()) {
            try {
                $success = $this->actions_model->send_action_to_email($id, 'action-send-to-customer', $this->input->post('attach_pdf'), $this->input->post('cc'));
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo $message;
                if (strpos($message, 'Unable to get the size of the image') !== false) {
                    show_pdf_unable_to_get_image_size_error();
                }
                die;
            }

            if ($success) {
                set_alert('success', _l('action_sent_to_email_success'));
            } else {
                set_alert('danger', _l('action_sent_to_email_fail'));
            }

            if ($this->set_action_pipeline_autoload($id)) {
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(admin_url('actions/list_actions/' . $id));
            }
        }
    }

    public function copy($id)
    {
        if (!has_permission('actions', '', 'create')) {
            access_denied('actions');
        }
        $new_id = $this->actions_model->copy($id);
        if ($new_id) {
            set_alert('success', _l('action_copy_success'));
            $this->set_action_pipeline_autoload($new_id);
            redirect(admin_url('actions/action/' . $new_id));
        } else {
            set_alert('success', _l('action_copy_fail'));
        }
        if ($this->set_action_pipeline_autoload($id)) {
            redirect(admin_url('actions'));
        } else {
            redirect(admin_url('actions/list_actions/' . $id));
        }
    }

    public function mark_action_status($status, $id)
    {
        if (!has_permission('actions', '', 'edit')) {
            access_denied('actions');
        }
        $success = $this->actions_model->mark_action_status($status, $id);
        if ($success) {
            set_alert('success', _l('action_status_changed_success'));
        } else {
            set_alert('danger', _l('action_status_changed_fail'));
        }
        if ($this->set_action_pipeline_autoload($id)) {
            redirect(admin_url('actions'));
        } else {
            redirect(admin_url('actions/list_actions/' . $id));
        }
    }

    public function delete($id)
    {
        if (!has_permission('actions', '', 'delete')) {
            access_denied('actions');
        }
        $response = $this->actions_model->delete($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('action')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('action_lowercase')));
        }
        redirect(admin_url('actions'));
    }

    public function get_relation_data_values($rel_id, $rel_type)
    {
        echo json_encode($this->actions_model->get_relation_data_values($rel_id, $rel_type));
    }

    public function add_action_comment()
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->actions_model->add_comment($this->input->post()),
            ]);
        }
    }

    public function edit_comment($id)
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->actions_model->edit_comment($this->input->post(), $id),
                'message' => _l('comment_updated_successfully'),
            ]);
        }
    }

    public function get_action_comments($id)
    {
        $data['comments'] = $this->actions_model->get_comments($id);
        $this->load->view('admin/actions/comments_template', $data);
    }

    public function remove_comment($id)
    {
        $this->db->where('id', $id);
        $comment = $this->db->get('tblactioncomments')->row();
        if ($comment) {
            if ($comment->staffid != get_staff_user_id() && !is_admin()) {
                echo json_encode([
                    'success' => false,
                ]);
                die;
            }
            echo json_encode([
                'success' => $this->actions_model->remove_comment($id),
            ]);
        } else {
            echo json_encode([
                'success' => false,
            ]);
        }
    }

    public function save_action_data()
    {
        if (!has_permission('actions', '', 'edit') && !has_permission('actions', '', 'create')) {
            header('HTTP/1.0 400 Bad error');
            echo json_encode([
                'success' => false,
                'message' => _l('access_denied'),
            ]);
            die;
        }
        $success = false;
        $message = '';

        $this->db->where('id', $this->input->post('action_id'));
        $this->db->update('tblactions', [
            'content' => $this->input->post('content', false),
        ]);

        $success = $this->db->affected_rows() > 0;
        $message = _l('updated_successfully', _l('action'));

        echo json_encode([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Pipeline
    public function pipeline($set = 0, $manual = false)
    {
        if ($set == 1) {
            $set = 'true';
        } else {
            $set = 'false';
        }
        $this->session->set_userdata([
            'actions_pipeline' => $set,
        ]);
        if ($manual == false) {
            redirect(admin_url('actions'));
        }
    }

    public function pipeline_open($id)
    {
        if (has_permission('actions', '', 'view') || has_permission('actions', '', 'view_own') || get_option('allow_staff_view_actions_assigned') == 1) {
            $data['action']      = $this->get_action_data_ajax($id, true);
            $data['action_data'] = $this->actions_model->get($id);
            $this->load->view('admin/actions/pipeline/action', $data);
        }
    }

    public function update_pipeline()
    {
        if (has_permission('actions', '', 'edit')) {
            $this->actions_model->update_pipeline($this->input->post());
        }
    }

    public function get_pipeline()
    {
        if (has_permission('actions', '', 'view') || has_permission('actions', '', 'view_own') || get_option('allow_staff_view_actions_assigned') == 1) {
            $data['statuses'] = $this->actions_model->get_statuses();
            $this->load->view('admin/actions/pipeline/pipeline', $data);
        }
    }

    public function pipeline_load_more()
    {
        $status = $this->input->get('status');
        $page   = $this->input->get('page');

        $actions = $this->actions_model->do_kanban_query($status, $this->input->get('search'), $page, [
            'sort_by' => $this->input->get('sort_by'),
            'sort'    => $this->input->get('sort'),
        ]);

        foreach ($actions as $action) {
            $this->load->view('admin/actions/pipeline/_kanban_card', [
                'action' => $action,
                'status'   => $status,
            ]);
        }
    }

    public function set_action_pipeline_autoload($id)
    {
        if ($id == '') {
            return false;
        }

        if ($this->session->has_userdata('actions_pipeline') && $this->session->userdata('actions_pipeline') == 'true') {
            $this->session->set_flashdata('actionid', $id);

            return true;
        }

        return false;
    }

    public function get_due_date()
    {
        if ($this->input->post()) {
            $date    = $this->input->post('date');
            $duedate = '';
            if (get_option('action_due_after') != 0) {
                $date    = to_sql_date($date);
                $d       = date('Y-m-d', strtotime('+' . get_option('action_due_after') . ' DAY', strtotime($date)));
                $duedate = _d($d);
                echo $duedate;
            }
        }
    }
}
