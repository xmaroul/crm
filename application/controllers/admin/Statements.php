<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Statements extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('statements_model');
        $this->load->model('currencies_model');
        $this->load->helper('date');
    }

    public function index()
    {
        close_setup_menu();
        $data['statuses'] = $this->statements_model->get_statement_statuses();
        $data['title']    = _l('statements');
        $this->load->view('admin/statements/manage', $data);
    }

    public function table($clientid = '')
    {
        $this->app->get_table_data('statements', [
            'clientid' => $clientid,
        ]);
    }

    public function staff_statements()
    {
        $this->app->get_table_data('staff_statements');
    }

    public function expenses($id)
    {
        $this->load->model('expenses_model');
        $this->app->get_table_data('statement_expenses', [
            'statement_id' => $id,
        ]);
    }

    public function add_expense()
    {
        if ($this->input->post()) {
            $this->load->model('expenses_model');
            $id = $this->expenses_model->add($this->input->post());
            if ($id) {
                set_alert('success', _l('added_successfully', _l('expense')));
                echo json_encode([
                    'url'       => admin_url('statements/view/' . $this->input->post('statement_id') . '/?group=statement_expenses'),
                    'expenseid' => $id,
                ]);
                die;
            }
            echo json_encode([
                'url' => admin_url('statements/view/' . $this->input->post('statement_id') . '/?group=statement_expenses'),
            ]);
            die;
        }
    }

    public function statement($id = '')
    {
        if (!has_permission('statements', '', 'edit') && !has_permission('statements', '', 'create')) {
            access_denied('Projects');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            $data['description'] = $this->input->post('description', false);
            if ($id == '') {
                if (!has_permission('statements', '', 'create')) {
                    access_denied('Projects');
                }
                $id = $this->statements_model->add($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('statement')));
                    redirect(admin_url('statements/view/' . $id));
                }
            } else {
                if (!has_permission('statements', '', 'edit')) {
                    access_denied('Projects');
                }
                $success = $this->statements_model->update($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('statement')));
                }
                redirect(admin_url('statements/view/' . $id));
            }
        }
        if ($id == '') {
            $title                            = _l('add_new', _l('statement_lowercase'));
            $data['auto_select_billing_type'] = $this->statements_model->get_most_used_billing_type();
        } else {
            $data['statement']                               = $this->statements_model->get($id);
            $data['statement']->settings->available_features = unserialize($data['statement']->settings->available_features);

            $data['statement_members'] = $this->statements_model->get_statement_members($id);
            $title                   = _l('edit', _l('statement_lowercase'));
        }

        if ($this->input->get('customer_id')) {
            $data['customer_id'] = $this->input->get('customer_id');
        }

        $data['last_statement_settings'] = $this->statements_model->get_last_statement_settings();

        if (count($data['last_statement_settings'])) {
            $key                                          = array_search('available_features', array_column($data['last_statement_settings'], 'name'));
            $data['last_statement_settings'][$key]['value'] = unserialize($data['last_statement_settings'][$key]['value']);
        }

        $data['settings'] = $this->statements_model->get_settings();
        $data['statuses'] = $this->statements_model->get_statement_statuses();
        $data['staff']    = $this->staff_model->get('', ['active' => 1]);

        $data['title'] = $title;
        $this->load->view('admin/statements/statement', $data);
    }

    public function gantt()
    {
        $data['title'] = _l('statement_gant');

        $data['statements_assets'] = true;

        $selected_statuses = [];
        $selectedMember = null;
        $data['statuses'] = $this->statements_model->get_statement_statuses();

        $appliedStatuses = $this->input->get('status');
        $appliedMember = $this->input->get('member');

        $allStatusesIds = [];
        foreach ($data['statuses'] as $status) {
            if (!isset($status['filter_default'])
                || (isset($status['filter_default']) && $status['filter_default'])
                && !$appliedStatuses) {
                $selected_statuses[] = $status['id'];
            } elseif ($appliedStatuses) {
                if (in_array($status['id'], $appliedStatuses)) {
                    $selected_statuses[] = $status['id'];
                }
            } else {
                // All statuses
                $allStatusesIds[] = $status['id'];
            }
        }

        if (count($selected_statuses) == 0) {
            $selected_statuses = $allStatusesIds;
        }

        $data['selected_statuses'] = $selected_statuses;

        if(has_permission('statements','','view')){
            $selectedMember = $appliedMember;
            $data['selectedMember'] = $selectedMember;
            $data['statement_members'] = $this->statements_model->get_distinct_statements_members();
        }

        $data['gantt_data'] = $this->statements_model->get_all_statements_gantt_data([
            'status'  => $selected_statuses,
            'member' => $selectedMember,
        ]);

        $this->load->view('admin/statements/gantt', $data);
    }

    public function view($id)
    {
        if ($this->statements_model->is_member($id) || has_permission('statements', '', 'view')) {
            close_setup_menu();
            $statement = $this->statements_model->get($id);

            if (!$statement) {
                blank_page(_l('statement_not_found'));
            }

            $statement->settings->available_features = unserialize($statement->settings->available_features);
            $data['statuses']                      = $this->statements_model->get_statement_statuses();

            if (!$this->input->get('group')) {
                $view = 'statement_overview';
            } else {
                $view = $this->input->get('group');
            }

            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get('', [], true);

            $data['statement']  = $statement;
            $data['currency'] = $this->statements_model->get_currency($id);

            $data['statement_total_logged_time'] = $this->statements_model->total_logged_time($id);

            $data['staff']     = $this->staff_model->get('', ['active' => 1]);
            $percent           = $this->statements_model->calc_progress($id);
            $data['bodyclass'] = '';
            if ($view == 'statement_overview') {
                $data['members'] = $this->statements_model->get_statement_members($id);
                $i               = 0;
                foreach ($data['members'] as $member) {
                    $data['members'][$i]['total_logged_time'] = 0;
                    $member_timesheets                        = $this->tasks_model->get_unique_member_logged_task_ids($member['staff_id'], ' AND task_id IN (SELECT id FROM tblstafftasks WHERE rel_type="statement" AND rel_id="' . $id . '")');

                    foreach ($member_timesheets as $member_task) {
                        $data['members'][$i]['total_logged_time'] += $this->tasks_model->calc_task_total_time($member_task->task_id, ' AND staff_id=' . $member['staff_id']);
                    }

                    $i++;
                }

                $data['statement_total_days']        = round((human_to_unix($data['statement']->deadline . ' 00:00') - human_to_unix($data['statement']->start_date . ' 00:00')) / 3600 / 24);
                $data['statement_days_left']         = $data['statement_total_days'];
                $data['statement_time_left_percent'] = 100;
                if ($data['statement']->deadline) {
                    if (human_to_unix($data['statement']->start_date . ' 00:00') < time() && human_to_unix($data['statement']->deadline . ' 00:00') > time()) {
                        $data['statement_days_left']         = round((human_to_unix($data['statement']->deadline . ' 00:00') - time()) / 3600 / 24);
                        $data['statement_time_left_percent'] = $data['statement_days_left'] / $data['statement_total_days'] * 100;
                        $data['statement_time_left_percent'] = round($data['statement_time_left_percent'], 2);
                    }
                    if (human_to_unix($data['statement']->deadline . ' 00:00') < time()) {
                        $data['statement_days_left']         = 0;
                        $data['statement_time_left_percent'] = 0;
                    }
                }

                $__total_where_tasks = 'rel_type = "statement" AND rel_id=' . $id;
                if (!has_permission('tasks', '', 'view')) {
                    $__total_where_tasks .= ' AND tblstafftasks.id IN (SELECT taskid FROM tblstafftaskassignees WHERE staffid = ' . get_staff_user_id() . ')';

                    if (get_option('show_all_tasks_for_statement_member') == 1) {
                        $__total_where_tasks .= ' AND (rel_type="statement" AND rel_id IN (SELECT statement_id FROM tblstatementmembers WHERE staff_id=' . get_staff_user_id() . '))';
                    }
                }
                $where = ($__total_where_tasks == '' ? '' : $__total_where_tasks . ' AND ') . 'status != 5';

                $data['tasks_not_completed'] = total_rows('tblstafftasks', $where);
                $total_tasks                 = total_rows('tblstafftasks', $__total_where_tasks);
                $data['total_tasks']         = $total_tasks;

                $where = ($__total_where_tasks == '' ? '' : $__total_where_tasks . ' AND ') . 'status = 5 AND rel_type="statement" AND rel_id="' . $id . '"';

                $data['tasks_completed'] = total_rows('tblstafftasks', $where);

                $data['tasks_not_completed_progress'] = ($total_tasks > 0 ? number_format(($data['tasks_completed'] * 100) / $total_tasks, 2) : 0);
                $data['tasks_not_completed_progress'] = round($data['tasks_not_completed_progress'], 2);

                @$percent_circle        = $percent / 100;
                $data['percent_circle'] = $percent_circle;


                $data['statement_overview_chart'] = $this->statements_model->get_statement_overview_weekly_chart_data($id, ($this->input->get('overview_chart') ? $this->input->get('overview_chart'):'this_week'));
            } elseif ($view == 'statement_invoices') {
                $this->load->model('invoices_model');

                $data['invoiceid']   = '';
                $data['status']      = '';
                $data['custom_view'] = '';

                $data['invoices_years']       = $this->invoices_model->get_invoices_years();
                $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();
                $data['invoices_statuses']    = $this->invoices_model->get_statuses();
            } elseif ($view == 'statement_gantt') {
                $gantt_type         = (!$this->input->get('gantt_type') ? 'milestones' : $this->input->get('gantt_type'));
                $taskStatus         = (!$this->input->get('gantt_task_status') ? null : $this->input->get('gantt_task_status'));
                $data['gantt_data'] = $this->statements_model->get_gantt_data($id, $gantt_type, $taskStatus);
            } elseif ($view == 'statement_milestones') {
                $data['bodyclass'] .= 'statement-milestones ';
                $data['milestones_exclude_completed_tasks'] = $this->input->get('exclude_completed') && $this->input->get('exclude_completed') == 'yes' || !$this->input->get('exclude_completed');

                $data['total_milestones'] = total_rows('tblmilestones', ['statement_id' => $id]);
                $data['milestones_found'] = $data['total_milestones'] > 0 || (!$data['total_milestones'] && total_rows('tblstafftasks', ['rel_id' => $id, 'rel_type' => 'statement', 'milestone' => 0]) > 0);
            } elseif ($view == 'statement_files') {
                $data['files'] = $this->statements_model->get_files($id);
            } elseif ($view == 'statement_expenses') {
                $this->load->model('taxes_model');
                $this->load->model('expenses_model');
                $data['taxes']              = $this->taxes_model->get();
                $data['expense_categories'] = $this->expenses_model->get_category();
                $data['currencies']         = $this->currencies_model->get();
            } elseif ($view == 'statement_activity') {
                $data['activity'] = $this->statements_model->get_activity($id);
            } elseif ($view == 'statement_notes') {
                $data['staff_notes'] = $this->statements_model->get_staff_notes($id);
            } elseif ($view == 'statement_estimates') {
                $this->load->model('estimates_model');
                $data['estimates_years']       = $this->estimates_model->get_estimates_years();
                $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();
                $data['estimate_statuses']     = $this->estimates_model->get_statuses();
                $data['estimateid']            = '';
                $data['switch_pipeline']       = '';
            } elseif ($view == 'statement_tickets') {
                $data['chosen_ticket_status'] = '';
                $this->load->model('tickets_model');
                $data['ticket_assignees'] = $this->tickets_model->get_tickets_assignes_disctinct();

                $this->load->model('departments_model');
                $data['staff_deparments_ids']          = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                $data['default_tickets_list_statuses'] = do_action('default_tickets_list_statuses', [1, 2, 4]);
            } elseif ($view == 'statement_timesheets') {
                // Tasks are used in the timesheet dropdown
                // Completed tasks are excluded from this list because you can't add timesheet on completed task.
                $data['tasks']                = $this->statements_model->get_tasks($id, 'status != 5 AND billed=0');
                $data['timesheets_staff_ids'] = $this->statements_model->get_distinct_tasks_timesheets_staff($id);
            }

            // Discussions
            if ($this->input->get('discussion_id')) {
                $data['discussion_user_profile_image_url'] = staff_profile_image_url(get_staff_user_id());
                $data['discussion']                        = $this->statements_model->get_discussion($this->input->get('discussion_id'), $id);
                $data['current_user_is_admin']             = is_admin();
            }

            $data['percent'] = $percent;

            $data['statements_assets']       = true;
            $data['circle_progress_asset'] = true;

            $other_statements       = [];
            $other_statements_where = 'id !=' . $id . ' and status = 2';

            if (!has_permission('statements', '', 'view')) {
                $other_statements_where .= ' AND tblstatements.id IN (SELECT statement_id FROM tblstatementmembers WHERE staff_id=' . get_staff_user_id() . ')';
            }

            $data['other_statements'] = $this->statements_model->get('', $other_statements_where);
            $data['title']          = $data['statement']->name;
            $data['bodyclass'] .= 'statement invoices-total-manual estimates-total-manual';
            $data['statement_status'] = get_statement_status_by_id($statement->status);

            $hook_data = do_action('statement_group_access_admin', [
                'id'       => $statement->id,
                'view'     => $view,
                'all_data' => $data,
            ]);

            $data = $hook_data['all_data'];
            $view = $hook_data['view'];

            // Unable to load the requested file: admin/statements/statement_tasks#.php - FIX
            if (strpos($view, '#') !== false) {
                $view = str_replace('#', '', $view);
            }

            $view               = trim($view);
            $data['view']       = $view;
            $data['group_view'] = $this->load->view('admin/statements/' . $view, $data, true);

            $this->load->view('admin/statements/view', $data);
        } else {
            access_denied('Project View');
        }
    }

    public function mark_as()
    {
        $success = false;
        $message = '';
        if ($this->input->is_ajax_request()) {
            if (has_permission('statements', '', 'create') || has_permission('statements', '', 'edit')) {
                $status = get_statement_status_by_id($this->input->post('status_id'));

                $message = _l('statement_marked_as_failed', $status['name']);
                $success = $this->statements_model->mark_as($this->input->post());

                if ($success) {
                    $message = _l('statement_marked_as_success', $status['name']);
                }
            }
        }
        echo json_encode([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function file($id, $statement_id)
    {
        $data['discussion_user_profile_image_url'] = staff_profile_image_url(get_staff_user_id());
        $data['current_user_is_admin']             = is_admin();

        $data['file'] = $this->statements_model->get_file($id, $statement_id);
        if (!$data['file']) {
            header('HTTP/1.0 404 Not Found');
            die;
        }
        $this->load->view('admin/statements/_file', $data);
    }

    public function update_file_data()
    {
        if ($this->input->post()) {
            $this->statements_model->update_file_data($this->input->post());
        }
    }

    public function add_external_file()
    {
        if ($this->input->post()) {
            $data                        = [];
            $data['statement_id']          = $this->input->post('statement_id');
            $data['files']               = $this->input->post('files');
            $data['external']            = $this->input->post('external');
            $data['visible_to_customer'] = ($this->input->post('visible_to_customer') == 'true' ? 1 : 0);
            $data['staffid']             = get_staff_user_id();
            $this->statements_model->add_external_file($data);
        }
    }

    public function download_all_files($id)
    {
        if ($this->statements_model->is_member($id) || has_permission('statements', '', 'view')) {
            $files = $this->statements_model->get_files($id);
            if (count($files) == 0) {
                set_alert('warning', _l('no_files_found'));
                redirect(admin_url('statements/view/' . $id . '?group=statement_files'));
            }
            $path = get_upload_path_by_type('statement') . $id;
            $this->load->library('zip');
            foreach ($files as $file) {
                $this->zip->read_file($path . '/' . $file['file_name']);
            }
            $this->zip->download(slug_it(get_statement_name_by_id($id)) . '-files.zip');
            $this->zip->clear_data();
        }
    }

    public function export_statement_data($id)
    {
        if (has_permission('statements', '', 'create')) {
            $statement = $this->statements_model->get($id);
            $this->load->library('pdf');
            $members                = $this->statements_model->get_statement_members($id);
            $statement->currency_data = $this->statements_model->get_currency($id);

            // Add <br /> tag and wrap over div element every image to prevent overlaping over text
            $statement->description = preg_replace('/(<img[^>]+>(?:<\/img>)?)/i', '<br><br><div>$1</div><br><br>', $statement->description);

            $data['statement']    = $statement;
            $data['milestones'] = $this->statements_model->get_milestones($id);
            $data['timesheets'] = $this->statements_model->get_timesheets($id);

            $data['tasks']             = $this->statements_model->get_tasks($id, [], false);
            $data['total_logged_time'] = seconds_to_time_format($this->statements_model->total_logged_time($statement->id));
            if ($statement->deadline) {
                $data['total_days'] = round((human_to_unix($statement->deadline . ' 00:00') - human_to_unix($statement->start_date . ' 00:00')) / 3600 / 24);
            } else {
                $data['total_days'] = '/';
            }
            $data['total_members'] = count($members);
            $data['total_tickets'] = total_rows('tbltickets', [
                'statement_id' => $id,
            ]);
            $data['total_invoices'] = total_rows('tblinvoices', [
                'statement_id' => $id,
            ]);

            $this->load->model('invoices_model');

            $data['invoices_total_data'] = $this->invoices_model->get_invoices_total([
                'currency'   => $statement->currency_data->id,
                'statement_id' => $statement->id,
            ]);

            $data['total_milestones']     = count($data['milestones']);
            $data['total_files_attached'] = total_rows('tblstatementfiles', [
                'statement_id' => $statement->id,
            ]);
            $data['total_discussion'] = total_rows('tblstatementdiscussions', [
                'statement_id' => $statement->id,
            ]);
            $data['members'] = $members;
            $this->load->view('admin/statements/export_data_pdf', $data);
        }
    }

    public function update_task_milestone()
    {
        if ($this->input->post()) {
            $this->statements_model->update_task_milestone($this->input->post());
        }
    }

    public function update_milestones_order()
    {
        if ($post_data = $this->input->post()) {
            $this->statements_model->update_milestones_order($post_data);
        }
    }

    public function pin_action($statement_id)
    {
        $this->statements_model->pin_action($statement_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function add_edit_members($statement_id)
    {
        if (has_permission('statements', '', 'edit') || has_permission('statements', '', 'create')) {
            $this->statements_model->add_edit_members($this->input->post(), $statement_id);
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function discussions($statement_id)
    {
        if ($this->statements_model->is_member($statement_id) || has_permission('statements', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('statement_discussions', [
                    'statement_id' => $statement_id,
                ]);
            }
        }
    }

    public function discussion($id = '')
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            if (!$this->input->post('id')) {
                $id = $this->statements_model->add_discussion($this->input->post());
                if ($id) {
                    $success = true;
                    $message = _l('added_successfully', _l('statement_discussion'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->statements_model->edit_discussion($data, $id);
                if ($success) {
                    $message = _l('updated_successfully', _l('statement_discussion'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
            die;
        }
    }

    public function get_discussion_comments($id, $type)
    {
        echo json_encode($this->statements_model->get_discussion_comments($id, $type));
    }

    public function add_discussion_comment($discussion_id, $type)
    {
        echo json_encode($this->statements_model->add_discussion_comment($this->input->post(), $discussion_id, $type));
    }

    public function update_discussion_comment()
    {
        echo json_encode($this->statements_model->update_discussion_comment($this->input->post()));
    }

    public function delete_discussion_comment($id)
    {
        echo json_encode($this->statements_model->delete_discussion_comment($id));
    }

    public function delete_discussion($id)
    {
        $success = false;
        if (has_permission('statements', '', 'delete')) {
            $success = $this->statements_model->delete_discussion($id);
        }
        $alert_type = 'warning';
        $message    = _l('statement_discussion_failed_to_delete');
        if ($success) {
            $alert_type = 'success';
            $message    = _l('statement_discussion_deleted');
        }
        echo json_encode([
            'alert_type' => $alert_type,
            'message'    => $message,
        ]);
    }

    public function change_milestone_color()
    {
        if ($this->input->post()) {
            $this->statements_model->update_milestone_color($this->input->post());
        }
    }

    public function upload_file($statement_id)
    {
        handle_statement_file_uploads($statement_id);
    }

    public function change_file_visibility($id, $visible)
    {
        if ($this->input->is_ajax_request()) {
            $this->statements_model->change_file_visibility($id, $visible);
        }
    }

    public function change_activity_visibility($id, $visible)
    {
        if (has_permission('statements', '', 'create')) {
            if ($this->input->is_ajax_request()) {
                $this->statements_model->change_activity_visibility($id, $visible);
            }
        }
    }

    public function remove_file($statement_id, $id)
    {
        $this->statements_model->remove_file($id);
        redirect(admin_url('statements/view/' . $statement_id . '?group=statement_files'));
    }

    public function milestones_kanban()
    {
        $data['milestones_exclude_completed_tasks'] = $this->input->get('exclude_completed_tasks') && $this->input->get('exclude_completed_tasks') == 'yes';

        $data['statement_id'] = $this->input->get('statement_id');
        $data['milestones'] = [];

        $data['milestones'][] = [
          'name'              => _l('milestones_uncategorized'),
          'id'                => 0,
          'total_logged_time' => $this->statements_model->calc_milestone_logged_time($data['statement_id'], 0),
          'color'             => null,
          ];

        $_milestones = $this->statements_model->get_milestones($data['statement_id']);

        foreach ($_milestones as $m) {
            $data['milestones'][] = $m;
        }

        echo $this->load->view('admin/statements/milestones_kan_ban', $data, true);
    }

    public function milestones_kanban_load_more()
    {
        $milestones_exclude_completed_tasks = $this->input->get('exclude_completed_tasks') && $this->input->get('exclude_completed_tasks') == 'yes';

        $status     = $this->input->get('status');
        $page       = $this->input->get('page');
        $statement_id = $this->input->get('statement_id');
        $where      = [];
        if ($milestones_exclude_completed_tasks) {
            $where['status !='] = 5;
        }
        $tasks = $this->statements_model->do_milestones_kanban_query($status, $statement_id, $page, $where);
        foreach ($tasks as $task) {
            $this->load->view('admin/statements/_milestone_kanban_card', ['task' => $task, 'milestone' => $status]);
        }
    }

    public function milestones($statement_id)
    {
        if ($this->statements_model->is_member($statement_id) || has_permission('statements', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('milestones', [
                    'statement_id' => $statement_id,
                ]);
            }
        }
    }

    public function milestone($id = '')
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            if (!$this->input->post('id')) {
                $id = $this->statements_model->add_milestone($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('statement_milestone')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->statements_model->update_milestone($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('statement_milestone')));
                }
            }
        }

        redirect(admin_url('statements/view/' . $this->input->post('statement_id') . '?group=statement_milestones'));
    }

    public function delete_milestone($statement_id, $id)
    {
        if (has_permission('statements', '', 'delete')) {
            if ($this->statements_model->delete_milestone($id)) {
                set_alert('deleted', 'statement_milestone');
            }
        }
        redirect(admin_url('statements/view/' . $statement_id . '?group=statement_milestones'));
    }

    public function bulk_action_files()
    {
        do_action('before_do_bulk_action_for_statement_files');
        $total_deleted       = 0;
        $hasPermissionDelete = has_permission('statements', '', 'delete');
        // bulk action for statements currently only have delete button
        if ($this->input->post()) {
            $fVisibility = $this->input->post('visible_to_customer') == 'true' ? 1 : 0;
            $ids         = $this->input->post('ids');
            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($hasPermissionDelete && $this->input->post('mass_delete') && $this->statements_model->remove_file($id)) {
                        $total_deleted++;
                    } else {
                        $this->statements_model->change_file_visibility($id, $fVisibility);
                    }
                }
            }
        }
        if ($this->input->post('mass_delete')) {
            set_alert('success', _l('total_files_deleted', $total_deleted));
        }
    }

    public function timesheets($statement_id)
    {
        if ($this->statements_model->is_member($statement_id) || has_permission('statements', '', 'view')) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('timesheets', [
                    'statement_id' => $statement_id,
                ]);
            }
        }
    }

    public function timesheet()
    {
        if ($this->input->post()) {
            $message = '';
            $success = false;
            $success = $this->tasks_model->timesheet($this->input->post());
            if ($success === true) {
                $message = _l('added_successfully', _l('statement_timesheet'));
            } elseif (is_array($success) && isset($success['end_time_smaller'])) {
                $message = _l('failed_to_add_statement_timesheet_end_time_smaller');
            } else {
                $message = _l('statement_timesheet_not_updated');
            }
            echo json_encode([
                'success' => $success,
                'message' => $message,
            ]);
            die;
        }
    }

    public function timesheet_task_assignees($task_id, $statement_id, $staff_id = 'undefined')
    {
        $assignees             = $this->tasks_model->get_task_assignees($task_id);
        $data                  = '';
        $has_permission_edit   = has_permission('statements', '', 'edit');
        $has_permission_create = has_permission('statements', '', 'edit');
        // The second condition if staff member edit their own timesheet
        if ($staff_id == 'undefined' || $staff_id != 'undefined' && (!$has_permission_edit || !$has_permission_create)) {
            $staff_id     = get_staff_user_id();
            $current_user = true;
        }
        foreach ($assignees as $staff) {
            $selected = '';
            // maybe is admin and not statement member
            if ($staff['assigneeid'] == $staff_id && $this->statements_model->is_member($statement_id, $staff_id)) {
                $selected = ' selected';
            }
            if ((!$has_permission_edit || !$has_permission_create) && isset($current_user)) {
                if ($staff['assigneeid'] != $staff_id) {
                    continue;
                }
            }
            $data .= '<option value="' . $staff['assigneeid'] . '"' . $selected . '>' . get_staff_full_name($staff['assigneeid']) . '</option>';
        }
        echo $data;
    }

    public function remove_team_member($statement_id, $staff_id)
    {
        if (has_permission('statements', '', 'edit') || has_permission('statements', '', 'create')) {
            if ($this->statements_model->remove_team_member($statement_id, $staff_id)) {
                set_alert('success', _l('statement_member_removed'));
            }
        }
        redirect(admin_url('statements/view/' . $statement_id));
    }

    public function save_note($statement_id)
    {
        if ($this->input->post()) {
            $success = $this->statements_model->save_note($this->input->post(null, false), $statement_id);
            if ($success) {
                set_alert('success', _l('updated_successfully', _l('statement_note')));
            }
            redirect(admin_url('statements/view/' . $statement_id . '?group=statement_notes'));
        }
    }

    public function delete($statement_id)
    {
        if (has_permission('statements', '', 'delete')) {
            $statement = $this->statements_model->get($statement_id);
            $success = $this->statements_model->delete($statement_id);
            if ($success) {
                set_alert('success', _l('deleted', _l('statement')));
                if (strpos($_SERVER['HTTP_REFERER'], 'clients/') !== false) {
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    redirect(admin_url('statements'));
                }
            } else {
                set_alert('warning', _l('problem_deleting', _l('statement_lowercase')));
                redirect(admin_url('statements/view/' . $statement_id));
            }
        }
    }

    public function copy($statement_id)
    {
        if (has_permission('statements', '', 'create')) {
            $id = $this->statements_model->copy($statement_id, $this->input->post());
            if ($id) {
                set_alert('success', _l('statement_copied_successfully'));
                redirect(admin_url('statements/view/' . $id));
            } else {
                set_alert('danger', _l('failed_to_copy_statement'));
                redirect(admin_url('statements/view/' . $statement_id));
            }
        }
    }

    public function mass_stop_timers($statement_id, $billable = 'false')
    {
        if (has_permission('invoices', '', 'create')) {
            $where = [
                'billed'       => 0,
                'startdate <=' => date('Y-m-d'),
            ];
            if ($billable == 'true') {
                $where['billable'] = true;
            }
            $tasks                = $this->statements_model->get_tasks($statement_id, $where);
            $total_timers_stopped = 0;
            foreach ($tasks as $task) {
                $this->db->where('task_id', $task['id']);
                $this->db->where('end_time IS NULL');
                $this->db->update('tbltaskstimers', [
                    'end_time' => time(),
                ]);
                $total_timers_stopped += $this->db->affected_rows();
            }
            $message = _l('statement_tasks_total_timers_stopped', $total_timers_stopped);
            $type    = 'success';
            if ($total_timers_stopped == 0) {
                $type = 'warning';
            }
            echo json_encode([
                'type'    => $type,
                'message' => $message,
            ]);
        }
    }

    public function get_pre_invoice_statement_info($statement_id)
    {
        if (has_permission('invoices', '', 'create')) {
            $data['billable_tasks'] = $this->statements_model->get_tasks($statement_id, [
                'billable'     => 1,
                'billed'       => 0,
                'startdate <=' => date('Y-m-d'),
            ]);

            $data['not_billable_tasks'] = $this->statements_model->get_tasks($statement_id, [
                'billable'    => 1,
                'billed'      => 0,
                'startdate >' => date('Y-m-d'),
            ]);

            $data['statement_id']   = $statement_id;
            $data['billing_type'] = get_statement_billing_type($statement_id);

            $this->load->model('expenses_model');
            $this->db->where('invoiceid IS NULL');
            $data['expenses'] = $this->expenses_model->get('', [
                'statement_id' => $statement_id,
                'billable'   => 1,
            ]);

            $this->load->view('admin/statements/statement_pre_invoice_settings', $data);
        }
    }

    public function get_invoice_statement_data()
    {
        if (has_permission('invoices', '', 'create')) {
            $type       = $this->input->post('type');
            $statement_id = $this->input->post('statement_id');
            // Check for all cases
            if ($type == '') {
                $type == 'single_line';
            }
            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get('', [
                'expenses_only !=' => 1,
            ]);
            $this->load->model('taxes_model');
            $data['taxes']         = $this->taxes_model->get();
            $data['currencies']    = $this->currencies_model->get();
            $data['base_currency'] = $this->currencies_model->get_base_currency();
            $this->load->model('invoice_items_model');

            $data['ajaxItems'] = false;
            if (total_rows('tblitems') <= ajax_on_total_items()) {
                $data['items'] = $this->invoice_items_model->get_grouped();
            } else {
                $data['items']     = [];
                $data['ajaxItems'] = true;
            }

            $data['items_groups'] = $this->invoice_items_model->get_groups();
            $data['staff']        = $this->staff_model->get('', ['active' => 1]);
            $statement              = $this->statements_model->get($statement_id);
            $data['statement']      = $statement;
            $items                = [];

            $statement    = $this->statements_model->get($statement_id);
            $item['id'] = 0;

            $default_tax     = unserialize(get_option('default_tax'));
            $item['taxname'] = $default_tax;

            $tasks = $this->input->post('tasks');
            if ($tasks) {
                $item['long_description'] = '';
                $item['qty']              = 0;
                $item['task_id']          = [];
                if ($type == 'single_line') {
                    $item['description'] = $statement->name;
                    foreach ($tasks as $task_id) {
                        $task = $this->tasks_model->get($task_id);
                        $sec  = $this->tasks_model->calc_task_total_time($task_id);
                        $item['long_description'] .= $task->name . ' - ' . seconds_to_time_format($sec) . ' ' . _l('hours') . "\r\n";
                        $item['task_id'][] = $task_id;
                        if ($statement->billing_type == 2) {
                            if ($sec < 60) {
                                $sec = 0;
                            }
                            $item['qty'] += sec2qty($sec);
                        }
                    }
                    if ($statement->billing_type == 1) {
                        $item['qty']  = 1;
                        $item['rate'] = $statement->statement_cost;
                    } elseif ($statement->billing_type == 2) {
                        $item['rate'] = $statement->statement_rate_per_hour;
                    }
                    $item['unit'] = '';
                    $items[]      = $item;
                } elseif ($type == 'task_per_item') {
                    foreach ($tasks as $task_id) {
                        $task                     = $this->tasks_model->get($task_id);
                        $sec                      = $this->tasks_model->calc_task_total_time($task_id);
                        $item['description']      = $statement->name . ' - ' . $task->name;
                        $item['qty']              = floatVal(sec2qty($sec));
                        $item['long_description'] = seconds_to_time_format($sec) . ' ' . _l('hours');
                        if ($statement->billing_type == 2) {
                            $item['rate'] = $statement->statement_rate_per_hour;
                        } elseif ($statement->billing_type == 3) {
                            $item['rate'] = $task->hourly_rate;
                        }
                        $item['task_id'] = $task_id;
                        $item['unit']    = '';
                        $items[]         = $item;
                    }
                } elseif ($type == 'timesheets_individualy') {
                    $timesheets     = $this->statements_model->get_timesheets($statement_id, $tasks);
                    $added_task_ids = [];
                    foreach ($timesheets as $timesheet) {
                        if ($timesheet['task_data']->billed == 0 && $timesheet['task_data']->billable == 1) {
                            $item['description'] = $statement->name . ' - ' . $timesheet['task_data']->name;
                            if (!in_array($timesheet['task_id'], $added_task_ids)) {
                                $item['task_id'] = $timesheet['task_id'];
                            }

                            array_push($added_task_ids, $timesheet['task_id']);

                            $item['qty']              = floatVal(sec2qty($timesheet['total_spent']));
                            $item['long_description'] = _l('statement_invoice_timesheet_start_time', _dt($timesheet['start_time'], true)) . "\r\n" . _l('statement_invoice_timesheet_end_time', _dt($timesheet['end_time'], true)) . "\r\n" . _l('statement_invoice_timesheet_total_logged_time', seconds_to_time_format($timesheet['total_spent'])) . ' ' . _l('hours');

                            if ($this->input->post('timesheets_include_notes') && $timesheet['note']) {
                                $item['long_description'] .= "\r\n\r\n" . _l('note') . ': ' . $timesheet['note'];
                            }

                            if ($statement->billing_type == 2) {
                                $item['rate'] = $statement->statement_rate_per_hour;
                            } elseif ($statement->billing_type == 3) {
                                $item['rate'] = $timesheet['task_data']->hourly_rate;
                            }
                            $item['unit'] = '';
                            $items[]      = $item;
                        }
                    }
                }
            }
            if ($statement->billing_type != 1) {
                $data['hours_quantity'] = true;
            }
            if ($this->input->post('expenses')) {
                if (isset($data['hours_quantity'])) {
                    unset($data['hours_quantity']);
                }
                if (count($tasks) > 0) {
                    $data['qty_hrs_quantity'] = true;
                }
                $expenses       = $this->input->post('expenses');
                $addExpenseNote = $this->input->post('expenses_add_note');
                $addExpenseName = $this->input->post('expenses_add_name');

                if (!$addExpenseNote) {
                    $addExpenseNote = [];
                }

                if (!$addExpenseName) {
                    $addExpenseName = [];
                }

                $this->load->model('expenses_model');
                foreach ($expenses as $expense_id) {
                    // reset item array
                    $item                     = [];
                    $item['id']               = 0;
                    $expense                  = $this->expenses_model->get($expense_id);
                    $item['expense_id']       = $expense->expenseid;
                    $item['description']      = _l('item_as_expense') . ' ' . $expense->name;
                    $item['long_description'] = $expense->description;

                    if (in_array($expense_id, $addExpenseNote) && !empty($expense->note)) {
                        $item['long_description'] .= PHP_EOL . $expense->note;
                    }

                    if (in_array($expense_id, $addExpenseName) && !empty($expense->expense_name)) {
                        $item['long_description'] .= PHP_EOL . $expense->expense_name;
                    }

                    $item['qty'] = 1;

                    $item['taxname'] = [];
                    if ($expense->tax != 0) {
                        array_push($item['taxname'], $expense->tax_name . '|' . $expense->taxrate);
                    }
                    if ($expense->tax2 != 0) {
                        array_push($item['taxname'], $expense->tax_name2 . '|' . $expense->taxrate2);
                    }
                    $item['rate']  = $expense->amount;
                    $item['order'] = 1;
                    $item['unit']  = '';
                    $items[]       = $item;
                }
            }
            $data['customer_id']          = $statement->clientid;
            $data['invoice_from_statement'] = true;
            $data['add_items']            = $items;
            $this->load->view('admin/statements/invoice_statement', $data);
        }
    }

    public function get_rel_statement_data($id, $task_id = '')
    {
        if ($this->input->is_ajax_request()) {
            $selected_milestone = '';
            if ($task_id != '' && $task_id != 'undefined') {
                $task               = $this->tasks_model->get($task_id);
                $selected_milestone = $task->milestone;
            }

            $allow_to_view_tasks = 0;
            $this->db->where('statement_id', $id);
            $this->db->where('name', 'view_tasks');
            $statement_settings = $this->db->get('tblstatementsettings')->row();
            if ($statement_settings) {
                $allow_to_view_tasks = $statement_settings->value;
            }

            echo json_encode([
                'allow_to_view_tasks' => $allow_to_view_tasks,
                'billing_type'        => get_statement_billing_type($id),
                'milestones'          => render_select('milestone', $this->statements_model->get_milestones($id), [
                    'id',
                    'name',
                ], 'task_milestone', $selected_milestone),
            ]);
        }
    }

    public function invoice_statement($statement_id)
    {
        if (has_permission('invoices', '', 'create')) {
            $this->load->model('invoices_model');
            $data               = $this->input->post();
            $data['statement_id'] = $statement_id;
            $invoice_id         = $this->invoices_model->add($data);
            if ($invoice_id) {
                $this->statements_model->log_activity($statement_id, 'statement_activity_invoiced_statement', format_invoice_number($invoice_id));
                set_alert('success', _l('statement_invoiced_successfully'));
            }
            redirect(admin_url('statements/view/' . $statement_id . '?group=statement_invoices'));
        }
    }

    public function view_statement_as_client($id, $clientid)
    {
        if (is_admin()) {
            login_as_client($clientid);
            redirect(site_url('clients/statement/' . $id));
        }
    }
}
