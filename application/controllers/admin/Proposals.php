<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Proposals extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('proposals_model');
        $this->load->model('currencies_model');
    }

    public function index($proposal_id = '')
    {
        $this->list_proposals($proposal_id);
    }

    public function list_proposals($proposal_id = '')
    {
        close_setup_menu();

        if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('proposals');
        }

        $isPipeline = $this->session->userdata('proposals_pipeline') == 'true';

        if ($isPipeline && !$this->input->get('status')) {
            $data['title']           = _l('proposals_pipeline');
            $data['bodyclass']       = 'proposals-pipeline';
            $data['switch_pipeline'] = false;
            // Direct access
            if (is_numeric($proposal_id)) {
                $data['proposalid'] = $proposal_id;
            } else {
                $data['proposalid'] = $this->session->flashdata('proposalid');
            }

            $this->load->view('admin/proposals/pipeline/manage', $data);
        } else {

            // Pipeline was initiated but user click from home page and need to show table only to filter
            if ($this->input->get('status') && $isPipeline) {
                $this->pipeline(0, true);
            }

            $data['proposal_id']           = $proposal_id;
            $data['switch_pipeline']       = true;
            $data['title']                 = _l('proposals');
            $data['statuses']              = $this->proposals_model->get_statuses();
            $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
            $data['years']                 = $this->proposals_model->get_proposals_years();
            $this->load->view('admin/proposals/manage', $data);
        }
    }

    public function table()
    {
        if (!has_permission('proposals', '', 'view')
            && !has_permission('proposals', '', 'view_own')
            && get_option('allow_staff_view_proposals_assigned') == 0) {
            ajax_access_denied();
        }

        $this->app->get_table_data('proposals');
    }

    public function proposal_relations($rel_id, $rel_type)
    {
        $this->app->get_table_data('proposals_relations', [
            'rel_id'   => $rel_id,
            'rel_type' => $rel_type,
        ]);
    }

    public function delete_attachment($id)
    {
        $file = $this->misc_model->get_file($id);
        if ($file->staffid == get_staff_user_id() || is_admin()) {
            echo $this->proposals_model->delete_attachment($id);
        } else {
            ajax_access_denied();
        }
    }

    public function clear_signature($id)
    {
        if (has_permission('proposals', '', 'delete')) {
            $this->proposals_model->clear_signature($id);
        }

        redirect(admin_url('proposals/list_proposals/' . $id));
    }

    public function sync_data()
    {
        if (has_permission('proposals', '', 'create') || has_permission('proposals', '', 'edit')) {
            $has_permission_view = has_permission('proposals', '', 'view');

            $this->db->where('rel_id', $this->input->post('rel_id'));
            $this->db->where('rel_type', $this->input->post('rel_type'));

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }

            $address = trim($this->input->post('address'));
            $address = nl2br($address);
            $this->db->update('tblproposals', [
                'phone'   => $this->input->post('phone'),
                'zip'     => $this->input->post('zip'),
                'country' => $this->input->post('country'),
                'state'   => $this->input->post('state'),
                'address' => $address,
                'city'    => $this->input->post('city'),
            ]);

            if ($this->db->affected_rows() > 0) {
                echo json_encode([
                    'message' => _l('all_data_synced_successfully'),
                ]);
            } else {
                echo json_encode([
                    'message' => _l('sync_proposals_up_to_date'),
                ]);
            }
        }
    }

    public function proposal($id = '')
    {
        if ($this->input->post()) {
            $proposal_data = $this->input->post();
            if ($id == '') {
                if (!has_permission('proposals', '', 'create')) {
                    access_denied('proposals');
                }
                $id = $this->proposals_model->add($proposal_data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('proposal')));
                    if ($this->set_proposal_pipeline_autoload($id)) {
                        redirect(admin_url('proposals'));
                    } else {
                        redirect(admin_url('proposals/list_proposals/' . $id));
                    }
                }
            } else {
                if (!has_permission('proposals', '', 'edit')) {
                    access_denied('proposals');
                }
                $success = $this->proposals_model->update($proposal_data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('proposal')));
                }
                if ($this->set_proposal_pipeline_autoload($id)) {
                    redirect(admin_url('proposals'));
                } else {
                    redirect(admin_url('proposals/list_proposals/' . $id));
                }
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('proposal_lowercase'));
        } else {
            $data['proposal']			= $this->proposals_model->get($id);
			$data['proposal_maketas']	= $this->proposals_model->get_proposal_maketas($id);
			$data['proposal_taxes']		= $this->proposals_model->get_proposal_taxes($id);
			$data['proposal_metaforika']= $this->proposals_model->get_proposal_metaforika($id);
			$data['proposal_topostropos']= $this->proposals_model->get_proposal_topostropos($id);
			$data['proposal_xronos']= $this->proposals_model->get_proposal_xronos($id);
            if (!$data['proposal'] || !user_can_view_proposal($id)) {
                blank_page(_l('proposal_not_found'));
            }

            $data['estimate']    = $data['proposal'];
            $data['is_proposal'] = true;
            $title               = _l('edit', _l('proposal_lowercase'));
        }

        $this->load->model('taxes_model');
        $data['taxes'] = $this->taxes_model->get();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['statuses']			= $this->proposals_model->get_statuses();
		$data['maketas']			= $this->proposals_model->get_maketas();
		$data['prodiagrafes']		= $this->proposals_model->get_prodiagrafes();
		$data['prodiagrafes_tags']	= $this->proposals_model->get_prodiagrafes_tags();
		$data['taxes']				= $this->proposals_model->get_taxes();
		$data['metaforika']			= $this->proposals_model->get_metaforika();
		$data['topostropos']		= $this->proposals_model->get_topostropos();
		$data['xronos']				= $this->proposals_model->get_xronos();
		$data['pliromi']			= $this->proposals_model->get_pliromi();
        $data['staff']				= $this->staff_model->get('', ['active' => 1]);
        $data['currencies']			= $this->currencies_model->get();
        $data['base_currency']		= $this->currencies_model->get_base_currency();

        $data['title'] = $title;
        $this->load->view('admin/proposals/proposal', $data);
    }

    public function get_template()
    {
        $name = $this->input->get('name');
        echo $this->load->view('admin/proposals/templates/' . $name, [], true);
    }

    public function send_expiry_reminder($id)
    {
        $canView = user_can_view_proposal($id);
        if (!$canView) {
            access_denied('proposals');
        } else {
            if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && $canView == false) {
                access_denied('proposals');
            }
        }

        $success = $this->proposals_model->send_expiry_reminder($id);
        if ($success) {
            set_alert('success', _l('sent_expiry_reminder_success'));
        } else {
            set_alert('danger', _l('sent_expiry_reminder_fail'));
        }
        if ($this->set_proposal_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('proposals/list_proposals/' . $id));
        }
    }

    public function clear_acceptance_info($id)
    {
        if (is_admin()) {
            $this->db->where('id', $id);
            $this->db->update('tblproposals', get_acceptance_info_array(true));
        }

        redirect(admin_url('proposals/list_proposals/' . $id));
    }

    public function pdf($id)
    {
        if (!$id) {
            redirect(admin_url('proposals'));
        }

        $canView = user_can_view_proposal($id);
        if (!$canView) {
            access_denied('proposals');
        } else {
            if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && $canView == false) {
                access_denied('proposals');
            }
        }

        $proposal = $this->proposals_model->get($id);

        try {
            $pdf = proposal_pdf($proposal);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';

        if ($this->input->get('output_type')) {
            $type = $this->input->get('output_type');
        }

        if ($this->input->get('print')) {
            $type = 'I';
        }

        $proposal_number = format_proposal_number($id);
        $pdf->Output($proposal_number . '.pdf', $type);
    }

    public function get_proposal_data_ajax($id, $to_return = false)
    {
        if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && get_option('allow_staff_view_proposals_assigned') == 0) {
            echo _l('access_denied');
            die;
        }

        $proposal = $this->proposals_model->get($id, [], true);

        if (!$proposal || !user_can_view_proposal($id)) {
            echo _l('proposal_not_found');
            die;
        }

        $template_name         = 'proposal-send-to-customer';
        $data['template_name'] = $template_name;

        $this->db->where('slug', $template_name);
        $this->db->where('language', 'english');
        $template_result = $this->db->get('tblemailtemplates')->row();

        $data['template_system_name'] = $template_result->name;
        $data['template_id']          = $template_result->emailtemplateid;

        $data['template_disabled'] = false;
        if (total_rows('tblemailtemplates', ['slug' => $data['template_name'], 'active' => 0]) > 0) {
            $data['template_disabled'] = true;
        }

        define('EMAIL_TEMPLATE_PROPOSAL_ID_HELP', $proposal->id);

        $data['template'] = get_email_template_for_sending($template_name, $proposal->email);

        $proposal_merge_fields  = get_available_merge_fields();
        $_proposal_merge_fields = [];
        array_push($_proposal_merge_fields, [
            [
                'name' => 'Items Table',
                'key'  => '{proposal_items}',
            ],
        ]);
        foreach ($proposal_merge_fields as $key => $val) {
            foreach ($val as $type => $f) {
                if ($type == 'proposals') {
                    foreach ($f as $available) {
                        foreach ($available['available'] as $av) {
                            if ($av == 'proposals') {
                                array_push($_proposal_merge_fields, $f);

                                break;
                            }
                        }

                        break;
                    }
                } elseif ($type == 'other') {
                    array_push($_proposal_merge_fields, $f);
                }
            }
        }
        $data['proposal_statuses']     = $this->proposals_model->get_statuses();
        $data['members']               = $this->staff_model->get('', ['active' => 1]);
        $data['proposal_merge_fields'] = $_proposal_merge_fields;
        $data['proposal']              = $proposal;
        $data['totalNotes']            = total_rows('tblnotes', ['rel_id' => $id, 'rel_type' => 'proposal']);
        if ($to_return == false) {
            $this->load->view('admin/proposals/proposals_preview_template', $data);
        } else {
            return $this->load->view('admin/proposals/proposals_preview_template', $data, true);
        }
    }

    public function add_note($rel_id)
    {
        if ($this->input->post() && user_can_view_proposal($rel_id)) {
            $this->misc_model->add_note($this->input->post(), 'proposal', $rel_id);
            echo $rel_id;
        }
    }

    public function get_notes($id)
    {
        if (user_can_view_proposal($id)) {
            $data['notes'] = $this->misc_model->get_notes($id, 'proposal');
            $this->load->view('admin/includes/sales_notes_template', $data);
        }
    }

    public function convert_to_estimate($id)
    {
        if (!has_permission('estimates', '', 'create')) {
            access_denied('estimates');
        }
        if ($this->input->post()) {
            $this->load->model('estimates_model');
            $estimate_id = $this->estimates_model->add($this->input->post());
            if ($estimate_id) {
                set_alert('success', _l('proposal_converted_to_estimate_success'));
                $this->db->where('id', $id);
                $this->db->update('tblproposals', [
                    'estimate_id' => $estimate_id,
                    'status'      => 3,
                ]);
                logActivity('Proposal Converted to Estimate [EstimateID: ' . $estimate_id . ', ProposalID: ' . $id . ']');

                do_action('proposal_converted_to_estimate', ['proposal_id' => $id, 'estimate_id' => $estimate_id]);

                redirect(admin_url('estimates/estimate/' . $estimate_id));
            } else {
                set_alert('danger', _l('proposal_converted_to_estimate_fail'));
            }
            if ($this->set_proposal_pipeline_autoload($id)) {
                redirect(admin_url('proposals'));
            } else {
                redirect(admin_url('proposals/list_proposals/' . $id));
            }
        }
    }

    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('invoices');
        }
        if ($this->input->post()) {
            $this->load->model('invoices_model');
            $invoice_id = $this->invoices_model->add($this->input->post());
            if ($invoice_id) {
                set_alert('success', _l('proposal_converted_to_invoice_success'));
                $this->db->where('id', $id);
                $this->db->update('tblproposals', [
                    'invoice_id' => $invoice_id,
                    'status'     => 3,
                ]);
                logActivity('Proposal Converted to Invoice [InvoiceID: ' . $invoice_id . ', ProposalID: ' . $id . ']');
                do_action('proposal_converted_to_invoice', ['proposal_id' => $id, 'invoice_id' => $invoice_id]);
                redirect(admin_url('invoices/invoice/' . $invoice_id));
            } else {
                set_alert('danger', _l('proposal_converted_to_invoice_fail'));
            }
            if ($this->set_proposal_pipeline_autoload($id)) {
                redirect(admin_url('proposals'));
            } else {
                redirect(admin_url('proposals/list_proposals/' . $id));
            }
        }
    }

    public function get_invoice_convert_data($id)
    {
        $this->load->model('payment_modes_model');
        $data['payment_modes'] = $this->payment_modes_model->get('', [
            'expenses_only !=' => 1,
        ]);
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']          = $this->staff_model->get('', ['active' => 1]);
        $data['proposal']       = $this->proposals_model->get($id);
        $data['billable_tasks'] = [];
        $data['add_items']      = $this->_parse_items($data['proposal']);

        if ($data['proposal']->rel_type == 'lead') {
            $this->db->where('leadid', $data['proposal']->rel_id);
            $data['customer_id'] = $this->db->get('tblclients')->row()->userid;
        } else {
            $data['customer_id'] = $data['proposal']->rel_id;
        }
        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'proposal',
            'rel_id'     => $id,
        ];
        $this->load->view('admin/proposals/invoice_convert_template', $data);
    }

    public function get_estimate_convert_data($id)
    {
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows('tblitems') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']     = $this->staff_model->get('', ['active' => 1]);
        $data['proposal']  = $this->proposals_model->get($id);
        $data['add_items'] = $this->_parse_items($data['proposal']);

        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();
        if ($data['proposal']->rel_type == 'lead') {
            $this->db->where('leadid', $data['proposal']->rel_id);
            $data['customer_id'] = $this->db->get('tblclients')->row()->userid;
        } else {
            $data['customer_id'] = $data['proposal']->rel_id;
        }

        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'proposal',
            'rel_id'     => $id,
        ];

        $this->load->view('admin/proposals/estimate_convert_template', $data);
    }

    private function _parse_items($proposal)
    {
        $items = [];
        foreach ($proposal->items as $item) {
            $taxnames = [];
            $taxes    = get_proposal_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                array_push($taxnames, $tax['taxname']);
            }
            $item['taxname']        = $taxnames;
            $item['parent_item_id'] = $item['id'];
            $item['id']             = 0;
            $items[]                = $item;
        }

        return $items;
    }

    /* Send proposal to email */
    public function send_to_email($id)
    {
        $canView = user_can_view_proposal($id);
        if (!$canView) {
            access_denied('proposals');
        } else {
            if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && $canView == false) {
                access_denied('proposals');
            }
        }

        if ($this->input->post()) {
            try {
                $success = $this->proposals_model->send_proposal_to_email($id, 'proposal-send-to-customer', $this->input->post('attach_pdf'), $this->input->post('cc'));
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo $message;
                if (strpos($message, 'Unable to get the size of the image') !== false) {
                    show_pdf_unable_to_get_image_size_error();
                }
                die;
            }

            if ($success) {
                set_alert('success', _l('proposal_sent_to_email_success'));
            } else {
                set_alert('danger', _l('proposal_sent_to_email_fail'));
            }

            if ($this->set_proposal_pipeline_autoload($id)) {
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(admin_url('proposals/list_proposals/' . $id));
            }
        }
    }

    public function copy($id)
    {
        if (!has_permission('proposals', '', 'create')) {
            access_denied('proposals');
        }
        $new_id = $this->proposals_model->copy($id);
        if ($new_id) {
            set_alert('success', _l('proposal_copy_success'));
            $this->set_proposal_pipeline_autoload($new_id);
            redirect(admin_url('proposals/proposal/' . $new_id));
        } else {
            set_alert('success', _l('proposal_copy_fail'));
        }
        if ($this->set_proposal_pipeline_autoload($id)) {
            redirect(admin_url('proposals'));
        } else {
            redirect(admin_url('proposals/list_proposals/' . $id));
        }
    }

    public function mark_action_status($status, $id)
    {
        if (!has_permission('proposals', '', 'edit')) {
            access_denied('proposals');
        }
        $success = $this->proposals_model->mark_action_status($status, $id);
        if ($success) {
            set_alert('success', _l('proposal_status_changed_success'));
        } else {
            set_alert('danger', _l('proposal_status_changed_fail'));
        }
        if ($this->set_proposal_pipeline_autoload($id)) {
            redirect(admin_url('proposals'));
        } else {
            redirect(admin_url('proposals/list_proposals/' . $id));
        }
    }
	
		public function pliromi()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_pliromi') == '0') {
            access_denied('Proposal Pliromi');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_pliromi($data);
                $message = $id ? _l('added_successfully', _l('proposal_pliromi')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_pliromi($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('proposal_pliromi'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function pliromi_all()
    {
        if (!is_admin()) {
            access_denied('Proposal Pliromi');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_pliromi');
        }
        $data['title'] = _l('proposal_pliromi');
        $data['proposal_pliromi'] = $this->proposals_model->get_proposals_pliromi();
        $this->load->view('admin/proposals/pliromi_manage', $data);
    }
	
	
	
	public function metaforika()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_metaforika') == '0') {
            access_denied('Proposal Metaforika');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_metaforika($data);
                $message = $id ? _l('added_successfully', _l('proposal_metaforika')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_metaforika($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('proposal_metaforika'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function metaforika_all()
    {
        if (!is_admin()) {
            access_denied('Proposal Metaforika');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_metaforika');
        }
        $data['title'] = _l('proposal_metaforika');
        $data['proposal_metaforika'] = $this->proposals_model->get_proposals_metaforika();
        $this->load->view('admin/proposals/metaforika_manage', $data);
    }
	
	public function topostropos()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_topostropos') == '0') {
            access_denied('Proposal Topos Tropos');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_topostropos($data);
                $message = $id ? _l('added_successfully', _l('proposal_topostropos')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_topostropos($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('proposal_topostropos'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function topostropos_all()
    {
        if (!is_admin()) {
            access_denied('Proposal Topos Tropos');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_topostropos');
        }
        $data['title'] = _l('proposals_topostropos');
        $data['proposal_topostropos'] = $this->proposals_model->get_proposals_topostropos();
        $this->load->view('admin/proposals/topostropos_manage', $data);
    }
	
	public function xronos()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_xronos') == '0') {
            access_denied('Proposal Xronos');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_xronos($data);
                $message = $id ? _l('added_successfully', _l('proposal_xronos')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_xronos($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('proposal_xronos'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function xronos_all()
    {
        if (!is_admin()) {
            access_denied('Proposal Xronos');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_xronos');
        }
        $data['title'] = _l('proposals_xronos');
        $data['proposal_xronos'] = $this->proposals_model->get_proposals_xronos();
        $this->load->view('admin/proposals/topostropos_manage', $data);
    }
	
	
	public function maketa()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_maketas') == '0') {
            access_denied('Proposal Maketas');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_maketa($data);
                $message = $id ? _l('added_successfully', _l('proposal_maketa')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_maketa($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('customer_maketa'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function maketas()
    {
        if (!is_admin() ) {
            access_denied('Proposal Maketas');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_maketas');
        }
        $data['title'] = _l('proposal_maketas');
        $data['proposal_maketas'] = $this->proposals_model->get_proposals_maketas();
        $this->load->view('admin/proposals/maketas_manage', $data);
    }
	
	public function prodiagrafe()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_prodiagrafes') == '0') {
            access_denied('Proposal Maketas');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_prodiagrafe($data);
                $message = $id ? _l('added_successfully', _l('proposal_prodiagrafe')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
					'tag'     => $data['tag'],
                ]);
            } else {
                $success = $this->proposals_model->edit_prodiagrafe($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('customer_prodiagrafe'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function prodiagrafes()
    {
        if (!is_admin()) {
            access_denied('Proposal prodiagrafes');
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->proposals_model->get_prodiagrafes());
        }
        $data['title'] = _l('proposal_prodiagrafes');
        $data['proposal_maketas'] = $this->proposals_model->get_proposals_prodiagrafes();
        $this->load->view('admin/proposals/prodiagrafes_manage', $data);
    }
	
	public function get_prodiagrafes()
    {
        if ($this->input->is_ajax_request()) {
			echo json_encode($this->proposals_model->get_prodiagrafes());
		}else{
			$data['title'] = _l('proposal_prodiagrafes');
			$data['proposal_maketas'] = $this->proposals_model->get_proposals_prodiagrafes();
			$this->load->view('admin/proposals/prodiagrafes_manage', $data);
		}
        
    }
	
	public function get_prodiagrafes_tags()
    {
        if ($this->input->is_ajax_request()) {
			echo json_encode($this->proposals_model->get_prodiagrafes_tags());
		}
    }
	
	public function tax()
    {
        if (!is_admin() && get_option('staff_members_create_inline_proposal_taxes') == '0') {
            access_denied('Proposal Taxes');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->proposals_model->add_tax($data);
                $message = $id ? _l('added_successfully', _l('proposal_tax')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->proposals_model->edit_tax($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('customer_tax'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }
	
	public function taxes()
    {
        if (!is_admin()) {
            access_denied('Proposal Taxes');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('proposals_taxes');
        }
        $data['title'] = _l('proposal_taxes');
        $data['proposal_taxes'] = $this->proposals_model->get_proposals_taxes();
        $this->load->view('admin/proposals/taxes_manage', $data);
    }
	
	public function delete_maketa($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Maketas');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_maketa($id);
                $message = $id ? _l('deleted_successfully', _l('proposal_maketa')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_prodiagrafe($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Prodiagrafe');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_prodiagrafe($id);
                $message = $id ? _l('deleted_successfully', _l('proposal_prodiagrafes')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_prodiagrafe_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Prodiagrafe');
        }
        if (!$id) {
            redirect(admin_url('proposals/prodiagrafes'));
        }
        $response = $this->proposals_model->delete_prodiagrafe($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_prodiagrafes')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_prodiagrafes_lowercase')));
        }
		get_instance()->session->set_userdata([
			'red_url' => current_full_url(),
		]);
        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function delete_xronos($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Xronos');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_xronos($id);
                $message = $id ? _l('deleted', _l('proposal_xronos')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_xronos_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Proposal Xronos');
        }
        if (!$id) {
            redirect(admin_url('proposals/xronos'));
        }
        $response = $this->proposals_model->delete_xronos($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_xronos')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_xronos_lowercase')));
        }
		
		if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function delete_topostropos($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Topostropos');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_topostropos($id);
                $message = $id ? _l('deleted', _l('proposal_topostropos')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_topostropos_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Proposal Topos tropos');
        }
        if (!$id) {
            redirect(admin_url('proposals/topostropos'));
        }
        $response = $this->proposals_model->delete_topostropos($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_topostropos')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_topostropos_lowercase')));
        }
		if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function delete_metaforika($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Proposal Metaforika');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_metaforika($id);
                $message = $id ? _l('deleted', _l('proposal_metaforika')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_metaforika_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Proposal Metaforika');
        }
        if (!$id) {
            redirect(admin_url('proposals/metaforika'));
        }
        $response = $this->proposals_model->delete_metaforika($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_metaforika')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_metaforika_lowercase')));
        }
        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function delete_tax($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Proposal Tax');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_tax($id);
                $message = $id ? _l('deleted', _l('proposal_tax')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_tax_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Proposal Tax');
        }
        if (!$id) {
            redirect(admin_url('proposals/taxes'));
        }
        $response = $this->proposals_model->delete_tax($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_tax')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_tax_lowercase')));
        }
        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }
	
		public function delete_pliromi($id){
		$staffid = get_staff_user_id();
		if (!is_admin() && $staffid != 4) {
            access_denied('Proposal Proposal Pliromi');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if (!empty($data['id'])) {
				$response = $this->proposals_model->delete_pliromi($id);
                $message = $id ? _l('deleted', _l('proposal_pliromi')) : '';
                echo json_encode([
                    'success' => $response,
                    'message' => $message,
                ]);
            }
        }
	}
	
	public function delete_pliromi_($id)
    {
        $staffid = get_staff_user_id();
        if (!is_admin() && $staffid != 4) {
            access_denied('Delete Proposal Pliromi');
        }
        if (!$id) {
            redirect(admin_url('proposals/pliromi'));
        }
        $response = $this->proposals_model->delete_pliromi($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal_pliromi')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_pliromi_lowercase')));
        }
        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete($id)
    {
        if (!has_permission('proposals', '', 'delete')) {
            access_denied('proposals');
        }
        $response = $this->proposals_model->delete($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('proposal')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('proposal_lowercase')));
        }
        redirect(admin_url('proposals'));
    }

    public function get_relation_data_values($rel_id, $rel_type)
    {
        echo json_encode($this->proposals_model->get_relation_data_values($rel_id, $rel_type));
    }

    public function add_proposal_comment()
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->proposals_model->add_comment($this->input->post()),
            ]);
        }
    }

    public function edit_comment($id)
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->proposals_model->edit_comment($this->input->post(), $id),
                'message' => _l('comment_updated_successfully'),
            ]);
        }
    }

    public function get_proposal_comments($id)
    {
        $data['comments'] = $this->proposals_model->get_comments($id);
        $this->load->view('admin/proposals/comments_template', $data);
    }

    public function remove_comment($id)
    {
        $this->db->where('id', $id);
        $comment = $this->db->get('tblproposalcomments')->row();
        if ($comment) {
            if ($comment->staffid != get_staff_user_id() && !is_admin()) {
                echo json_encode([
                    'success' => false,
                ]);
                die;
            }
            echo json_encode([
                'success' => $this->proposals_model->remove_comment($id),
            ]);
        } else {
            echo json_encode([
                'success' => false,
            ]);
        }
    }

    public function save_proposal_data()
    {
        if (!has_permission('proposals', '', 'edit') && !has_permission('proposals', '', 'create')) {
            header('HTTP/1.0 400 Bad error');
            echo json_encode([
                'success' => false,
                'message' => _l('access_denied'),
            ]);
            die;
        }
        $success = false;
        $message = '';

        $this->db->where('id', $this->input->post('proposal_id'));
        $this->db->update('tblproposals', [
            'content' => $this->input->post('content', false),
        ]);

        $success = $this->db->affected_rows() > 0;
        $message = _l('updated_successfully', _l('proposal'));

        echo json_encode([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Pipeline
    public function pipeline($set = 0, $manual = false)
    {
        if ($set == 1) {
            $set = 'true';
        } else {
            $set = 'false';
        }
        $this->session->set_userdata([
            'proposals_pipeline' => $set,
        ]);
        if ($manual == false) {
            redirect(admin_url('proposals'));
        }
    }

    public function pipeline_open($id)
    {
        if (has_permission('proposals', '', 'view') || has_permission('proposals', '', 'view_own') || get_option('allow_staff_view_proposals_assigned') == 1) {
            $data['proposal']      = $this->get_proposal_data_ajax($id, true);
            $data['proposal_data'] = $this->proposals_model->get($id);
            $this->load->view('admin/proposals/pipeline/proposal', $data);
        }
    }

    public function update_pipeline()
    {
        if (has_permission('proposals', '', 'edit')) {
            $this->proposals_model->update_pipeline($this->input->post());
        }
    }

    public function get_pipeline()
    {
        if (has_permission('proposals', '', 'view') || has_permission('proposals', '', 'view_own') || get_option('allow_staff_view_proposals_assigned') == 1) {
            $data['statuses'] = $this->proposals_model->get_statuses();
            $this->load->view('admin/proposals/pipeline/pipeline', $data);
        }
    }

    public function pipeline_load_more()
    {
        $status = $this->input->get('status');
        $page   = $this->input->get('page');

        $proposals = $this->proposals_model->do_kanban_query($status, $this->input->get('search'), $page, [
            'sort_by' => $this->input->get('sort_by'),
            'sort'    => $this->input->get('sort'),
        ]);

        foreach ($proposals as $proposal) {
            $this->load->view('admin/proposals/pipeline/_kanban_card', [
                'proposal' => $proposal,
                'status'   => $status,
            ]);
        }
    }

    public function set_proposal_pipeline_autoload($id)
    {
        if ($id == '') {
            return false;
        }

        if ($this->session->has_userdata('proposals_pipeline') && $this->session->userdata('proposals_pipeline') == 'true') {
            $this->session->set_flashdata('proposalid', $id);

            return true;
        }

        return false;
    }

    public function get_due_date()
    {
        if ($this->input->post()) {
            $date    = $this->input->post('date');
            $duedate = '';
            if (get_option('proposal_due_after') != 0) {
                $date    = to_sql_date($date);
                $d       = date('Y-m-d', strtotime('+' . get_option('proposal_due_after') . ' DAY', strtotime($date)));
                $duedate = _d($d);
                echo $duedate;
            }
        }
    }
}
