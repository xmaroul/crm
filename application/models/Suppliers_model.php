<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Suppliers_model extends CRM_Model
{
    private $contact_columns;

    public function __construct()
    {
        parent::__construct();

        $this->contact_columns = do_action('contact_columns', ['firstname', 'lastname', 'email', 'phonenumber', 'title', 'password', 'send_set_password_email', 'donotsendwelcomeemail', 'permissions', 'direction', 'invoice_emails', 'estimate_emails', 'credit_note_emails', 'contract_emails', 'task_emails', 'project_emails', 'ticket_emails', 'is_primary']);

        $this->load->model(['supplier_vault_entries_model', 'supplier_groups_model', 'statement_model']);
    }

    /**
     * Get supplier object based on passed supplierid if not passed supplierid return array of all suppliers
     * @param  mixed $id    supplier id
     * @param  array  $where
     * @return mixed
     */
    public function get($id = '', $where = [])
    {
        $this->db->select(implode(',', prefixed_table_fields_array('tblsuppliers')) . ',' . get_sql_select_supplier_company());

        $this->db->join('tblcountries', 'tblcountries.country_id = tblsuppliers.country', 'left');
        $this->db->join('tblcontacts', 'tblcontacts.userid = tblsuppliers.userid AND is_primary = 1', 'left');
        $this->db->where($where);

        if (is_numeric($id)) {
            $this->db->where('tblsuppliers.userid', $id);
            $supplier = $this->db->get('tblsuppliers')->row();

            if (get_option('company_requires_vat_number_field') == 0) {
                $supplier->vat = null;
            }

            return $supplier;
        }

        $this->db->order_by('company', 'asc');

        return $this->db->get('tblsuppliers')->result_array();
    }

    /**
     * Get suppliers contacts
     * @param  mixed $supplier_id
     * @param  array  $where       perform where in query
     * @return array
     */
    public function get_contacts($supplier_id = '', $where = ['active' => 1])
    {
        $this->db->where($where);
        if ($supplier_id != '') {
            $this->db->where('userid', $supplier_id);
        }
        $this->db->order_by('is_primary', 'DESC');

        return $this->db->get('tblcontacts')->result_array();
    }

    /**
     * Get single contacts
     * @param  mixed $id contact id
     * @return object
     */
    public function get_contact($id)
    {
        $this->db->where('id', $id);

        return $this->db->get('tblcontacts')->row();
    }

    /**
     * @param array $_POST data
     * @param supplier_request is this request from the supplier area
     * @return integer Insert ID
     * Add new supplier to database
     */
    public function add($data, $supplier_or_lead_convert_request = false)
    {
        $contact_data = [];
        foreach ($this->contact_columns as $field) {
            if (isset($data[$field])) {
                $contact_data[$field] = $data[$field];
                // Phonenumber is also used for the company profile
                if ($field != 'phonenumber') {
                    unset($data[$field]);
                }
            }
        }

        // From supplier profile register
        if (isset($data['contact_phonenumber'])) {
            $contact_data['phonenumber'] = $data['contact_phonenumber'];
            unset($data['contact_phonenumber']);
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (is_staff_logged_in()) {
            $data['addedfrom'] = get_staff_user_id();
        }

        $hook_data = do_action('before_supplier_added', ['data' => $data]);
        $data      = $hook_data['data'];

        $this->db->insert('tblsuppliers', $data);

        $userid = $this->db->insert_id();
        if ($userid) {
            if (isset($custom_fields)) {
                $_custom_fields = $custom_fields;
                // Possible request from the register area with 2 types of custom fields for contact and for comapny/supplier
                if (count($custom_fields) == 2) {
                    unset($custom_fields);
                    $custom_fields['suppliers']                = $_custom_fields['suppliers'];
                    $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                } elseif (count($custom_fields) == 1) {
                    if (isset($_custom_fields['contacts'])) {
                        $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                        unset($custom_fields);
                    }
                }
                handle_custom_fields_post($userid, $custom_fields);
            }
            /**
             * Used in Import, Lead Convert, Register
             */
            if ($supplier_or_lead_convert_request == true) {
                $contact_id = $this->add_contact($contact_data, $userid, $supplier_or_lead_convert_request);
            }
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    $this->db->insert('tblsuppliergroups_in', [
                        'supplier_id' => $userid,
                        'groupid'     => $group,
                    ]);
                }
            }
            do_action('after_supplier_added', $userid);
            $log = 'ID: ' . $userid;

            if ($log == '' && isset($contact_id)) {
                $log = get_contact_full_name($contact_id);
            }

            $isStaff = null;
            if (!is_supplier_logged_in() && is_staff_logged_in()) {
                $log .= ', From Staff: ' . get_staff_user_id();
                $isStaff = get_staff_user_id();
            }

            logActivity('New Client Created [' . $log . ']', $isStaff);
        }

        return $userid;
    }
	
	   public function contact_add($data, $supplier_or_lead_convert_request = false)
    {
        $contact_data = [];
        foreach ($this->contact_columns as $field) {
            if (isset($data[$field])) {
                $contact_data[$field] = $data[$field];
                // Phonenumber is also used for the company profile
                if ($field != 'phonenumber') {
                    unset($data[$field]);
                }
            }
        }

        // From supplier profile register
        if (isset($data['contact_phonenumber'])) {
            $contact_data['phonenumber'] = $data['contact_phonenumber'];
            unset($data['contact_phonenumber']);
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (is_staff_logged_in()) {
            $data['addedfrom'] = get_staff_user_id();
        }

        $hook_data = do_action('before_supplier_added', ['data' => $data]);
        $data      = $hook_data['data'];

        $this->db->insert('tblsuppliers', $data);

        $userid = $this->db->insert_id();
        if ($userid) {
            if (isset($custom_fields)) {
                $_custom_fields = $custom_fields;
                // Possible request from the register area with 2 types of custom fields for contact and for comapny/supplier
                if (count($custom_fields) == 2) {
                    unset($custom_fields);
                    $custom_fields['suppliers']                = $_custom_fields['suppliers'];
                    $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                } elseif (count($custom_fields) == 1) {
                    if (isset($_custom_fields['contacts'])) {
                        $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                        unset($custom_fields);
                    }
                }
                handle_custom_fields_post($userid, $custom_fields);
            }
            /**
             * Used in Import, Lead Convert, Register
             */
            if ($supplier_or_lead_convert_request == true) {
                $contact_id = $this->add_contact($contact_data, $userid, $supplier_or_lead_convert_request);
            }
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    $this->db->insert('tblsuppliergroups_in', [
                        'supplier_id' => $userid,
                        'groupid'     => $group,
                    ]);
                }
            }
            do_action('after_supplier_added', $userid);
            $log = 'ID: ' . $userid;

            if ($log == '' && isset($contact_id)) {
                $log = get_contact_full_name($contact_id);
            }

            $isStaff = null;
            if (!is_supplier_logged_in() && is_staff_logged_in()) {
                $log .= ', From Staff: ' . get_staff_user_id();
                $isStaff = get_staff_user_id();
            }

            logActivity('New Client Created [' . $log . ']', $isStaff);
        }

        return $userid;
    }

	    /**
     * @param array $_POST data
     * @param supplier_request is this request from the supplier area
     * @return integer Insert ID
     * Add new supplier to database
     */
    public function custom_add($data, $supplier_or_lead_convert_request = false)
    {
		$obsolete_groups_map = array(
				'ΔΗΜΟΣΙΟ' => 1,
				'ΕΠΑΓΓΕΛΜΑΤΙΑΣ' => 2,
				'ΙΔΙΩΤΗΣ' => 3,
				'ΣΥΛΛΟΓΟΣ' => 4,
				'Μεταπωλητής' => 5,
				'Συνεργάτης' => 6
			);
		if (!empty($data['group1'])){
			$data['groups_in'][] = $obsolete_groups_map[$data['group1']];
		}
		unset($data['group1']);
		if (!empty($data['group2'])){
			$data['groups_in'][] = $obsolete_groups_map[$data['group2']];
		}
		unset($data['group2']);
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (is_staff_logged_in()) {
            $data['addedfrom'] = get_staff_user_id();
        }

        $hook_data = do_action('before_supplier_added', ['data' => $data]);
        $data      = $hook_data['data'];

        $this->db->insert('tblsuppliers', $data);

        $userid = $this->db->insert_id();
        if ($userid) {
            if (isset($custom_fields)) {
                $_custom_fields = $custom_fields;
                // Possible request from the register area with 2 types of custom fields for contact and for comapny/supplier
                if (count($custom_fields) == 2) {
                    unset($custom_fields);
                    $custom_fields['suppliers']                = $_custom_fields['suppliers'];
                    $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                } elseif (count($custom_fields) == 1) {
                    if (isset($_custom_fields['contacts'])) {
                        $contact_data['custom_fields']['contacts'] = $_custom_fields['contacts'];
                        unset($custom_fields);
                    }
                }
                handle_custom_fields_post($userid, $custom_fields);
            }
            /**
             * Used in Import, Lead Convert, Register
             */
//            if ($supplier_or_lead_convert_request == true) {
//                $contact_id = $this->add_contact($contact_data, $userid, $supplier_or_lead_convert_request);
//            }
            if (isset($groups_in)) {
                foreach ($groups_in as $group) {
                    $this->db->insert('tblsuppliergroups_in', [
                        'supplier_id' => $userid,
                        'groupid'     => $group,
                    ]);
                }
            }
            do_action('after_supplier_added', $userid);
            $log = 'ID: ' . $userid;

            if ($log == '' && isset($contact_id)) {
                $log = get_contact_full_name($contact_id);
            }

            $isStaff = null;
            if (!is_supplier_logged_in() && is_staff_logged_in()) {
                $log .= ', From Staff: ' . get_staff_user_id();
                $isStaff = get_staff_user_id();
            }

            logActivity('New Client Created [' . $log . ']', $isStaff);
        }

        return $userid;
    }
	
    /**
     * @param  array $_POST data
     * @param  integer ID
     * @return boolean
     * Update supplier informations
     */
    public function update($data, $id, $supplier_request = false)
    {
        if (isset($data['update_all_other_transactions'])) {
            $update_all_other_transactions = true;
            unset($data['update_all_other_transactions']);
        }

        if (isset($data['update_credit_notes'])) {
            $update_credit_notes = true;
            unset($data['update_credit_notes']);
        }

        $affectedRows = 0;
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        if (isset($data['groups_in'])) {
            $groups_in = $data['groups_in'];
            unset($data['groups_in']);
        }

        $data = $this->check_zero_columns($data);

        $_data = do_action('before_supplier_updated', [
            'userid' => $id,
            'data'   => $data,
        ]);

        $data = $_data['data'];
        $this->db->where('userid', $id);
        $this->db->update('tblsuppliers', $data);

        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }

        if (isset($update_all_other_transactions) || isset($update_credit_notes)) {
            $transactions_update = [
                    'billing_street'   => $data['billing_street'],
                    'billing_city'     => $data['billing_city'],
                    'billing_state'    => $data['billing_state'],
                    'billing_zip'      => $data['billing_zip'],
                    'billing_country'  => $data['billing_country'],
                    'shipping_street'  => $data['shipping_street'],
                    'shipping_city'    => $data['shipping_city'],
                    'shipping_state'   => $data['shipping_state'],
                    'shipping_zip'     => $data['shipping_zip'],
                    'shipping_country' => $data['shipping_country'],
                ];
            if (isset($update_all_other_transactions)) {

                // Update all invoices except paid ones.
                $this->db->where('supplierid', $id);
                $this->db->where('status !=', 2);
                $this->db->update('tblinvoices', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }

                // Update all estimates
                $this->db->where('supplierid', $id);
                $this->db->update('tblestimates', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }
            }
            if (isset($update_credit_notes)) {
                $this->db->where('supplierid', $id);
                $this->db->where('status !=', 2);
                $this->db->update('tblcreditnotes', $transactions_update);
                if ($this->db->affected_rows() > 0) {
                    $affectedRows++;
                }
            }
        }

        if (!isset($groups_in)) {
            $groups_in = false;
        }

        if ($this->supplier_groups_model->sync_supplier_groups($id, $groups_in)) {
            $affectedRows++;
        }

        if ($affectedRows > 0) {
            do_action('after_supplier_updated', $id);
            logActivity('Supplier Info Updated [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Update contact data
     * @param  array  $data           $_POST data
     * @param  mixed  $id             contact id
     * @param  boolean $supplier_request is request from suppliers area
     * @return mixed
     */
    public function update_contact($data, $id, $supplier_request = false)
    {
        $affectedRows = 0;
        $contact      = $this->get_contact($id);
        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $this->load->helper('phpass');
            $hasher                       = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password']             = $hasher->HashPassword($data['password']);
            $data['last_password_change'] = date('Y-m-d H:i:s');
        }

        $send_set_password_email = isset($data['send_set_password_email']) ? true : false;
        $set_password_email_sent = false;

        $permissions        = isset($data['permissions']) ? $data['permissions'] : [];
        $data['is_primary'] = isset($data['is_primary']) ? 1 : 0;

        // Contact cant change if is primary or not
        if ($supplier_request == true) {
            unset($data['is_primary']);
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        if ($supplier_request == false) {
            $data['invoice_emails']     = isset($data['invoice_emails']) ? 1 :0;
            $data['estimate_emails']    = isset($data['estimate_emails']) ? 1 :0;
            $data['credit_note_emails'] = isset($data['credit_note_emails']) ? 1 :0;
            $data['contract_emails']    = isset($data['contract_emails']) ? 1 :0;
            $data['task_emails']        = isset($data['task_emails']) ? 1 :0;
            $data['project_emails']     = isset($data['project_emails']) ? 1 :0;
            $data['ticket_emails']      = isset($data['ticket_emails']) ? 1 :0;
        }

        $hook_data = do_action('before_update_contact', ['data' => $data, 'id' => $id]);
        $data      = $hook_data['data'];

        $this->db->where('id', $id);
        $this->db->update('tblcontacts', $data);

        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            if (isset($data['is_primary']) && $data['is_primary'] == 1) {
                $this->db->where('userid', $contact->userid);
                $this->db->where('id !=', $id);
                $this->db->update('tblcontacts', [
                    'is_primary' => 0,
                ]);
            }
        }

        if ($supplier_request == false) {
            $supplier_permissions = $this->roles_model->get_contact_permissions($id);
            if (sizeof($supplier_permissions) > 0) {
                foreach ($supplier_permissions as $supplier_permission) {
                    if (!in_array($supplier_permission['permission_id'], $permissions)) {
                        $this->db->where('userid', $id);
                        $this->db->where('permission_id', $supplier_permission['permission_id']);
                        $this->db->delete('tblcontactpermissions');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
                foreach ($permissions as $permission) {
                    $this->db->where('userid', $id);
                    $this->db->where('permission_id', $permission);
                    $_exists = $this->db->get('tblcontactpermissions')->row();
                    if (!$_exists) {
                        $this->db->insert('tblcontactpermissions', [
                            'userid'        => $id,
                            'permission_id' => $permission,
                        ]);
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                }
            } else {
                foreach ($permissions as $permission) {
                    $this->db->insert('tblcontactpermissions', [
                        'userid'        => $id,
                        'permission_id' => $permission,
                    ]);
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            if ($send_set_password_email) {
                $set_password_email_sent = $this->authentication_model->set_password_email($data['email'], 0);
            }
        }
        if ($affectedRows > 0 && !$set_password_email_sent) {
            logActivity('Contact Updated [ID: ' . $id . ']');

            return true;
        } elseif ($affectedRows > 0 && $set_password_email_sent) {
            return [
                'set_password_email_sent_and_profile_updated' => true,
            ];
        } elseif ($affectedRows == 0 && $set_password_email_sent) {
            return [
                'set_password_email_sent' => true,
            ];
        }

        return false;
    }

    /**
     * Add new contact
     * @param array  $data               $_POST data
     * @param mixed  $supplier_id        supplier id
     * @param boolean $not_manual_request is manual from admin area supplier profile or register, convert to lead
     */
    public function add_contact($data, $supplier_id, $not_manual_request = false)
    {
        $send_set_password_email = isset($data['send_set_password_email']) ? true : false;

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        if (isset($data['permissions'])) {
            $permissions = $data['permissions'];
            unset($data['permissions']);
        }

        $send_welcome_email = true;
        if (isset($data['donotsendwelcomeemail'])) {
            $send_welcome_email = false;
        } elseif (strpos($_SERVER['HTTP_REFERER'], 'register') !== false) {
            $send_welcome_email = true;

            // Do not send welcome email if confirmation for registration is enabled
            if (get_option('suppliers_register_require_confirmation') == '1') {
                $send_welcome_email = false;
            }
            // If supplier register set this auto contact as primary
            $data['is_primary'] = 1;
        }

        if (isset($data['is_primary'])) {
            $data['is_primary'] = 1;
            $this->db->where('userid', $supplier_id);
            $this->db->update('tblcontacts', [
                'is_primary' => 0,
            ]);
        } else {
            $data['is_primary'] = 0;
        }

        $password_before_hash = '';
        $data['userid']       = $supplier_id;
        if (isset($data['password'])) {
            $password_before_hash = $data['password'];
            $this->load->helper('phpass');
            $hasher           = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $data['password'] = $hasher->HashPassword($data['password']);
        }

        $data['datecreated'] = date('Y-m-d H:i:s');

        if (!$not_manual_request) {
            $data['invoice_emails']     = isset($data['invoice_emails']) ? 1 :0;
            $data['estimate_emails']    = isset($data['estimate_emails']) ? 1 :0;
            $data['credit_note_emails'] = isset($data['credit_note_emails']) ? 1 :0;
            $data['contract_emails']    = isset($data['contract_emails']) ? 1 :0;
            $data['task_emails']        = isset($data['task_emails']) ? 1 :0;
            $data['project_emails']     = isset($data['project_emails']) ? 1 :0;
            $data['ticket_emails']      = isset($data['ticket_emails']) ? 1 :0;
        }

        $hook_data = [
            'data'               => $data,
            'not_manual_request' => $not_manual_request,
        ];

        $hook_data = do_action('before_create_contact', $hook_data);
        $data      = $hook_data['data'];

        $data['email'] = trim($data['email']);

        $this->db->insert('tblcontacts', $data);
        $contact_id = $this->db->insert_id();

        if ($contact_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($contact_id, $custom_fields);
            }
            // request from admin area
            if (!isset($permissions) && $not_manual_request == false) {
                $permissions = [];
            } elseif ($not_manual_request == true) {
                $permissions         = [];
                $_permissions        = get_contact_permissions();
                $default_permissions = @unserialize(get_option('default_contact_permissions'));
                if (is_array($default_permissions)) {
                    foreach ($_permissions as $permission) {
                        if (in_array($permission['id'], $default_permissions)) {
                            array_push($permissions, $permission['id']);
                        }
                    }
                }
            }

            if ($not_manual_request == true) {
                // update all email notifications to 0
                $this->db->where('id', $contact_id);
                $this->db->update('tblcontacts', [
                    'invoice_emails'     => 0,
                    'estimate_emails'    => 0,
                    'credit_note_emails' => 0,
                    'contract_emails'    => 0,
                    'task_emails'        => 0,
                    'project_emails'     => 0,
                    'ticket_emails'      => 0,
                ]);
            }
            foreach ($permissions as $permission) {
                $this->db->insert('tblcontactpermissions', [
                    'userid'        => $contact_id,
                    'permission_id' => $permission,
                ]);

                // Auto set email notifications based on permissions
                if ($not_manual_request == true) {
                    if ($permission == 6) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', ['project_emails' => 1, 'task_emails' => 1]);
                    } elseif ($permission == 3) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', ['contract_emails' => 1]);
                    } elseif ($permission == 2) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', ['estimate_emails' => 1]);
                    } elseif ($permission == 1) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', ['invoice_emails' => 1, 'credit_note_emails' => 1]);
                    } elseif ($permission == 5) {
                        $this->db->where('id', $contact_id);
                        $this->db->update('tblcontacts', ['ticket_emails' => 1]);
                    }
                }
            }

            $lastAnnouncement = $this->db->query('SELECT announcementid FROM tblannouncements WHERE showtousers = 1 AND announcementid = (SELECT MAX(announcementid) FROM tblannouncements)')->row();
            if ($lastAnnouncement) {
                // Get all announcements and set it to read.
                $this->db->select('announcementid')
                ->from('tblannouncements')
                ->where('showtousers', 1)
                ->where('announcementid !=', $lastAnnouncement->announcementid);

                $announcements = $this->db->get()->result_array();
                foreach ($announcements as $announcement) {
                    $this->db->insert('tbldismissedannouncements', [
                        'announcementid' => $announcement['announcementid'],
                        'staff'          => 0,
                        'userid'         => $contact_id,
                    ]);
                }
            }


            if ($send_welcome_email == true) {
                $this->load->model('emails_model');
                $merge_fields = [];
                $merge_fields = array_merge($merge_fields, get_supplier_contact_merge_fields($data['userid'], $contact_id, $password_before_hash));
                $this->emails_model->send_email_template('new-supplier-created', $data['email'], $merge_fields);
            }

            if ($send_set_password_email) {
                $this->authentication_model->set_password_email($data['email'], 0);
            }

            logActivity('Contact Created [ID: ' . $contact_id . ']');
            do_action('contact_created', $contact_id);

            return $contact_id;
        }

        return false;
    }

    /**
     * Used to update company details from suppliers area
     * @param  array $data $_POST data
     * @param  mixed $id
     * @return boolean
     */
    public function update_company_details($data, $id)
    {
        $affectedRows = 0;
        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }
        if (isset($data['country']) && $data['country'] == '' || !isset($data['country'])) {
            $data['country'] = 0;
        }
        if (isset($data['billing_country']) && $data['billing_country'] == '') {
            $data['billing_country'] = 0;
        }
        if (isset($data['shipping_country']) && $data['shipping_country'] == '') {
            $data['shipping_country'] = 0;
        }

        // From v.1.9.4 these fields are textareas
        $data['address'] = trim($data['address']);
        $data['address'] = nl2br($data['address']);
        if (isset($data['billing_street'])) {
            $data['billing_street'] = trim($data['billing_street']);
            $data['billing_street'] = nl2br($data['billing_street']);
        }
        if (isset($data['shipping_street'])) {
            $data['shipping_street'] = trim($data['shipping_street']);
            $data['shipping_street'] = nl2br($data['shipping_street']);
        }

        $this->db->where('userid', $id);
        $this->db->update('tblsuppliers', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        if ($affectedRows > 0) {
            do_action('supplier_updated_company_info', $id);
            logActivity('Supplier Info Updated From Clients Area [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Get supplier staff members that are added as supplier admins
     * @param  mixed $id supplier id
     * @return array
     */
    public function get_admins($id)
    {
        $this->db->where('supplier_id', $id);

        return $this->db->get('tblsupplieradmins')->result_array();
    }

    /**
     * Get unique staff id's of supplier admins
     * @return array
     */
    public function get_suppliers_admin_unique_ids()
    {
        return $this->db->query('SELECT DISTINCT(staff_id) FROM tblsupplieradmins')->result_array();
    }

    /**
     * Assign staff members as admin to suppliers
     * @param  array $data $_POST data
     * @param  mixed $id   supplier id
     * @return boolean
     */
    public function assign_admins($data, $id)
    {
        $affectedRows = 0;

        if (count($data) == 0) {
            $this->db->where('supplier_id', $id);
            $this->db->delete('tblsupplieradmins');
            if ($this->db->affected_rows() > 0) {
                $affectedRows++;
            }
        } else {
            $current_admins     = $this->get_admins($id);
            $current_admins_ids = [];
            foreach ($current_admins as $c_admin) {
                array_push($current_admins_ids, $c_admin['staff_id']);
            }
            foreach ($current_admins_ids as $c_admin_id) {
                if (!in_array($c_admin_id, $data['supplier_admins'])) {
                    $this->db->where('staff_id', $c_admin_id);
                    $this->db->where('supplier_id', $id);
                    $this->db->delete('tblsupplieradmins');
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            foreach ($data['supplier_admins'] as $n_admin_id) {
                if (total_rows('tblsupplieradmins', [
                    'supplier_id' => $id,
                    'staff_id' => $n_admin_id,
                ]) == 0) {
                    $this->db->insert('tblsupplieradmins', [
                        'supplier_id'   => $id,
                        'staff_id'      => $n_admin_id,
                        'date_assigned' => date('Y-m-d H:i:s'),
                    ]);
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
        }
        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param  integer ID
     * @return boolean
     * Delete supplier, also deleting rows from, dismissed supplier announcements, ticket replies, tickets, autologin, user notes
     */
    public function delete($id)
    {
        $affectedRows = 0;

        if (!is_gdpr() && is_reference_in_table('supplierid', 'tblinvoices', $id)) {
            return [
                'referenced' => true,
            ];
        }

        if (!is_gdpr() && is_reference_in_table('supplierid', 'tblestimates', $id)) {
            return [
                'referenced' => true,
            ];
        }

        if (!is_gdpr() && is_reference_in_table('supplierid', 'tblcreditnotes', $id)) {
            return [
                'referenced' => true,
            ];
        }

        do_action('before_supplier_deleted', $id);

        $last_activity = get_last_system_activity_id();
        $company       = get_company_name($id);

        $this->db->where('userid', $id);
        $this->db->delete('tblsuppliers');
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            // Delete all user contacts
            $this->db->where('userid', $id);
            $contacts = $this->db->get('tblcontacts')->result_array();
            foreach ($contacts as $contact) {
                $this->delete_contact($contact['id']);
            }

            // Delete all tickets start here
            $this->db->where('userid', $id);
            $tickets = $this->db->get('tbltickets')->result_array();
            $this->load->model('tickets_model');
            foreach ($tickets as $ticket) {
                $this->tickets_model->delete($ticket['ticketid']);
            }

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'supplier');
            $this->db->delete('tblnotes');

            if (is_gdpr() && get_option('gdpr_on_forgotten_remove_invoices_credit_notes') == '1') {
                $this->load->model('invoices_model');
                $this->db->where('supplierid', $id);
                $invoices = $this->db->get('tblinvoices')->result_array();
                foreach ($invoices as $invoice) {
                    $this->invoices_model->delete($invoice['id'], true);
                }

                $this->load->model('credit_notes_model');
                $this->db->where('supplierid', $id);
                $credit_notes = $this->db->get('tblcreditnotes')->result_array();
                foreach ($credit_notes as $credit_note) {
                    $this->credit_notes_model->delete($credit_note['id'], true);
                }
            } elseif (is_gdpr()) {
                $this->db->where('supplierid', $id);
                $this->db->update('tblinvoices', ['deleted_supplier_name' => $company]);

                $this->db->where('supplierid', $id);
                $this->db->update('tblcreditnotes', ['deleted_supplier_name' => $company]);
            }

            $this->db->where('supplierid', $id);
            $this->db->update('tblcreditnotes', [
                'supplierid'   => 0,
                'project_id' => 0,
            ]);

            $this->db->where('supplierid', $id);
            $this->db->update('tblinvoices', [
                'supplierid'                 => 0,
                'recurring'                => 0,
                'recurring_type'           => null,
                'custom_recurring'         => 0,
                'cycles'                   => 0,
                'last_recurring_date'      => null,
                'project_id'               => 0,
                'subscription_id'          => 0,
                'cancel_overdue_reminders' => 1,
                'last_overdue_reminder'    => null,
            ]);

            if (is_gdpr() && get_option('gdpr_on_forgotten_remove_estimates') == '1') {
                $this->load->model('estimates_model');
                $this->db->where('supplierid', $id);
                $estimates = $this->db->get('tblestimates')->result_array();
                foreach ($estimates as $estimate) {
                    $this->estimates_model->delete($estimate['id'], true);
                }
            } elseif (is_gdpr()) {
                $this->db->where('supplierid', $id);
                $this->db->update('tblestimates', ['deleted_supplier_name' => $company]);
            }

            $this->db->where('supplierid', $id);
            $this->db->update('tblestimates', [
                'supplierid'           => 0,
                'project_id'         => 0,
                'is_expiry_notified' => 1,
            ]);

            $this->load->model('subscriptions_model');
            $this->db->where('supplierid', $id);
            $subscriptions = $this->db->get('tblsubscriptions')->result_array();
            foreach ($subscriptions as $subscription) {
                $this->subscriptions_model->delete($subscription['id'], true);
            }
            // Get all supplier contracts
            $this->load->model('contracts_model');
            $this->db->where('supplier', $id);
            $contracts = $this->db->get('tblcontracts')->result_array();
            foreach ($contracts as $contract) {
                $this->contracts_model->delete($contract['id']);
            }
            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'suppliers');
            $this->db->delete('tblcustomfieldsvalues');

            // Get supplier related tasks
            $this->db->where('rel_type', 'supplier');
            $this->db->where('rel_id', $id);
            $tasks = $this->db->get('tblstafftasks')->result_array();

            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id'], false);
            }

            $this->db->where('rel_type', 'supplier');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblreminders');

            $this->db->where('supplier_id', $id);
            $this->db->delete('tblsupplieradmins');

            $this->db->where('supplier_id', $id);
            $this->db->delete('tblvault');

            $this->db->where('supplier_id', $id);
            $this->db->delete('tblsuppliergroups_in');

            $this->load->model('proposals_model');
            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'supplier');
            $proposals = $this->db->get('tblproposals')->result_array();
            foreach ($proposals as $proposal) {
                $this->proposals_model->delete($proposal['id']);
            }
			
			$this->load->model('actions_model');
            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'supplier');
            $actions = $this->db->get('tblactions')->result_array();
            foreach ($actions as $action) {
                $this->proposals_model->delete($action['id']);
            }
			
			
            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'supplier');
            $attachments = $this->db->get('tblfiles')->result_array();
            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            $this->db->where('supplierid', $id);
            $expenses = $this->db->get('tblexpenses')->result_array();

            $this->load->model('expenses_model');
            foreach ($expenses as $expense) {
                $this->expenses_model->delete($expense['id'], true);
            }

            $this->db->where('supplier_id', $id);
            $this->db->delete('tblusermeta');

            $this->db->where('supplier_id', $id);
            $this->db->update('tblleads', ['supplier_id' => 0]);

            // Delete all projects
            $this->load->model('projects_model');
            $this->db->where('supplierid', $id);
            $projects = $this->db->get('tblprojects')->result_array();
            foreach ($projects as $project) {
                $this->projects_model->delete($project['id']);
            }
        }
        if ($affectedRows > 0) {
            do_action('after_supplier_deleted', $id);

            // Delete activity log caused by delete supplier function
            if ($last_activity) {
                $this->db->where('id >', $last_activity->id);
                $this->db->delete('tblactivitylog');
            }

            logActivity('Client Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete supplier contact
     * @param  mixed $id contact id
     * @return boolean
     */
    public function delete_contact($id)
    {
        $this->db->where('id', $id);
        $result      = $this->db->get('tblcontacts')->row();
        $supplier_id = $result->userid;

        do_action('before_delete_contact', $id);

        $last_activity = get_last_system_activity_id();

        $this->db->where('id', $id);
        $this->db->delete('tblcontacts');

        if ($this->db->affected_rows() > 0) {
            if (is_dir(get_upload_path_by_type('contact_profile_images') . $id)) {
                delete_dir(get_upload_path_by_type('contact_profile_images') . $id);
            }

            $this->db->where('contact_id', $id);
            $this->db->delete('tblconsents');

            $this->db->where('contact_id', $id);
            $this->db->delete('tblsupplierfiles_shares');

            $this->db->where('userid', $id);
            $this->db->where('staff', 0);
            $this->db->delete('tbldismissedannouncements');

            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'contacts');
            $this->db->delete('tblcustomfieldsvalues');

            $this->db->where('userid', $id);
            $this->db->delete('tblcontactpermissions');

            $this->db->where('user_id', $id);
            $this->db->where('staff', 0);
            $this->db->delete('tbluserautologin');

            $this->db->select('ticketid');
            $this->db->where('contactid', $id);
            $this->db->where('userid', $supplier_id);
            $tickets = $this->db->get('tbltickets')->result_array();

            $this->load->model('tickets_model');
            foreach ($tickets as $ticket) {
                $this->tickets_model->delete($ticket['ticketid']);
            }

            $this->load->model('tasks_model');

            $this->db->where('addedfrom', $id);
            $this->db->where('is_added_from_contact', 1);
            $tasks = $this->db->get('tblstafftasks')->result_array();

            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id'], false);
            }

            // Added from contact in supplier profile
            $this->db->where('contact_id', $id);
            $this->db->where('rel_type', 'supplier');
            $attachments = $this->db->get('tblfiles')->result_array();

            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            // Remove contact files uploaded to tasks
            $this->db->where('rel_type', 'task');
            $this->db->where('contact_id', $id);
            $filesUploadedFromContactToTasks = $this->db->get('tblfiles')->result_array();

            foreach ($filesUploadedFromContactToTasks as $file) {
                $this->tasks_model->remove_task_attachment($file['id']);
            }

            $this->db->where('contact_id', $id);
            $tasksComments = $this->db->get('tblstafftaskcomments')->result_array();
            foreach ($tasksComments as $comment) {
                $this->tasks_model->remove_comment($comment['id'], true);
            }

            $this->load->model('projects_model');

            $this->db->where('contact_id', $id);
            $files = $this->db->get('tblprojectfiles')->result_array();
            foreach ($files as $file) {
                $this->projects_model->remove_file($file['id'], false);
            }

            $this->db->where('contact_id', $id);
            $discussions = $this->db->get('tblprojectdiscussions')->result_array();
            foreach ($discussions as $discussion) {
                $this->projects_model->delete_discussion($discussion['id'], false);
            }

            $this->db->where('contact_id', $id);
            $discussionsComments = $this->db->get('tblprojectdiscussioncomments')->result_array();
            foreach ($discussionsComments as $comment) {
                $this->projects_model->delete_discussion_comment($comment['id'], false);
            }

            $this->db->where('contact_id', $id);
            $this->db->delete('tblusermeta');

            $this->db->where('(email="' . $result->email . '" OR bcc LIKE "%' . $result->email . '%" OR cc LIKE "%' . $result->email . '%")');
            $this->db->delete('tblemailqueue');

            $this->db->where('email', $result->email);
            $this->db->delete('tblsurveysemailsendcron');

            if (is_gdpr()) {
                $this->db->where('email', $result->email);
                $this->db->delete('tbllistemails');

                if (!empty($result->last_ip)) {
                    $this->db->where('ip', $result->last_ip);
                    $this->db->delete('tblknowledgebasearticleanswers');

                    $this->db->where('ip', $result->last_ip);
                    $this->db->delete('tblsurveyresultsets');
                }

                $this->db->where('email', $result->email);
                $this->db->delete('tblticketpipelog');

                $this->db->where('email', $result->email);
                $this->db->delete('tblemailstracking');

                $this->db->where('contact_id', $id);
                $this->db->delete('tblprojectactivity');

                $this->db->where('(additional_data LIKE "%' . $result->email . '%" OR full_name LIKE "%' . $result->firstname . ' ' . $result->lastname . '%")');
                $this->db->where('additional_data != "" AND additional_data IS NOT NULL');
                $this->db->delete('tblsalesactivity');

                $whereActivityLog = '(description LIKE "%' . $result->email . '%" OR description LIKE "%' . $result->firstname . ' ' . $result->lastname . '%" OR description LIKE "%' . $result->firstname . '%" OR description LIKE "%' . $result->lastname . '%" OR description LIKE "%' . $result->phonenumber . '%"';
                if (!empty($result->last_ip)) {
                    $whereActivityLog .= ' OR description LIKE "%' . $result->last_ip . '%"';
                }
                $whereActivityLog .= ')';
                $this->db->where($whereActivityLog);
                $this->db->delete('tblactivitylog');
            }



            // Delete activity log caused by delete contact function
            if ($last_activity) {
                $this->db->where('id >', $last_activity->id);
                $this->db->delete('tblactivitylog');
            }

            return true;
        }

        return false;
    }

    /**
     * Get supplier default currency
     * @param  mixed $id supplier id
     * @return mixed
     */
    public function get_supplier_default_currency($id)
    {
        $this->db->select('default_currency');
        $this->db->where('userid', $id);
        $result = $this->db->get('tblsuppliers')->row();
        if ($result) {
            return $result->default_currency;
        }

        return false;
    }

    /**
     *  Get supplier billing details
     * @param   mixed $id   supplier id
     * @return  array
     */
    public function get_supplier_billing_and_shipping_details($id)
    {
        $this->db->select('billing_street,billing_city,billing_state,billing_zip,billing_country,shipping_street,shipping_city,shipping_state,shipping_zip,shipping_country');
        $this->db->from('tblsuppliers');
        $this->db->where('userid', $id);

        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            $result[0]['billing_street']  = clear_textarea_breaks($result[0]['billing_street']);
            $result[0]['shipping_street'] = clear_textarea_breaks($result[0]['shipping_street']);
        }

        return $result;
    }

    /**
     * Get supplier files uploaded in the supplier profile
     * @param  mixed $id    supplier id
     * @param  array  $where perform where
     * @return array
     */
    public function get_supplier_files($id, $where = [])
    {
        $this->db->where($where);
        $this->db->where('rel_id', $id);
        $this->db->where('rel_type', 'supplier');
        $this->db->order_by('dateadded', 'desc');

        return $this->db->get('tblfiles')->result_array();
    }

    /**
     * Delete supplier attachment uploaded from the supplier profile
     * @param  mixed $id attachment id
     * @return boolean
     */
    public function delete_attachment($id)
    {
        $this->db->where('id', $id);
        $attachment = $this->db->get('tblfiles')->row();
        $deleted    = false;
        if ($attachment) {
            if (empty($attachment->external)) {
                $relPath  = get_upload_path_by_type('supplier') . $attachment->rel_id . '/';
                $fullPath = $relPath . $attachment->file_name;
                unlink($fullPath);
                $fname     = pathinfo($fullPath, PATHINFO_FILENAME);
                $fext      = pathinfo($fullPath, PATHINFO_EXTENSION);
                $thumbPath = $relPath . $fname . '_thumb.' . $fext;
                if (file_exists($thumbPath)) {
                    unlink($thumbPath);
                }
            }

            $this->db->where('id', $id);
            $this->db->delete('tblfiles');
            if ($this->db->affected_rows() > 0) {
                $deleted = true;
                $this->db->where('file_id', $id);
                $this->db->delete('tblsupplierfiles_shares');
                logActivity('Supplier Attachment Deleted [ID: ' . $attachment->rel_id . ']');
            }

            if (is_dir(get_upload_path_by_type('supplier') . $attachment->rel_id)) {
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('supplier') . $attachment->rel_id);
                if (count($other_attachments) == 0) {
                    delete_dir(get_upload_path_by_type('supplier') . $attachment->rel_id);
                }
            }
        }

        return $deleted;
    }

    /**
     * @param  integer ID
     * @param  integer Status ID
     * @return boolean
     * Update contact status Active/Inactive
     */
    public function change_contact_status($id, $status)
    {
        $hook_data['id']     = $id;
        $hook_data['status'] = $status;
        $hook_data           = do_action('change_contact_status', $hook_data);
        $status              = $hook_data['status'];
        $id                  = $hook_data['id'];
        $this->db->where('id', $id);
        $this->db->update('tblcontacts', [
            'active' => $status,
        ]);
        if ($this->db->affected_rows() > 0) {
            logActivity('Contact Status Changed [ContactID: ' . $id . ' Status(Active/Inactive): ' . $status . ']');

            return true;
        }

        return false;
    }

    /**
     * @param  integer ID
     * @param  integer Status ID
     * @return boolean
     * Update supplier status Active/Inactive
     */
    public function change_supplier_status($id, $status)
    {
        $this->db->where('userid', $id);
        $this->db->update('tblsuppliers', [
            'active' => $status,
        ]);

        if ($this->db->affected_rows() > 0) {
            logActivity('Supplier Status Changed [ID: ' . $id . ' Status(Active/Inactive): ' . $status . ']');

            return true;
        }

        return false;
    }

    /**
     * @param  mixed $_POST data
     * @return mixed
     * Change contact password, used from supplier area
     */
    public function change_contact_password($data)
    {
        $hook_data['data'] = $data;
        $hook_data         = do_action('before_contact_change_password', $hook_data);
        $data              = $hook_data['data'];

        // Get current password
        $this->db->where('id', get_contact_user_id());
        $supplier = $this->db->get('tblcontacts')->row();
        $this->load->helper('phpass');
        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        if (!$hasher->CheckPassword($data['oldpassword'], $supplier->password)) {
            return [
                'old_password_not_match' => true,
            ];
        }
        $update_data['password']             = $hasher->HashPassword($data['newpasswordr']);
        $update_data['last_password_change'] = date('Y-m-d H:i:s');
        $this->db->where('id', get_contact_user_id());
        $this->db->update('tblcontacts', $update_data);
        if ($this->db->affected_rows() > 0) {
            logActivity('Contact Password Changed [ContactID: ' . get_contact_user_id() . ']');

            return true;
        }

        return false;
    }

    /**
     * Get supplier groups where supplier belongs
     * @param  mixed $id supplier id
     * @return array
     */
    public function get_supplier_groups($id)
    {
        return $this->supplier_groups_model->get_supplier_groups($id);
    }

    /**
     * Get all supplier groups
     * @param  string $id
     * @return mixed
     */
    public function get_groups($id = '')
    {
        return $this->supplier_groups_model->get_groups($id);
    }

    /**
     * Delete supplier groups
     * @param  mixed $id group id
     * @return boolean
     */
    public function delete_group($id)
    {
        return $this->supplier_groups_model->delete($id);
    }

    /**
     * Add new supplier groups
     * @param array $data $_POST data
     */
    public function add_group($data)
    {
        return $this->supplier_groups_model->add($data);
    }

    /**
     * Edit supplier group
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit_group($data)
    {
        return $this->supplier_groups_model->edit($data);
    }

    /**
    * Create new vault entry
    * @param  array $data        $_POST data
    * @param  mixed $supplier_id supplier id
    * @return boolean
    */
    public function vault_entry_create($data, $supplier_id)
    {
        return $this->supplier_vault_entries_model->create($data, $supplier_id);
    }

    /**
     * Update vault entry
     * @param  mixed $id   vault entry id
     * @param  array $data $_POST data
     * @return boolean
     */
    public function vault_entry_update($id, $data)
    {
        return $this->supplier_vault_entries_model->update($id, $data);
    }

    /**
     * Delete vault entry
     * @param  mixed $id entry id
     * @return boolean
     */
    public function vault_entry_delete($id)
    {
        return $this->supplier_vault_entries_model->delete($id);
    }

    /**
     * Get supplier vault entries
     * @param  mixed $supplier_id
     * @param  array  $where       additional wher
     * @return array
     */
    public function get_vault_entries($supplier_id, $where = [])
    {
        return $this->supplier_vault_entries_model->get_by_supplier_id($supplier_id, $where);
    }

    /**
     * Get single vault entry
     * @param  mixed $id vault entry id
     * @return object
     */
    public function get_vault_entry($id)
    {
        return $this->supplier_vault_entries_model->get($id);
    }

    /**
    * Get supplier statement formatted
    * @param  mixed $supplier_id supplier id
    * @param  string $from        date from
    * @param  string $to          date to
    * @return array
    */
    public function get_statement($supplier_id, $from, $to)
    {
        return $this->statement_model->get_statement($supplier_id, $from, $to);
    }

    /**
    * Send supplier statement to email
    * @param  mixed $supplier_id supplier id
    * @param  array $send_to     array of contact emails to send
    * @param  string $from        date from
    * @param  string $to          date to
    * @param  string $cc          email CC
    * @return boolean
    */
    public function send_statement_to_email($supplier_id, $send_to, $from, $to, $cc = '')
    {
        return $this->statement_model->send_statement_to_email($supplier_id, $send_to, $from, $to, $cc);
    }

    /**
     * When supplier register, mark the contact and the supplier as inactive and set the registration_confirmed field to 0
     * @param  mixed $supplier_id  the supplier id
     * @return boolean
     */
    public function require_confirmation($supplier_id)
    {
        $contact_id = get_primary_contact_user_id($supplier_id);
        $this->db->where('userid', $supplier_id);
        $this->db->update('tblsuppliers', ['active' => 0, 'registration_confirmed' => 0]);

        $this->db->where('id', $contact_id);
        $this->db->update('tblcontacts', ['active' => 0]);

        return true;
    }

    public function confirm_registration($supplier_id)
    {
        $contact_id = get_primary_contact_user_id($supplier_id);
        $this->db->where('userid', $supplier_id);
        $this->db->update('tblsuppliers', ['active' => 1, 'registration_confirmed' => 1]);

        $this->db->where('id', $contact_id);
        $this->db->update('tblcontacts', ['active' => 1]);

        $this->db->where('id', $contact_id);
        $contact = $this->db->get('tblcontacts')->row();

        if ($contact) {
            $this->load->model('emails_model');
            $merge_fields = [];
            $merge_fields = array_merge($merge_fields, get_supplier_contact_merge_fields($supplier_id, $contact_id));
            $this->emails_model->send_email_template('supplier-registration-confirmed', $contact->email, $merge_fields);

            return true;
        }

        return false;
    }

    public function get_suppliers_distinct_countries()
    {
        return $this->db->query('SELECT DISTINCT(country_id), short_name FROM tblsuppliers JOIN tblcountries ON tblcountries.country_id=tblsuppliers.country')->result_array();
    }

    private function check_zero_columns($data)
    {
        if (!isset($data['show_primary_contact'])) {
            $data['show_primary_contact'] = 0;
        }

        if (isset($data['default_currency']) && $data['default_currency'] == '' || !isset($data['default_currency'])) {
            $data['default_currency'] = 0;
        }

        if (isset($data['country']) && $data['country'] == '' || !isset($data['country'])) {
            $data['country'] = 0;
        }

        if (isset($data['billing_country']) && $data['billing_country'] == '' || !isset($data['billing_country'])) {
            $data['billing_country'] = 0;
        }

        if (isset($data['shipping_country']) && $data['shipping_country'] == '' || !isset($data['shipping_country'])) {
            $data['shipping_country'] = 0;
        }

        return $data;
    }
}
