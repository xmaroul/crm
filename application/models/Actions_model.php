<?php

defined('BASEPATH') or exit('No direct script access allowed');
class	Actions_model extends CRM_Model
{
    private $statuses;

    private $copy = false;

    public function __construct()
    {
        parent::__construct();
        $this->statuses = do_action('before_set_action_statuses', [
            1,
            2,
            3,
			4,
        ]);
    }

    public function get_statuses()
    {
        return $this->statuses;
    }

    public function get_sale_agents()
    {
        return $this->db->query('SELECT DISTINCT(assigned) as sale_agent FROM tblactions WHERE assigned != 0')->result_array();
    }

    public function get_actions_years()
    {
        return $this->db->query('SELECT DISTINCT(YEAR(date)) as year FROM tblactions')->result_array();
    }

    public function do_kanban_query($status, $search = '', $page = 1, $sort = [], $count = false)
    {
		$default_pipeline_order      = get_option('default_actions_pipeline_sort');
        $default_pipeline_order_type = get_option('default_actions_pipeline_sort_type');
        $limit                       = get_option('actions_pipeline_limit');

        $has_permission_view                 = has_permission('actions', '', 'view');
        $has_permission_view_own             = has_permission('actions', '', 'view_own');
        $allow_staff_view_actions_assigned = get_option('allow_staff_view_actions_assigned');
        $staffId                             = get_staff_user_id();

        $this->db->select('id,invoice_id,estimate_id,subject,rel_type,rel_id,total,date,open_till,currency,action_to,status');
        $this->db->from('tblactions');
        $this->db->where('status', $status);
        if (!$has_permission_view) {
            $this->db->where(get_actions_sql_where_staff(get_staff_user_id()));
        }
        if ($search != '') {
            if (!_startsWith($search, '#')) {
                $this->db->where('(
                phone LIKE "%' . $search . '%"
                OR
                zip LIKE "%' . $search . '%"
                OR
                content LIKE "%' . $search . '%"
                OR
                state LIKE "%' . $search . '%"
                OR
                city LIKE "%' . $search . '%"
                OR
                email LIKE "%' . $search . '%"
                OR
                address LIKE "%' . $search . '%"
                OR
                action_to LIKE "%' . $search . '%"
                OR
                total LIKE "%' . $search . '%"
                OR
                subject LIKE "%' . $search . '%")');
            } else {
                $this->db->where('tblactions.id IN
                (SELECT rel_id FROM tbltags_in WHERE tag_id IN
                (SELECT id FROM tbltags WHERE name="' . strafter($search, '#') . '")
                AND tbltags_in.rel_type=\'action\' GROUP BY rel_id HAVING COUNT(tag_id) = 1)
                ');
            }
        }

        if (isset($sort['sort_by']) && $sort['sort_by'] && isset($sort['sort']) && $sort['sort']) {
            $this->db->order_by($sort['sort_by'], $sort['sort']);
        } else {
            $this->db->order_by($default_pipeline_order, $default_pipeline_order_type);
        }

        if ($count == false) {
            if ($page > 1) {
                $page--;
                $position = ($page * $limit);
                $this->db->limit($limit, $position);
            } else {
                $this->db->limit($limit);
            }
        }

        if ($count == false) {
            return $this->db->get()->result_array();
        }

        return $this->db->count_all_results();
    }

    /**
     * Inserting new action function
     * @param mixed $data $_POST data
     */
    public function add($data)
    {
        $data['allow_comments'] = isset($data['allow_comments']) ? 1 : 0;

        $save_and_send = isset($data['save_and_send']);

        $tags = isset($data['tags']) ? $data['tags'] : '';

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

//        $data['address'] = trim($data['address']);
//        $data['address'] = nl2br($data['address']);

        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['addedfrom']   = get_staff_user_id();
        $data['hash']        = app_generate_hash();

        if (empty($data['rel_type'])) {
            unset($data['rel_type']);
            unset($data['rel_id']);
        } else {
            if (empty($data['rel_id'])) {
                unset($data['rel_type']);
                unset($data['rel_id']);
            }
        }

        $items = [];
        if (isset($data['newitems'])) {
            $items = $data['newitems'];
            unset($data['newitems']);
        }

        if (isset($data['assigned'])){
            $action_members = $data['assigned'];
            unset($data['assigned']);
        }
        
        
        
        if ($this->copy == false) {
            $data['content'] = '{action_items}';
        }

        $hook_data = do_action('before_create_action', [
            'data'  => $data,
            'items' => $items,
        ]);

        $data  = $hook_data['data'];
        $items = $hook_data['items'];

        $this->db->insert('tblactions', $data);
        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            if (isset($action_members)) {
                $_pm['action_members'] = $action_members;
            }
            $this->add_edit_members($_pm, $insert_id);
               
            handle_tags_save($tags, $insert_id, 'action');

            foreach ($items as $key => $item) {
                if ($itemid = add_new_sales_item_post($item, $insert_id, 'action')) {
                    _maybe_insert_post_item_tax($itemid, $item, $insert_id, 'action');
                }
            }

            $action = $this->get($insert_id);
            foreach ($action->assigned as $assigned){
                 if ($assigned['staff_id'] != get_staff_user_id()) {
                    $notified = add_notification([
                        'description'     => 'not_action_assigned_to_you',
                        'touserid'        => $assigned['staff_id'],
                        'fromuserid'      => get_staff_user_id(),
                        'link'            => 'actions/list_actions/' . $insert_id,
                        'additional_data' => serialize([
                            $action->subject,
                        ]),
                    ]);
                    if ($notified) {
                        pusher_trigger_notification([$action->assigned]);
                    }
                }
            }

            if ($data['rel_type'] == 'lead') {
                $this->load->model('leads_model');
                $this->leads_model->log_lead_activity($data['rel_id'], 'not_lead_activity_created_action', false, serialize([
                    '<a href="' . admin_url('actions/list_actions/' . $insert_id) . '" target="_blank">' . $data['subject'] . '</a>',
                ]));
            }

            update_sales_total_tax_column($insert_id, 'action', 'tblactions');
            logActivity('New Action Created [ID:' . $insert_id . ']');

            if ($save_and_send === true) {
                $this->send_action_to_email($insert_id, 'action-send-to-customer', true);
            }

            do_action('action_created', $insert_id);

            return $insert_id;
        }

        return false;
    }

    /**
     * Update action
     * @param  mixed $data $_POST data
     * @param  mixed $id   action id
     * @return boolean
     */
    public function update($data, $id)
    {
        $affectedRows = 0;

        $data['allow_comments'] = isset($data['allow_comments']) ? 1 : 0;

        $current_action = $this->get($id);

        $save_and_send = isset($data['save_and_send']);

        if (empty($data['rel_type'])) {
            $data['rel_id']   = null;
            $data['rel_type'] = '';
        } else {
            if (empty($data['rel_id'])) {
                $data['rel_id']   = null;
                $data['rel_type'] = '';
            }
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        $items = [];
        if (isset($data['items'])) {
            $items = $data['items'];
            unset($data['items']);
        }

        $newitems = [];
        if (isset($data['newitems'])) {
            $newitems = $data['newitems'];
            unset($data['newitems']);
        }

        if (isset($data['tags'])) {
            if (handle_tags_save($data['tags'], $id, 'action')) {
                $affectedRows++;
            }
        }

//        $data['address'] = trim($data['address']);
//        $data['address'] = nl2br($data['address']);

        $hook_data = do_action('before_action_updated', [
            'data'          => $data,
            'id'            => $id,
            'items'         => $items,
            'newitems'      => $newitems,
            'removed_items' => isset($data['removed_items']) ? $data['removed_items'] : [],
        ]);

        $data                  = $hook_data['data'];
        $data['removed_items'] = $hook_data['removed_items'];
        $newitems              = $hook_data['newitems'];
        $items                 = $hook_data['items'];

        // Delete items checked to be removed from database
        foreach ($data['removed_items'] as $remove_item_id) {
            if (handle_removed_sales_item_post($remove_item_id, 'action')) {
                $affectedRows++;
            }
        }

        unset($data['removed_items']);

        if (isset($data['assigned'])){
            $action_members = $data['assigned'];
            unset($data['assigned']);
        }
        $_am = [];
        if (isset($action_members)) {
            $_pm['action_members'] = $action_members;
        }
        if ($this->add_edit_members($_pm, $id)) {
            $affectedRows++;
        }
        $data['subject'] = $this->input->post('subject', FALSE);
        $this->db->where('id', $id);
        $this->db->update('tblactions', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            $action_now = $this->get($id);
            if ($current_action->assigned != $action_now->assigned) {
                if ($action_now->assigned != get_staff_user_id()) {
                    $notified = add_notification([
                        'description'     => 'not_action_assigned_to_you',
                        'touserid'        => $action_now->assigned,
                        'fromuserid'      => get_staff_user_id(),
                        'link'            => 'actions/list_actions/' . $id,
                        'additional_data' => serialize([
                            $action_now->subject,
                        ]),
                    ]);
                    if ($notified) {
                        pusher_trigger_notification([$action_now->assigned]);
                    }
                }
            }
        }

        foreach ($items as $key => $item) {
            if (update_sales_item_post($item['itemid'], $item)) {
                $affectedRows++;
            }

            if (isset($item['custom_fields'])) {
                if (handle_custom_fields_post($item['itemid'], $item['custom_fields'])) {
                    $affectedRows++;
                }
            }

            if (!isset($item['taxname']) || (isset($item['taxname']) && count($item['taxname']) == 0)) {
                if (delete_taxes_from_item($item['itemid'], 'action')) {
                    $affectedRows++;
                }
            } else {
                $item_taxes        = get_action_item_taxes($item['itemid']);
                $_item_taxes_names = [];
                foreach ($item_taxes as $_item_tax) {
                    array_push($_item_taxes_names, $_item_tax['taxname']);
                }
                $i = 0;
                foreach ($_item_taxes_names as $_item_tax) {
                    if (!in_array($_item_tax, $item['taxname'])) {
                        $this->db->where('id', $item_taxes[$i]['id'])
                        ->delete('tblitemstax');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                    $i++;
                }
                if (_maybe_insert_post_item_tax($item['itemid'], $item, $id, 'action')) {
                    $affectedRows++;
                }
            }
        }

        foreach ($newitems as $key => $item) {
            if ($new_item_added = add_new_sales_item_post($item, $id, 'action')) {
                _maybe_insert_post_item_tax($new_item_added, $item, $id, 'action');
                $affectedRows++;
            }
        }

        if ($affectedRows > 0) {
            update_sales_total_tax_column($id, 'action', 'tblactions');
            logActivity('Action Updated [ID:' . $id . ']');
        }

        if ($save_and_send === true) {
            $this->send_action_to_email($id, 'action-send-to-customer', true);
        }

        if ($affectedRows > 0) {
            do_action('after_action_updated', $id);

            return true;
        }

        return false;
    }

	public function add_edit_members($data, $id)
    {
        $affectedRows = 0;
        if (isset($data['action_members'])) {
            $action_members = $data['action_members'];
        }

        $this->db->select('subject');
        $this->db->where('id', $id);
        $action      = $this->db->get('tblactions')->row();
        $action_name = $action->subject;

        $action_members_in = $this->get_action_members($id);
        if (sizeof($action_members_in) > 0) {
            foreach ($action_members_in as $action_member) {
                if (isset($action_member)) {
                    if (!in_array($action_member['staff_id'], $action_members)) {
                        $this->db->where('action_id', $id);
                        $this->db->where('staff_id', $action_member['staff_id']);
                        $this->db->delete('tblactionmembers');
                    }
                } else {
                    $this->db->where('action_id', $id);
                    $this->db->delete('tblactionmembers');
                    if ($this->db->affected_rows() > 0) {
                        $affectedRows++;
                    }
                }
            }
            if (isset($action_members)) {
                foreach ($action_members as $staff_id) {
                    $this->db->where('action_id', $id);
                    $this->db->where('staff_id', $staff_id);
                    $_exists = $this->db->get('tblactionmembers')->row();
                    if (!$_exists) {
                        if (empty($staff_id)) {
                            continue;
                        }
                        $this->db->insert('tblactionmembers', [
                            'action_id' => $id,
                            'staff_id'   => $staff_id,
                        ]);
                        
                    }
                }
            }
        } else {
            if (isset($action_members)) {
                foreach ($action_members as $staff_id) {
                    if (empty($staff_id)) {
                        continue;
                    }
                    $this->db->insert('tblactionmembers', [
                        'action_id' => $id,
                        'staff_id'   => $staff_id,
                    ]);
                }
            }
        }

        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }
	
    public function get_action_members($id)
    {
        $this->db->select('email,action_id,staff_id');
        $this->db->join('tblstaff', 'tblstaff.staffid=tblactionmembers.staff_id');
        $this->db->where('action_id', $id);

        return $this->db->get('tblactionmembers')->result_array();
    }
	
	
    /**
     * Get actions
     * @param  mixed $id action id OPTIONAL
     * @return mixed
     */
    public function get($id = '', $where = [], $for_editor = false)
    {
        $this->db->where($where);

        if (is_client_logged_in()) {
            $this->db->where('status !=', 0);
        }

        $this->db->select('*,tblcurrencies.id as currencyid, tblactions.id as id, tblcurrencies.name as currency_name');
        $this->db->from('tblactions');
        $this->db->join('tblcurrencies', 'tblcurrencies.id = tblactions.currency', 'left');

        if (is_numeric($id)) {
            $this->db->where('tblactions.id', $id);
            $action = $this->db->get()->row();
            if ($action) {
                $action->attachments                           = $this->get_attachments($id);
                $action->assigned                              = $this->get_action_members($id);
                $action->items                                 = get_items_by_type('action', $id);
                $action->visible_attachments_to_customer_found = false;
                foreach ($action->attachments as $attachment) {
                    if ($attachment['visible_to_customer'] == 1) {
                        $action->visible_attachments_to_customer_found = true;

                        break;
                    }
                }
                if ($for_editor == false) {
                    $action = parse_action_content_merge_fields($action);
                }
            }

            return $action;
        }

        return $this->db->get()->result_array();
    }

    public function clear_signature($id)
    {
        $this->db->select('signature');
        $this->db->where('id', $id);
        $action = $this->db->get('tblactions')->row();

        if ($action) {
            $this->db->where('id', $id);
            $this->db->update('tblactions', ['signature' => null]);

            if (!empty($action->signature)) {
                unlink(get_upload_path_by_type('action') . $id . '/' . $action->signature);
            }

            return true;
        }

        return false;
    }

    public function update_pipeline($data)
    {
        $this->mark_action_status($data['status'], $data['actionid']);
        foreach ($data['order'] as $order_data) {
            $this->db->where('id', $order_data[0]);
            $this->db->update('tblactions', [
                'pipeline_order' => $order_data[1],
            ]);
        }
    }

    public function get_attachments($action_id, $id = '')
    {
        // If is passed id get return only 1 attachment
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        } else {
            $this->db->where('rel_id', $action_id);
        }
        $this->db->where('rel_type', 'action');
        $result = $this->db->get('tblfiles');
        if (is_numeric($id)) {
            return $result->row();
        }

        return $result->result_array();
    }

    /**
     *  Delete action attachment
     * @param   mixed $id  attachmentid
     * @return  boolean
     */
    public function delete_attachment($id)
    {
        $attachment = $this->get_attachments('', $id);
        $deleted    = false;
        if ($attachment) {
            if (empty($attachment->external)) {
                unlink(get_upload_path_by_type('action') . $attachment->rel_id . '/' . $attachment->file_name);
            }
            $this->db->where('id', $attachment->id);
            $this->db->delete('tblfiles');
            if ($this->db->affected_rows() > 0) {
                $deleted = true;
                logActivity('Action Attachment Deleted [ID: ' . $attachment->rel_id . ']');
            }
            if (is_dir(get_upload_path_by_type('action') . $attachment->rel_id)) {
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('action') . $attachment->rel_id);
                if (count($other_attachments) == 0) {
                    // okey only index.html so we can delete the folder also
                    delete_dir(get_upload_path_by_type('action') . $attachment->rel_id);
                }
            }
        }

        return $deleted;
    }

    /**
     * Add action comment
     * @param mixed  $data   $_POST comment data
     * @param boolean $client is request coming from the client side
     */
    public function add_comment($data, $client = false)
    {
        if (is_staff_logged_in()) {
            $client = false;
        }

        if (isset($data['action'])) {
            unset($data['action']);
        }
        $data['dateadded'] = date('Y-m-d H:i:s');
        if ($client == false) {
            $data['staffid'] = get_staff_user_id();
        }
        $data['content'] = nl2br($data['content']);
        $this->db->insert('tblactioncomments', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $action = $this->get($data['actionid']);

            // No notifications client when action is with draft status
            if ($action->status == '6' && $client == false) {
                return true;
            }

            $merge_fields = [];
            $merge_fields = array_merge($merge_fields, get_action_merge_fields($action->id));

            $this->load->model('emails_model');

            $this->emails_model->set_rel_id($data['actionid']);
            $this->emails_model->set_rel_type('action');

            if ($client == true) {
                // Get creator and assigned
                $this->db->select('staffid,email,phonenumber');
                $this->db->where('staffid', $action->addedfrom);
                $this->db->or_where('staffid', $action->assigned);
                $staff_action = $this->db->get('tblstaff')->result_array();
                $notifiedUsers  = [];
                foreach ($staff_action as $member) {
                    $notified = add_notification([
                        'description'     => 'not_action_comment_from_client',
                        'touserid'        => $member['staffid'],
                        'fromcompany'     => 1,
                        'fromuserid'      => null,
                        'link'            => 'actions/list_actions/' . $data['actionid'],
                        'additional_data' => serialize([
                            $action->subject,
                        ]),
                    ]);

                    if ($notified) {
                        array_push($notifiedUsers, $member['staffid']);
                    }

                    // Send email/sms to admin that client commented
                    $this->emails_model->send_email_template('action-comment-to-admin', $member['email'], $merge_fields);
                    $this->sms->trigger(SMS_TRIGGER_ACTION_NEW_COMMENT_TO_STAFF, $member['phonenumber'], $merge_fields);
                }
                pusher_trigger_notification($notifiedUsers);
            } else {
                // Send email/sms to client that admin commented
                $this->emails_model->send_email_template('action-comment-to-client', $action->email, $merge_fields);
                $this->sms->trigger(SMS_TRIGGER_ACTION_NEW_COMMENT_TO_CUSTOMER, $action->phone, $merge_fields);
            }

            return true;
        }

        return false;
    }

    public function edit_comment($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tblactioncomments', [
            'content' => nl2br($data['content']),
        ]);
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get action comments
     * @param  mixed $id action id
     * @return array
     */
    public function get_comments($id)
    {
        $this->db->where('actionid', $id);
        $this->db->order_by('dateadded', 'ASC');

        return $this->db->get('tblactioncomments')->result_array();
    }

    /**
     * Get action single comment
     * @param  mixed $id  comment id
     * @return object
     */
    public function get_comment($id)
    {
        $this->db->where('id', $id);

        return $this->db->get('tblactioncomments')->row();
    }

	public function get_distinct_actions_members()
    {
        return $this->db->query('SELECT staff_id, firstname, lastname FROM tblactionmembers JOIN tblstaff ON tblstaff.staffid=tblactionmembers.staff_id GROUP by staff_id order by firstname ASC')->result_array();
    }
	
	
    /**
     * Remove action comment
     * @param  mixed $id comment id
     * @return boolean
     */
    public function remove_comment($id)
    {
        $comment = $this->get_comment($id);
        $this->db->where('id', $id);
        $this->db->delete('tblactioncomments');
        if ($this->db->affected_rows() > 0) {
            logActivity('Action Comment Removed [ActionID:' . $comment->actionid . ', Comment Content: ' . $comment->content . ']');

            return true;
        }

        return false;
    }

    /**
     * Copy action
     * @param  mixed $id action id
     * @return mixed
     */
    public function copy($id)
    {
        $this->copy      = true;
        $action        = $this->get($id, [], true);
        $not_copy_fields = [
            'addedfrom',
            'id',
            'datecreated',
            'hash',
            'status',
            'invoice_id',
            'estimate_id',
            'is_expiry_notified',
            'date_converted',
            'signature',
            'acceptance_firstname',
            'acceptance_lastname',
            'acceptance_email',
            'acceptance_date',
            'acceptance_ip',
        ];
        $fields      = $this->db->list_fields('tblactions');
        $insert_data = [];
        foreach ($fields as $field) {
            if (!in_array($field, $not_copy_fields)) {
                $insert_data[$field] = $action->$field;
            }
        }

        $insert_data['addedfrom']   = get_staff_user_id();
        $insert_data['datecreated'] = date('Y-m-d H:i:s');
        $insert_data['date']        = _d(date('Y-m-d'));
        $insert_data['status']      = 6;
        $insert_data['hash']        = app_generate_hash();

        // in case open till is expired set new 7 days starting from current date
        if ($insert_data['open_till'] && get_option('action_due_after') != 0) {
            $insert_data['open_till'] = _d(date('Y-m-d', strtotime('+' . get_option('action_due_after') . ' DAY', strtotime(date('Y-m-d')))));
        }

        $insert_data['newitems'] = [];
        $custom_fields_items     = get_custom_fields('items');
        $key                     = 1;
        foreach ($action->items as $item) {
            $insert_data['newitems'][$key]['description']      = $item['description'];
            $insert_data['newitems'][$key]['long_description'] = clear_textarea_breaks($item['long_description']);
            $insert_data['newitems'][$key]['qty']              = $item['qty'];
            $insert_data['newitems'][$key]['unit']             = $item['unit'];
            $insert_data['newitems'][$key]['taxname']          = [];
            $taxes                                             = get_action_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                // tax name is in format TAX1|10.00
                array_push($insert_data['newitems'][$key]['taxname'], $tax['taxname']);
            }
            $insert_data['newitems'][$key]['rate']  = $item['rate'];
            $insert_data['newitems'][$key]['order'] = $item['item_order'];
            foreach ($custom_fields_items as $cf) {
                $insert_data['newitems'][$key]['custom_fields']['items'][$cf['id']] = get_custom_field_value($item['id'], $cf['id'], 'items', false);

                if (!defined('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST')) {
                    define('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST', true);
                }
            }
            $key++;
        }

        $id = $this->add($insert_data);

        if ($id) {
            $custom_fields = get_custom_fields('action');
            foreach ($custom_fields as $field) {
                $value = get_custom_field_value($action->id, $field['id'], 'action', false);
                if ($value == '') {
                    continue;
                }
                $this->db->insert('tblcustomfieldsvalues', [
                    'relid'   => $id,
                    'fieldid' => $field['id'],
                    'fieldto' => 'action',
                    'value'   => $value,
                ]);
            }

            $tags = get_tags_in($action->id, 'action');
            handle_tags_save($tags, $id, 'action');

            logActivity('Copied Action ' . format_action_number($action->id));

            return $id;
        }

        return false;
    }

    /**
     * Take action action (change status) manually
     * @param  mixed $status status id
     * @param  mixed  $id     action id
     * @param  boolean $client is request coming from client side or not
     * @return boolean
     */
    public function mark_action_status($status, $id, $client = false)
    {
        $original_action = $this->get($id);
        $this->db->where('id', $id);
        $this->db->update('tblactions', [
            'status' => $status,
        ]);

        if ($this->db->affected_rows() > 0) {
            // Client take action
            if ($client == true) {
                $revert = false;
                // Declined
                if ($status == 2) {
                    $message = 'not_action_action_declined';
                } elseif ($status == 3) {
                    $message = 'not_action_action_accepted';
                // Accepted
                } else {
                    $revert = true;
                }
                // This is protection that only 3 and 4 statuses can be taken as action from the client side
                if ($revert == true) {
                    $this->db->where('id', $id);
                    $this->db->update('tblactions', [
                        'status' => $original_action->status,
                    ]);

                    return false;
                }
                $merge_fields = [];
                $merge_fields = array_merge($merge_fields, get_action_merge_fields($original_action->id));

                // Get creator and assigned;
                $this->db->where('staffid', $original_action->addedfrom);
                $this->db->or_where('staffid', $original_action->assigned);
                $staff_action = $this->db->get('tblstaff')->result_array();
                $notifiedUsers  = [];
                foreach ($staff_action as $member) {
                    $notified = add_notification([
                            'fromcompany'     => true,
                            'touserid'        => $member['staffid'],
                            'description'     => $message,
                            'link'            => 'actions/list_actions/' . $id,
                            'additional_data' => serialize([
                                format_action_number($id),
                            ]),
                        ]);
                    if ($notified) {
                        array_push($notifiedUsers, $member['staffid']);
                    }
                }

                pusher_trigger_notification($notifiedUsers);

                $this->load->model('emails_model');

                $this->emails_model->set_rel_id($id);
                $this->emails_model->set_rel_type('action');

                // Send thank you to the customer email template
                if ($status == 3) {
                    foreach ($staff_action as $member) {
                        $this->emails_model->send_email_template('action-client-accepted', $member['email'], $merge_fields);
                    }
                    $this->emails_model->send_email_template('action-client-thank-you', $original_action->email, $merge_fields);
                    do_action('action_accepted', $id);
                } else {
                    // Client declined send template to admin
                    foreach ($staff_action as $member) {
                        $this->emails_model->send_email_template('action-client-declined', $member['email'], $merge_fields);
                    }
                    do_action('action_declined', $id);
                }
            } else {
                // in case admin mark as open the the open till date is smaller then current date set open till date 7 days more
                if ((date('Y-m-d', strtotime($original_action->open_till)) < date('Y-m-d')) && $status == 1) {
                    $open_till = date('Y-m-d', strtotime('+7 DAY', strtotime(date('Y-m-d'))));
                    $this->db->where('id', $id);
                    $this->db->update('tblactions', [
                        'open_till' => $open_till,
                    ]);
                }
            }
            logActivity('Action Status Changes [ActionID:' . $id . ', Status:' . format_action_status($status, '', false) . ',Client Action: ' . (int) $client . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete action
     * @param  mixed $id action id
     * @return boolean
     */
    public function delete($id)
    {
        $this->clear_signature($id);
        $action = $this->get($id);

        $this->db->where('id', $id);
        $this->db->delete('tblactions');
        if ($this->db->affected_rows() > 0) {
            delete_tracked_emails($id, 'action');

            $this->db->where('actionid', $id);
            $this->db->delete('tblactioncomments');
            // Get related tasks
            $this->db->where('rel_type', 'action');
            $this->db->where('rel_id', $id);

            $tasks = $this->db->get('tblstafftasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }

            $attachments = $this->get_attachments($id);
            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'action');
            $this->db->delete('tblnotes');

            $this->db->where('relid IN (SELECT id from tblitems_in WHERE rel_type="action" AND rel_id="' . $id . '")');
            $this->db->where('fieldto', 'items');
            $this->db->delete('tblcustomfieldsvalues');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'action');
            $this->db->delete('tblitems_in');


            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'action');
            $this->db->delete('tblitemstax');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'action');
            $this->db->delete('tbltags_in');

            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'action');
            $this->db->delete('tblcustomfieldsvalues');

            $this->db->where('rel_type', 'action');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblreminders');

            $this->db->where('rel_type', 'action');
            $this->db->where('rel_id', $id);
            $this->db->delete('tblviewstracking');

            logActivity('Action Deleted [ActionID:' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Get relation action data. Ex lead or customer will return the necesary db fields
     * @param  mixed $rel_id
     * @param  string $rel_type customer/lead
     * @return object
     */
    public function get_relation_data_values($rel_id, $rel_type)
    {
        $data = new StdClass();
        if ($rel_type == 'customer') {
            $this->db->where('userid', $rel_id);
            $_data = $this->db->get('tblclients')->row();

            $primary_contact_id = get_primary_contact_user_id($rel_id);

            if ($primary_contact_id) {
                $contact     = $this->clients_model->get_contact($primary_contact_id);
                $data->email = $contact->email;
            }

            $data->phone            = $_data->phonenumber;
            $data->is_using_company = false;
            if (isset($contact)) {
                $data->to = $contact->firstname . ' ' . $contact->lastname;
            } else {
                if (!empty($_data->company)) {
                    $data->to               = $_data->company;
                    $data->is_using_company = true;
                }
            }
            $data->company = $_data->company;
            $data->address = clear_textarea_breaks($_data->address);
            $data->zip     = $_data->zip;
            $data->country = $_data->country;
            $data->state   = $_data->state;
            $data->city    = $_data->city;

            $default_currency = $this->clients_model->get_customer_default_currency($rel_id);
            if ($default_currency != 0) {
                $data->currency = $default_currency;
            }
        } elseif ($rel_type = 'lead') {
            $this->db->where('id', $rel_id);
            $_data       = $this->db->get('tblleads')->row();
            $data->phone = $_data->phonenumber;

            $data->is_using_company = false;

            if (empty($_data->company)) {
                $data->to = $_data->name;
            } else {
                $data->to               = $_data->company;
                $data->is_using_company = true;
            }

            $data->company = $_data->company;
            $data->address = $_data->address;
            $data->email   = $_data->email;
            $data->zip     = $_data->zip;
            $data->country = $_data->country;
            $data->state   = $_data->state;
            $data->city    = $_data->city;
        }

        return $data;
    }

    /**
     * Sent action to email
     * @param  mixed  $id        actionid
     * @param  string  $template  email template to sent
     * @param  boolean $attachpdf attach action pdf or not
     * @return boolean
     */
    public function send_expiry_reminder($id)
    {
        $action = $this->get($id);
        $pdf      = action_pdf($action);
        $attach   = $pdf->Output(slug_it($action->subject) . '.pdf', 'S');

        // For all cases update this to prevent sending multiple reminders eq on fail
        $this->db->where('id', $action->id);
        $this->db->update('tblactions', [
            'is_expiry_notified' => 1,
        ]);

        $this->load->model('emails_model');

        $this->emails_model->set_rel_id($id);
        $this->emails_model->set_rel_type('action');

        $this->emails_model->add_attachment([
            'attachment' => $attach,
            'filename'   => slug_it($action->subject) . '.pdf',
            'type'       => 'application/pdf',
        ]);

        $merge_fields = [];
        $merge_fields = array_merge($merge_fields, get_action_merge_fields($action->id));
        $sent         = $this->emails_model->send_email_template('action-expiry-reminder', $action->email, $merge_fields);

        if (can_send_sms_based_on_creation_date($action->datecreated)) {
            $sms_sent = $this->sms->trigger(SMS_TRIGGER_ACTION_EXP_REMINDER, $action->phone, $merge_fields);
        }

        return true;
    }

    public function send_action_to_email($id, $template = '', $attachpdf = true, $cc = '')
    {
        $this->load->model('emails_model');

        $this->emails_model->set_rel_id($id);
        $this->emails_model->set_rel_type('action');

        $action = $this->get($id);

        // Action status is draft update to sent
        if ($action->status == 6) {
            $this->db->where('id', $id);
            $this->db->update('tblactions', ['status' => 4]);
            $action = $this->get($id);
        }

        if ($attachpdf) {
            $pdf    = action_pdf($action);
            $attach = $pdf->Output(slug_it($action->subject) . '.pdf', 'S');
            $this->emails_model->add_attachment([
                'attachment' => $attach,
                'filename'   => slug_it($action->subject) . '.pdf',
                'type'       => 'application/pdf',
            ]);
        }

        if ($this->input->post('email_attachments')) {
            $_other_attachments = $this->input->post('email_attachments');
            foreach ($_other_attachments as $attachment) {
                $_attachment = $this->get_attachments($id, $attachment);
                $this->emails_model->add_attachment([
                    'attachment' => get_upload_path_by_type('action') . $id . '/' . $_attachment->file_name,
                    'filename'   => $_attachment->file_name,
                    'type'       => $_attachment->filetype,
                    'read'       => true,
                ]);
            }
        }

        $merge_fields = [];
        $merge_fields = array_merge($merge_fields, get_action_merge_fields($action->id));
        $sent         = $this->emails_model->send_email_template($template, $action->email, $merge_fields, '', $cc);
        if ($sent) {

            // Set to status sent
            $this->db->where('id', $id);
            $this->db->update('tblactions', [
                'status' => 4,
            ]);

            do_action('action_sent', $id);

            return true;
        }

        return false;
    }
}
