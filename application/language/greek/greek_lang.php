<?php

# Version 1.0.0
#
# General
$lang['id']                   = 'ID';
$lang['name']                 = 'Όνομα';
$lang['options']              = 'Επιλογές';
$lang['submit']               = 'Αποθήκευση';
$lang['added_successfully']   = '%s προστέθηκαν με επιτυχία.';
$lang['updated_successfully'] = '%s ενημερώθηκαν με επιτυχία.';
$lang['edit']                 = 'Επεξεργασία %s';
$lang['add_new']              = 'Προσθήκη νέου-ας %s';
$lang['deleted']              = '%s διαγράφηκαν';
$lang['problem_deleting']     = 'Πρόβλημα κατά την διαγραφή %s';
$lang['is_referenced']        = 'Το ID του %s χρησιμοποιείται ήδη.';
$lang['close']                = 'Κλείσιμο';
$lang['send']                 = 'Αποστολή';
$lang['cancel']               = 'Ακύρωση';
$lang['go_back']              = 'Επιστροφή';
$lang['error_uploading_file'] = 'Πρόβλημα μεταφόρτωσης αρχείου';
$lang['load_more']            = 'Φόρτωση περισσότερα';
$lang['cant_delete_default']  = 'Αδυναμία διαγραφής προεπιλεγμένου %s';

# Invoice General
$lang['invoice_status_paid']                   = 'Εξοφλημένο';
$lang['invoice_status_unpaid']                 = 'Ανεξόφλητο';
$lang['invoice_status_overdue']                = 'Εκπρόθεσμο';
$lang['invoice_status_not_paid_completely']    = 'Μερικώς εξοφλημένο';
$lang['invoice_pdf_heading']                   = 'ΤΙΜΟΛΟΓΙΟ';
$lang['invoice_table_item_heading']            = 'Είδος';
$lang['invoice_table_quantity_heading']        = 'Ποσότητα';
$lang['invoice_table_rate_heading']            = 'Τιμή Μονάδας';
$lang['invoice_table_tax_heading']             = 'ΦΠΑ';
$lang['invoice_table_amount_heading']          = 'Αξία';
$lang['invoice_subtotal']                      = 'Μερικό σύνολο';
$lang['invoice_adjustment']                    = 'Adjustment';
$lang['invoice_total']                         = 'Σύνολο';
$lang['invoice_bill_to']                       = 'Χρέωση σε';
$lang['invoice_data_date']                     = 'Ημερομηνία έκδοσης:';
$lang['invoice_data_duedate']                  = 'Ημερομηνία λήξης:';
$lang['invoice_received_payments']             = 'Συναλλαγές';
$lang['invoice_no_payments_found']             = 'Δεν βρέθηκαν πληρωμές';
$lang['invoice_note']                          = 'Σημείωση:';
$lang['invoice_payments_table_number_heading'] = 'Πληρωμή #';
$lang['invoice_payments_table_mode_heading']   = 'Τρόπος πληρωμής';
$lang['invoice_payments_table_date_heading']   = 'Ημερομηνία';
$lang['invoice_payments_table_amount_heading'] = 'Ποσό';

# Announcements
$lang['announcement']                 = 'Announcement';
$lang['announcement_lowercase']       = 'announcement';
$lang['announcements']                = 'Announcements';
$lang['announcements_lowercase']      = 'announcements';
$lang['new_announcement']             = 'New Announcement';
$lang['announcement_name']            = 'Subject';
$lang['announcement_message']         = 'Message';
$lang['announcement_show_to_staff']   = 'Show to staff';
$lang['announcement_show_to_clients'] = 'Show to clients';
$lang['announcement_show_my_name']    = 'Show my name';

# Clients
$lang['clients']                                 = 'Πελάτες';
$lang['client']                                  = 'Πελάτης';
$lang['new_client']                              = 'Νέος πελάτης';
$lang['client_extra_contact_fields']			 = 'Επιπλέον τρόποι επικοινωνίας';
$lang['client_lowercase']                        = 'πελάτης';
$lang['client_firstname']                        = 'Όνομα';
$lang['client_lastname']                         = 'Επώνυμο';
$lang['client_email']                            = 'Email';
$lang['client_company']                          = 'Επωνυμία';
$lang['client_fullname']                         = 'Ονοματεπώνυμο';
$lang['client_trade_name']                       = 'Διακριτικός τίτλος';
$lang['client_occupation']                       = 'Επάγγελμα';
$lang['client_vat_number']                       = 'ΑΦΜ';
$lang['client_doy']								 = 'ΔΟΥ';
$lang['client_address']                          = 'Διεύθυνση';
$lang['client_city']                             = 'Πόλη';
$lang['client_postal_code']                      = 'Τ.Κ.';
$lang['client_state']                            = 'Νομός';
$lang['client_password']                         = 'Κωδικός';
$lang['client_password_change_populate_note']    = 'Σημείωση: Αν δημοσιοποιήσετε αυτό το πεδίο, ο κωδικός θα αλλάξει για αυτή την επαφή.';
$lang['client_password_last_changed']            = 'Τελευταία αλλαγή κωδικού:';
$lang['suppliers']                                 = 'Προμηθευτές';
$lang['suppliers_actions']                         = 'Κινήσεις Προμηθευτών';
$lang['clients_actions']                           = 'Κινήσεις Πελατών';
$lang['supplier']                                  = 'Προμηθευτής';
$lang['new_supplier']                              = 'Νέος Προμηθευτής';
$lang['supplier_lowercase']                        = 'προμηθευτής';
$lang['supplier_firstname']                        = 'Όνομα';
$lang['supplier_lastname']                         = 'Επώνυμο';
$lang['supplier_email']                            = 'Email';
$lang['supplier_company']                          = 'Επωνυμία';
$lang['supplier_fullname']                         = 'Ονοματεπώνυμο';
$lang['supplier_trade_name']                       = 'Διακριτικός τίτλος';
$lang['supplier_occupation']                       = 'Επάγγελμα';
$lang['supplier_vat_number']                       = 'ΑΦΜ';
$lang['supplier_doy']								 = 'ΔΟΥ';
$lang['supplier_address']                          = 'Διεύθυνση';
$lang['supplier_city']                             = 'Πόλη';
$lang['supplier_postal_code']                      = 'Τ.Κ.';
$lang['supplier_state']                            = 'Νομός';
$lang['supplier_password']                         = 'Κωδικός';
$lang['supplier_password_change_populate_note']    = 'Σημείωση: Αν δημοσιοποιήσετε αυτό το πεδίο, ο κωδικός θα αλλάξει για αυτή την επαφή.';
$lang['supplier_password_last_changed']            = 'Τελευταία αλλαγή κωδικού:';
$lang['action_supplier_xreosi_value']			 = 'Χρέωση';
$lang['action_supplier_pistosi_value']			 = 'Πίστωση';
$lang['login_as_client']                         = 'Είσοδος ως πελάτης';
$lang['client_invoices_tab']                     = 'Τιμολόγια';
$lang['contracts_invoices_tab']                  = 'Συμβάσεις';
$lang['contracts_tickets_tab']                   = 'Αιτήματα';
$lang['contracts_notes_tab']                     = 'Σημειώσεις';
$lang['note_description']                        = 'Περιγραφή';
$lang['client_do_not_send_welcome_email']        = 'Ναμη αποσταλεί email καλοσωρίσματος';
$lang['clients_notes_table_description_heading'] = 'Περιγραφή';
$lang['clients_notes_table_addedfrom_heading']   = 'Προστέθηκε από';
$lang['clients_notes_table_dateadded_heading']   = 'Προστέθηκε την';
$lang['clients_list_full_name']                  = 'Ονοματεπώνυμο';
$lang['clients_list_last_login']                 = 'Τελευταία είσοδος';

# Contracts
$lang['contracts']                = 'Contracts';
$lang['contract']                 = 'Contract';
$lang['new_contract']             = 'New Contract';
$lang['contract_lowercase']       = 'contract';
$lang['contract_start_date']      = 'Ημερομηνία Έναρξης';
$lang['contract_end_date']        = 'End Date';
$lang['contract_subject']         = 'Subject';
$lang['contract_description']     = 'Περιγραφή';
$lang['contract_subject_tooltip'] = 'Subject is also visible to customer';
$lang['contract_client_string']   = 'Πελάτης';
$lang['contract_attach']          = 'Attach document';
$lang['contract_list_client']     = 'Πελάτης';
$lang['contract_list_subject']    = 'Subject';
$lang['contract_list_start_date'] = 'Ημερομηνία Έναρξης';
$lang['contract_list_end_date']   = 'End Date';

# Currencies
$lang['currencies']                    = 'Νόμισμα';
$lang['currency']                      = 'Νόμισμα';
$lang['new_currency']                  = 'Νέο Νόμισμα';
$lang['currency_lowercase']            = 'νόμισμα';
$lang['base_currency_set']             = 'This is now your base currency.';
$lang['make_base_currency']            = 'Make base currency';
$lang['base_currency_string']          = 'Βασικό Νόμισμα';
$lang['currency_list_name']            = 'Όνομα';
$lang['currency_list_symbol']          = 'Σύμβολο';
$lang['currency_add_edit_description'] = 'Κωδικός Νομίσματος';
$lang['currency_add_edit_rate']        = 'Σύμβολο';
$lang['currency_edit_heading']         = 'Επεξεργασία Νομίσματος';
$lang['currency_add_heading']          = 'Προσθήκη νέου Νομίσματος';

# Department
$lang['departments']                 = 'Τμήματα';
$lang['department']                  = 'Τμήμα';
$lang['new_department']              = 'Νέο τμήμα';
$lang['department_lowercase']        = 'τμήμα';
$lang['department_name']             = 'Όνομα τμήματος';
$lang['department_email']            = 'Email τμήματος';
$lang['department_hide_from_client'] = 'απόκρυψη από τον πελάτη?';
$lang['department_list_name']        = 'Όνομα';

# Email Templates
$lang['email_templates']                        = 'Email Templates';
$lang['email_template']                         = 'Email Template';
$lang['email_template_lowercase']               = 'email template';
$lang['email_templates_lowercase']              = 'email templates';
$lang['email_template_ticket_fields_heading']   = 'Αιτήματα';
$lang['email_template_invoices_fields_heading'] = 'Τιμολόγια';
$lang['email_template_clients_fields_heading']  = 'Πελάτες';

$lang['template_name']                = 'Template Name';
$lang['template_subject']             = 'Subject';
$lang['template_fromname']            = 'From Name';
$lang['template_fromemail']           = 'From Email';
$lang['send_as_plain_text']           = 'Send as Plaintext';
$lang['email_template_disabled']      = 'Disabled';
$lang['email_template_email_message'] = 'Email message';
$lang['available_merge_fields']       = 'Available merge fields';
# Home
$lang['dashboard_string']                          = 'Dashboard';
$lang['home_latest_todos']                         = 'Πρόσφατες εκκρεμότητες';
$lang['home_no_latest_todos']                      = 'Δεν βρέθηκαν εκκρεμότητες';
$lang['home_latest_finished_todos']                = 'Latest finished to do\'s';
$lang['home_no_finished_todos_found']              = 'No finished todos found';
$lang['home_tickets_awaiting_reply_by_department'] = 'Tickets Awaiting Reply by Department';
$lang['home_tickets_awaiting_reply_by_status']     = 'Tickets Awaiting Reply by Status';
$lang['home_this_week_events']                     = 'This Week events';
$lang['home_upcoming_events_next_week']            = 'Upcoming events Next Week';
$lang['home_event_added_by']                       = 'Event added by';
$lang['home_public_event']                         = 'Public event';
$lang['home_weekly_payment_records']               = 'Weekly Payment Records';
$lang['home_weekend_ticket_opening_statistics']    = 'Weekly Ticket Openings Statistics';
# Newsfeed
$lang['whats_on_your_mind']                                 = 'Share documents, ideas..';
$lang['new_post']                                           = 'Post';
$lang['newsfeed_upload_tooltip']                            = 'Tip:Drag and drop files to upload';
$lang['newsfeed_all_departments']                           = 'All Departments';
$lang['newsfeed_pin_post']                                  = 'Pin post';
$lang['newsfeed_unpin_post']                                = 'Unpin post';
$lang['newsfeed_delete_post']                               = 'Delete';
$lang['newsfeed_published_post']                            = 'Published';
$lang['newsfeed_you_like_this']                             = 'You like this';
$lang['newsfeed_like_this']                                 = 'like this';
$lang['newsfeed_one_other']                                 = 'other';
$lang['newsfeed_you']                                       = 'You';
$lang['newsfeed_and']                                       = 'and';
$lang['newsfeed_you_and']                                   = 'You and';
$lang['newsfeed_like_this_saying']                          = 'Like this';
$lang['newsfeed_unlike_this_saying']                        = 'Unlike this';
$lang['newsfeed_show_more_comments']                        = 'Show more comments';
$lang['comment_this_post_placeholder']                      = 'Comment this post..';
$lang['newsfeed_post_likes_modal_heading']                  = 'Colleagues who like this post';
$lang['newsfeed_comment_likes_modal_heading']               = 'Colleagues who like this comment';
$lang['newsfeed_newsfeed_post_only_visible_to_departments'] = 'This post is only visible to the following departments: %s';

# Invoice Items
$lang['invoice_items']                     = 'Τιμολογιμένα Είδη';
$lang['invoice_item']                      = 'Τιμολογιμένο Είδος';
$lang['new_invoice_item']                  = 'Νέο Είδος';
$lang['invoice_item_lowercase']            = 'τιμολογιμένο είδος';
$lang['invoice_items_list_description']    = 'Περιγραφή';
$lang['invoice_items_list_rate']           = 'Ποσό';
$lang['invoice_item_add_edit_description'] = 'Περιγραφή';
$lang['invoice_item_add_edit_rate']        = 'Ποσό';
$lang['invoice_item_edit_heading']         = 'Επεξεργασία Είδους';
$lang['invoice_item_add_heading']          = 'Προσθέστε Νέο Είδος';

# Invoices
$lang['invoices']                                = 'Τιμολόγια';
$lang['invoice']                                 = 'Τιμολόγιο';
$lang['invoice_lowercase']                       = 'τιμολόγιο';
$lang['create_new_invoice']                      = 'Δημιουργία νέου Τιμολογίου';
$lang['view_invoice']                            = 'Προβολή Τιμολογίου';
$lang['invoice_payment_recorded']                = 'Η πληρωμή καταγράφηκε';
$lang['invoice_payment_record_failed']           = 'Αποτυχία καταγραφής πληρωμής';
$lang['invoice_sent_to_client_success']          = 'Το τιμολόγιο έχει σταλεί στον πελάτη';
$lang['invoice_sent_to_client_fail']             = 'Problem while sending the invoice';
$lang['invoice_reminder_send_problem']           = 'Problem sending invoice overdue reminder';
$lang['invoice_overdue_reminder_sent']           = 'Invoice Overdue Reminder Successfully Sent';
$lang['invoice_details']                         = 'Στοιχεία Τιμολογίου';
$lang['invoice_view']                            = 'Προβολή Τιμολογίου';
$lang['invoice_select_customer']                 = 'Πελάτης';
$lang['invoice_add_edit_number']                 = 'Αριθμός Τιμολογίου';
$lang['invoice_add_edit_date']                   = 'Ημερομηνία';
$lang['invoice_add_edit_duedate']                = 'Ημερομηνία Λήξης';
$lang['invoice_add_edit_currency']               = 'Νόμισμα';
$lang['invoice_add_edit_client_note']            = 'Σημείωση Πελάτη';
$lang['invoice_add_edit_admin_note']             = 'Σημείωση Διαχειριστή';
$lang['invoices_toggle_table_tooltip']           = 'Προβολή Πίνακα';
$lang['edit_invoice_tooltip']                    = 'Επεξεργασία Τιμολογίου';
$lang['delete_invoice_tooltip']                  = 'Διαγραφή Τιμολογίου. Προσοχή: Θα διαγραφούν όλες οι σχετικές πληρωμές, αν υπάρχουν.';
$lang['invoice_sent_to_email_tooltip']           = 'Αποστολή με Email';
$lang['invoice_already_send_to_client_tooltip']  = 'Το τιμολόγιο έχει ήδη αποσταλεί στον πελάτη %s';
$lang['send_overdue_notice_tooltip']             = 'Send Overdue Notice';
$lang['invoice_view_activity_tooltip']           = 'Activity Log';
$lang['invoice_record_payment']                  = 'Record Payment';
$lang['invoice_send_to_client_modal_heading']    = 'Αποστολή Τιμολογίου στον Πελάτη';
$lang['invoice_send_to_client_attach_pdf']       = 'Συνημμένο Τιμολόγιο PDF';
$lang['invoice_send_to_client_preview_template'] = 'Preview Email Template';
$lang['invoice_dt_table_heading_number']         = 'Τιμολόγιο #';
$lang['invoice_dt_table_heading_date']           = 'Ημερομηνία';
$lang['invoice_dt_table_heading_client']         = 'Πελάτης';
$lang['invoice_dt_table_heading_duedate']        = 'Ημερομηνία Λήξης';
$lang['invoice_dt_table_heading_amount']         = 'Ποσό';
$lang['invoice_dt_table_heading_status']         = 'Κατάσταση';
$lang['record_payment_for_invoice']              = 'Record Payment for';
$lang['record_payment_amount_received']          = 'Ποσό που εισπράχθηκε';
$lang['record_payment_date']                     = 'Ημερομηνία Πληρωμής';
$lang['record_payment_leave_note']               = 'Leave a note';
$lang['invoice_payments_received']               = 'Πληρωμές';
$lang['invoice_record_payment_note_placeholder'] = 'Σημείωση Διαχειριστή';
$lang['no_payments_found']                       = 'No Payments found for this invoice';

# Payments
$lang['payments']                             = 'Πληρωμές';
$lang['payment']                              = 'Πληρωμή';
$lang['payment_lowercase']                    = 'πληρωμή';
$lang['payments_table_number_heading']        = 'πληρωμή #';
$lang['payments_table_invoicenumber_heading'] = 'Τιμολόγιο #';
$lang['payments_table_mode_heading']          = 'Τρόπος πληρωμής';
$lang['payments_table_date_heading']          = 'Ημερομηνία';
$lang['payments_table_amount_heading']        = 'Ποσό';
$lang['payments_table_client_heading']        = 'Πελάτης';
$lang['payment_not_exists']                   = 'Δεν υπάρχει πληρωμή';
$lang['payment_edit_for_invoice']             = 'Πληρωμή για τιμολόγιο';
$lang['payment_edit_amount_received']         = 'Ποσό που εισπράχθηκε';
$lang['payment_edit_date']                    = 'Ημερομηνία πληρωμής';

# Knowledge Base
$lang['kb_article_add_edit_subject']   = 'Subject';
$lang['kb_article_add_edit_group']     = 'Group';
$lang['kb_string']                     = 'Knowledge Base';
$lang['kb_article']                    = 'Article';
$lang['kb_article_lowercase']          = 'article';
$lang['kb_article_new_article']        = 'New Article';
$lang['kb_article_disabled']           = 'Disabled';
$lang['kb_article_description']        = 'Article description';
$lang['kb_no_articles_found']          = 'No knowledge base articles found';
$lang['kb_dt_article_name']            = 'Article Name';
$lang['kb_dt_group_name']              = 'Group';
$lang['new_group']                     = 'New Group';
$lang['kb_group_add_edit_name']        = 'Group Name';
$lang['kb_group_add_edit_description'] = 'Short description';
$lang['kb_group_add_edit_disabled']    = 'Disabled';
$lang['kb_group_add_edit_note']        = 'Note: All articles in this group will be hidden if disabled is checked';
$lang['group_table_name_heading']      = 'Name';
$lang['group_table_isactive_heading']  = 'Ενεργό';
$lang['kb_no_groups_found']            = 'No knowledge base groups found';

# Mail Lists
$lang['mail_lists']                            = 'Mail Lists';
$lang['mail_list']                             = 'Mail List';
$lang['new_mail_list']                         = 'New Mail List';
$lang['mail_list_lowercase']                   = 'mail list';
$lang['custom_field_deleted_success']          = 'Custom field deleted';
$lang['custom_field_deleted_fail']             = 'Problem deleting custom field';
$lang['email_removed_from_list']               = 'Email removed from list';
$lang['email_remove_fail']                     = 'Email removed from list';
$lang['staff_mail_lists']                      = 'Staff Mail List';
$lang['clients_mail_lists']                    = 'Clients Mail List';
$lang['mail_list_total_imported']              = 'Total emails imported: %s';
$lang['mail_list_total_duplicate']             = 'Total duplicate emails: %s';
$lang['mail_list_total_failed_to_insert']      = 'Emails failed to insert: %s';
$lang['mail_list_total_invalid']               = 'Invalid email address: %s';
$lang['cant_edit_mail_list']                   = 'You cant edit this list, this list is populated automatically';
$lang['mail_list_add_edit_name']               = 'Mail List Name';
$lang['mail_list_add_edit_customfield']        = 'Add custom field';
$lang['mail_lists_view_email_email_heading']   = 'Email';
$lang['mail_lists_view_email_date_heading']    = 'Date Added';
$lang['add_new_email_to']                      = 'Add New Email to %s';
$lang['import_emails_to']                      = 'Import Emails to %s';
$lang['mail_list_new_email_edit_add_label']    = 'Email';
$lang['mail_list_import_file']                 = 'Import File';
$lang['mail_list_available_custom_fields']     = 'Available Custom Fields';
$lang['submit_import_emails']                  = 'Import Emails';
$lang['btn_import_emails']                     = 'Import Emails (Excel)';
$lang['btn_add_email_to_list']                 = 'Add Email to This List';
$lang['mail_lists_dt_list_name']               = 'List Name';
$lang['mail_lists_dt_datecreated']             = 'Ημερομηνία Δημιουργίας';
$lang['mail_lists_dt_creator']                 = 'Creator';
$lang['email_added_to_mail_list_successfully'] = 'Email added to list';
$lang['email_is_duplicate_mail_list']          = 'Email already exists in this list';

# Media
$lang['media_files'] = 'Files';

# Payment modes
$lang['new_payment_mode']           = 'New Payment Mode';
$lang['payment_modes']              = 'Payment Modes';
$lang['payment_mode']               = 'Payment Mode';
$lang['payment_mode_lowercase']     = 'payment mode';
$lang['payment_modes_dt_name']      = 'Payment Mode Name';
$lang['payment_mode_add_edit_name'] = 'Payment Mode Name';
$lang['payment_mode_edit_heading']  = 'Edit Payment Mode';
$lang['payment_mode_add_heading']   = 'Add New Payment Mode';

# Predefined Ticket Replies
$lang['new_predefined_reply']              = 'New Predefined Reply';
$lang['predefined_replies']                = 'Predefined Replies';
$lang['predefined_reply']                  = 'Predefined Reply';
$lang['predefined_reply_lowercase']        = 'predefined reply';
$lang['predefined_replies_dt_name']        = 'Predefined Reply Name';
$lang['predefined_reply_add_edit_name']    = 'Predefined Reply Name';
$lang['predefined_reply_add_edit_content'] = 'Reply Content';

# Ticket Priorities
$lang['new_ticket_priority']           = 'Νέα Προτεραιότητα';
$lang['ticket_priorities']             = 'Ticket Priorities';
$lang['ticket_priority']               = 'Ticket Priority';
$lang['ticket_priority_lowercase']     = 'ticket priority';
$lang['no_ticket_priorities_found']    = 'No Ticket Priorities Found';
$lang['ticket_priority_dt_name']       = 'Ticket Priority Name';
$lang['ticket_priority_add_edit_name'] = 'Priority Name';

# Reports
$lang['kb_reports']                                       = 'Knowledge base articles reports';
$lang['sales_reports']                                    = 'Αναφορές Πωλήσεων';
$lang['reports_choose_kb_group']                          = 'Επιλογή ομάδας';
$lang['report_kb_yes']                                    = 'Ναι';
$lang['report_kb_no']                                     = 'Οχι';
$lang['report_kb_no_votes']                               = 'No votes yet';
$lang['report_this_week_leads_conversions']               = 'This Week Leads Conversions';
$lang['report_leads_sources_conversions']                 = 'Sources Conversion';
$lang['report_leads_monthly_conversions']                 = 'Monthly';
$lang['sales_report_heading']                             = 'Αναφορά Πωλήσεων';
$lang['report_sales_type_income']                         = 'Συνολικά Έσοδα';
$lang['report_sales_type_customer']                       = 'Ανφορά Πελατών';
$lang['report_sales_base_currency_select_explanation']    = 'You need to select currency because you have invoices with different currency';
$lang['report_sales_from_date']                           = 'Από ημερομηνία';
$lang['report_sales_to_date']                             = 'Μέχρι ημερομηνία';
$lang['report_sales_months_all_time']                     = 'Διαχρονικά';
$lang['report_sales_months_six_months']                   = 'Τελευταίοι 6 μήνες';
$lang['report_sales_months_twelve_months']                = 'Τελευταίοι 12 μήνες';
$lang['reports_sales_generated_report']                   = 'Παραχθείσα Αναφορά';
$lang['reports_sales_dt_customers_client']                = 'Πελάτης';
$lang['reports_sales_dt_customers_total_invoices']        = 'Σύνολο Τιμολογίων';
$lang['reports_sales_dt_items_customers_amount']          = 'Ποσό';
$lang['reports_sales_dt_items_customers_amount_with_tax'] = 'Ποσό με ΦΠΑ';

# Roles
$lang['new_role']           = 'Νέος Ρόλος';
$lang['all_roles']          = 'Όλοι οι Ρόλοι';
$lang['roles']              = 'Ρόλοι Προσωπικού';
$lang['role']               = 'Ρόλος';
$lang['role_lowercase']     = 'ρόλος';
$lang['roles_total_users']  = 'Σύνολο Χρηστών: ';
$lang['roles_dt_name']      = 'Όνομα Ρόλου';
$lang['role_add_edit_name'] = 'Όνομα Ρόλου';

# Service
$lang['new_service']           = 'New Service';
$lang['services']              = 'Services';
$lang['service']               = 'Service';
$lang['service_lowercase']     = 'service';
$lang['services_dt_name']      = 'Service Name';
$lang['service_add_edit_name'] = 'Service Name';

# Settings
$lang['settings']                                                  = 'Ρυθμίσεις';
$lang['settings_updated']                                          = 'Οι ρυθμίσεις αποθηκεύτηκαν';
$lang['settings_save']                                             = 'Αποθήκευση Ρυθμίσεων';
$lang['settings_group_general']                                    = 'Γενικά';
$lang['settings_group_localization']                               = 'Τοπικές ρυθμίσεις';
$lang['settings_group_tickets']                                    = 'Αιτήματα';
$lang['settings_group_sales']                                      = 'Οικονομικά';
$lang['settings_group_email']                                      = 'Email';
$lang['settings_group_clients']                                    = 'Πελάτες';
$lang['settings_group_newsfeed']                                   = 'Νέα';
$lang['settings_group_cronjob']                                    = 'Cron Job';
$lang['settings_yes']                                              = 'Yes';
$lang['settings_no']                                               = 'No';
$lang['settings_clients_default_theme']                            = 'Default customers theme';
$lang['settings_clients_allow_registration']                       = 'Allow customers to register';
$lang['settings_clients_allow_kb_view_without_registration']       = 'Allow knowledge base to be viewed without registration';
$lang['settings_cron_send_overdue_reminder']                       = 'Send invoice overdue reminder';
$lang['settings_cron_send_overdue_reminder_tooltip']               = 'Send overdue email to client when invoice status updated to overdue from Cron Job';
$lang['automatically_send_invoice_overdue_reminder_after']         = 'Auto send reminder after (days)';
$lang['automatically_resend_invoice_overdue_reminder_after']       = 'Auto re-send reminder after (days)';
$lang['settings_email_host']                                       = 'SMTP Host';
$lang['settings_email_port']                                       = 'SMTP Port';
$lang['settings_email']                                            = 'Email';
$lang['settings_email_password']                                   = 'SMTP Password';
$lang['settings_email_charset']                                    = 'Email Charset';
$lang['settings_email_signature']                                  = 'Email Signature';
$lang['settings_general_company_logo']                             = 'Company Logo';
$lang['settings_general_company_logo_tooltip']                     = 'Recommended dimensions: 150 x 34px';
$lang['settings_general_company_remove_logo_tooltip']              = 'Remove company logo';
$lang['settings_general_company_name']                             = 'Company Name';
$lang['settings_general_company_main_domain']                      = 'Company Main Domain';
$lang['settings_general_use_knowledgebase']                        = 'Use Knowledge Base';
$lang['settings_general_use_knowledgebase_tooltip']                = 'If you allow this options knowledge base will be shown also on clients side';
$lang['settings_general_tables_limit']                             = 'Tables Pagination Limit';
$lang['settings_general_default_staff_role']                       = 'Default Staff Role';
$lang['settings_general_default_staff_role_tooltip']               = 'When you add new staff member this role will be selected by default';
$lang['settings_localization_date_format']                         = 'Date Format';
$lang['settings_localization_default_timezone']                    = 'Default Timezone';
$lang['settings_localization_default_language']                    = 'Default Language';
$lang['settings_newsfeed_max_file_upload_post']                    = 'Maximum files upload on post';
$lang['settings_reminders_contracts']                              = 'Contract expiration reminder before';
$lang['settings_reminders_contracts_tooltip']                      = 'Expiration reminder notification in days';
$lang['settings_tickets_use_services']                             = 'Use services';
$lang['settings_tickets_max_attachments']                          = 'Maximum ticket attachments';
$lang['settings_tickets_allow_departments_access']                 = 'Allow staff to access only ticket that belongs to staff departments';
$lang['settings_tickets_allowed_file_extensions']                  = 'Allowed attachments file extensions';
$lang['settings_sales_general']                                    = 'General';
$lang['settings_sales_general_note']                               = 'General settings';
$lang['settings_sales_invoice_prefix']                             = 'Invoice Number Prefix';
$lang['settings_sales_decimal_separator']                          = 'Decimal Separator';
$lang['settings_sales_thousand_separator']                         = 'Thousand Separator';
$lang['settings_sales_currency_placement']                         = 'Currency Placement';
$lang['settings_sales_currency_placement_before']                  = 'Before Amount';
$lang['settings_sales_currency_placement_after']                   = 'After Amount';
$lang['settings_sales_require_client_logged_in_to_view_invoice']   = 'Require client to be logged in to view invoice';
$lang['settings_sales_next_invoice_number']                        = 'Next Invoice Number';
$lang['settings_sales_next_invoice_number_tooltip']                = 'Set this field to 1 if you want to start from beginning';
$lang['settings_sales_decrement_invoice_number_on_delete']         = 'Decrement invoice number on delete';
$lang['settings_sales_decrement_invoice_number_on_delete_tooltip'] = 'Do you want to decrement the invoice number when the last invoice is deleted? eq. If is set this option to YES and before invoice delete the next invoice number is 15 the next invoice number will decrement to 14. If is set to NO the number will remain to 15.  If you have setup delete only on last invoice to NO you should set this option to NO too to keep the next invoice number not decremented.';
$lang['settings_sales_invoice_number_format']                      = 'Invoice Number Format';
$lang['settings_sales_invoice_number_format_year_based']           = 'Year Based';
$lang['settings_sales_invoice_number_format_number_based']         = 'Number Based (000001)';
$lang['settings_sales_company_info_note']                          = 'These information will be displayed on invoices/estimates/payments and other PDF documents where company info is required';
$lang['settings_sales_company_name']                               = 'Company Name';
$lang['settings_sales_address']                                    = 'Διεύθυνση';
$lang['settings_sales_city']                                       = 'Πόλη';
$lang['settings_sales_country_code']                               = 'Country Code';
$lang['settings_sales_postal_code']                                = 'Τ.Κ.';
$lang['settings_sales_phonenumber']                                = 'Τηλέφωνο';

# Leads
$lang['new_lead']                          = 'Νέος μη Πελάτης';
$lang['leads']                             = 'Μη Πελάτες';
$lang['lead']                              = 'Μη Πελάτης';
$lang['lead_lowercase']                    = 'μη πελάτης';
$lang['leads_all']                         = 'Όλοι';
$lang['leads_canban_notes']                = 'Σημειώσεις: %s';
$lang['leads_canban_source']               = 'Πηγή: %s';
$lang['lead_new_source']                   = 'Νέα Πηγή';
$lang['lead_sources']                      = 'Lead Sources';
$lang['lead_source']                       = 'Πηγή Επαφής';
$lang['lead_source_lowercase']             = 'πηγή επαφής';
$lang['leads_sources_not_found']           = 'No leads sources found';
$lang['leads_sources_table_name']          = 'Source Name';
$lang['leads_source_add_edit_name']        = 'Source Name';
$lang['lead_new_status']                   = 'New Lead Status';
$lang['lead_status']                       = 'Lead Status';
$lang['lead_status_lowercase']             = 'lead status';
$lang['leads_status_table_name']           = 'Status Name';
$lang['leads_status_add_edit_name']        = 'Status Name';
$lang['leads_status_add_edit_order']       = 'Order';
$lang['lead_statuses_not_found']           = 'No leads statuses found';
$lang['leads_search']                      = 'Search Leads';
$lang['leads_table_total']                 = 'Total Leads: %s';
$lang['leads_dt_name']                     = 'Όνομα';
$lang['leads_dt_email']                    = 'Email';
$lang['leads_dt_phonenumber']              = 'Τηλέφωνο';
$lang['leads_dt_assigned']                 = 'Ανατέθηκε';
$lang['leads_dt_status']                   = 'Κατάσταση';
$lang['leads_dt_last_contact']             = 'Τελευταία επικοινωνία';
$lang['lead_add_edit_name']                = 'Ονοματεπώνυμο';
$lang['lead_trade_name']                   = 'Διακριτικός τίτλος';
$lang['lead_occupation']                   = 'Επάγγελμα';
$lang['lead_vat_number']				   = 'ΑΦΜ';
$lang['lead_doy']								 = 'ΔΟΥ';
$lang['lead_add_edit_email']               = 'Διεύθυνση Email';
$lang['lead_add_edit_phonenumber']         = 'Τηλέφωνο';
$lang['lead_add_edit_source']              = 'Πηγή';
$lang['lead_add_edit_status']              = 'Κατάσταση';
$lang['lead_add_edit_assigned']            = 'Ανατέθηκε';
$lang['lead_add_edit_datecontacted']       = 'Ημερομηνία επικοινωνίας';
$lang['lead_add_edit_contacted_today']     = 'Επικοινώνησε σήμερα';
$lang['lead_add_edit_activity']            = 'Δραστηριότητα';
$lang['lead_add_edit_notes']               = 'Σημειώσεις';
$lang['lead_add_edit_add_note']            = 'Προσθήκη σημείωσης';
$lang['lead_not_contacted']                = 'Δεν έχω επικοινωνήσει με την επαφή';
$lang['lead_add_edit_contacted_this_lead'] = 'Έχω επικοινωνήσει με την επαφή';

# Misc
$lang['access_denied'] = 'Δεν έχετε δικαίωμα πρόσβασης';
$lang['prev']          = 'Προηγ';
$lang['next']          = 'Επομ';

# Datatables
$lang['dt_paginate_first']    = 'Αρχή';
$lang['dt_paginate_last']     = 'Τέλος';
$lang['dt_paginate_next']     = 'Επόμενο';
$lang['dt_paginate_previous'] = 'Προηγούμενο';
$lang['dt_search']            = 'Αναζήτηση...';
$lang['dt_zero_records']      = 'Δεν βρέθηκαν εγγραφές';
$lang['dt_loading_records']   = 'Φόρτωση...';
$lang['dt_length_menu']       = 'Προβολή _MENU_ εγγραφών';
$lang['dt_info_filtered']     = '(filtered from _MAX_ total {0})';
$lang['dt_info_empty']        = 'Προβολή 0 μέχρι 0 από 0 {0}';
$lang['dt_info']              = 'Προβολή _START_ μέχρι _END_ από _TOTAL_ {0}';
$lang['dt_empty_table']       = 'Δεν βρέθηκαν {0}';
$lang['dt_sort_ascending']    = ' ενεργοποιήστε για αύξουσα ταξινόμηση';
$lang['dt_sort_descending']   = ' ενεργοποιήστε για φθήνουσα ταξινόμηση';

# Invoice Activity Log
$lang['user_sent_overdue_reminder'] = '%s sent invoice overdue reminder';

# Weekdays
$lang['wd_monday']    = 'Δευτέρα';
$lang['wd_tuesday']   = 'Τρίτη';
$lang['wd_thursday']  = 'Τετάρτη';
$lang['wd_wednesday'] = 'Πέμπτη';
$lang['wd_friday']    = 'Παρασκευή';
$lang['wd_saturday']  = 'Σαββάτο';
$lang['wd_sunday']    = 'Κυριακή';

# Admin Left Sidebar
$lang['als_actions']			   = 'Κινήσεις';
$lang['als_clients_actions']	   = 'Κινήσεις Πελατών';
$lang['als_suppliers_actions']	   = 'Κινήσεις Προμηθευτών';
$lang['als_dashboard']             = 'Αρχική';
$lang['als_clients']               = 'Πελατες';
$lang['als_leads']                 = 'Μη Πελατες';
$lang['als_contracts']             = 'Συμβάσεις';
$lang['als_sales']                 = 'Πωλήσεις';
$lang['als_staff']                 = 'Προσωπικό';
$lang['als_tasks']                 = 'Εργασίες';
$lang['als_kb']                    = 'Γνωσιακή Βάση';
$lang['als_surveys']               = 'Έρευνες';
$lang['als_media']                 = 'Πολυμέσα';
$lang['als_reports']               = 'Αναφορές';
$lang['als_reports_sales_submenu'] = 'Πωλήσεις';
$lang['als_reports_leads_submenu'] = 'Μη Πελατες';
$lang['als_kb_articles_submenu']   = 'KB Άρθρα';
$lang['als_utilities']             = 'Χρήσιμα';
$lang['als_announcements_submenu'] = 'Ανακοινώσεις';
$lang['als_calendar_submenu']      = 'Ημερολόγιο';
$lang['als_activity_log_submenu']  = 'Αρχείο Δραστηριοτήτων';

# Admin Customizer Sidebar
$lang['acs_ticket_priority_submenu']           = 'Προτεραιότητα Αιτήματος';
$lang['acs_ticket_statuses_submenu']           = 'Κατάσταση Αιτημάτων';
$lang['acs_ticket_predefined_replies_submenu'] = 'Προεπιλεγμένες απαντήσεις';
$lang['acs_ticket_services_submenu']           = 'Υπηρεσίες';
$lang['acs_departments']                       = 'Τμήματα';
$lang['acs_leads']                             = 'Επαφές';
$lang['acs_leads_sources_submenu']             = 'Πηγές';
$lang['acs_leads_statuses_submenu']            = 'Καταστάσεις';
$lang['acs_sales_taxes_submenu']               = 'Ποσοστά ΦΠΑ';
$lang['acs_sales_currencies_submenu']          = 'Νόμισμα';
$lang['acs_sales_payment_modes_submenu']       = 'Τρόποι πληρωμής';
$lang['acs_email_templates']                   = 'Πρότυπα Email';
$lang['acs_roles']                             = 'Ρόλοι';
$lang['acs_settings']                          = 'Ρυθμίσεις';

# Tickets
$lang['new_ticket']                                          = 'Open New Ticket';
$lang['tickets']                                             = 'Tickets';
$lang['ticket']                                              = 'Ticket';
$lang['ticket_lowercase']                                    = 'ticket';
$lang['support_tickets']                                     = 'Support Tickets';
$lang['support_ticket']                                      = 'Support Ticket';
$lang['ticket_settings_to']                                  = 'Name';
$lang['ticket_settings_email']                               = 'Email address';
$lang['ticket_settings_departments']                         = 'Τμήμα';
$lang['ticket_settings_service']                             = 'Service';
$lang['ticket_settings_priority']                            = 'Προτεραιότητα';
$lang['ticket_settings_subject']                             = 'Θέμα';
$lang['ticket_settings_assign_to']                           = 'Assign ticket (default is current user)';
$lang['ticket_add_body']                                     = 'Ticket Body';
$lang['ticket_add_attachments']                              = 'Attachments';
$lang['ticket_no_reply_yet']                                 = 'No Reply Yet';
$lang['new_ticket_added_successfully']                       = 'Ticket #%s added successfully';
$lang['replied_to_ticket_successfully']                      = 'Replied to ticket #%s successfully';
$lang['ticket_settings_updated_successfully']                = 'Ticket settings updated successfully';
$lang['ticket_settings_updated_successfully_and_reassigned'] = 'Ticket settings updated successfully - reassigned to department %s';
$lang['ticket_dt_subject']                                   = 'Subject';
$lang['ticket_dt_department']                                = 'Department';
$lang['ticket_dt_service']                                   = 'Service';
$lang['ticket_dt_submitter']                                 = 'Contact';
$lang['ticket_dt_status']                                    = 'Κατάσταση';
$lang['ticket_dt_priority']                                  = 'Προτεραιότητα';
$lang['ticket_dt_last_reply']                                = 'Last Reply';
$lang['ticket_single_add_reply']                             = 'Add Reply';
$lang['ticket_single_add_note']                              = 'Add note';
$lang['ticket_single_other_user_tickets']                    = 'Other Tickets';
$lang['ticket_single_settings']                              = 'Settings';
$lang['ticket_single_priority']                              = 'Προτεραιότητα: %s';
$lang['ticket_single_last_reply']                            = 'Last Reply: %s';
$lang['ticket_single_ticket_note_by']                        = 'Ticket note by %s';
$lang['ticket_single_note_added']                            = 'Note added: %s';
$lang['ticket_single_change_status']                         = 'Change Κατάσταση';
$lang['ticket_single_assign_to_me_on_update']                = 'Assign this ticket to me automatically';
$lang['ticket_single_insert_predefined_reply']               = 'Insert predefined reply';
$lang['ticket_single_insert_knowledge_base_link']            = 'Insert knowledge base link';
$lang['ticket_single_attachments']                           = 'Attachments';
$lang['ticket_single_add_response']                          = 'Add Response';
$lang['ticket_single_note_heading']                          = 'Note';
$lang['ticket_single_add_note']                              = 'Add note';
$lang['ticket_settings_none_assigned']                       = 'None';
$lang['ticket_status_changed_successfully']                  = 'Ticket Κατάσταση Changed';
$lang['ticket_status_changed_fail']                          = 'Problem Changing Ticket Κατάσταση';
$lang['ticket_staff_string']                                 = 'Staff';
$lang['ticket_client_string']                                = 'Πελάτης';
$lang['ticket_posted']                                       = 'Posted: %s';
$lang['ticket_access_by_department_denied']                  = 'You do not have access to this ticket. This ticket belongs to department that you are not assigned.';

# Staff
$lang['new_staff']                                     = 'New Staff Member';
$lang['staff_members']                                 = 'Staff Members';
$lang['staff_member']                                  = 'Staff Member';
$lang['staff_member_lowercase']                        = 'staff member';
$lang['staff_profile_updated']                         = 'Your Profile has Been Updated';
$lang['staff_old_password_incorrect']                  = 'Your old password is incorrect';
$lang['staff_password_changed']                        = 'Your password has been changed';
$lang['staff_problem_changing_password']               = 'Problem changing your password';
$lang['staff_profile_string']                          = 'Profile';
$lang['staff_cant_remove_main_admin']                  = 'Cant remove main administrator';
$lang['staff_cant_remove_yourself_from_admin']         = 'You cant remove yourself the administrator role';
$lang['staff_dt_name']                                 = 'Full Name';
$lang['staff_dt_email']                                = 'Email';
$lang['staff_dt_last_Login']                           = 'Last Login';
$lang['staff_dt_active']                               = 'Ενεργός';
$lang['staff_add_edit_firstname']                      = 'First Name';
$lang['staff_add_edit_lastname']                       = 'Last Name';
$lang['staff_add_edit_email']                          = 'Email';
$lang['staff_add_edit_phonenumber']                    = 'Τηλέφωνο';
$lang['staff_add_edit_facebook']                       = 'Facebook';
$lang['staff_add_edit_linkedin']                       = 'LinkedIn';
$lang['staff_add_edit_skype']                          = 'Skype';
$lang['staff_add_edit_departments']                    = 'Member departments';
$lang['staff_add_edit_role']                           = 'Role';
$lang['staff_add_edit_permissions']                    = 'Permissions';
$lang['staff_add_edit_administrator']                  = 'Administrator';
$lang['staff_add_edit_password']                       = 'Password';
$lang['staff_add_edit_password_note']                  = 'Note: if you populate this field, password will be changed on this member.';
$lang['staff_add_edit_password_last_changed']          = 'Password last changed';
$lang['staff_add_edit_notes']                          = 'Notes';
$lang['staff_add_edit_note_description']               = 'Note description';
$lang['staff_notes_table_description_heading']         = 'Note';
$lang['staff_notes_table_addedfrom_heading']           = 'Added From';
$lang['staff_notes_table_dateadded_heading']           = 'Date Added';
$lang['staff_admin_profile']                           = 'This is admin profile';
$lang['staff_profile_notifications']                   = 'Notifications';
$lang['staff_profile_departments']                     = 'Departments';
$lang['staff_edit_profile_image']                      = 'Profile Image';
$lang['staff_edit_profile_your_departments']           = 'Departments';
$lang['staff_edit_profile_change_your_password']       = 'Change your password';
$lang['staff_edit_profile_change_old_password']        = 'Old password';
$lang['staff_edit_profile_change_new_password']        = 'New password';
$lang['staff_edit_profile_change_repeat_new_password'] = 'Repeat new password';

# Surveys
$lang['new_survey']                                = 'New Survey';
$lang['surveys']                                   = 'Surveys';
$lang['survey']                                    = 'Survey';
$lang['survey_lowercase']                          = 'survey';
$lang['survey_no_mail_lists_selected']             = 'No mail lists selected';
$lang['survey_send_success_note']                  = 'All Survey Emails(%s) will be send via CRON';
$lang['survey_result']                             = 'Result for Survey: %s';
$lang['question_string']                           = 'Question';
$lang['question_field_string']                     = 'Field';
$lang['survey_list_view_tooltip']                  = 'View Survey';
$lang['survey_list_view_results_tooltip']          = 'View Results';
$lang['survey_add_edit_subject']                   = 'Survey subject';
$lang['survey_add_edit_email_description']         = 'Survey description (Email Description)';
$lang['survey_include_survey_link']                = 'Include survey link in description';
$lang['survey_available_mail_lists_custom_fields'] = 'Available custom fields from email lists';
$lang['survey_mail_lists_custom_fields_tooltip']   = 'Custom fields can be used for email editor.';
$lang['survey_add_edit_short_description_view']    = 'Survey short description (View Description)';
$lang['survey_add_edit_from']                      = 'From (displayed in email)';
$lang['survey_add_edit_redirect_url']              = 'Survey redirect URL';
$lang['survey_add_edit_red_url_note']              = 'When user finish survey where to be redirected (leave blank for this site url)';
$lang['survey_add_edit_disabled']                  = 'Disabled';
$lang['survey_add_edit_only_for_logged_in']        = 'Only for logged in participants (staff,customers)';
$lang['send_survey_string']                        = 'Send Survey';
$lang['survey_send_mail_list_clients']             = 'Πελάτες';
$lang['survey_send_mail_list_staff']               = 'Προσωπικό';
$lang['survey_send_mail_lists_string']             = 'Mail Lists';
$lang['survey_send_mail_lists_note_logged_in']     = 'Note: If you are sending survey to mail lists Only for logged in participants need to be unchecked';
$lang['survey_send_string']                        = 'Αποστολή';
$lang['survey_send_to_total']                      = 'Send to total %s emails';
$lang['survey_send_till_now']                      = 'Till now';
$lang['survey_send_finished']                      = 'Survey send finished: %s';
$lang['survey_added_to_queue']                     = 'This survey is added to cron queue on %s';
$lang['survey_questions_string']                   = 'Ερωτήσεις';
$lang['survey_insert_field']                       = 'Insert Field';
$lang['survey_field_checkbox']                     = 'Checkbox';
$lang['survey_field_radio']                        = 'Radio';
$lang['survey_field_input']                        = 'Input Field';
$lang['survey_field_textarea']                     = 'Text area';
$lang['survey_question_required']                  = 'Required';
$lang['survey_question_only_for_preview']          = 'Only for preview';
$lang['survey_create_first']                       = 'You need to create the survey first then you will be able to insert the questions.';
$lang['survey_dt_name']                            = 'Name';
$lang['survey_dt_total_questions']                 = 'Total Questions';
$lang['survey_dt_total_participants']              = 'Total Participants';
$lang['survey_dt_date_created']                    = 'Ημερομηνία Δημιουργίας';
$lang['survey_dt_active']                          = 'Ενεργή';
$lang['survey_text_questions_results']             = 'Text questions result';
$lang['survey_view_all_answers']                   = 'View all answers';

# Staff Tasks
$lang['new_task']                           = 'Νέα Εργασία';
$lang['tasks']                              = 'Εργασίες';
$lang['task']                               = 'Εργασία';
$lang['task_lowercase']                     = 'εργασία';
$lang['comment_string']                     = 'Σχόλιο';
$lang['task_marked_as_complete']            = 'Η Εργασία ορίστηκε ως ολοκληρωμένη';
$lang['task_follower_removed']              = 'Task follower removed successfully';
$lang['task_assignee_removed']              = 'Task assignee removed successfully';
$lang['task_no_assignees']                  = 'Η εργασία δεν έχει ανατεθεί';
$lang['task_no_followers']                  = 'No followers for this task';
$lang['task_list_all']                      = 'Όλα';
$lang['task_list_not_assigned']             = 'Δεν έχει ανατεθεί';
$lang['task_list_duedate_passed']           = 'Due Date Passed';
$lang['tasks_dt_name']                      = 'Όνομα';
$lang['task_single_priority']               = 'Προτεραιότητα';
$lang['task_single_start_date']             = 'Ημερομηνία Έναρξης';
$lang['task_single_due_date']               = 'Ημερομηνία Λήξης';
$lang['task_single_finished']               = 'Ολοκληρωμένη';
$lang['task_single_mark_as_complete']       = 'Σήμανση ως Ολοκληρωμένη';
$lang['task_single_edit']                   = 'Επεξεργασία';
$lang['task_single_delete']                 = 'Διαγραφή';
$lang['task_single_assignees']              = 'Αποδέκτες';
$lang['task_single_assignees_select_title'] = 'Ανάθεση Εργασίας';
$lang['task_single_followers']              = 'Followers';
$lang['task_single_followers_select_title'] = 'Add Followers';
$lang['task_single_add_new_comment']        = 'Προσθήκη Σχολίου';
$lang['task_add_edit_subject']              = 'Θέμα';
$lang['task_add_edit_priority']             = 'Προτεραιότητα';
$lang['task_priority_low']                  = 'Χαμηλή';
$lang['task_priority_medium']               = 'Κανονική';
$lang['task_priority_high']                 = 'Υψηλή';
$lang['task_priority_urgent']               = 'Επείγων';
$lang['task_add_edit_start_date']           = 'Ημερομηνία Έναρξης';
$lang['task_add_edit_due_date']             = 'Ημερομηνία Λήξης';
$lang['task_add_edit_description']          = 'Περιγραφή Εργασίας';

# Taxes
$lang['new_tax']           = 'Νεος Φόρος';
$lang['taxes']             = 'Φόροι';
$lang['tax']               = 'Φόρος';
$lang['tax_lowercase']     = 'φόρος';
$lang['tax_dt_name']       = 'Όνομα Φόρου';
$lang['tax_dt_rate']       = 'Ποσοστό Φόρου';
$lang['tax_add_edit_name'] = 'Όνομα Φόρου';
$lang['tax_add_edit_rate'] = 'Tax Rate (percent)';
$lang['tax_edit_title']    = 'Edit Tax';
$lang['tax_add_title']     = 'Add New Tax';

# Ticket Κατάστασηes
$lang['new_ticket_status']            = 'New Ticket Κατάσταση';
$lang['ticket_statuses']              = 'Ticket Κατάστασηes';
$lang['ticket_status']                = 'Ticket Κατάσταση';
$lang['ticket_status_lowercase']      = 'ticket status';
$lang['ticket_statuses_dt_name']      = 'Ticket Κατάσταση Name';
$lang['no_ticket_statuses_found']     = 'No ticket statuses found';
$lang['ticket_statuses_table_total']  = 'Total %s';
$lang['ticket_status_add_edit_name']  = 'Ticket Κατάσταση Name';
$lang['ticket_status_add_edit_color'] = 'Pick Color';
$lang['ticket_status_add_edit_order'] = 'Κατάσταση Order';

# Todos
$lang['new_todo']                  = 'New To Do';
$lang['my_todos']                  = 'My To Do Items';
$lang['todo']                      = 'Todo Item';
$lang['todo_lowercase']            = 'todo';
$lang['todo_status_changed']       = 'Todo Κατάσταση Changed';
$lang['todo_add_title']            = 'Add New Todo';
$lang['add_new_todo_description']  = 'Περιγραφή';
$lang['no_finished_todos_found']   = 'No finished todos found';
$lang['no_unfinished_todos_found'] = 'No todos found';
$lang['unfinished_todos_title']    = 'Unfinished to do\'s';
$lang['finished_todos_title']      = 'Latest finished to do\'s';

# Utilities
$lang['utility_activity_log']                        = 'Activity Log';
$lang['utility_activity_log_filter_by_date']         = 'Filter by date';
$lang['utility_activity_log_dt_description']         = 'Περιγραφή';
$lang['utility_activity_log_dt_date']                = 'Date';
$lang['utility_activity_log_dt_staff']               = 'Staff';
$lang['utility_calendar_new_event_title']            = 'Add new event';
$lang['utility_calendar_new_event_start_date']       = 'Ημερομηνία Έναρξης';
$lang['utility_calendar_new_event_end_date']         = 'Ημερομηνία Λήξης';
$lang['utility_calendar_new_event_make_public']      = 'Public Event';
$lang['utility_calendar_event_added_successfully']   = 'New event added successfully';
$lang['utility_calendar_event_deleted_successfully'] = 'Event deleted';
$lang['utility_calendar_new_event_placeholder']      = 'Event title';

# Navigation
$lang['nav_notifications']          = 'Ειδοποιήσεις';
$lang['nav_my_profile']             = 'Ο Λογαριασμός μου';
$lang['nav_edit_profile']           = 'Επεξεργασία Λογαριασμού';
$lang['nav_logout']                 = 'Αποσύνδεση';
$lang['nav_no_notifications']       = 'Δεν βρέθηκαν Ειδοποιήσεις';
$lang['nav_view_all_notifications'] = 'Προβολή όλων των Ειδοποιήσεων';
$lang['nav_notifications_tooltip']  = 'Προβολή Ειδοποιήσεων';

# Footer
$lang['clients_copyright'] = 'Copyright %s';

# Contracts
$lang['clients_contracts']               = 'Contracts';
$lang['clients_contracts_dt_subject']    = 'Θέμα';
$lang['clients_contracts_dt_start_date'] = 'Ημερομηνία Έναρξης';
$lang['clients_contracts_dt_end_date']   = 'Ημερομηνία Λήξης';

# Home
$lang['clients_quick_invoice_info']           = 'Quick Invoices Info';
$lang['clients_home_currency_select_tooltip'] = 'You need to select currency because you have invoices with different currency';

# Invoices
$lang['clients_invoice_html_btn_download'] = 'Download';
$lang['clients_my_invoices']               = 'Invoices';
$lang['clients_invoice_dt_number']         = 'Invoice #';
$lang['clients_invoice_dt_date']           = 'Date';
$lang['clients_invoice_dt_duedate']        = 'Due Date';
$lang['clients_invoice_dt_amount']         = 'Amount';
$lang['clients_invoice_dt_status']         = 'Κατάσταση';

# Profile
$lang['clients_profile_heading'] = 'Στοιχεία Πελάτη';

# Used for edit profile and register START
$lang['clients_firstname'] = 'Όνομα';
$lang['clients_lastname']  = 'Επώνυμο';
$lang['clients_email']     = 'Email';
$lang['clients_company']   = 'Εταιρία';
$lang['clients_vat']       = 'ΑΦΜ';
$lang['clients_phone']     = 'Τηλέφωνο';
$lang['clients_country']   = 'Χώρα';
$lang['clients_city']      = 'Πόλη';
$lang['clients_address']   = 'Διεύθυνση';
$lang['clients_zip']       = 'ΤΚ';
$lang['clients_state']     = 'State';
# Used for edit profile and register END

$lang['clients_register_password']                    = 'Password';
$lang['clients_register_password_repeat']             = 'Repeat Password';
$lang['clients_edit_profile_update_btn']              = 'Update';
$lang['clients_edit_profile_change_password_heading'] = 'Change Password';
$lang['clients_edit_profile_old_password']            = 'Old Password';
$lang['clients_edit_profile_new_password']            = 'New Password';
$lang['clients_edit_profile_new_password_repeat']     = 'Repeat Password';
$lang['clients_edit_profile_change_password_btn']     = 'Change Password';
$lang['clients_profile_last_changed_password']        = 'Password last changed %s';

# Knowledge base
$lang['clients_knowledge_base']                    = 'Knowledge Base';
$lang['clients_knowledge_base_articles_not_found'] = 'No knowledge base articles found';
$lang['clients_knowledge_base_find_useful']        = 'Did you find this article useful?';
$lang['clients_knowledge_base_find_useful_yes']    = 'Yes';
$lang['clients_knowledge_base_find_useful_no']     = 'No';
$lang['clients_article_only_1_vote_today']         = 'You can vote once in 24 hours';
$lang['clients_article_voted_thanks_for_feedback'] = 'Thanks for your feedback';

# Tickets
$lang['clients_ticket_open_subject']               = 'Open Ticket';
$lang['clients_ticket_open_departments']           = 'Department';
$lang['clients_tickets_heading']                   = 'Support Tickets';
$lang['clients_ticket_open_service']               = 'Service';
$lang['clients_ticket_open_priority']              = 'Προτεραιότητα';
$lang['clients_ticket_open_body']                  = 'Ticket Body';
$lang['clients_ticket_attachments']                = 'Attachments';
$lang['clients_single_ticket_string']              = 'Ticket';
$lang['clients_single_ticket_replied']             = 'Replied: %s';
$lang['clients_single_ticket_information_heading'] = 'Ticket Information';
$lang['clients_tickets_dt_number']                 = 'Ticket #';
$lang['clients_tickets_dt_subject']                = 'Subject';
$lang['clients_tickets_dt_department']             = 'Department';
$lang['clients_tickets_dt_service']                = 'Service';
$lang['clients_tickets_dt_status']                 = 'Κατάσταση';
$lang['clients_tickets_dt_last_reply']             = 'Last Reply';
$lang['clients_ticket_single_department']          = 'Department: %s';
$lang['clients_ticket_single_submitted']           = 'Submitted: %s';
$lang['clients_ticket_single_status']              = 'Κατάσταση:';
$lang['clients_ticket_single_priority']            = 'Προτεραιότητα: %s';
$lang['clients_ticket_single_add_reply_btn']       = 'Add Reply';
$lang['clients_ticket_single_add_reply_heading']   = 'Add reply to this ticket';

# Login
$lang['clients_login_heading_no_register'] = 'Παρακαλώ κάντε Είσοδο';
$lang['clients_login_heading_register']    = 'Παρακαλώ κάντε Είσοδο ή  Εγγραφή';
$lang['clients_login_email']               = 'Διεύθυνση Email';
$lang['clients_login_password']            = 'Κωδικός';
$lang['clients_login_remember']            = 'Να με θυμάσαι';
$lang['clients_login_login_string']        = 'Είσοδος';

# Register
$lang['clients_register_string']  = 'Εγγραφή';
$lang['clients_register_heading'] = 'Εγγραφή';

# Navigation
$lang['clients_nav_login']     = 'Είσοδος';
$lang['clients_nav_register']  = 'Εγγραφή';
$lang['clients_nav_invoices']  = 'Τιμολόγια';
$lang['clients_nav_contracts'] = 'Συμβάσεις';
$lang['clients_nav_kb']        = 'Γνωσιακή Βάση';
$lang['clients_nav_profile']   = 'Στοιχεία';
$lang['clients_nav_logout']    = 'Αποσύνδεση';

# Version 1.0.1
$lang['payment_receipt']                               = 'Payment Receipt';
$lang['payment_for_string']                            = 'Payment For';
$lang['payment_date']                                  = 'Payment Date:';
$lang['payment_view_mode']                             = 'Payment Mode:';
$lang['payment_total_amount']                          = 'Total Amount';
$lang['payment_table_invoice_number']                  = 'Invoice Number';
$lang['payment_table_invoice_date']                    = 'Invoice Date';
$lang['payment_table_invoice_amount_total']            = 'Invoice Amount';
$lang['payment_table_payment_amount_total']            = 'Payment Amount';
$lang['payments_table_transaction_id']                 = 'Transaction ID: %s';
$lang['payment_getaway_token_not_found']               = 'Token Not Found';
$lang['online_payment_recorded_success']               = 'Payment recorded successfully';
$lang['online_payment_recorded_success_fail_database'] = 'Payment is successful but failed to insert payment to database, contact administrator';

# Leads
$lang['lead_convert_to_client']                = 'Μετατροπή σε πελάτη';
$lang['lead_convert_to_email']                 = 'Email';
$lang['lead_convert_to_client_firstname']      = 'Όνομα';
$lang['lead_convert_to_client_lastname']       = 'Επώνυμο';
$lang['lead_email_already_exists']             = 'Το email της επαφής υπάρχει ήδη σε κάποιο πελάτη';
$lang['lead_to_client_base_converted_success'] = 'Η επαφή μετατράπηκε σε πελάτη';
$lang['lead_have_client_profile']              = 'Αυτή η επαφή έχει εγγραφή πελάτη.';
$lang['lead_converted_edit_client_profile']    = 'Επεξεργασία';

# Invoices
$lang['view_invoice_as_customer_tooltip']                                     = 'View invoice as customer';
$lang['invoice_add_edit_recurring']                                           = 'Recurring Invoice?';
$lang['invoice_add_edit_recurring_no']                                        = 'No';
$lang['invoice_add_edit_recurring_month']                                     = 'Every %s month';
$lang['invoice_add_edit_recurring_months']                                    = 'Every %s months';
$lang['invoices_list_all']                                                    = 'All';
$lang['invoices_list_not_have_payment']                                       = 'Invoices with no payment records';
$lang['invoices_list_recurring']                                              = 'Recurring Invoices';
$lang['invoices_list_made_payment_by']                                        = 'Made Payment by %s';
$lang['invoices_create_invoice_from_recurring_only_on_paid_invoices']         = 'Create new invoice from recurring invoice only if the invoice is with status paid?';
$lang['invoices_create_invoice_from_recurring_only_on_paid_invoices_tooltip'] = 'If this field is set to YES and the recurring invoices is not with status PAID, the new invoice will NOT be created.';
$lang['view_invoice_pdf_link_pay']                                            = 'Pay Invoice';

# Payment modes
$lang['payment_mode_add_edit_description']         = 'Bank Accounts / Description';
$lang['payment_mode_add_edit_description_tooltip'] = 'You can set here bank accounts information. Will be shown on HTML Invoice';
$lang['payment_modes_dt_description']              = 'Bank Accounts / Description';
$lang['payment_modes_add_edit_announcement']       = 'Note: Payment modes listed below are offline modes. Online payment modes can be configured in Setup -> Settings -> Payment Gateways';
$lang['payment_mode_add_edit_active']              = 'Ενεργό';
$lang['payment_modes_dt_active']                   = 'Ενεργό';

# Contracts
$lang['contract_not_found'] = 'Contract not found. Maybe is deleted, check activity log';

# Settings
$lang['setting_bar_heading']                               = 'Setup';
$lang['settings_group_online_payment_modes']               = 'Payment Gateways';
$lang['settings_paymentmethod_mode_label']                 = 'Label';
$lang['settings_paymentmethod_active']                     = 'Active';
$lang['settings_paymentmethod_currencies']                 = 'Currencies (coma separated)';
$lang['settings_paymentmethod_testing_mode']               = 'Enable Test Mode';
$lang['settings_paymentmethod_paypal_username']            = 'PayPal API Username';
$lang['settings_paymentmethod_paypal_password']            = 'PayPal API Password';
$lang['settings_paymentmethod_paypal_signature']           = 'API Signature';
$lang['settings_paymentmethod_stripe_api_secret_key']      = 'Stripe API Secret Key';
$lang['settings_paymentmethod_stripe_api_publishable_key'] = 'Stripe Publishable Key';
$lang['settings_limit_top_search_bar_results']             = 'Limit Top Search Bar Results to';

## Clients
$lang['client_phonenumber'] = 'Τηλέφωνο';

# Main Clients
$lang['clients_register']                          = 'Register';
$lang['clients_profile_updated']                   = 'Your profile has been updated';
$lang['clients_successfully_registered']           = 'Thank your for registering';
$lang['clients_account_created_but_not_logged_in'] = 'Your account has been created but you are not logged in our system automatically. Please try to login';

# Tickets
$lang['clients_tickets_heading'] = 'Support Tickets';

# Payments
$lang['payment_for_invoice'] = 'Payment for Invoice';
$lang['payment_total']       = 'Total: %s';

# Invoice
$lang['invoice_html_online_payment']             = 'Online Payment';
$lang['invoice_html_online_payment_button_text'] = 'Pay Now';
$lang['invoice_html_payment_modes_not_selected'] = 'Please Select Payment Mode';
$lang['invoice_html_amount_blank']               = 'Total amount cant be blank or zero';
$lang['invoice_html_offline_payment']            = 'Offline Payment';
$lang['invoice_html_amount']                     = 'Amount';
# Version 1.0.2
# DataTables
$lang['dt_button_column_visibility'] = 'Visibility';
$lang['dt_button_reload']            = 'Ανανέωση';
$lang['dt_button_excel']             = 'Excel';
$lang['dt_button_csv']               = 'CSV';
$lang['dt_button_pdf']               = 'PDF';
$lang['dt_button_print']             = 'Εκτύπωση';
$lang['is_not_active_export']        = 'Όχι';
$lang['is_active_export']            = 'Ναι';

# Invoice
$lang['invoice_add_edit_advanced_options']                = 'Advanced Options';
$lang['invoice_add_edit_allowed_payment_modes']           = 'Δυνατότητες πληρωμής τιμολογίου';
$lang['invoice_add_edit_recurring_invoices_from_invoice'] = 'Created invoices from this recurring invoice';
$lang['invoice_add_edit_no_payment_modes_found']          = 'No payment modes found.';
$lang['invoice_html_total_pay']                           = 'Total: %s';

# Email templates
$lang['email_templates_table_heading_name'] = 'Template Name';

# General
$lang['discount_type']            = 'Τύπος Έκπτωσης';
$lang['discount_type_after_tax']  = 'Με ΦΠΑ';
$lang['discount_type_before_tax'] = 'Χωρίς ΦΠΑ';
$lang['terms_and_conditions']     = 'Όροι & Προϋποθέσεις';
$lang['reference_no']             = 'Reference #';
$lang['no_discount']              = 'No discount';
$lang['view_stats_tooltip']       = 'View Quick Stats';

# Clients
$lang['zip_from_date']            = 'From Date:';
$lang['zip_to_date']              = 'To Date:';
$lang['client_zip_payments']      = 'ZIP Payments';
$lang['client_zip_invoices']      = 'ZIP Invoices';
$lang['client_zip_estimates']     = 'ZIP Estimates';
$lang['client_zip_status']        = 'Κατάσταση';
$lang['client_zip_status_all']    = 'All';
$lang['client_zip_payment_modes'] = 'Payment made by';
$lang['client_zip_no_data_found'] = 'No %s found';

# Payments
$lang['payment_mode']         = 'Payment Mode';
$lang['payment_view_heading'] = 'Payment';

# Settings
$lang['settings_allow_payment_amount_to_be_modified']               = 'Allow customer to modify the amount to pay (for online payments)';
$lang['settings_survey_send_emails_per_cron_run']                   = 'How much emails to sent per hour';
$lang['settings_survey_send_emails_per_cron_run_tooltip']           = 'This option is used when sending Surveys. The Survey cron will sent X emails per hour. Some hosting providers have limit for sending emails per hour.';
$lang['settings_delete_only_on_last_invoice']                       = 'Delete invoice allowed only on last invoice';
$lang['settings_sales_estimate_prefix']                             = 'Estimate Number Prefix';
$lang['settings_sales_next_estimate_number']                        = 'Next estimate Number';
$lang['settings_sales_next_estimate_number_tooltip']                = 'Set this field to 1 if you want to start from beginning';
$lang['settings_sales_decrement_estimate_number_on_delete']         = 'Decrement estimate number on delete';
$lang['settings_sales_decrement_estimate_number_on_delete_tooltip'] = 'Do you want to decrement the estimate number when the last estimate is deleted? eq. If is set this option to YES and before estimate delete the next estimate number is 15 the next estimate number will decrement to 14.If is set to NO the number will remain to 15. If you have setup delete only on last estimate to NO you should set this option to NO too to keep the next estimate number not decremented.';
$lang['settings_sales_estimate_number_format']                      = 'Estimate Number Format';
$lang['settings_sales_estimate_number_format_year_based']           = 'Year Based';
$lang['settings_sales_estimate_number_format_number_based']         = 'Number Based (000001)';
$lang['settings_delete_only_on_last_estimate']                      = 'Delete estimate allowed only on last invoice';
$lang['settings_send_test_email_heading']                           = 'Send Test Email';
$lang['settings_send_test_email_subheading']                        = 'Send test email to make sure that your SMTP settings is set correctly.';
$lang['settings_send_test_email_string']                            = 'Email Address';
$lang['settings_smtp_settings_heading']                             = 'SMTP Settings';
$lang['settings_smtp_settings_subheading']                          = 'Setup main email';
$lang['settings_sales_heading_general']                             = 'General';
$lang['settings_sales_heading_invoice']                             = 'Invoice';
$lang['settings_sales_heading_estimates']                           = 'Estimates';
$lang['settings_sales_cron_invoice_heading']                        = 'Invoice';

# Tasks
$lang['tasks_dt_datestart'] = 'Ημερομηνία Έναρξης';

# Invoice General
$lang['invoice_discount'] = 'Έκπτωση';

# Settings
$lang['settings_rtl_support_admin']                                   = 'RTL Admin Area (Right to Left)';
$lang['settings_rtl_support_client']                                  = 'RTL Customers Area (Right to Left)';
$lang['settings_estimate_auto_convert_to_invoice_on_client_accept']   = 'Auto convert the estimate to invoice after client accept';
$lang['settings_exclude_estimate_from_client_area_with_draft_status'] = 'Exclude estimates with draft status from customers area';

# Months
$lang['January']   = 'Ιανουάριος';
$lang['February']  = 'Φεβρουάριος';
$lang['March']     = 'Μάρτιος';
$lang['April']     = 'Απρίλιος';
$lang['May']       = 'Μάιος';
$lang['June']      = 'Ιούνιος';
$lang['July']      = 'Ιούλιος';
$lang['August']    = 'Αύγουστος';
$lang['September'] = 'Σεπτέμβριος';
$lang['October']   = 'Οκτώβριος';
$lang['November']  = 'Νοέμβριος';
$lang['December']  = 'Δεκέμβριος';

# Time ago function translate
$lang['time_ago_just_now']  = 'Just now';
$lang['time_ago_minute']    = 'one minute ago';
$lang['time_ago_minutes']   = '%s minutes ago';
$lang['time_ago_hour']      = 'an hour ago';
$lang['time_ago_hours']     = '%s hrs ago';
$lang['time_ago_yesterday'] = 'yesterday';
$lang['time_ago_days']      = '%s days ago';
$lang['time_ago_week']      = 'a week ago';
$lang['time_ago_weeks']     = '%s weeks ago';
$lang['time_ago_month']     = 'a month ago';
$lang['time_ago_months']    = '%s months ago';
$lang['time_ago_year']      = 'one year ago';
$lang['time_ago_years']     = '%s years ago';

# Estimates
$lang['estimates']                                = 'Estimates';
$lang['estimate']                                 = 'Estimate';
$lang['estimate_lowercase']                       = 'estimate';
$lang['create_new_estimate']                      = 'Create New Estimate';
$lang['view_estimate']                            = 'View estimate';
$lang['estimate_sent_to_client_success']          = 'The estimate is sent successfully to the client';
$lang['estimate_sent_to_client_fail']             = 'Problem while sending the estimate';
$lang['estimate_view']                            = 'View estimate';
$lang['estimate_select_customer']                 = 'Πελάτης';
$lang['estimate_add_edit_number']                 = 'Estimate Number';
$lang['estimate_add_edit_date']                   = 'Estimate Date';
$lang['estimate_add_edit_expirydate']             = 'Expiry Date';
$lang['estimate_add_edit_currency']               = 'Νόμισμα';
$lang['estimate_add_edit_client_note']            = 'Client Note';
$lang['estimate_add_edit_admin_note']             = 'Admin Note';
$lang['estimates_toggle_table_tooltip']           = 'Προβολή Πίνακα';
$lang['estimate_add_edit_advanced_options']       = 'Advanced Options';
$lang['estimate_to']                              = 'To';
$lang['estimates_list_all']                       = 'All';
$lang['estimate_invoiced_date']                   = 'Estimate Invoiced on %s';
$lang['edit_estimate_tooltip']                    = 'Edit Estimate';
$lang['delete_estimate_tooltip']                  = 'Delete Estimate';
$lang['estimate_sent_to_email_tooltip']           = 'Send to Email';
$lang['estimate_already_send_to_client_tooltip']  = 'This estimate is already sent to the client %s';
$lang['estimate_view_activity_tooltip']           = 'Activity Log';
$lang['estimate_send_to_client_modal_heading']    = 'Send estimate to client';
$lang['estimate_send_to_client_attach_pdf']       = 'Attach estimate PDF';
$lang['estimate_send_to_client_preview_template'] = 'Preview Email Template';
$lang['estimate_dt_table_heading_number']         = 'Estimate #';
$lang['estimate_dt_table_heading_date']           = 'Date';
$lang['estimate_dt_table_heading_client']         = 'Πελάτης';
$lang['estimate_dt_table_heading_expirydate']     = 'Expiry Date';
$lang['estimate_dt_table_heading_amount']         = 'Amount';
$lang['estimate_dt_table_heading_status']         = 'Κατάσταση';
$lang['estimate_convert_to_invoice']              = 'Convert to Invoice';

# Clients
$lang['client_payments_tab'] = 'Πληρωμές';

# Estimate General
$lang['estimate_pdf_heading']            = 'ESTIMATE';
$lang['estimate_table_item_heading']     = 'Είδος';
$lang['estimate_table_quantity_heading'] = 'Ποσότητα';
$lang['estimate_table_rate_heading']     = 'Τιμή μονάδας';
$lang['estimate_table_tax_heading']      = 'ΦΠΑ';
$lang['estimate_table_amount_heading']   = 'Αξία';
$lang['estimate_subtotal']               = 'Μερικό Σύνολο';
$lang['estimate_adjustment']             = 'Προσαρμογή';
$lang['estimate_discount']               = 'Έκπτωση';
$lang['estimate_total']                  = 'Σύνολο';
$lang['estimate_to']                     = 'To';
$lang['estimate_data_date']              = 'Estimate Date';
$lang['estimate_data_expiry_date']       = 'Expiry Date';
$lang['estimate_note']                   = 'Σημείωση:';
$lang['estimate_status_draft']           = 'Πρόχειρο';
$lang['estimate_status_sent']            = 'Sent';
$lang['estimate_status_declined']        = 'Declined';
$lang['estimate_status_accepted']        = 'Accepted';
$lang['estimate_status_expired']         = 'Expired';
$lang['estimate_note']                   = 'Note:';

## Clients
$lang['clients_estimate_dt_number']             = 'Estimate #';
$lang['clients_estimate_dt_date']               = 'Ημερομηνία';
$lang['clients_estimate_dt_duedate']            = 'Ημερομηνία Λήξης';
$lang['clients_estimate_dt_amount']             = 'Ποσό';
$lang['clients_estimate_dt_status']             = 'Κατάσταση';
$lang['clients_nav_estimates']                  = 'Estimates';
$lang['clients_decline_estimate']               = 'Decline';
$lang['clients_accept_estimate']                = 'Αποδοχή';
$lang['clients_my_estimates']                   = 'Estimates';
$lang['clients_estimate_invoiced_successfully'] = 'Thank you for accepting the estimate. Please review the created invoice for the estimate';
$lang['clients_estimate_accepted_not_invoiced'] = 'Thank you for accepting this estimate';
$lang['clients_estimate_declined']              = 'Estimate declined. You can accept the estimate any time before expiry date';
$lang['clients_estimate_failed_action']         = 'Failed to take action on this estimate';
$lang['client_add_edit_profile']                = 'Πληροφορίες Πελάτη';
$lang['supplier_add_edit_profile']              = 'Πληροφορίες Προμηθευτή';
$lang['supplier_attachments']                   = 'Αρχεία';
$lang['supplier_map']                           = 'Χάρτης';

# Custom Fields
$lang['custom_field']                          = 'Προσαρμοσμένο Πεδίο';
$lang['custom_field_lowercase']                = 'προσαρμοσμένο πεδίο';
$lang['custom_fields']                         = 'Προσαρμοσμένα Πεδία';
$lang['new_custom_field']                      = 'Νέο Πεδίο';
$lang['custom_field_name']                     = 'Όνομα Πεδίου';
$lang['custom_field_add_edit_type']            = 'Τύπος';
$lang['custom_field_add_edit_belongs_top']     = 'Το πεδίο ανήκει σε';
$lang['custom_field_add_edit_options']         = 'Επιλογές';
$lang['custom_field_add_edit_options_tooltip'] = 'Χρησιμοποιείται μόνο σε πεδία τύπου Select, Checkbox. Οι επιλογές του πεδίου χωρισμένες με κόμα. Π.χ apple,orange,banana';
$lang['custom_field_add_edit_order']           = 'Σειρά';
$lang['custom_field_dt_field_to']              = 'Ανήκει σε';
$lang['custom_field_dt_field_name']            = 'Όνομα';
$lang['custom_field_dt_field_type']            = 'Τύπος';
$lang['custom_field_add_edit_active']          = 'Ενεργό';
$lang['custom_field_add_edit_disabled']        = 'Ανενεργό';

# Ticket replies
$lang['ticket_reply'] = 'Ticket Reply';

# Admin Customizer Sidebar
$lang['asc_custom_fields'] = 'Custom Fields';

# Contracts
$lang['contract_types']           = 'Contracts Types';
$lang['contract_type']            = 'Contract type';
$lang['new_contract_type']        = 'New Contract Type';
$lang['contract_type_lowercase']  = 'contract';
$lang['contract_type_name']       = 'Name';
$lang['contract_types_list_name'] = 'Contract Type';

# Customizer Menu
$lang['acs_contracts']      = 'Contracts';
$lang['acs_contract_types'] = 'Contract Types';

# Version 1.0.4
# Invoice Items
$lang['invoice_item_long_description'] = 'Πλήρης Περιγραφή';

# Customers
$lang['clients_list_phone']                = 'Τηλέφωνο';
$lang['suppliers_list_phone']              = 'Τηλέφωνο';
$lang['client_expenses_tab']               = 'Έξοδα';
$lang['customers_summary']                 = 'Σύνοψη Πελατών';
$lang['customers_summary_active']          = 'Ενεργές Επαφές';
$lang['customers_summary_inactive']        = 'Ανενεργές Επαφές';
$lang['customers_summary_logged_in_today'] = 'Επαφές που συνδέθηκαν σήμερα';

# Authentication
$lang['admin_auth_forgot_password_email']     = 'Διεύθυνση Email';
$lang['admin_auth_forgot_password_heading']   = 'Ξέχασα τον κωδικό μου';
$lang['admin_auth_login_heading']             = 'Είσοδος';
$lang['admin_auth_login_email']               = 'Διεύθυνση Email';
$lang['admin_auth_login_password']            = 'Κωδικός';
$lang['admin_auth_login_remember_me']         = 'Να με θυμάσαι';
$lang['admin_auth_login_button']              = 'Είσοδος';
$lang['admin_auth_login_fp']                  = 'Ξεχάσατε τον κωδικό σας;';
$lang['admin_auth_reset_password_heading']    = 'Επαναφορά κωδικού';
$lang['admin_auth_reset_password']            = 'Κωδικός';
$lang['admin_auth_reset_password_repeat']     = 'Επανάληψη Κωδικού';
$lang['admin_auth_invalid_email_or_password'] = 'Λάθος email ή κωδικός';
$lang['admin_auth_inactive_account']          = 'Ανενεργός Λογαριασμός';

# Calender
$lang['calendar_estimate']          = 'Estimate';
$lang['calendar_invoice']           = 'Invoice';
$lang['calendar_contract']          = 'Contract';
$lang['calendar_customer_reminder'] = 'Client Reminder';
$lang['calendar_event']             = 'Event';
$lang['calendar_task']              = 'Task';

# Leads
$lang['lead_edit_delete_tooltip'] = 'Διαγραφή Επαφής';
$lang['lead_attachments']         = 'Συνημμένα';

# Admin Customizer Sidebar
$lang['acs_finance'] = 'Οικονομικά';

# Settings
$lang['settings_show_sale_agent_on_invoices']       = 'Show Sale Agent On Invoice';
$lang['settings_show_sale_agent_on_estimates']      = 'Show Sale Agent On Estimate';
$lang['settings_predefined_predefined_term']        = 'Predefined Terms & Conditions';
$lang['settings_predefined_clientnote']             = 'Predefined Client Note';
$lang['settings_custom_pdf_logo_image_url']         = 'Custom PDF Company Logo URL';
$lang['settings_custom_pdf_logo_image_url_tooltip'] = 'Probably you will have problems with PNG images with transparency that are handled in different way depending on the php-imagick or php-gd version used. Try to update php-imagick and disable php-gd
. If you leave this field blank the uploaded logo will be used.';

# General
$lang['sale_agent_string']               = 'Υπέυθυνος';
$lang['amount_display_in_base_currency'] = 'Amount is displayed in your base currency - Only use this report if you are using 1 currency for payments and expenses.';
# Leads
$lang['leads_summary'] = 'Σύνοψη επαφών';

# Contracts
$lang['contract_value']                       = 'Contract Value';
$lang['contract_trash']                       = 'Trash';
$lang['contracts_view_trash']                 = 'View Trash';
$lang['contracts_view_all']                   = 'All';
$lang['contracts_view_exclude_trashed']       = 'Exclude Trashed Contracts';
$lang['contract_value_tooltip']               = 'Base currency will be used.';
$lang['contract_trash_tooltip']               = 'If you add contract to trash, won\'t be shown on client side, won\'t be included in chart and other stats and also by default won\'t be shown when you will list all contracts.';
$lang['contract_summary_active']              = 'Ενεργό';
$lang['contract_renew_heading']               = 'Renew Contract';
$lang['contract_summary_heading']             = 'Contract Summary';
$lang['contract_summary_expired']             = 'Expired';
$lang['contract_summary_about_to_expire']     = 'About to Expire';
$lang['contract_summary_recently_added']      = 'Recently Added';
$lang['contract_summary_trash']               = 'Trash';
$lang['contract_summary_by_type']             = 'Contracts by Type';
$lang['contract_summary_by_type_value']       = 'Contracts Value by Type';
$lang['contract_renewed_successfully']        = 'Contract renewed successfully';
$lang['contract_renewed_fail']                = 'Problem while renewing the contract. Contact administrator';
$lang['no_contract_renewals_found']           = 'Renewals for this contract are not found';
$lang['no_contract_renewals_history_heading'] = 'Contract Renewal History';
$lang['contract_renewed_by']                  = '%s renewed this contract';
$lang['contract_renewal_deleted']             = 'Renewal successfully deleted';
$lang['contract_renewal_delete_fail']         = 'Failed to delete contract renewal. Contact administrator';
$lang['contract_renewal_new_value']           = 'New Contract Value: %s';
$lang['contract_renewal_old_value']           = 'Old Contract Value: %s';
$lang['contract_renewal_new_start_date']      = 'New Start Date: %s';
$lang['contract_renewal_old_start_date']      = 'Old Contract Start Date was: %s';
$lang['contract_renewal_new_end_date']        = 'New End Date: %s';
$lang['contract_renewal_old_end_date']        = 'Old Contract End Date was: %s';
$lang['contract_attachment']                  = 'Attachment';

# Admin Aside Menu
$lang['als_goals_tracking']     = 'Goals Tracking';
$lang['als_expenses']           = 'Δαπάνες';
$lang['als_reports_expenses']   = 'Δαπάνες';
$lang['als_expenses_vs_income'] = 'Δαπάνες - Έσοδα';

# Invoices
$lang['invoice_attach_file']           = 'Επισύναψη αρχείου';
$lang['invoice_mark_as_sent']          = 'Mark as Sent';
$lang['invoice_marked_as_sent']        = 'Invoice marked as sent successfully';
$lang['invoice_marked_as_sent_failed'] = 'Failed to mark invoice as sent';

# Goals Tracking
$lang['goals']                                                          = 'Goals';
$lang['goal']                                                           = 'Goal';
$lang['goals_tracking']                                                 = 'Goals Tracking';
$lang['new_goal']                                                       = 'New Goal';
$lang['goal_lowercase']                                                 = 'goal';
$lang['goal_start_date']                                                = 'Start Date';
$lang['goal_end_date']                                                  = 'End Date';
$lang['goal_subject']                                                   = 'Subject';
$lang['goal_description']                                               = 'Περιγραφή';
$lang['goal_type']                                                      = 'Goal Type';
$lang['goal_achievement']                                               = 'Achievement';
$lang['goal_contract_type']                                             = 'Contract Type';
$lang['goal_notify_when_fail']                                          = 'Notify staff members when goal failed to achieve';
$lang['goal_notify_when_achieve']                                       = 'Notify staff members when goal achieve';
$lang['goal_progress']                                                  = 'Progress';
$lang['goal_total']                                                     = 'Total: %s';
$lang['goal_result_heading']                                            = 'Goal Progress';
$lang['goal_income_shown_in_base_currency']                             = 'Total income is shown in your base currency';
$lang['goal_notify_when_end_date_arrives']                              = 'The staff members will be notified when the end date arrives (Requires CRON).';
$lang['goal_staff_members_notified_about_achievement']                  = 'The staff members are notified about this goal achievement';
$lang['goal_staff_members_notified_about_failure']                      = 'Staff member are notified about the failure';
$lang['goal_notify_staff_manually']                                     = 'Notify Staff Members Manually';
$lang['goal_notify_staff_notified_manually_success']                    = 'The staff members are notified about this goal result';
$lang['goal_notify_staff_notified_manually_fail']                       = 'Failed to notify staff members about this goal result';
$lang['goal_achieved']                                                  = 'Achieved';
$lang['goal_failed']                                                    = 'Failed';
$lang['goal_close']                                                     = 'Very Close';
$lang['goal_type_total_income']                                         = 'Achieve Total Income';
$lang['goal_type_convert_leads']                                        = 'Convert X Leads';
$lang['goal_type_increase_customers_without_leads_conversions']         = 'Increase Customer Number';
$lang['goal_type_increase_customers_without_leads_conversions_subtext'] = 'Leads Conversion is Excluded';
$lang['goal_type_increase_customers_with_leads_conversions']            = 'Increase Customer Number';
$lang['goal_type_increase_customers_with_leads_conversions_subtext']    = 'Leads Conversions is Included';
$lang['goal_type_make_contracts_by_type_calc_database']                 = 'Make Contracts By Type';
$lang['goal_type_make_contracts_by_type_calc_database_subtext']         = 'Is calculated from the date added to database';
$lang['goal_type_make_contracts_by_type_calc_date']                     = 'Make Contracts By Type';
$lang['goal_type_make_contracts_by_type_calc_date_subtext']             = 'Is calculated from the contract start date';
$lang['goal_type_total_estimates_converted']                            = 'X Estimates Conversion ';
$lang['goal_type_total_estimates_converted_subtext']                    = 'Will be taken only estimates that will be converted to invoices';
$lang['goal_type_income_subtext']                                       = 'Income will be calculated in your base currency (not converted)';

# Payments
$lang['payment_transaction_id'] = 'Transaction ID';

# Expenses
$lang['acs_expense_categories']                       = 'Κατηγορίες Δαπανών';
$lang['expense_category']                             = 'Κατηγορία Δαπάνης';
$lang['expense_category_lowercase']                   = 'κατηγορία δαπάνης';
$lang['new_expense']                                  = 'Καταγραφή Δαπάνης';
$lang['expense_add_edit_name']                        = 'Όνομα Κατηγορίας';
$lang['expense_add_edit_description']                 = 'Περιγραφή Κατηγορίας';
$lang['expense_categories']                           = 'Κατηγορίες Δαπάνης';
$lang['new_expense_category']                         = 'Νέα Κατηγορία';
$lang['dt_expense_description']                       = 'Περιγραφή';
$lang['expense']                                      = 'Δαπάνη';
$lang['expenses']                                     = 'Δαπάνες';
$lang['expense_lowercase']                            = 'δαπάνη';
$lang['expense_add_edit_customer']                    = 'Πελάτης';
$lang['expense_add_edit_note']                        = 'Σημείωση';
$lang['expense_add_edit_date']                        = 'Ημερομηνία Δαπάνης';
$lang['expense_add_edit_amount']                      = 'Ποσό';
$lang['expense_add_edit_billable']                    = 'Τιμολόγιο';
$lang['expense_add_edit_attach_receipt']              = 'Επισύναψη Απόδειξης';
$lang['expense_add_edit_reference_no']                = 'Αναφορά #';
$lang['expense_receipt']                              = 'Απόδειξη Δαπάνης';
$lang['expense_receipt_lowercase']                    = 'απόδειξη δαπάνης';
$lang['expense_dt_table_heading_category']            = 'Κατηγορία';
$lang['expense_dt_table_heading_amount']              = 'Ποσό';
$lang['expense_dt_table_heading_date']                = 'Ημερομηνία';
$lang['expense_dt_table_heading_reference_no']        = 'Reference #';
$lang['expense_dt_table_heading_customer']            = 'Πελάτης';
$lang['expense_dt_table_heading_payment_mode']        = 'Τρόπος Πληρωμής';
$lang['expense_converted_to_invoice']                 = 'Expense successfully converted to invoice';
$lang['expense_converted_to_invoice_fail']            = 'Failed to convert this expense to invoice check error log.';
$lang['expense_copy_success']                         = 'The expense is copied successfully.';
$lang['expense_copy_fail']                            = 'Failed to copy expense. Please check the required fields and try again';
$lang['expenses_list_all']                            = 'All';
$lang['expenses_list_billable']                       = 'Billable';
$lang['expenses_list_non_billable']                   = 'Non Billable';
$lang['expenses_list_invoiced']                       = 'Τιμολογημένο';
$lang['expenses_list_unbilled']                       = 'Not Invoiced';
$lang['expenses_list_recurring']                      = 'Recurring';
$lang['expense_invoice_delete_not_allowed']           = 'You cant delete this expense. The expense is already invoiced.';
$lang['expense_convert_to_invoice']                   = 'Convert To Invoice';
$lang['expense_edit']                                 = 'Επεξεργασία Δαπάνης';
$lang['expense_delete']                               = 'Διαγραφή';
$lang['expense_copy']                                 = 'Αντιγραφή';
$lang['expense_invoice_not_created']                  = 'Invoice Not Created';
$lang['expense_billed']                               = 'Billed';
$lang['expense_not_billed']                           = 'Invoice Not Paid';
$lang['expense_customer']                             = 'Πελάτης';
$lang['expense_note']                                 = 'Note:';
$lang['expense_date']                                 = 'Date:';
$lang['expense_ref_noe']                              = 'Ref #:';
$lang['expense_amount']                               = 'Amount:';
$lang['expense_recurring_indicator']                  = 'Recurring';
$lang['expense_already_invoiced']                     = 'This expense is already invoiced';
$lang['expense_recurring_auto_create_invoice']        = 'Auto Create Invoice';
$lang['expense_recurring_send_custom_on_renew']       = 'Send the invoice to customer email when expense re-created';
$lang['expense_recurring_autocreate_invoice_tooltip'] = 'If this option is checked the invoice for the customer will be auto created when the expense will be renewed.';
$lang['expenses_yearly_by_categories']                = 'Expenses Yearly by Categories';
$lang['total_expenses_for']                           = 'Total Expenses for'; // year
$lang['expenses_report_for']                          = 'Expenses for'; // year

# Custom fields
$lang['custom_field_required']    = 'Required';
$lang['custom_field_show_on_pdf'] = 'Show on PDF';
$lang['custom_field_leads']       = 'Επαφές';
$lang['custom_field_customers']   = 'Πελάτες';
$lang['custom_field_staff']       = 'Προσωπικό';
$lang['custom_field_contracts']   = 'Contracts';
$lang['custom_field_tasks']       = 'Εργασίες';
$lang['custom_field_expenses']    = 'Δαπάνες';
$lang['custom_field_invoice']     = 'Τιμολόγιο';
$lang['custom_field_estimate']    = 'Estimate';

# Tickets
$lang['ticket_single_private_staff_notes'] = 'Private Staff Notes';

# Business News
$lang['business_news'] = 'Business News';

# Navigation
$lang['nav_todo_items'] = 'Todo items';

# Contracts
$lang['clients_contracts_type'] = 'Contract Type';

# Version 1.0.5
# General
$lang['no_tax']                              = 'Χωρίς ΦΠΑ';
$lang['numbers_not_formatted_while_editing'] = 'The number in the input field is not formatted while edit/add item and should remain not formatted do not try to format it manually in here.';
# Contracts
$lang['contracts_view_expired']         = 'Expired';
$lang['contracts_view_without_dateend'] = 'Contracts Without Date End';

# Email Templates
$lang['email_template_contracts_fields_heading'] = 'Contracts';

# Invoices General
$lang['invoice_estimate_general_options'] = 'General Options';
$lang['invoice_table_item_description']   = 'Περιγραφή';
$lang['invoice_recurring_indicator']      = 'Recurring';

# Estimates
$lang['estimate_convert_to_invoice_successfully'] = 'Estimate converted to invoice successfully';
$lang['estimate_table_item_description']          = 'Περιγραφή';

# Version 1.0.6
# Invoices
# Currencies
$lang['cant_delete_base_currency'] = 'You cant delete the base currency. You need to assign new base currency then perform delete.';
$lang['invoice_copy']              = 'Copy Invoice';
$lang['invoice_copy_success']      = 'Invoice copied successfully';
$lang['invoice_copy_fail']         = 'Failed to copy invoice';
$lang['invoice_due_after_help']    = 'Set zero to avoid calculation';
$lang['show_shipping_on_invoice']  = 'Show shipping details in invoice';

# Estimates
$lang['show_shipping_on_estimate']         = 'Show shipping details in estimate';
$lang['is_invoiced_estimate_delete_error'] = 'This estimate is invoiced. You cant delete the estimate';

# Customers & Invoices / Estimates
$lang['ship_to']                            = 'Αποστολή σε';
$lang['customer_profile_details']           = 'Στοιχεία Πελάτη';
$lang['supplier_profile_details']           = 'Στοιχεία Πελάτη';
$lang['billing_shipping']                   = 'Χρέωση & Αποστολή';
$lang['billing_address']                    = 'Διεύθυνση Χρέωσης';
$lang['shipping_address']                   = 'Διεύθυνση Αποστολής';
$lang['billing_street']                     = 'Οδός';
$lang['billing_city']                       = 'Πόλη';
$lang['billing_state']                      = 'Νομός';
$lang['billing_zip']                        = 'Τ.Κ.';
$lang['billing_country']                    = 'Χώρα';
$lang['shipping_street']                    = 'Οδός';
$lang['shipping_city']                      = 'Πόλη';
$lang['shipping_state']                     = 'Νομός';
$lang['shipping_zip']                       = 'Τ.Κ.';
$lang['shipping_country']                   = 'Χώρα';
$lang['get_shipping_from_customer_profile'] = 'Άντληση πληροφοριών αποστολής από τα στοιχεία του πελάτη';

# Customer
$lang['customer_default_currency']                             = 'Προεπιλεγμένο Νόμισμα';
$lang['customer_update_address_info_on_invoices']              = 'Επικαιροποίηση διευθυνσης τιμολόγισης και αποστολής σε όλα τα προηγούμενα τιμολόγια';
$lang['customer_update_address_info_on_invoices_help']         = 'If you check this field shipping and billing info will be updated to all invoices and estimates. Note: Invoices with status paid won\'t be affected.';
$lang['no_google_api_key_map_found']						   = 'Δεν βρέθηκε google maps api key';
$lang['customer_attachments_file']                             = 'Αρχείο';
$lang['supplier_attachments_file']                             = 'Αρχείο';
$lang['client_send_set_password_email']                        = 'Send SET password email';
$lang['customer_billing_same_as_profile']                      = 'Όμοια με τις πληροφορίες πελάτη';
$lang['customer_billing_copy']                                 = 'Αντιγραφή διεύθυνσης χρέωσης';
$lang['customer_map']                                          = 'Χάρτης';
$lang['set_password_email_sent_to_client']                     = 'Email to set password is successfully sent to contact';
$lang['set_password_email_sent_to_client_and_profile_updated'] = 'Profile updated and email to set password is successfully sent to contact';
$lang['customer_attachments']                                  = 'Αρχεία';
$lang['customer_longitude']                                    = 'Γεωγραφικό μήκος (Google Maps)';
$lang['customer_latitude']                                     = 'Γεωγραφικό πλάτος (Google Maps)';
$lang['supplier_longitude']                                    = 'Γεωγραφικό μήκος (Google Maps)';
$lang['supplier_latitude']                                     = 'Γεωγραφικό πλάτος (Google Maps)';


# Authentication
$lang['admin_auth_set_password']         = 'Κωδικός';
$lang['admin_auth_set_password_repeat']  = 'Επανάληψη Κωδικού';
$lang['admin_auth_set_password_heading'] = 'Ορίστε Κωδικό';

# General
$lang['apply']                         = 'Εφαρμογή';
$lang['department_calendar_id']        = 'Google Calendar ID';
$lang['localization_default_language'] = 'Προεπιλεγμένη Γλώσσα';
$lang['system_default_string']         = 'Προεπιλογή Συστήματος';
$lang['advanced_options']              = 'Επιπλέον Επιλογές';

# Expenses
$lang['expense_list_invoice']  = 'Τιμολογιμένα';
$lang['expense_list_billed']   = 'Χρεωμένα';
$lang['expense_list_unbilled'] = 'Μη Τιμολογιμένα';

# Leads
$lang['lead_merge_custom_field']          = 'Merge as custom field';
$lang['lead_merge_custom_field_existing'] = 'Merge with database field';
$lang['lead_dont_merge_custom_field']     = 'Do not merge';
$lang['lost_leads']                       = 'Χαμένες Επαφές';
$lang['junk_leads']                       = 'Μη χρήσιμες επαφές';
$lang['lead_mark_as_lost']                = 'Σήμανση ως χαμένης';
$lang['lead_unmark_as_lost']              = 'Unmark Lead as lost';
$lang['lead_marked_as_lost']              = 'Lead marked as lost successfully';
$lang['lead_unmarked_as_lost']            = 'Lead unmarked as lost successfully';
$lang['leads_status_color']               = 'Color';
$lang['lead_mark_as_junk']                = 'Σήμανση ως μη χρήσιμης';
$lang['lead_unmark_as_junk']              = 'Unmark Lead as junk';
$lang['lead_marked_as_junk']              = 'Lead marked as junk successfully';
$lang['lead_unmarked_as_junk']            = 'Lead unmarked as junk successfully';
$lang['lead_not_found']                   = 'Lead Not Found';
$lang['lead_lost']                        = 'Χαμένες';
$lang['lead_junk']                        = 'Μη χρήσιμες';
$lang['leads_not_assigned']               = 'Not Assigned';

# Contacts
$lang['contract_not_visible_to_client'] = 'Hide from customer';
$lang['contract_edit_overview']         = 'Contract Overview';
$lang['contract_attachments']           = 'Attachments';

# Tasks
$lang['task_view_make_public']     = 'Make public';
$lang['task_is_private']           = 'Private Task';
$lang['task_finished']             = 'Finished';
$lang['task_single_related']       = 'Related';
$lang['task_unmark_as_complete']   = 'Unmark as complete';
$lang['task_unmarked_as_complete'] = 'Task unmarked as complete';
$lang['task_relation']             = 'Related';
$lang['task_public']               = 'Public';
$lang['task_public_help']          = 'If you set this task to public will be visible for all staff members. Otherwise will be only visible to members who are assignees,followers,creator or administrators';

# Settings
$lang['settings_general_favicon']                                            = 'Favicon';
$lang['settings_output_client_pdfs_from_admin_area_in_client_language']      = 'Output client PDF documents from admin area in client language';
$lang['settings_output_client_pdfs_from_admin_area_in_client_language_help'] = 'If this options is set to yes and eq. the system default language is English and client have setup language french the pdf documents will be outputted in the client language';
$lang['settings_cron_surveys']                                               = 'Surveys';
$lang['settings_default_tax']                                                = 'Default Tax';
$lang['setup_calendar_by_departments']                                       = 'Setup calendar by Departments';
$lang['settings_calendar']                                                   = 'Calendar';
$lang['settings_sales_invoice_due_after']                                    = 'Invoice due after (days)';
$lang['settings_google_api']                                                 = 'Google API Key';
$lang['settings_gcal_main_calendar_id']                                      = 'Google Calendar ID';
$lang['settings_gcal_main_calendar_id_help']                                 = 'This is the main company calendar. All events from this calendar will be shown. If you want to specify a calendar based on departments you can add in the department Google Calendar ID.';
$lang['show_on_calendar']                                                    = 'Show on Calendar';
$lang['show_invoices_on_calendar']                                           = 'Invoices';
$lang['show_estimates_on_calendar']                                          = 'Estimates';
$lang['show_contracts_on_calendar']                                          = 'Contracts';
$lang['show_tasks_on_calendar']                                              = 'Tasks';
$lang['show_customer_reminders_on_calendar']                                 = 'Customer Reminders';

# Leads
$lang['copy_custom_fields_convert_to_customer']                      = 'Copy custom fields to customer profile';
$lang['copy_custom_fields_convert_to_customer_help']                 = 'If any of the following custom fields do not exists for customer will be auto created with the same name otherwise only the value will be copied from the lead profile.';
$lang['lead_profile']                                                = 'Στοιχεία';
$lang['lead_is_client']                                              = 'Πελάτης';
$lang['leads_email_integration_folder_no_encryption']                = 'No Encryption';
$lang['leads_email_integration']                                     = 'Email Integration';
$lang['leads_email_active']                                          = 'Ενεργό';
$lang['leads_email_integration_imap']                                = 'IMAP Server';
$lang['leads_email_integration_email']                               = 'Email address (Login)';
$lang['leads_email_integration_password']                            = 'Password';
$lang['leads_email_integration_default_source']                      = 'Default Source';
$lang['leads_email_integration_check_every']                         = 'Check Every (minutes)';
$lang['leads_email_integration_default_assigned']                    = 'Responsible for new lead';
$lang['leads_email_encryption']                                      = 'Encryption';
$lang['leads_email_integration_updated']                             = 'Email Integration Updated';
$lang['leads_email_integration_default_status']                      = 'Προεπιλεγμένη Κατάσταση';
$lang['leads_email_integration_folder']                              = 'Folder';
$lang['leads_email_integration_notify_when_lead_imported']           = 'Notify when lead imported';
$lang['leads_email_integration_only_check_unseen_emails']            = 'Only check non opened emails';
$lang['leads_email_integration_only_check_unseen_emails_help']       = 'The script will auto set the email to opened after check. This is used to prevent checking all the emails again and again. Its not recommended to uncheck this option if you have a lot emails and you have setup a lot forwarding to the email you setup for leads';
$lang['leads_email_integration_notify_when_lead_contact_more_times'] = 'Notify if lead send email multiple times';
$lang['leads_email_integration_test_connection']                     = 'Test IMAP Connection';
$lang['lead_email_connection_ok']                                    = 'IMAP Connection is good';
$lang['lead_email_connection_not_ok']                                = 'IMAP Connection is not OK';
$lang['lead_email_activity']                                         = 'Email Activity';
$lang['leads_email_integration_notify_roles']                        = 'Roles to Notify';
$lang['leads_email_integration_notify_staff']                        = 'Staff Members to Notify';
$lang['lead_public']                                                 = 'Public';

# Knowledge Base
$lang['kb_group_color'] = 'Color';
$lang['kb_group_order'] = 'Order';

# Utilities - BULK PDF Exporter
$lang['bulk_pdf_exporter']             = 'Bulk PDF Export';
$lang['bulk_export_pdf_payments']      = 'Πληρωμές';
$lang['bulk_export_pdf_estimates']     = 'Estimates';
$lang['bulk_export_pdf_invoices']      = 'Invoices';
$lang['bulk_pdf_export_button']        = 'Export';
$lang['bulk_pdf_export_select_type']   = 'Select Type';
$lang['no_data_found_bulk_pdf_export'] = 'No data found for export';
$lang['bulk_export_status_all']        = 'All';
$lang['bulk_export_status']            = 'Κατάσταση';
$lang['bulk_export_zip_payment_modes'] = 'Made payments by';
$lang['bulk_export_include_tag']       = 'Include Tag';
$lang['bulk_export_include_tag_help']  = 'eq. Original or Copy. The tag will be outputted in the PDF. Recommended to use only 1 tag';

## Clients area
$lang['auto_backup_options_updated']     = 'Auto backup options updated';
$lang['auto_backup_every']               = 'Create backup every X days';
$lang['auto_backup_enabled']             = 'Enabled (Requires Cron)';
$lang['auto_backup']                     = 'Auto backup';
$lang['backup_delete']                   = 'Backup Deleted';
$lang['backup_success']                  = 'Backup is made successfully';
$lang['utility_backup']                  = 'Database Backup';
$lang['utility_create_new_backup_db']    = 'Create Database Backup';
$lang['utility_backup_table_backupname'] = 'Backup';
$lang['utility_backup_table_backupsize'] = 'Backup size';
$lang['utility_backup_table_backupdate'] = 'Date';
$lang['utility_db_backup_note']          = 'Note: Due to the limited execution time and memory available to PHP, backing up very large databases may not be possible. If your database is very large you might need to backup directly from your SQL server via the command line, or have your server admin do it for you if you do not have root privileges.';

# Version 1.0.7
## Customers - portal
$lang['clients_nav_proposals'] = 'Προσφορές';
$lang['clients_nav_actions'] = 'Κινήσεις';
$lang['clients_nav_support']   = 'Υποστήριξη';

# General
$lang['more']            = 'Περισσότερα';
$lang['add_item']        = 'Προσθήκη';
$lang['goto_admin_area'] = 'Διαχείριση';
$lang['delete']          = 'Διαγραφή %s';
$lang['welcome_top']     = 'Καλωςήρθες %s';

# Customers
$lang['customer_permissions']         = 'Δικαιώματα';
$lang['customer_permission_invoice']  = 'Τιμολόγια';
$lang['customer_permission_estimate'] = 'Estimates';
$lang['customer_permission_proposal'] = 'Προσφορές';
$lang['customer_permission_action'] = 'Κινήσεις';
$lang['customer_permission_contract'] = 'Επαφές';
$lang['customer_permission_support']  = 'Υποστήριξη';

#Tasks
$lang['task_related_to'] = 'Σχετίζεται με';

# Send file
$lang['custom_file_fail_send']    = 'Failed to send file';
$lang['custom_file_success_send'] = 'The file is successfully send to %s';
$lang['send_file_subject']        = 'Email Subject';
$lang['send_file_email']          = 'Email Address';
$lang['send_file_message']        = 'Message';
$lang['send_file']                = 'Send File';
$lang['add_checklist_item']       = 'Checklist Item';
$lang['task_checklist_items']     = 'Checklist Items';

# Import
$lang['default_pass_clients_import'] = 'Προεπιλεγμένος κωδικός για όλες τις επαφές';
$lang['simulate_import']             = 'Simulate Import';
$lang['import_upload_failed']        = 'Αποτυχία Μεταφόρτωσης';
$lang['import_total_imported']       = 'Σύνολο εισαχθέντων: %s';
$lang['import_leads']                = 'Εισαγωγή Επαφών';
$lang['import_customers']            = 'Εισαγωγή Πελατών';
$lang['choose_csv_file']             = 'Επιλογή αρχείου CSV';
$lang['import']                      = 'Εισαγωγή';
$lang['lead_import_status']          = 'Κατάσταση';
$lang['lead_import_source']          = 'Πηγή';

# Bulk PDF Export
$lang['bulk_export_pdf_proposals'] = 'Προσφορές';
$lang['bulk_export_pdf_actions'] = 'Κινήσεις';

# Invoices
$lang['delete_invoice'] = 'Διαγραφή';
$lang['items']          = 'Είδη';
$lang['support']        = 'Υποστήριξη';
$lang['new_ticket']     = 'New Ticket';

# Reminders
$lang['calendar_lead_reminder']  = 'Lead Reminder';
$lang['lead_set_reminder_title'] = 'Add lead reminder';
$lang['set_reminder_tooltip']    = 'This option allows you to never forget anything about your customers.';
$lang['client_reminders_tab']    = 'Υπενθυμίσεις';
$lang['leads_reminders_tab']     = 'Υπενθυμίσεις';

# Tickets
$lang['delete_ticket_reply']  = 'Delete Reply';
$lang['ticket_priority_edit'] = 'Edit Priority';
$lang['ticket_priority_add']  = 'Add Priority';
$lang['ticket_status_edit']   = 'Edit Ticket Κατάσταση';
$lang['ticket_service_edit']  = 'Edit Ticket Service';
$lang['edit_department']      = 'Edit Department';

# Expenses
$lang['edit_expense_category'] = 'Επεξεργασία Κατηγορίας Εξόδων';

# Settings
$lang['customer_default_country']                                 = 'Προεπιλεγμένη Χώρα';
$lang['settings_sales_require_client_logged_in_to_view_estimate'] = 'Require client to be logged in to view estimate';
$lang['set_reminder']                                             = 'Ορισμός Υπενθύμισης';
$lang['set_reminder_date']                                        = 'Ημερομημνία Υπενθύμισης';
$lang['reminder_description']                                     = 'Περιγραφή';
$lang['reminder_notify_me_by_email']                              = 'Αποστολή email για αυτή την υπενθύμιση';
$lang['reminder_added_successfully']                              = 'Η υπενθύμιση προστέθηκε.';
$lang['reminder_description']                                     = 'Περιγραφή';
$lang['reminder_date']                                            = 'Ημέρα';
$lang['reminder_staff']                                           = 'Υπενθύμιση';
$lang['reminder_is_notified']                                     = 'Ενημερώθηκε;';
$lang['reminder_is_notified_boolean_no']                          = 'Όχι';
$lang['reminder_is_notified_boolean_yes']                         = 'Ναι';
$lang['reminder_set_to']                                          = 'Υπενθύμιση στον/ην';
$lang['reminder_deleted']                                         = 'Η υπενθύμιση διαγράφηκε';
$lang['reminder_failed_to_delete']                                = 'Failed to delete the reminder';
$lang['show_invoice_estimate_status_on_pdf']                      = 'Show invoice/estimate status on PDF';
$lang['email_piping_default_priority']                            = 'Default priority on piped ticket';
$lang['show_lead_reminders_on_calendar']                          = 'Lead Reminders';
$lang['tickets_piping']                                           = 'Email Piping';
$lang['email_piping_only_replies']                                = 'Only Replies Allowed by Email';
$lang['email_piping_only_registered']                             = 'Pipe Only on Registered Users';

# Estimates
$lang['view_estimate_as_client']         = 'View estimate as customer';
$lang['estimate_mark_as']                = 'Mark as %s';
$lang['estimate_status_changed_success'] = 'Estimate status changed';
$lang['estimate_status_changed_fail']    = 'Failed to change estimate status';

# Proposals
$lang['proposal_to']                            = 'Εταιρία / Όνομα';
$lang['proposal_date']                          = 'Ημερομηνία';
$lang['proposal_address']                       = 'Διεύθυνση';
$lang['proposal_phone']                         = 'Τηλέφωνο';
$lang['proposal_email']                         = 'Email';
$lang['proposal_date_created']                  = 'Ημερομηνία Δημιουργίας';
$lang['proposal_open_till']                     = 'Ισχύει μέχρι';
$lang['proposal_status_open']                   = 'Ανοιχτή';
$lang['proposal_status_accepted']               = 'Αποδεκτή';
$lang['proposal_status_declined']               = 'Απορρίφθηκε';
$lang['proposal_status_sent']                   = 'Απεστάλει';
$lang['proposal_expired']                       = 'Έληξε';
$lang['proposal_subject']                       = 'Θέμα';
$lang['proposal_total']                         = 'Σύνολο';
$lang['proposal_status']                        = 'Κατάσταση';
$lang['proposals_list_all']                     = 'Όλα';
$lang['proposals_leads_related']                = 'Σχετιζόμενες Επαφές';
$lang['proposals_customers_related']            = 'Σχετιζόμενοι Πελάτες';
$lang['proposal_related']                       = 'Σχετίζεται με';
$lang['proposal_for_lead']                      = 'Επαφή';
$lang['proposal_for_customer']                  = 'Πελάτης';
$lang['proposal']                               = 'Προσφορά';
$lang['proposal_lowercase']                     = 'προσφορά';
$lang['proposals']                              = 'Προσφορες';
$lang['proposals_lowercase']                    = 'προσφορές';
$lang['new_proposal']                           = 'Νέα Προσφορά';
$lang['proposal_currency']                      = 'Νόμισμα';
$lang['proposal_allow_comments']                = 'Επιτρέπονται τα σχόλια';
$lang['proposal_allow_comments_help']           = 'Αν το επιλέξετε ο πελάτης μπορεί να σχολιάσει';
$lang['proposal_edit']                          = 'Επεξεργασία';
$lang['proposal_pdf']                           = 'PDF';
$lang['proposal_send_to_email']                 = 'Αποστολή στο Email';
$lang['proposal_send_to_email_title']           = 'Αποστολή προσφοράς στο Email';
$lang['proposal_attach_pdf']                    = 'Επισύναψη PDF';
$lang['proposal_preview_template']              = 'Προεπισκόπηση Προτύπου';
$lang['proposal_view']                          = 'Προβολή Προσφοράς';
$lang['proposal_copy']                          = 'Αντιγραφή';
$lang['proposal_delete']                        = 'Διαγραφή';
$lang['proposal_to']                            = 'Προς';
$lang['proposal_add_comment']                   = 'Προσθήκη σχολίου';
$lang['proposal_sent_to_email_success']         = 'Η προφσορά στάλθηκε';
$lang['proposal_sent_to_email_fail']            = 'Αποτυχία αποστολής προσφοράς';
$lang['proposal_copy_fail']                     = 'αποτυχία αντιγραφής προσφοράς';
$lang['proposal_copy_success']                  = 'Η προσφορά αντιγράφηκε';
$lang['proposal_status_changed_success']        = 'Η κατάσταση της προσφοράς άλλαξε με επιτυχία';
$lang['proposal_status_changed_fail']           = 'Αποτυχία αλλαγής κατάστασης προσφοράς';
$lang['proposal_assigned']                      = 'Ανατέθηκε';
$lang['proposal_comments']                      = 'Σχόλια';
$lang['proposal_convert']                       = 'Convert';
$lang['proposal_convert_estimate']              = 'Estimate';
$lang['proposal_convert_invoice']               = 'Τιμολόγιο';
$lang['proposal_convert_to_estimate']           = 'Convert to Estimate';
$lang['proposal_convert_to_invoice']            = 'Convert to Invoice';
$lang['proposal_convert_to_lead_disabled_help'] = 'You need to convert the lead to customer in order to create %s';
$lang['proposal_convert_not_related_help']      = 'The proposal needs to be related to customer in order to convert to %s';
$lang['proposal_converted_to_estimate_success'] = 'Proposal converted to estimate successfully';
$lang['proposal_converted_to_invoice_success']  = 'Proposal converted to invoice successfully';
$lang['proposal_converted_to_estimate_fail']    = 'Failed to convert proposal to estimate';
$lang['proposal_converted_to_invoice_fail']     = 'Failed to convert proposal to invoice';
$lang['proposal_maketas']						= 'Δημιουργικό - Μακέτα';
$lang['proposal_maketa_add_heading']			= 'Προσθήκη Επιλογής Δημιουργικό - Μακέτα';
$lang['proposal_maketa_edit_heading']			= 'Επεξεργασία Επιλογής Δημιουργικό - Μακέτα';
$lang['proposal_maketa_name']					= 'Περιγραφή νέας επιλογής';
$lang['proposal_maketa_delete_heading']			= 'Διαγραφή Επιλογής Δημιουργικό - Μακέτα';
$lang['proposal_prodiagrafes']					= 'Προδιαγραφές';
$lang['proposal_prodiagrafe']					= 'Προδιαγραφή';
$lang['proposal_prodiagrafes_add_heading']		= 'Προσθήκη Προδιαγραφές';
$lang['proposal_prodiagrafes_edit_heading']		= 'Επεξεργασία Προδιαγραφές';
$lang['proposal_prodiagrafes_name']				= 'Προδιαγραφές';
$lang['proposal_prodiagrafes_delete_heading']	= 'Διαγραφή Προδιαγραφής';
$lang['proposal_prodiagrafes_tag']				= 'Κατηγορία';
$lang['proposal_dimiourgiko_maketa_allo']		= '<span class="allo">Άλλο</span>';
$lang['proposal_fori_allo']						= '<span class="allo">Άλλο</span>';
$lang['proposal_metaforika_allo']		        = '<span class="allo">Άλλο</span>';
$lang['proposal_topos_tropos_paradosis_i_apostolis_allo']		= '<span class="allo">Άλλο</span>';
$lang['proposal_chronos_paradosis_i_apostolis_allo']		= '<span class="allo">Άλλο</span>';
$lang['proposal_tropos_pliromis_allo']		= '<span class="allo">Άλλο</span>';

$lang['action_to']                            = 'Εταιρία / Όνομα';
$lang['action_date']                          = 'Ημερομηνία';
$lang['action_address']                       = 'Διεύθυνση';
$lang['action_phone']                         = 'Τηλέφωνο';
$lang['action_email']                         = 'Email';
$lang['action_date_created']                  = 'Ημερομηνία Δημιουργίας';
$lang['action_open_till']                     = 'Ισχύει μέχρι';
$lang['action_status_waiting']                = 'Σε Αναμονή';
$lang['action_status_to_produce']             = 'Προς Παραγωγή';
$lang['action_status_to_delivery']            = 'Προς Παράδοση';
$lang['action_status_completed']              = 'Ολοκληρώθηκε';
$lang['action_expired']                       = 'Έληξε';
$lang['action_catalogue_value']               = 'Τιμή Καταλόγου';
$lang['action_sales_rate']                    = 'Έκπτωση(&#37;)';
$lang['action_extra_sales_rate']              = 'Επιπλέον Έκπτωση';
$lang['action_charge_comment']                = 'Χρεώσης';
$lang['action_chreosi_supplier']              = 'Χρεώση';
$lang['action_pistosi_supplier']              = 'Πίστωση';
$lang['suppliers_summary'] = "Προμηθευτές";
$lang['action_extra_sales_comment']           = 'Αιτιολογία';
$lang['action_kind']						  = 'Είδος Κίνησης';
$lang['action_production_comment']            = 'Παραγωγής';
$lang['action_deliver_comment']               = 'Παράδοσης';
$lang['action_charge']                        = 'Χρεώση';
$lang['action_delivery_cost']                 = 'Μεταφορικά';
$lang['action_maketa_cost']                   = 'Μακέτα';
$lang['action_aeras_cost']                    = 'Αέρας';
$lang['action_total']                         = 'Σύνολο';
$lang['action_full_total']                    = 'Γενικό Σύνολο';
$lang['action_credit']                        = 'Πίστωση';
$lang['action_balance']                       = 'Υπόλοιπο';
$lang['action_status_sent']                   = 'Απεστάλει';
$lang['action_expired']                       = 'Έληξε';
$lang['action_subject']                       = 'Περιγραφή';
$lang['action_total']                         = 'Σύνολο';
$lang['action_status']                        = 'Κατάσταση';
$lang['actions_list_all']                     = 'Όλα';
$lang['actions_leads_related']                = 'Σχετιζόμενες Επαφές';
$lang['actions_customers_related']            = 'Σχετιζόμενοι Πελάτες';
$lang['action_related']                       = 'Σχετίζεται με';
$lang['action_for_lead']                      = 'Επαφή';
$lang['action_for_customer']                  = 'Πελάτης';
$lang['action_for_supplier']                  = 'Προμηθευτής';
$lang['action']                               = 'Κίνηση';
$lang['action_lowercase']                     = 'κίνηση';
$lang['actions']                              = 'Κινησεις';
$lang['actions_lowercase']                    = 'κινήσεις';
$lang['new_action']                           = 'Νέα κίνηση ';
$lang['action_currency']                      = 'Νόμισμα';
$lang['action_allow_comments']                = 'Επιτρέπονται τα σχόλια';
$lang['action_allow_comments_help']           = 'Αν το επιλέξετε ο πελάτης μπορεί να σχολιάσει';
$lang['action_edit']                          = 'Επεξεργασία';
$lang['action_pdf']                           = 'PDF';
$lang['action_send_to_email']                 = 'Αποστολή στο Email';
$lang['action_send_to_email_title']           = 'Αποστολή προσφοράς στο Email';
$lang['action_attach_pdf']                    = 'Επισύναψη PDF';
$lang['action_preview_template']              = 'Προεπισκόπηση Προτύπου';
$lang['action_view']                          = 'Προβολή Κίνησης';
$lang['action_copy']                          = 'Αντιγραφή';
$lang['action_delete']                        = 'Διαγραφή';
//$lang['action_to']                            = 'Προς';
$lang['action_add_comment']                   = 'Προσθήκη σχολίου';
$lang['action_sent_to_email_success']         = 'Η κίνηση στάλθηκε';
$lang['action_sent_to_email_fail']            = 'Αποτυχία αποστολής κίνησης';
$lang['action_copy_fail']                     = 'αποτυχία αντιγραφής κίνησης';
$lang['action_copy_success']                  = 'Η κίνηση αντιγράφηκε';
$lang['action_status_changed_success']        = 'Η κατάσταση της κίνησης άλλαξε με επιτυχία';
$lang['action_status_changed_fail']           = 'Αποτυχία αλλαγής κατάστασης κίνησης';
$lang['action_assigned']                      = 'Υπεύθυνοι';
$lang['action_comments']                      = 'Σχόλια';
$lang['action_delay_comment']				  = 'Αιτία αναμονής';
$lang['action_convert']                       = 'Convert';
$lang['action_convert_estimate']              = 'Estimate';
$lang['action_convert_invoice']               = 'Τιμολόγιο';
$lang['action_convert_to_estimate']           = 'Convert to Estimate';
$lang['action_convert_to_invoice']            = 'Convert to Invoice';
$lang['action_convert_to_lead_disabled_help'] = 'You need to convert the lead to customer in order to create %s';
$lang['action_convert_not_related_help']      = 'The action needs to be related to customer in order to convert to %s';
$lang['action_converted_to_estimate_success'] = 'Proposal converted to estimate successfully';
$lang['action_converted_to_invoice_success']  = 'Proposal converted to invoice successfully';
$lang['action_converted_to_estimate_fail']    = 'Failed to convert action to estimate';
$lang['action_converted_to_invoice_fail']     = 'Failed to convert action to invoice';

# Proposals - view proposal template
$lang['proposal_total_info']   = 'Σύνολο %s';
$lang['proposal_accept_info']  = 'Αποδοχή';
$lang['proposal_decline_info'] = 'Ακύρωση';
$lang['proposal_pdf_info']     = 'PDF';

# Actions - view proposal template
$lang['action_total_info']   = 'Σύνολο %s';
$lang['action_accept_info']  = 'Αποδοχή';
$lang['action_decline_info'] = 'Ακύρωση';
$lang['action_pdf_info']     = 'PDF';

# Customers Portal
$lang['customer_reset_action']            = 'Reset';
$lang['customer_reset_password_heading']  = 'Reset your password';
$lang['customer_forgot_password_heading'] = 'Forgot Password';
$lang['customer_forgot_password']         = 'Forgot Password?';
$lang['customer_reset_password']          = 'Password';
$lang['customer_reset_password_repeat']   = 'Repeat Password';
$lang['customer_forgot_password_email']   = 'Email Address';
$lang['customer_forgot_password_submit']  = 'Submit';
$lang['customer_ticket_subject']          = 'Subject';

# Email templates
$lang['email_template_proposals_fields_heading'] = 'Proposals';

# Email templates
$lang['email_template_actions_fields_heading'] = 'Κινήσεις';

# Tasks
$lang['add_task_attachments']  = 'Attachment';
$lang['task_view_attachments'] = 'Συνημμένα';
$lang['task_view_description'] = 'Περιγραφή';

# Customer Groups
$lang['customer_group_add_heading']      = 'Προσθήκη Ομάδας Πελάτών';
$lang['customer_group_edit_heading']     = 'Επεξεργασία Ομάδας Πελάτών';
$lang['customer_group_delete_heading']   = 'Διαγραφή Ομάδας Πελάτών';
$lang['new_customer_group']              = 'Νέα ομάδα πελατών';
$lang['new_supplier_group']              = 'Νέα ομάδα προμηθευτών';
$lang['customer_group_name']             = 'Όνομα';
$lang['customer_groups']                 = 'Ομάδες';
$lang['customer_group']                  = 'Ομάδα πελατών';
$lang['customer_group_lowercase']        = 'ομάδα πελατών';
$lang['customer_have_invoices_by']       = 'Contains invoices by status %s';
$lang['customer_have_estimates_by']      = 'Contains estimates by status %s';
$lang['customer_have_contracts_by_type'] = 'Having contracts by type %s';

# Custom fields
$lang['custom_field_show_on_table']              = 'Show on table';
$lang['custom_field_show_on_client_portal']      = 'Show on client portal';
$lang['custom_field_show_on_client_portal_help'] = 'If this field is checked also will be shown in tables';
$lang['custom_field_visibility']                 = 'Visibility';

# Utilities # Menu Builder
$lang['utilities_menu_translate_name_help'] = 'You can add here also translate strings. So if staff/system have language other then the default the menu item names will be outputted in the staff language. Otherwise if the string do not exists in the translate file will be taken the string you enter here.';
$lang['utilities_menu_icon']                = 'Εικονίδιο';
$lang['active_menu_items']                  = 'Ενεργά Στοιχεία Μενού';
$lang['inactive_menu_items']                = 'Ανενεργά Στοιχεία Μενού';
$lang['utilities_menu_url']                 = 'URL';
$lang['utilities_menu_name']                = 'Όνομα';
$lang['utilities_menu_save']                = 'Αποθήκευση Μενού';

# Knowledge Base
$lang['view_articles_list']     = 'View Articles';
$lang['view_articles_list_all'] = 'All Articles';
$lang['als_all_articles']       = 'Articles';
$lang['als_kb_groups']          = 'Ομάδες';

# Customizer Menu
$lang['menu_builder']            = 'Menu Setup';
$lang['main_menu']               = 'Main Menu';
$lang['setup_menu']              = 'Setup Menu';
$lang['utilities_menu_url_help'] = '%s is auto appended to the url';

# Spam Filter - Tickets
$lang['spam_filters']                 = 'Spam Filters';
$lang['spam_filter']                  = 'Spam Filter';
$lang['new_spam_filter']              = 'New Spam Filter';
$lang['spam_filter_blocked_senders']  = 'Blocked Senders';
$lang['spam_filter_blocked_subjects'] = 'Blocked Subjects';
$lang['spam_filter_blocked_phrases']  = 'Blocked Phrases';
$lang['spam_filter_content']          = 'Content';
$lang['spamfilter_edit_heading']      = 'Edit Spam Filter';
$lang['spamfilter_add_heading']       = 'Add Spam Filter';
$lang['spamfilter_type']              = 'Type';
$lang['spamfilter_type_subject']      = 'Subject';
$lang['spamfilter_type_sender']       = 'Sender';
$lang['spamfilter_type_phrase']       = 'Phrase';

# Tickets
$lang['block_sender']                = 'Block Sender';
$lang['sender_blocked']              = 'Sender Blocked';
$lang['sender_blocked_successfully'] = 'Sender Blocked Successfully';
$lang['ticket_date_created']         = 'Created';

#KB
$lang['edit_kb_group'] = 'Edit group';

# Leads
$lang['edit_source'] = 'Edit Source';
$lang['edit_status'] = 'Edit Κατάσταση';

# Contacts
$lang['contract_type_edit'] = 'Edit Contract Type';

# Reports
$lang['report_by_customer_groups'] = 'Total Value By Customer Groups';

#Utilities
$lang['ticket_pipe_log']      = 'Ticket Pipe Log';
$lang['ticket_pipe_name']     = 'From Name';
$lang['ticket_pipe_email_to'] = 'To';
$lang['ticket_pipe_email']    = 'From Email';
$lang['ticket_pipe_subject']  = 'Subject';
$lang['ticket_pipe_message']  = 'Message';
$lang['ticket_pipe_date']     = 'Date';
$lang['ticket_pipe_status']   = 'Κατάσταση';

# Home
$lang['home_latest_activity']   = 'Latest Activity';
$lang['home_my_tasks']          = 'My Tasks';
$lang['home_latest_activity']   = 'Latest Activity';
$lang['home_my_todo_items']     = 'My To Do Items';
$lang['home_widget_view_all']   = 'View All';
$lang['home_stats_full_report'] = 'Full Report';

# Validation
$lang['form_validation_required']    = 'The {field} field is required.';
$lang['form_validation_valid_email'] = 'The {field} field must contain a valid email address.';
$lang['form_validation_matches']     = 'The {field} field does not match the {param} field.';
$lang['form_validation_is_unique']   = 'The {field} field must contain a unique value.';

# Version 1.0.8
# Notifications & Leads/Estimates/Invoices Activity Log
$lang['not_event']                                    = 'Calendar event today - %s ...';
$lang['not_event_public']                             = 'Public event start today - %s ...';
$lang['not_contract_expiry_reminder']                 = 'Contract expiry reminder - %s ...';
$lang['not_recurring_expense_cron_activity_heading']  = 'Recurring Expenses Cron Job Activity';
$lang['not_recurring_invoices_cron_activity_heading'] = 'Recurring Invoices Cron Job Activity';
$lang['not_recurring_total_renewed']                  = 'Total Renewed: %s';
$lang['not_recurring_expenses_action_taken_from']     = 'Action taken from recurring expense';
$lang['not_invoice_created']                          = 'Invoice Created:';
$lang['not_invoice_renewed']                          = 'Renewed Invoice:';
$lang['not_expense_renewed']                          = 'Renewed Expense:';
$lang['not_invoice_sent_to_customer']                 = 'Invoice Sent to Customer: %s';
$lang['not_invoice_sent_yes']                         = 'Yes';
$lang['not_invoice_sent_not']                         = 'No';
$lang['not_action_taken_from_recurring_invoice']      = 'Action taken from recurring invoice:';
$lang['not_new_reminder_for']                         = 'New Reminder for %s';
$lang['not_received_one_or_more_messages_lead']       = 'Received one more email message from lead';
$lang['not_received_lead_imported_email_integration'] = 'Lead Imported From Email Integration';
$lang['not_lead_imported_attachment']                 = 'Imported attachment from email';
$lang['not_estimate_status_change']                   = 'Imported attachment from email';
$lang['not_estimate_status_updated']                  = 'Estimate Κατάσταση Updated: From: %s to %s';
$lang['not_goal_message_success']                     = 'Congratulations! We achieved new goal.<br /> Goal Type: %s
<br />Goal Achievement: %s
<br />Total Achievement: %s
<br />Start Date: %s
<br />End Date: %s';
$lang['not_assigned_lead_to_you']                                 = 'assigned lead %s to you';
$lang['not_lead_activity_assigned_to']                            = '%s assigned to %s';
$lang['not_lead_activity_attachment_deleted']                     = 'Deleted Attachment';
$lang['not_lead_activity_status_updated']                         = '%s updated lead status from %s to %s';
$lang['not_lead_activity_contacted']                              = '%s contacted this lead on %s';
$lang['not_lead_activity_created']                                = '%s δημιούργησε την επαφή';
$lang['not_lead_activity_marked_lost']                            = 'Marked as lost';
$lang['not_lead_activity_unmarked_lost']                          = 'Unmarked as lost';
$lang['not_lead_activity_marked_junk']                            = 'Marked as junk';
$lang['not_lead_activity_unmarked_junk']                          = 'Unmarked as junk';
$lang['not_lead_activity_added_attachment']                       = 'Added attachment';
$lang['not_lead_activity_converted_email']                        = 'Lead email changed. First lead email was: %s and added as customer with email %s';
$lang['not_lead_activity_converted']                              = '%s Converted this lead to customer';
$lang['not_liked_your_post']                                      = '%s liked your post %s ...';
$lang['not_commented_your_post']                                  = '%s commented on your post %s ...';
$lang['not_liked_your_comment']                                   = '%s liked your comment %s ...';
$lang['not_proposal_assigned_to_you']                             = 'Proposal assigned to you - %s ...';
$lang['not_proposal_comment_from_client']                         = 'New comment from customer on proposal %s ...';
$lang['not_proposal_proposal_accepted']                           = 'Proposal Accepted - %s';
$lang['not_proposal_proposal_declined']                           = 'Proposal Declined - %s';
$lang['not_action_assigned_to_you']                               = 'Κινήσεις που σας αφορούν - %s ...';
$lang['not_action_comment_from_client']                           = 'New comment from customer on action %s ...';
$lang['not_action_action_accepted']                               = 'Κίνηση Αποδεκτή - %s';
$lang['not_action_action_declined']                               = 'Κίνηση Απορρίφθηκε - %s';
$lang['not_task_added_you_as_follower']                           = 'added you as follower on task %s ...';
$lang['not_task_added_someone_as_follower']                       = 'added %s as follower on task %s ...';
$lang['not_task_added_himself_as_follower']                       = 'added himself as follower on task %s ...';
$lang['not_task_assigned_to_you']                                 = 'assigned a task to you %s ...';
$lang['not_task_assigned_someone']                                = 'assigned %s to task %s ...';
$lang['not_task_will_do_user']                                    = 'will do task %s ...';
$lang['not_task_new_attachment']                                  = 'New Attachment Added';
$lang['not_task_marked_as_complete']                              = 'marked task as complete %s';
$lang['not_task_unmarked_as_complete']                            = 'unmarked task as complete %s';
$lang['not_ticket_assigned_to_you']                               = 'Ticket assigned to you - %s ...';
$lang['not_ticket_reassigned_to_you']                             = 'Ticket reassigned to you - %s ...';
$lang['not_estimate_customer_accepted']                           = 'Congratulations! Client accepted estimate with number %s';
$lang['not_estimate_customer_declined']                           = 'Client declined estimate with number %s';
$lang['estimate_activity_converted']                              = 'converted this estimate to invoice.<br /> %s';
$lang['estimate_activity_created']                                = 'Created the estimate';
$lang['invoice_estimate_activity_removed_item']                   = 'removed item <b>%s</b>';
$lang['estimate_activity_number_changed']                         = 'Estimate number changed from %s to %s';
$lang['invoice_activity_number_changed']                          = 'Invoice number changed from %s to %s';
$lang['invoice_estimate_activity_updated_item_short_description'] = 'updated item short description from %s to %s';
$lang['invoice_estimate_activity_updated_item_long_description']  = 'updated item long description from <b>%s</b> to <b>%s</b>';
$lang['invoice_estimate_activity_updated_item_rate']              = 'updated item rate from %s to %s';
$lang['invoice_estimate_activity_updated_qty_item']               = 'updated quantity on item <b>%s</b> from %s to %s';
$lang['invoice_estimate_activity_added_item']                     = 'added new item <b>%s</b>';
$lang['invoice_estimate_activity_sent_to_client']                 = 'sent estimate to client';
$lang['estimate_activity_client_accepted_and_converted']          = 'Customer accepted this estimate. Estimate is converted to invoice with number %s';
$lang['estimate_activity_client_accepted']                        = 'Customer accepted this estimate';
$lang['estimate_activity_client_declined']                        = 'Client declined this estimate';
$lang['estimate_activity_marked']                                 = 'marked estimate as %s';
$lang['invoice_activity_status_updated']                          = 'Invoice status updated from %s to %s';
$lang['invoice_activity_created']                                 = 'created the invoice';
$lang['invoice_activity_from_expense']                            = 'converted to invoice from expense';
$lang['invoice_activity_recurring_created']                       = '[Recurring] Invoice created by CRON';
$lang['invoice_activity_recurring_from_expense_created']          = '[Invoice From Expense] Invoice created by CRON';
$lang['invoice_activity_sent_to_client_cron']                     = 'Invoice sent to customer by CRON';
$lang['invoice_activity_sent_to_client']                          = 'sent invoice to customer';
$lang['invoice_activity_marked_as_sent']                          = 'marked invoice as sent';
$lang['invoice_activity_payment_deleted']                         = 'deleted payment for the invoice. Payment #%s, total amount %s';
$lang['invoice_activity_payment_made_by_client']                  = 'Client made payment for the invoice from total <b>%s</b> - %s';
$lang['invoice_activity_payment_made_by_staff']                   = 'recorded payment from total <b>%s</b> - %s';
$lang['invoice_activity_added_attachment']                        = 'Added attachment';

# Navigation
$lang['top_search_placeholder'] = 'Αναζήτηση...';

# Staff
$lang['staff_profile_inactive_account'] = 'This staff member account is inactive';

# Estimates
$lang['copy_estimate']                = 'Copy Estimate';
$lang['estimate_copied_successfully'] = 'Estimate copied successfully';
$lang['estimate_copied_fail']         = 'Failed to copy estimate';

# Tasks
$lang['tasks_view_assigned_to_user'] = 'Εργασίες που μου ανατέθηκαν';
$lang['tasks_view_follower_by_user'] = 'Tasks i\'m following';
$lang['no_tasks_found']              = 'Δεν βρέθηκαν Εργασίες';

# Leads
$lang['leads_dt_datecreated']       = 'Δημιουργήθηκε';
$lang['leads_sort_by']              = 'Ταξινόμηση κατά';
$lang['leads_sort_by_datecreated']  = 'Ημερομηνία Δημιουργίας';
$lang['leads_sort_by_kanban_order'] = 'Kan Ban Order';

# Authentication
$lang['check_email_for_resetting_password'] = 'Check your email for further instructions resetting your password';
$lang['inactive_account']                   = 'Inactive Account';
$lang['error_setting_new_password_key']     = 'Error setting new password';
$lang['password_reset_message']             = 'Your password has been reset. Please login now!';
$lang['password_reset_message_fail']        = 'Error resetting your password. Try again.';
$lang['password_reset_key_expired']         = 'Password key expired or invalid user';
$lang['auth_reset_pass_email_not_found']    = 'Email not found';
$lang['auth_reset_password_submit']         = 'Reset Password';

# Settings
$lang['settings_amount_to_words']          = 'Amount to words';
$lang['settings_amount_to_words_desc']     = 'Output total amount to words in invoice/estimate';
$lang['settings_amount_to_words_enabled']  = 'Enable';
$lang['settings_total_to_words_lowercase'] = 'Number words into lowercase';
$lang['settings_show_tax_per_item']        = 'Show TAX per item';

# Reports
$lang['report_sales_months_three_months'] = 'Last 3 months';
$lang['report_invoice_number']            = 'Invoice #';
$lang['report_invoice_customer']          = 'Πελάτης';
$lang['report_invoice_date']              = 'Date';
$lang['report_invoice_duedate']           = 'Due Date';
$lang['report_invoice_amount']            = 'Amount';
$lang['report_invoice_amount_with_tax']   = 'Amount with tax';
$lang['report_invoice_amount_open']       = 'Amount open';
$lang['report_invoice_status']            = 'Κατάσταση';

#Version 1.0.9

# Home stats
$lang['home_stats_by_project_status'] = 'Statistics by Project Κατάσταση';
$lang['home_invoice_overview']        = 'Τιμολόγια';
$lang['home_estimate_overview']       = 'Εκτιμήσεις';
$lang['home_proposal_overview']       = 'Προσφορές';
$lang['home_action_overview']         = 'Κινήσεις';
$lang['home_lead_overview']           = 'Leads Overview';
$lang['home_my_projects']             = 'Έργα';
$lang['home_announcements']           = 'Ανακοινώσεις';

# Settings
$lang['settings_leads_kanban_limit']                                    = 'Limit leads kan ban rows per status';
$lang['settings_group_misc']                                            = 'Misc';
$lang['show_projects_on_calendar']                                      = 'Projects';
$lang['settings_media_max_file_size_upload']                            = 'Max file size upload in Media (MB)';
$lang['settings_client_staff_add_edit_delete_task_comments_first_hour'] = 'Allow customer/staff to add/edit task comments only in the first hour (administrators not applied)';

# Email templates
$lang['email_template_only_domain_email'] = 'Only domain email';

# Announcements
$lang['dismiss_announcement']   = 'Dismiss announcement';
$lang['announcement_from']      = 'From:';
$lang['announcement_date']      = 'Date posted: %s';
$lang['announcement_not_found'] = 'Announcement not found';
$lang['announcements_recent']   = 'Recent Announcements';

# General
$lang['zip_invoices']         = 'Εξαγωγή Zip Αρχείου Τιμολογίων';
$lang['zip_estimates']        = 'Εξαγωγή Zip Αρχείου Εκτιμήσεων';
$lang['zip_payments']         = 'Εξαγωγή Zip Αρχείου Πληρωμών';
$lang['setup_help']           = 'Βοήθεια';
$lang['clients_list_company'] = 'Επωνυμία';
$lang['suppliers_list_company'] = 'Επωνυμία';
$lang['dt_button_export']     = 'Εξαγωγή';
$lang['dt_entries']           = 'εγγραφές';
$lang['invoice_total_paid']   = 'Σύνολο πληρωμών';
$lang['invoice_amount_due']   = 'Υπόλοιπο';

# Calendar
$lang['calendar_project'] = 'Έργο';

# Leads
$lang['leads_import_assignee']     = 'Υπέυθυνος (Αποδέκτης)';
$lang['customer_from_lead']        = 'Πελάτης από %s';
$lang['lead_kan_ban_attachments']  = 'Συνημμένα: %s';
$lang['leads_sort_by_lastcontact'] = 'Τελευταία επαφή';

# Tasks
$lang['task_comment_added']     = 'Το σχόλιο προστέθηκε';
$lang['task_duedate']           = 'Ημερομηνία Λήξης';
$lang['task_view_comments']     = 'Σχόλια';
$lang['task_comment_updated']   = 'Τα σχόλια ενημερώθηκαν';
$lang['task_visible_to_client'] = 'Ορατό στον πελάτη';
$lang['task_hourly_rate']       = 'Χρέωση ανά ώρα';
$lang['hours']                  = 'Ώρες';
$lang['seconds']                = 'Δευτερόλεπτα';
$lang['minutes']                = 'Λεπτά';
$lang['task_start_timer']       = 'Έναρξη';
$lang['task_stop_timer']        = 'Λήξη';
$lang['task_billable']          = 'Χρεώσιμο';
$lang['task_billable_yes']      = 'Χρεώσιμο';
$lang['task_billable_no']       = 'Μη Χρεώσιμο';
$lang['task_billed']            = 'Χρεωμένο';
$lang['task_billed_yes']        = 'Χρεωμένο';
$lang['task_billed_no']         = 'Μη Χρεωμένο';
$lang['task_user_logged_time']  = 'Ώρα εισόδου:';
$lang['task_total_logged_time'] = 'Συνολικός χρόνος παρουσίας:';
$lang['task_is_billed']         = 'Αυτή η εργασία έχει χρεωθεί στο τιμολόγιο με αριθμό %s';
$lang['task_statistics']        = 'Στατιστικά';
$lang['task_milestone']         = 'Milestone';

# Tickets
$lang['ticket_message_updated_successfully'] = 'Message updated successfully';

# Invoices
$lang['invoice_task_item_project_tasks_not_included'] = 'Projects tasks are not included in this list.';
$lang['show_quantity_as']                             = 'Προβολή ποσότητας ως:';
$lang['quantity_as_qty']                              = 'Τεμάχια';
$lang['quantity_as_hours']                            = 'Ώρες';
$lang['invoice_table_hours_heading']                  = 'Ώρες';
$lang['bill_tasks']                                   = 'Bill Tasks';
$lang['invoice_estimate_sent_to_email']               = 'Αποστολή Email στο';

# Estimates
$lang['estimate_table_hours_heading'] = 'Ώρες';

# General
$lang['is_customer_indicator']         = 'Πελάτης';
$lang['print']                         = 'Εκτύπωση';
$lang['customer_permission_projects']  = 'Έργα';
$lang['no_timers_found']               = 'No started timers found';
$lang['timers_started_confirm_logout'] = 'Started tasks timers found!<br />Are you sure you want to logout without stopping the timers?';
$lang['confirm_logout']                = 'Αποσύνδεση';
$lang['timer_top_started']             = 'Started at %s';

# Projects
$lang['cant_change_billing_type_billed_tasks_found']         = 'You cant change billing type. Billed tasks already found for this project.';
$lang['project_customer_permission_warning']                 = 'The system indicated that the primary contact do not have permission for projects. The primary contact won\'t be able to see the project. Consider add permission in the contact profile.';
$lang['project_invoice_timesheet_start_time']                = 'Start time: %s';
$lang['project_invoice_timesheet_end_time']                  = 'End time: %s';
$lang['project_invoice_timesheet_total_logged_time']         = 'Logged time: %s';
$lang['project_view_as_client']                              = 'View project as customer';
$lang['project_mark_all_tasks_as_completed']                 = 'Mark all tasks as completed and stop all timers (No notifications sent to project members)';
$lang['project_not_started_status_tasks_timers_found']       = 'Task timers found for this project but the project is with status Not Started. Recommended to change the project status to In Progress';
$lang['project_status']                                      = 'Κατάσταση';
$lang['project_status_1']                                    = 'Δεν ξεκίνησε';
$lang['project_status_2']                                    = 'Σε εξέλιξη';
$lang['project_status_3']                                    = 'Σε αναμονή';
$lang['project_status_4']                                    = 'Ολοκληρωμένο';
$lang['project_status_5']									 = 'Ακυρωμένο';
$lang['statement_status_1']                                  = 'Σε αναμονή';
$lang['statement_status_2']                                  = 'Προς παραγωγή';
$lang['statement_status_3']                                  = 'Προς παράδοση';
$lang['statement_status_4']                                  = 'Ολοκληρώθηκε';
$lang['statement_kind_1']                                    = 'Offset';
$lang['statement_kind_2']                                    = 'Digital';
$lang['statement_kind_3']                                    = 'Plotter';
$lang['statement_kind_4']                                    = 'Μακέτα';
$lang['statement_kind_5']                                    = 'Συνεργατών';
$lang['statement_kind_6']                                    = 'Είσπραξη';
$lang['statement_kind_7']                                    = 'ΦΠΑ';
$lang['statement_kind_8']                                    = 'Διάφορα';
$lang['project_file_dateadded']                              = 'Ημερομηνία μεταφόρτωσης';
$lang['project_file_filename']                               = 'Όνομα αρχείου';
$lang['project_file__filetype']                              = 'Τύπος αρχείου';
$lang['project_file_visible_to_customer']                    = 'Ορατό στον πελάτη';
$lang['project_file_uploaded_by']                            = 'Μεταφορτώθηκε από';
$lang['edit_project']                                        = 'Επεξεργασία Έργου';
$lang['copy_project']                                        = 'Αντιγραφή Έργου';
$lang['delete_project']                                      = 'Διαγραφή Έργου';
$lang['project_task_assigned_to_user']                       = 'Εργασίες που σας έχουν ανατεθεί';
$lang['seconds']                                             = 'Δευτερόλεπτα';
$lang['hours']                                               = 'Ώρες';
$lang['minutes']                                             = 'Λεπτά';
$lang['project']                                             = 'Έργο';
$lang['project_lowercase']                                   = 'έργο';
$lang['projects']                                            = 'Έργα';
$lang['projects_lowercase']                                  = 'έργα';
$lang['project_settings']                                    = 'Ρυθμίσεις Έργου';
$lang['statement']                                           = 'Κίνηση';
$lang['statement_lowercase']                                 = 'κίνηση';
$lang['statements']                                          = 'Κινήσεις';
$lang['statements_lowercase']                                = 'κινήσεις';
$lang['statement_settings']                                  = 'Ρυθμίσεις Κίνησης';
$lang['project_invoiced_successfully']                       = 'Το έργο τιμολογήθηκε με επιτυχία';
$lang['new_project']                                         = 'Νέο Έργο';
$lang['project_files']                                       = 'Αρχεία';
$lang['project_activity']                                    = 'Δραστηριότητα';
$lang['new_statement']                                       = 'Νέα Κίνηση';
$lang['project_files']                                       = 'Αρχεία';
$lang['project_activity']                                    = 'Δραστηριότητα';
$lang['statement_activity']                                  = 'Δραστηριότητα';
$lang['project_name']                                        = 'Όνομα Έργου';
$lang['project_description']                                 = 'Περιγραφή';
$lang['project_customer']                                    = 'Πελάτης';
$lang['project_start_date']                                  = 'Ημερομηνία Έναρξης';
$lang['project_datecreated']                                 = 'Ηεμρομηνία Δημιουργίας';
$lang['project_deadline']                                    = 'Προθεσμία';
$lang['statement_name']                                      = 'Όνομα Κίνησης';
$lang['statement_description']                               = 'Περιγραφή';
$lang['statement_customer']                                  = 'Πελάτης';
$lang['statement_start_date']                                = 'Ημερομηνία Έναρξης';
$lang['statement_datecreated']                               = 'Ηεμρομηνία Δημιουργίας';
$lang['statement_deadline']                                  = 'Προθεσμία';
$lang['project_billing_type']                                = 'Τύπος Τιμολόγησης';
$lang['project_billing_type_fixed_cost']                     = 'Καθορισμένη Χρέωση';
$lang['project_billing_type_project_hours']                  = 'Ώρες Έργου';
$lang['project_billing_type_project_task_hours']             = 'Ώρες Εργασίας';
$lang['project_billing_type_project_task_hours_hourly_rate'] = 'Based on task hourly rate';
$lang['project_rate_per_hour']                               = 'Rate Per Hour';
$lang['project_total_cost']                                  = 'Συνολική Χρέωση';
$lang['project_members']                                     = 'Μέλη';
$lang['statement_members']                                   = 'Μέλη';
$lang['project_member_removed']                              = 'Project member removed successfully';
$lang['project_overview']                                    = 'Επισκόπιση Έργου';
$lang['statement_overview']                                  = 'Επισκόπιση Κίνησης';
$lang['project_gant']                                        = 'Gantt View';
$lang['project_milestones']                                  = 'Milestones';
$lang['project_milestone_order']                             = 'Order';
$lang['project_milestone_duedate_passed']                    = 'Due date passed';
$lang['record_timesheet']                                    = 'Timesheet';
$lang['new_milestone']                                       = 'New Milestone';
$lang['edit_milestone']                                      = 'Edit Milestone';
$lang['milestone_name']                                      = 'Name';
$lang['milestone_due_date']                                  = 'Due date';
$lang['project_milestone']                                   = 'Milestone';
$lang['project_notes']                                       = 'Notes';
$lang['statement_notes']                                     = 'Notes';
$lang['project_timesheets']                                  = 'Timesheets';
$lang['project_timesheet']                                   = 'Timesheet';
$lang['milestone_total_logged_time']                         = 'Logged Time';
$lang['project_overview_total_logged_hours']                 = 'Total Logged Hours';
$lang['milestones_uncategorized']                            = 'Uncategorized';
$lang['milestone_no_tasks_found']                            = 'No tasks found';
$lang['project_copied_successfully']                         = 'Project data copied successfully';
$lang['failed_to_copy_project']                              = 'Failed to copy project';
$lang['copy_project_task_include_check_list_items']          = 'Copy checklist items';
$lang['copy_project_task_include_assignees']                 = 'Copy the same assignees';
$lang['copy_project_task_include_followers']                 = 'Copy the same followers';
$lang['project_days_left']                                   = 'Days Left';
$lang['project_open_tasks']                                  = 'Open Tasks';
$lang['timesheet_stop_timer']                                = 'Stop Timer';
$lang['failed_to_add_project_timesheet_end_time_smaller']    = 'Failed to add timesheet. End time is smaller then start time';
$lang['project_timesheet_user']                              = 'Member';
$lang['project_timesheet_start_time']                        = 'Start Time';
$lang['project_timesheet_end_time']                          = 'End Time';
$lang['project_timesheet_time_spend']                        = 'Time Spent';
$lang['project_timesheet_task']                              = 'Task';
$lang['project_invoices']                                    = 'Invoices';
$lang['total_logged_hours_by_staff']                         = 'Total Logged Time';
$lang['invoice_project']                                     = 'Invoice Project';
$lang['invoice_project_info']                                = 'Project Invoice Info';
$lang['invoice_project_data_single_line']                    = 'Single line';
$lang['invoice_project_data_task_per_item']                  = 'Task per item';
$lang['invoice_project_data_timesheets_individually']        = 'All timesheets individually';
$lang['invoice_project_item_name_data']                      = 'Όνομα Είδους';
$lang['invoice_project_description_data']                    = 'Περιγραφή';
$lang['invoice_project_projectname_taskname']                = 'Project name + Task name';
$lang['invoice_project_all_tasks_total_logged_time']         = 'All tasks + total logged time per task';
$lang['invoice_project_project_name_data']                   = 'Project name';
$lang['invoice_project_timesheet_individually_data']         = 'Timesheet start time + end time + total logged time';
$lang['invoice_project_total_logged_time_data']              = 'Total logged time';
$lang['project_allow_client_to']                             = 'Allow customer to %s';
$lang['project_setting_view_task_attachments']               = 'view task attachments';
$lang['project_setting_view_task_checklist_items']           = 'view task checklist items';
$lang['project_setting_upload_files']                        = 'upload files';
$lang['project_setting_view_task_comments']                  = 'view task comments';
$lang['project_setting_upload_on_tasks']                     = 'upload attachments on tasks';
$lang['project_setting_view_task_total_logged_time']         = 'view task total logged time';
$lang['project_setting_open_discussions']                    = 'open discussions';
$lang['project_setting_comment_on_tasks']                    = 'comment on project tasks';
$lang['project_setting_view_tasks']                          = 'view tasks';
$lang['project_setting_view_milestones']                     = 'view milestones';
$lang['project_setting_view_gantt']                          = 'view Gantt';
$lang['project_setting_view_timesheets']                     = 'view timesheets';
$lang['project_setting_view_activity_log']                   = 'view activity log';
$lang['project_setting_view_team_members']                   = 'view team members';
$lang['project_discussion_visible_to_customer_yes']          = 'Visible';
$lang['project_discussion_visible_to_customer_no']           = 'Not Visible';
$lang['project_discussion_posted_on']                        = 'Posted on %s';
$lang['project_discussion_posted_by']                        = 'Posted by %s';
$lang['project_discussion_failed_to_delete']                 = 'Failed to delete discussion';
$lang['project_discussion_deleted']                          = 'Discussion deleted successfully';
$lang['project_discussion_no_activity']                      = 'No Activity';
$lang['project_discussion']                                  = 'Discussion';
$lang['project_discussions']                                 = 'Discussions';
$lang['statement_discussions']                               = 'Σχόλια';
$lang['edit_discussion']                                     = 'Edit Discussion';
$lang['new_project_discussion']                              = 'Create Discussion';
$lang['project_discussion_subject']                          = 'Θέμα';
$lang['project_discussion_description']                      = 'Περιγραφή';
$lang['project_discussion_show_to_customer']                 = 'Visible to Customer';
$lang['project_discussion_total_comments']                   = 'Total Comments';
$lang['project_discussion_last_activity']                    = 'Last Activity';
$lang['discussion_add_comment']                              = 'Add comment';
$lang['discussion_newest']                                   = 'Newest';
$lang['discussion_oldest']                                   = 'Oldest';
$lang['discussion_attachments']                              = 'Attachments';
$lang['discussion_send']                                     = 'Send';
$lang['discussion_reply']                                    = 'Answer';
$lang['discussion_edit']                                     = 'Edit';
$lang['discussion_edited']                                   = 'Modified';
$lang['discussion_you']                                      = 'You';
$lang['discussion_save']                                     = 'Save';
$lang['discussion_delete']                                   = 'Delete';
$lang['discussion_view_all_replies']                         = 'Show all replies';
$lang['discussion_hide_replies']                             = 'Hide replies';
$lang['discussion_no_comments']                              = 'No comments';
$lang['discussion_no_attachments']                           = 'No attachments';
$lang['discussion_attachments_drop']                         = 'Drag and drop to upload file';
$lang['project_note']                                        = 'Note';
$lang['project_note_private']                                = 'Private notes';
$lang['project_save_note']                                   = 'Save note';

# Project Activity
$lang['project_activity_created']                      = 'Created the project';
$lang['project_activity_updated']                      = 'Updated project';
$lang['project_activity_removed_team_member']          = 'Removed team member';
$lang['project_activity_added_team_member']            = 'Added new team member';
$lang['project_activity_marked_all_tasks_as_complete'] = 'Marked all tasks as complete';
$lang['project_activity_recorded_timesheet']           = 'Recorded timesheet';
$lang['project_activity_task_name']                    = 'Task:';
$lang['project_activity_deleted_discussion']           = 'Deleted Discussion';
$lang['project_activity_created_discussion']           = 'Created discussion';
$lang['project_activity_updated_discussion']           = 'Updated discussion';
$lang['project_activity_commented_on_discussion']      = 'Commented on discussion';
$lang['project_activity_deleted_discussion_comment']   = 'Deleted discussion comment';
$lang['project_activity_deleted_milestone']            = 'Deleted milestone';
$lang['project_activity_updated_milestone']            = 'Updated milestone';
$lang['project_activity_created_milestone']            = 'Created new milestone';
$lang['project_activity_invoiced_project']             = 'Invoiced project';
$lang['project_activity_task_marked_complete']         = 'Task marked as complete';
$lang['project_activity_task_unmarked_complete']       = 'Task unmarked as complete';
$lang['project_activity_task_deleted']                 = 'Task deleted';
$lang['project_activity_new_task_comment']             = 'Commented on task';
$lang['project_activity_new_task_attachment']          = 'Uploaded attachment on task';
$lang['project_activity_new_task_assignee']            = 'Added new task assignee';
$lang['project_activity_task_assignee_removed']        = 'Removed task assignee';
$lang['project_activity_task_timesheet_deleted']       = 'Removed timesheet';
$lang['project_activity_uploaded_file']                = 'Uploaded project file';
$lang['project_activity_status_updated']               = 'Updated project status';
$lang['project_activity_visible_to_customer']          = 'Visible to customer';
$lang['project_activity_project_file_removed']         = 'Removed project file';

# Customers area
$lang['clients_my_estimates']                                  = 'Estimates';
$lang['client_no_reply']                                       = 'No Reply';
$lang['clients_nav_projects']                                  = 'Projects';
$lang['clients_my_projects']                                   = 'Projects';
$lang['client_profile_image']                                  = 'Εικόνα προφιλ';
$lang['customer_email']                                        = 'Email';
$lang['sales_report_cancelled_invoices_not_included']          = 'Cancelled invoices are excluded from the report';
$lang['invoices_merge_cancel_merged_invoices']                 = 'Mark merged invoices as cancelled instead of deleting';
$lang['invoice_marked_as_cancelled_successfully']              = 'Invoice marked as cancelled successfully';
$lang['invoice_unmarked_as_cancelled']                         = 'Invoice unmarked as cancelled successfully';
$lang['tasks_reminder_notification_before']                    = 'Task deadline reminder before (Days)';
$lang['not_task_deadline_reminder']                            = 'Task deadline reminder';
$lang['dt_length_menu_all']                                    = 'All';
$lang['task_not_finished']                                     = 'Not Completed';
$lang['task_billed_cant_start_timer']                          = 'Task billed. Timer cant be start';
$lang['invoice_task_billable_timers_found']                    = 'Started timers found';
$lang['project_timesheet_not_updated']                         = 'Timesheet not affected';
$lang['project_invoice_task_no_timers_found']                  = 'No timers found for this task';
$lang['invoice_project_tasks_not_started']                     = 'Not yet started | Start Date: %s';
$lang['invoice_project_see_billed_tasks']                      = 'See tasks that will be billed on this invoice';
$lang['invoice_project_all_billable_tasks_marked_as_finished'] = 'All billed tasks will be marked as finished';
$lang['invoice_project_nothing_to_bill']                       = 'No tasks to bill. Feel free to add whatever you want in the invoice items.';
$lang['invoice_project_start_date_tasks_not_passed']           = 'Tasks with future start date cannot be billed';
$lang['invoice_project_stop_all_timers']                       = 'Stop all timers';
$lang['invoice_project_stop_billable_timers_only']             = 'Stop only billable timers';
$lang['project_tasks_total_timers_stopped']                    = 'Stopped total %s timers';
$lang['project_invoice_timers_started']                        = 'Task timers found running on billable tasks, invoice cannot be created. Please stop task timers to create invoice.';
$lang['task_start_timer_only_assignee']                        = 'You need to be assigned on this task to start timer!';
$lang['task_comments']                                         = 'Comments';
$lang['invoice_total_tax']                                     = 'Total Tax';
$lang['estimates_total_tax']                                   = 'Total Tax';
$lang['report_invoice_total_tax']                              = 'Total Tax';
$lang['home_tickets']                                          = 'Tickets';
$lang['home_project_activity']                                 = 'Latest Project Activity';
$lang['view_tracking']                                         = 'Views Tracking';
$lang['view_date']                                             = 'Date';
$lang['view_ip']                                               = 'IP Address';
$lang['article_total_views']                                   = 'Total Views';
$lang['leads_source']                                          = 'Source';
$lang['invoices_available_for_merging']                        = 'Invoices Available For Merging';
$lang['invoices_merge_discount']                               = 'You will have to apply discount of total %s manually to this invoice';
$lang['invoice_merge_number_warning']                          = 'Merging invoices will create gaps in invoice numbers. Please do not merge invoices if you want no gaps in your invoice history. You also have the option of manually adjusting invoice numbers if you want to fill the gaps.';
$lang['invoice_mark_as']                                       = 'Mark as %s';
$lang['invoice_unmark_as']                                     = 'Unmark as %s';
$lang['invoice_status_cancelled']                              = 'Cancelled';
$lang['tasks_reminder_notification_before_help']               = 'Notify task assignees about deadline before X days. The notification/email is sent only to the assignees. If the difference between task start date and task due date is smaller then the reminders day no notification will be sent.';

# Version 1.1.0
$lang['project_invoice_select_all_tasks'] = 'Select all tasks';
$lang['lead_company']                     = 'Επωνυμία';

# Version 1.1.1
$lang['admin_auth_forgot_password_button']       = 'Confirm';
$lang['task_assigned']                           = 'Ανάθεση σε';
$lang['switch_to_pipeline']                      = 'Switch to pipeline';
$lang['switch_to_list_view']                     = 'Switch to list';
$lang['estimates_pipeline']                      = 'Estimates Pipeline';
$lang['estimates_pipeline_sort']                 = 'Ταξινόμηση';
$lang['estimates_sort_expiry_date']              = 'Ημερομηνία Λήξης';
$lang['estimates_sort_pipeline']                 = 'Pipeline Order';
$lang['estimates_sort_datecreated']              = 'Ημερομηνία Δημιουργίας';
$lang['estimates_sort_estimate_date']            = 'Estimate Date';
$lang['estimate_set_reminder_title']             = 'Set Estimate Reminder';
$lang['invoice_set_reminder_title']              = 'Set Invoice Reminder';
$lang['estimate_reminders']                      = 'Υπενθυμίσεις';
$lang['invoice_reminders']                       = 'Υπενθυμίσεις';
$lang['estimate_notes']                          = 'Σημειώσεις';
$lang['estimate_add_note']                       = 'Προσθήκη Σημείωσης';
$lang['dropdown_non_selected_tex']               = 'Καμια Επιλογή';
$lang['auto_close_ticket_after']                 = 'Auto close ticket after (Hours)';
$lang['event_description']                       = 'Περιγραφή';
$lang['delete_event']                            = 'Διαγραφή';
$lang['not_new_ticket_created']                  = 'New ticket opened in your department - %s';
$lang['receive_notification_on_new_ticket']      = 'Receive notification on new ticket opened';
$lang['receive_notification_on_new_ticket_help'] = 'All staff members which belong to the ticket department will receive notification that new ticket is opened';
$lang['event_updated']                           = 'Event updated successfully';
$lang['customer_contacts']                       = 'Επαφές';
$lang['supplier_contacts']                       = 'Επαφές';
$lang['new_contact']                             = 'Νέα Επαφή';
$lang['contact']                                 = 'Επαφή';
$lang['contact_lowercase']                       = 'επαφή';
$lang['contact_primary']                         = 'Προεπιλεγμένη Eπαφή';
$lang['contact_position']                        = 'Θέση';
$lang['contact_active']                          = 'Ενεργή';
$lang['client_company_info']                     = 'Πληροφορίες Εταιρίας';
$lang['proposal_save']                           = 'Αποθήκευση Προσφοράς';
$lang['action_save']                           = 'Αποθήκευση Κίνησης';
$lang['calendar']                                = 'Ημερολόγιο';
$lang['settings_pdf']                            = 'PDF';
$lang['settings_pdf_font']                       = 'PDF Font';
$lang['settings_pdf_table_heading_color']        = 'Items table heading color';
$lang['settings_pdf_table_heading_text_color']   = 'Items table heading text color';
$lang['settings_pdf_font_size']                  = 'Default font size';
$lang['proposal_status_draft']                   = 'Πρόχειρο';
$lang['action_status_draft']                   = 'Πρόχειρο';
$lang['custom_field_contacts']                   = 'Επαφές';
$lang['company_primary_email']                   = 'Προεπιλεγμένο Email';
$lang['client_register_contact_info']            = 'Primary Contact Information';
$lang['client_register_company_info']            = 'Company Information';
$lang['contact_permissions_info']                = 'Make sure to set appropriate permissions for this contact';
$lang['default_leads_kanban_sort']               = 'Default leads kan ban sort';
$lang['default_leads_kanban_sort_type']          = 'Sort';
$lang['order_ascending']                         = 'Ascending';
$lang['order_descending']                        = 'Descending';
$lang['calendar_expand']                         = 'expand';
$lang['proposal_reminders']                      = 'Υπενθυμίσεις';
$lang['proposal_topostropos_name']				 = 'Τόπος & τρόπος παράδοσης ή αποστολής';
$lang['proposal_topostropos_add_heading']        = 'Προσθήκη τόπου & τρόπου παράδοσης ή αποστολής';
$lang['proposal_topostropos']					 = 'Τόπος & τρόπος παράδοσης ή αποστολής';
$lang['proposal_metaforika_add_heading']		 = 'Προσθήκη μεταφορικά';
$lang['proposal_metaforika_name']				 = 'Μεταφορικά';
$lang['proposal_metaforika']					 = 'Μεταφορικά';
$lang['proposal_tax']							 = 'Φόρος';
$lang['proposal_taxes']							 = 'Φόροι';
$lang['proposal_tax_add_heading']				 = 'Προσθήκη φόρου';
$lang['proposal_tax_name']						 = 'Φόρος';
$lang['proposal_xronos']						 = 'Χρόνος παράδοσης ή αποστολής';
$lang['proposal_xronos_add_heading']			 = 'Προσθήκη χρόνος παράδοσης ή αποστολής';
$lang['proposal_xronos_name']					 = 'Χρόνος παράδοσης ή αποστολής';
$lang['proposal_xronos_delete_heading']			 = 'Διαγραφή χρόνος παράδοσης ή αποστολής';
$lang['proposal_pliromi']						 = 'Τρόπος πληρωμής';
$lang['proposal_pliromi_name']					 = 'Τρόπος πληρωμής';
$lang['proposal_pliromi_add_heading']			 = 'Προσθήκη Τρόπος πληρωμής';
$lang['proposal_pliromi_delete_heading']		 = 'Διαγραφή Τρόπος πληρωμής';
$lang['proposal_set_reminder_title']             = 'Δημιουργία Υπενθύμισης';
$lang['action_reminders']                        = 'Υπενθυμίσεις';
$lang['action_set_reminder_title']               = 'Δημιουργία Υπενθύμισης';
$lang['settings_allowed_upload_file_types']      = 'Allowed file types';
$lang['no_primary_contact']                      = 'This customer does have primary contact. You need to setup primary contact login as customer. Its recommended all customers to have primary contacts.';
$lang['leads_merge_customer']                    = 'Customer fields merging';
$lang['leads_merge_contact']                     = 'Contact fields merging';
$lang['leads_merge_as_contact_field']            = 'Merge as contact field';
$lang['lead_convert_to_client_phone']            = 'Τηλέφωνο';
$lang['invoice_status_report_all']               = 'Όλα';
$lang['import_contact_field']                    = 'Contact field';
$lang['file_uploaded_success']                   = 'There is no error, the file uploaded with success';
$lang['file_exceeds_max_filesize']               = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
$lang['file_exceeds_maxfile_size_in_form']       = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
$lang['file_uploaded_partially']                 = 'The uploaded file was only partially uploaded';
$lang['file_not_uploaded']                       = 'No file was uploaded';
$lang['file_missing_temporary_folder']           = 'Missing a temporary folder';
$lang['file_failed_to_write_to_disk']            = 'Failed to write file to disk.';
$lang['file_php_extension_blocked']              = 'A PHP extension stopped the file upload.';
$lang['calendar_expand']                         = 'expand';
$lang['view_pdf']                                = 'Προβολή PDF';
$lang['expense_repeat_every']                    = 'Επανάληψη';

# Version 1.1.2
$lang['leads_switch_to_kanban']                   = 'Switch to kan ban';
$lang['survey_no_questions']                      = 'This survey does not have questions added yet.';
$lang['survey_submit']                            = 'Submit';
$lang['contract_content']                         = 'Contract';
$lang['contract_save']                            = 'Save Contract';
$lang['contract_send_to_email']                   = 'Send to email';
$lang['contract_send_to_client_modal_heading']    = 'Send contract to email';
$lang['contract_send_to']                         = 'Send to';
$lang['contract_send_to_client_attach_pdf']       = 'Attach PDF';
$lang['contract_send_to_client_preview_template'] = 'Preview Email Template';
$lang['include_attachments_to_email']             = 'Include attachments to email';
$lang['contract_sent_to_client_success']          = 'The contract is successfully sent to the customer';
$lang['contract_sent_to_client_fail']             = 'Failed to send contract';

# Version 1.1.3
$lang['client_invalid_username_or_password'] = 'Invalid username or password';
$lang['client_old_password_incorrect']       = 'Your old password is incorrect';
$lang['client_password_changed']             = 'Your password has been changed';

# Version 1.1.4
$lang['total_leads_deleted']                              = 'Σύνολο επαφών που διαγράφηκαν: %s';
$lang['total_clients_deleted']                            = 'Σύνολο πελατών που διαγράφηκαν: %s';
$lang['confirm_action_prompt']                            = 'Είστε σίγουροι;';
$lang['mass_delete']                                      = 'Μαζική Διαγραφή';
$lang['email_protocol']                                   = 'Email Protocol';
$lang['add_edit_members']                                 = 'Add/Edit Members';
$lang['project_overview_logged_hours']                    = 'Logged Hours:';
$lang['project_overview_billable_hours']                  = 'Billable Hours:';
$lang['project_overview_billed_hours']                    = 'Billed Hours:';
$lang['project_overview_unbilled_hours']                  = 'Unbilled Hours:';
$lang['calendar_first_day']                               = 'First Day';
$lang['permission_view']                                  = 'View';
$lang['permission_edit']                                  = 'Edit';
$lang['permission_create']                                = 'Δημιουργία';
$lang['permission_delete']                                = 'Διαγραφή';
$lang['permission']                                       = 'Διακαίωμα';
$lang['permissions']                                      = 'Δικαιώματα';
$lang['proposals_pipeline']                               = 'Proposals Pipeline';
$lang['proposals_pipeline_sort']                          = 'Ταξινόμηση κατά';
$lang['proposals_sort_open_till']                         = 'Ανοιχτό μέχρι';
$lang['proposals_sort_pipeline']                          = 'Pipeline Order';
$lang['proposals_sort_datecreated']                       = 'Ημερομηνία Δημιουργίας';
$lang['proposals_sort_proposal_date']                     = 'Ημερομηνία Προσφοράς';
$lang['actions_pipeline_sort']                          = 'Ταξινόμηση κατά';
$lang['actions_sort_open_till']                         = 'Ανοιχτό μέχρι';
$lang['actions_sort_pipeline']                          = 'Pipeline Order';
$lang['actions_sort_datecreated']                       = 'Ημερομηνία Δημιουργίας';
$lang['actions_sort_action_date']                     = 'Ημερομηνία Κίνησης';
$lang['action_proteraiotita']							= 'Προτεραιότητα';
$lang['is_not_staff_member']                              = 'Not Staff Member';
$lang['lead_created']                                     = 'Δημιουργήθηκε';
$lang['access_tickets_to_none_staff_members']             = 'Allow access to tickets for non staff members';
$lang['project_expenses']                                 = 'Έξοδα';
$lang['expense_currency']                                 = 'Νόμισμα';
$lang['currency_valid_code_help']                         = 'Make sure to enter valid currency ISO code.';
$lang['week']                                             = 'Week';
$lang['weeks']                                            = 'Weeks';
$lang['month']                                            = 'Month';
$lang['months']                                           = 'Months';
$lang['year']                                             = 'Year';
$lang['years']                                            = 'Years';
$lang['expense_report_category']                          = 'Category';
$lang['expense_paid_via']                                 = 'Paid Via %s';
$lang['item_as_expense']                                  = '[Expense]';
$lang['show_help_on_setup_menu']                          = 'Show help menu item on setup menu';
$lang['customers_summary_total']                          = 'Σύνολο Πελατών';
$lang['suppliers_summary_total']                          = 'Σύνολο Προμηθευτών';
$lang['filter_by']                                        = 'Φιλτράρισμα με';
$lang['re_captcha']                                       = 'reCAPTCHA';
$lang['recaptcha_site_key']                               = 'Site key';
$lang['recaptcha_secret_key']                             = 'Secret key';
$lang['recaptcha_error']                                  = 'The reCAPTCHA field is telling that you are a robot.';
$lang['smtp_username']                                    = 'SMTP Username';
$lang['smtp_username_help']                               = 'Fill only if your email client use username for SMTP login.';
$lang['pinned_project']                                   = 'Pinned Project';
$lang['pin_project']                                      = 'Pin Project';
$lang['unpin_project']                                    = 'Unpin Project';
$lang['smtp_encryption']                                  = 'Email Encryption';
$lang['smtp_encryption_none']                             = 'None';
$lang['show_proposals_on_calendar']                       = 'Proposals';
$lang['show_actions_on_calendar']                         = 'Κινήσεις';
$lang['invoice_project_see_billed_expenses']              = 'See expenses that will be billed on this invoice';
$lang['project_overview_expenses']                        = 'Total Expenses';
$lang['project_overview_expenses_billable']               = 'Billable Expenses';
$lang['project_overview_expenses_billed']                 = 'Billed Expenses';
$lang['project_overview_expenses_unbilled']               = 'Unbilled Expenses';
$lang['announcement_date_list']                           = 'Date';
$lang['project_setting_view_finance_overview']            = 'view finance overview';
$lang['show_all_tasks_for_project_member']                = 'Allow all staff to see all tasks related to projects (includes non-staff)';
$lang['not_staff_added_as_project_member']                = 'Added you as project member';
$lang['report_expenses_base_currency_select_explanation'] = 'You need to select currency because the system found different currencies used for expenses.';

# Version 1.1.6
$lang['project_activity_recorded_expense']   = 'Recorded Expense';
$lang['save_customer_and_add_contact']       = 'Αποθήκευση και δημιουργία επαφής';
$lang['tickets_chart_weekly_opening_stats']  = 'Weekly Stats';
$lang['related_knowledgebase_articles']      = 'Related Articles';
$lang['detailed_overview']                   = 'Tasks Overview';
$lang['tasks_total_checklists_finished']     = 'Total checklist items marked as finished';
$lang['tasks_total_added_attachments']       = 'Total attachments added';
$lang['tasks_total_comments']                = 'Total comments';
$lang['task_finished_on_time']               = 'Finished on time?';
$lang['task_finished_on_time_indicator']     = 'Yes';
$lang['task_not_finished_on_time_indicator'] = 'No';
$lang['filter']                              = 'Filter';
$lang['task_filter_detailed_all_months']     = 'All Months';
$lang['kb_article_slug']                     = 'Slug';

# Version 1.1.7
$lang['email_template_ticket_warning']          = 'If ticket is imported with email piping and the contact does not exists in the CRM the fields won\'t be replaced.';
$lang['auto_stop_tasks_timers_on_new_timer']    = 'Stop all other started timers when starting new timer';
$lang['notification_when_customer_pay_invoice'] = 'Receive notification when customer pay invoice (built-in)';
$lang['not_invoice_payment_recorded']           = 'New invoice payment - %s';
$lang['email_template_contact_warning']         = 'If contact is not logged while making action the contact merge fields won\'t be replaced.';
$lang['theme_style']                            = 'Theme Style';
$lang['change_role_permission_warning']         = 'Changing role permissions won\'t affected current staff members permissions that are using this role.';
$lang['task_copied_successfully']               = 'Task copied successfully';
$lang['failed_to_copy_task']                    = 'Failed to copy task';
$lang['not_project_file_uploaded']              = 'New project file added';
$lang['settings_calendar_color']                = '%s Color';
$lang['settings_calendar_colors_heading']       = 'Styling';
$lang['reminder']                               = 'Reminder';
$lang['back_to_tasks_list']                     = 'Back to tasks list';
$lang['copy_task_confirm']                      = 'Confirm';
$lang['changing_items_affect_warning']          = 'Changing item info won\'t affect on the created invoices/estimates/proposals.';
$lang['tax_is_used_in_expenses_warning']        = 'You can\'t update this tax because the tax is used by expenses transactions.';
$lang['note']                                   = 'Note';
$lang['leads_staff_report_converted']           = 'Total converted leads';
$lang['leads_staff_report_created']             = 'Total created leads';
$lang['leads_staff_report_lost']                = 'Total lost leads';
$lang['client_go_to_dashboard']                 = 'Back to portal';
$lang['show_estimate_reminders_on_calendar']    = 'Estimate Reminders';
$lang['show_invoice_reminders_on_calendar']     = 'Invoice Reminders';
$lang['show_proposal_reminders_on_calendar']    = 'Proposal Reminders';
$lang['calendar_estimate_reminder']             = 'Estimate Reminder';
$lang['calendar_invoice_reminder']              = 'Invoice Reminder';
$lang['calendar_proposal_reminder']             = 'Proposal Reminder';
$lang['proposal_due_after']                     = 'Proposal Due After (days)';
$lang['project_progress']                       = 'Πρόοδος';
$lang['calculate_progress_through_tasks']       = 'Υπολογισμός προόδου μέσω των εργασιών του έργου';
$lang['allow_customer_to_change_ticket_status'] = 'Allow customer to change ticket status from customers area';
$lang['switch_to_general_report']               = 'Switch to staff report';
$lang['switch_to_staff_report']                 = 'Switch to general report';
$lang['generate']                               = 'Generate';
$lang['from_date']                              = 'From date';
$lang['to_date']                                = 'To date';
$lang['not_results_found']                      = 'No results found';
$lang['lead_lock_after_convert_to_customer']    = 'Do not allow leads to be edited after they are converted to customers (administrators not applied)';
$lang['default_pipeline_sort']                  = 'Default pipeline sort';
$lang['not_goal_message_failed']                = 'We failed to achieve goal!<br /> Goal Type: %s
<br />Goal Achievement: %s
<br />Total Achievement: %s
<br />Start Date: %s
<br />End Date: %s';
$lang['toggle_full_view']             = 'Toggle full view';
$lang['not_estimate_invoice_deleted'] = 'deleted the created invoice';
$lang['not_task_new_comment']         = 'commented on task %s';

# Version 1.1.8
$lang['invoice_number_exists']                  = 'This invoice number exists for the ongoing year.';
$lang['estimate_number_exists']                 = 'This estimate number exists for the ongoing year.';
$lang['email_exists']                           = 'Email already exists';
$lang['not_uploaded_project_file']              = 'New file uploaded';
$lang['not_created_new_project_discussion']     = 'New project discussion created';
$lang['not_commented_on_project_discussion']    = 'New comment on project discussion';
$lang['all_staff_members']                      = 'All staff members';
$lang['help_project_permissions']               = 'VIEW allows staff member to see ALL projects. If you only want them to see projects they are assigned (added as members), do not give VIEW permissions.';
$lang['help_tasks_permissions']                 = 'VIEW allows staff member to see ALL tasks. If you only want them to see tasks they are assigned to or following, do not give VIEW permissions.';
$lang['help_statement_permissions']               = 'VIEW allows staff member to see ALL projects. If you only want them to see projects they are assigned (added as members), do not give VIEW permissions.';
$lang['expense_recurring_days']                 = 'Day(s)';
$lang['expense_recurring_weeks']                = 'Week(s)';
$lang['expense_recurring_months']               = 'Month(s)';
$lang['expense_recurring_years']                = 'Years(s)';
$lang['reset_to_default_color']                 = 'Reset to default color';
$lang['pdf_logo_width']                         = 'Logo Width (PX)';
$lang['drop_files_here_to_upload']              = 'Σύρετε αρχεία για επισύναψη';
$lang['browser_not_support_drag_and_drop']      = 'Your browser does not support drag\'n\'drop file uploads';
$lang['remove_file']                            = 'Remove file';
$lang['you_can_not_upload_any_more_files']      = 'You can not upload any more files';
$lang['custom_field_only_admin']                = 'Restrict visibility for administrators only';
$lang['leads_default_source']                   = 'Default source';
$lang['clear_activity_log']                     = 'Clear log';
$lang['default_contact_permissions']            = 'Default contact permissions';
$lang['invoice_activity_marked_as_cancelled']   = 'marked invoice as cancelled';
$lang['invoice_activity_unmarked_as_cancelled'] = 'unmarked invoice as cancelled';
$lang['wait_text']                              = 'Παρακαλώ Περιμένετε...';
$lang['projects_summary']                       = 'Έργα';
$lang['dept_imap_host']                         = 'IMAP Host';
$lang['dept_encryption']                        = 'Encryption';
$lang['dept_email_password']                    = 'Password';
$lang['dept_email_no_encryption']               = 'No Encryption';
$lang['failed_to_decrypt_password']             = 'Failed to decrypt password';
$lang['delete_mail_after_import']               = 'Delete mail after import?';
$lang['expiry_reminder_enabled']                = 'Send expiration reminder';
$lang['send_expiry_reminder_before']            = 'Send expiration reminder before (DAYS)';
$lang['not_expiry_reminder_sent']               = 'Expiry reminder sent';
$lang['send_expiry_reminder']                   = 'Send expiration reminder';
$lang['sent_expiry_reminder_success']           = 'Expiration reminder successfully sent';
$lang['sent_expiry_reminder_fail']              = 'Failed to send expiration reminder';
$lang['leads_default_status']                   = 'Default status';
$lang['item_description_placeholder']           = 'Περιγραφή';
$lang['item_long_description_placeholder']      = 'Πλήρης Περιγραφή';
$lang['item_quantity_placeholder']              = 'Ποσότητα';
$lang['item_rate_placeholder']                  = 'Τιμή Μονάδας';
$lang['tickets_summary']                        = 'Tickets Summary';
$lang['tasks_list_priority']                    = 'Προτεραιότητα';
$lang['ticket_status_db_2']                     = 'Σε Εξέλιξη';
$lang['ticket_status_db_1']                     = 'Ανοιχτό';
$lang['ticket_status_db_3']                     = 'Answered';
$lang['ticket_status_db_4']                     = 'Σε Αναμονή';
$lang['ticket_status_db_5']                     = 'Κλειστό';
$lang['ticket_priority_db_1']                   = 'Χαμηλή';
$lang['ticket_priority_db_2']                   = 'Κανονική';
$lang['ticket_priority_db_3']                   = 'Υψηλή';
$lang['customer_have_projects_by']              = 'Contains projects by status %s';
$lang['customer_have_proposals_by']             = 'Με προσφορά σε κατάσταση %s';
$lang['customer_have_actions_by']               = 'Με κίνηση σε κατάσταση %s';
$lang['do_not_redirect_payment']                = 'Do not redirect me to the payment processor';
$lang['project_tickets']                        = 'Tickets';
$lang['invoice_report']                         = 'Invoices Report';
$lang['payment_modes_report']                   = 'Payment Modes (Transactions)';
$lang['customer_admins']                        = 'Customer Admins';
$lang['assign_admin']                           = 'Assign admin';
$lang['customer_admin_date_assigned']           = 'Date Assigned';
$lang['customer_admin_login_as_client_message'] = 'Hello %s. You are added as admin to this customer.';
$lang['ticket_form_validation_file_size']       = 'File size must be less than %s';
$lang['has_transactions_currency_base_change']  = 'Changing the base currency is possible only if there are no transactions recorded in that currency. Delete the transactions to change the base currency';
$lang['customers_sort_all']                     = 'Όλοι';

# Version 1.1.9
$lang['use_recaptcha_customers_area']    = 'Enable reCAPTCHA on customers area (Login/Register)';
$lang['project_marked_as_finished']      = 'Project completed';
$lang['project_status_updated']          = 'Project status updated';
$lang['remove_decimals_on_zero']         = 'Remove decimals on numbers/money with zero decimals (2.00 will become 2, 2.25 will stay 2.25)';
$lang['remove_tax_name_from_item_table'] = 'Remove the tax name from item table row';

# Version 1.2.0
$lang['not_billable_expenses_by_categories']      = 'Not billable expenses by categories';
$lang['billable_expenses_by_categories']          = 'Billable expenses by categories';
$lang['format_letter_size']                       = 'A4 Οριζόντια';
$lang['pdf_formats']                              = 'Μορφή Εγγράφου';
$lang['swap_pdf_info']                            = 'Swap Company/Customer Details (company details to right side, customer details to left side)';
$lang['expenses_filter_by_categories']            = 'Ανά Κατηγορία';
$lang['task_copy']                                = 'Copy';
$lang['estimate_status']                          = 'Κατάσταση';
$lang['expenses_report_exclude_billable']         = 'Exclude Billable Expenses';
$lang['expenses_total']                           = 'Σύνολο';
$lang['estimate_activity_added_attachment']       = 'Συνημμμένο';
$lang['show_to_customer']                         = 'Προβολή στον Πελάτη';
$lang['hide_from_customer']                       = 'Απόκρυψη από τον Πελάτη';
$lang['expenses_report_total']                    = 'Σύνολο';
$lang['expenses_report']                          = 'Αναφορά εξόδων';
$lang['expenses_report_total_tax']                = 'Σύνολο ΦΠΑ';
$lang['expenses_detailed_report']                 = 'Λεπτομερής αναφορά';
$lang['expense_not_billable']                     = 'Μη Χρεώσιμο';
$lang['notification_settings']                    = 'Notification settings';
$lang['staff_with_roles']                         = 'Staff members with roles';
$lang['specific_staff_members']                   = 'Specific Staff Members';
$lang['proposal_mark_as']                         = 'Σήμανση ως %s';
$lang['action_mark_as']                         = 'Σήμανση ως %s';
$lang['kb_report_total_answers']                  = 'Σύνολο';
$lang['ticket_message_edit']                      = 'Επεξεργασία';
$lang['invoice_files']                            = 'Invoice Files';
$lang['estimate_files']                           = 'Estimate Files';
$lang['proposal_files']                           = 'Αρχεία προσφοράς';
$lang['action_files']                             = 'Αρχεία κίνησης';
$lang['invoices_awaiting_payment']                = 'Τιμολόγια σε Αναμονή Πληρωμής';
$lang['tasks_not_finished']                       = 'Εκκρεμούν Εργασίες';
$lang['outstanding_invoices']                     = 'Τιμολόγια που Εκκρεμούν';
$lang['past_due_invoices']                        = 'Ληγμένα Τιμολόγια';
$lang['paid_invoices']                            = 'Πληρωμένα Τιμολόγια';
$lang['invoice_estimate_year']                    = 'Έτος';
$lang['task_stats_logged_hours']                  = 'Logged Hours';
$lang['leads_converted_to_client']                = 'Converted Leads';
$lang['task_assigned_from']                       = 'This task is assigned to you by %s';
$lang['new_note']                                 = 'Νεα Σημείωση';
$lang['my_tickets_assigned']                      = 'Tickets assigned to me';
$lang['filter_by_assigned']                       = 'By Assigned Member';
$lang['staff_stats_total_logged_time']            = 'Total Logged Time';
$lang['staff_stats_last_month_total_logged_time'] = 'Last Month Logged Time';
$lang['staff_stats_this_month_total_logged_time'] = 'This Month Logged Time';
$lang['staff_stats_last_week_total_logged_time']  = 'Last Week Logged Time';
$lang['staff_stats_this_week_total_logged_time']  = 'This Week Logged Time';
// Dont change this because are translated before for the projects timesheets and now are only used for readability.
$lang['timesheet_user']       = $lang['project_timesheet_user'];
$lang['timesheet_start_time'] = $lang['project_timesheet_start_time'];
$lang['timesheet_end_time']   = $lang['project_timesheet_end_time'];
$lang['timesheet_time_spend'] = $lang['project_timesheet_time_spend'];
$lang['task_timesheets']      = $lang['project_timesheets'];
$lang['task_log_time_start']  = $lang['project_timesheet_start_time'];
$lang['task_log_time_end']    = $lang['project_timesheet_end_time'];
$lang['task_single_log_user'] = $lang['project_timesheet_user'];

# Version 1.2.2
$lang['milestone_description']                      = 'Περιγραφή';
$lang['description_visible_to_customer']            = 'Show description to customer';
$lang['upcoming_tasks']                             = 'Upcoming Tasks';
$lang['paymentmethod_two_checkout_account_number']  = 'Account Number (Seller ID)';
$lang['paymentmethod_two_checkout_private_key']     = 'Private Key';
$lang['paymentmethod_two_checkout_publishable_key'] = 'Publishable Key';
$lang['payment_credit_card_number']                 = 'Card Number';
$lang['payment_credit_card_expiration_date']        = 'Expiration Date';
$lang['payment_billing_email']                      = 'Email';
$lang['submit_payment']                             = 'Submit Payment';
$lang['2checkout_notice_payment']                   = 'Credit card details are processed via secure token provided from 2Checkout';
$lang['2checkout_usage_notice']                     = 'SSL is required if you\'re using the 2Checkout payment API. It is required to safely call the tokenization and authorizations. The system will function without SSL, however, this will place you out of compliance, which risks deactivation of your API permissions.';
$lang['custom_field_disallow_customer_to_edit']     = 'Disallow customer to edit this field';
$lang['project_due_notice']                         = 'This project is overdue by %s days';
$lang['not_lead_added_attachment']                  = 'added new attachment to lead %s';
$lang['lead_note_date_added']                       = 'Note added: %s';
$lang['recurring_custom']                           = 'Custom';
// don't translate these, only added for better readability already translated for the expenses custom recurring feature.
$lang['invoice_recurring_months'] = $lang['expense_recurring_months'];
$lang['invoice_recurring_years']  = $lang['expense_recurring_years'];
$lang['invoice_recurring_days']   = $lang['expense_recurring_days'];
$lang['invoice_recurring_weeks']  = $lang['expense_recurring_weeks'];

# Version 1.2.4
$lang['attach_file']										= 'Επισύναψη Αρχείων';
$lang['document_direction']                                 = 'Direction';
$lang['notify_project_members_status_change']               = 'Notify project members that status is changed';
$lang['not_project_status_updated']                         = 'Project status updated from %s to %s';
$lang['ticket_not_found']                                   = 'Ticket not found';
$lang['project_not_found']                                  = 'Project not found';
$lang['export_project_data']                                = 'Export project data';
$lang['total_project_members']                              = 'Total Project Members';
$lang['total_project_files']                                = 'Files attached';
$lang['total_project_discussions_created']                  = 'Discussions created';
$lang['project_member']                                     = 'Member';
$lang['total_project_discussions_comments']                 = 'Total discussion comments';
$lang['staff_total_task_assigned']                          = 'Total tasks assigned';
$lang['staff_total_comments_on_tasks']                      = 'Comments on tasks';
$lang['project_members_overview']                           = 'Project members overview';
$lang['project_milestones_overview']                        = 'Milestones overview';
$lang['total_tasks_in_milestones']                          = 'Total tasks assigned';
$lang['total_task_members_assigned']                        = 'Total members assigned';
$lang['total_task_members_followers']                       = 'Total followers';
$lang['total_milestones']                                   = 'Total milestones';
$lang['total_project_worked_days']                          = 'Total days worked';
$lang['finance_overview']                                   = 'Finance Overview';
$lang['project_custom_fields']                              = 'Custom fields';
$lang['total_tickets_related_to_project']                   = 'Total tickets linked to project';
$lang['projects_total_invoices_created']                    = 'Total invoices created';
$lang['do_not_send_invoice_payment_email_template_contact'] = 'Do not send invoice payment recorded email to customer contacts';
$lang['no_preview_available_for_file']                      = 'No preview available for this file.';
$lang['project_activity_deleted_file_discussion_comment']   = 'File discussion comment deleted';
$lang['email_template_discussion_info']                     = 'This template is used for both project discussion comments emails. (files discussions and regular discussions)';
$lang['format_a4_portrait_size']                            = 'Portrait';
$lang['only_show_contact_tickets']                          = 'In customers area only show tickets related to the logged in contact (Primary contact not applied)';
$lang['cancel_overdue_reminders_invoice']                   = 'Prevent sending overdue reminders for this invoice';
$lang['customer_shipping_address_notice']                   = 'Do not fill shipping address information if you won\'t use shipping address on customer invoices';
$lang['timesheets_overview']                                = 'Timesheets overview';
$lang['invoice_status_draft']                               = 'Πρόχειρο';
$lang['save_as_draft']                                      = 'Απόθήκευση στα Πρόχειρα';
$lang['convert_and_save_as_draft']                          = 'Convert and save as draft';
$lang['convert']                                            = 'Convert';
$lang['exclude_invoices_draft_from_client_area']            = 'Exclude invoices with draft status from customers area';
$lang['invoice_draft_status_info']                          = 'This invoice is with status Draft, status will be auto changed when you send the invoice to the customer or mark as sent.';
$lang['task_info']                                          = 'Task Info';
$lang['recurring_tasks']                                    = 'Recurring';
// don't translate these, already translated
$lang['task_repeat_every']                         = $lang['expense_repeat_every'];
$lang['task_recurring_months']                     = $lang['expense_recurring_months'];
$lang['task_recurring_years']                      = $lang['expense_recurring_years'];
$lang['task_recurring_days']                       = $lang['expense_recurring_days'];
$lang['task_recurring_weeks']                      = $lang['expense_recurring_weeks'];
$lang['todays_tasks']                              = 'Today\'s tasks';
$lang['payment_mode_invoices_only']                = 'Invoices Only';
$lang['payment_mode_expenses_only']                = 'Expenses Only';
$lang['task_no_checklist_items_found']             = 'Checklist items not found for this task';
$lang['task_no_description']                       = 'No description for this task';
$lang['expenses_reminders']                        = 'Υπενθυμίσεις';
$lang['expense_set_reminder_title']                = 'Set expense reminder';
$lang['calendar_expense_reminder']                 = 'Expense Reminders';
$lang['recurring_task']                            = 'Recurring Task';
$lang['disable_email_from_being_sent']             = 'Disable this email from being sent';
$lang['not_sent_indicator']                        = 'Not Sent';
$lang['proposal_status_revised']                   = 'Revised';
$lang['customer_currency_change_notice']           = 'If the customer use other currency then the base currency make sure you select the appropriate currency for this customer. Changing the currency is not possible after transactions are recorded.';
$lang['click_to_add_content']                      = 'Click here to add content';
$lang['related_to_project']                        = 'This %s is related to %s: %s';
$lang['back_to_lead']                              = 'Επιστροφή';
$lang['add_task_timer_started_warning']            = 'Stop current started timer for this task to be able to add new timer manually.';
$lang['sending_email_contact_permissions_warning'] = 'Failed to auto select customer contacts. Please make sure that the customer have associated contacts with permission %s';

# Version 1.2.6
$lang['currently_supported_currencies']                       = 'Currently supported currencies';
$lang['authorize_notice']                                     = 'SSL is required if you\'re using the Authorize.Net AIM payment API. Authorize.net only supports 1 currency per account. Make sure you add only 1 currency associated with your Authorize account in the currencies field.';
$lang['settings_paymentmethod_developer_mode']                = 'Developer Mode';
$lang['payment_cardholder_name']                              = 'Cardholder\'s Name';
$lang['settings_paymentmethod_authorize_api_login_id']        = 'API Login ID';
$lang['settings_paymentmethod_mollie_api_key']                = 'API Key';
$lang['settings_paymentmethod_authorize_api_transaction_key'] = 'API Transaction ID';
$lang['settings_paymentmethod_authorize_secret_key']          = 'Secret Key';
$lang['leads_report_converted_notice']                        = 'Only leads that belongs in the default Customer status will be taken as converted leads, if the leads belongs to the default status client and its not converted to customer will be still counted as converted lead';
$lang['payment_method']                                       = 'Payment Method';
$lang['payment_method_info']                                  = 'Some payment gateways support different/multiple payment methods like Credit Card, PayPal, Bank.';

# Version 1.2.7
$lang['dropbox_app_key']                                    = 'Dropbox APP Key';
$lang['project_invoice_select_all_expenses']                = 'Select all expenses';
$lang['role_update_staff_permissions']                      = 'Update all staff members permissions that are using this role';
$lang['customer_active']                                    = 'Ενεργός';
$lang['supplier_active']                                    = 'Ενεργός';
$lang['note_updated_successfully']                          = 'Note updated successfully';
$lang['update_note']                                        = 'Update note';
$lang['update_comment']                                     = 'Update comment';
$lang['comment_updated_successfully']                       = 'Comment updated successfully';
$lang['staff_send_welcome_email']                           = 'Send welcome email';
$lang['proposal_warning_email_change']                      = 'Email changed for %s. This %s is linked to proposal/s. Do you want to update all proposals emails linked to %s?';
$lang['update_proposal_email_yes']                          = 'Yes update all linked emails.';
$lang['update_proposal_email_no']                           = 'No, i will update manually.';
$lang['proposals_emails_updated']                           = 'All proposals emails linked to this %s updated to %s';
$lang['custom_field_company']                               = 'Εταιρία';
$lang['actions']                                            = 'Κινησεις';
$lang['project_mark_as']                                    = 'Σήμανση ως %s';
$lang['todo_edit_title']                                    = 'Edit todo item';
$lang['additional_action_required']                         = 'Additional action required!';
$lang['project_mark_tasks_finished_confirm']                = 'Επιβεβαίωση';
$lang['project_marked_as_success']                          = 'Project marked as %s successfully';
$lang['project_marked_as_failed']                           = 'Failed to mark project as %s';
$lang['auto_assign_customer_admin_after_lead_convert']      = 'Auto assign as admin to customer after convert';
$lang['auto_assign_customer_admin_after_lead_convert_help'] = 'If this option is set to YES the staff member that converted lead to customer will be auto assigned as customer admin. NOTE: This option will apply only on staff members that do not have permission for customers VIEW';
$lang['auto_close_tickets_disable']                         = 'Set 0 to disable';
$lang['task_checklist_item_completed_by']                   = 'Completed by %s';
$lang['staff_email_signature_help']                         = 'If empty default email signature from settings will be used';
$lang['default_task_priority']                              = 'Προεπιλεγμένη Προτεραιότητα';
$lang['project_send_created_email']                         = 'Send project created email';
$lang['survey_send_to_lists']                               = 'Survey send lists';
$lang['survey_send_notice']                                 = 'Emails will be send via CRON JOB per hour.';

# Version 1.2.8
$lang['show_transactions_on_invoice_pdf']            = 'Show invoice payments (transactions) on PDF';
$lang['bulk_actions']                                = 'Μαζικές Ενέργειες';
$lang['additional_filters']                          = 'Πρόσθετα Φίλτρα';
$lang['expenses_available_to_bill']                  = 'Expenses available to bill';
$lang['bulk_action_customers_groups_warning']        = 'If you do not select any group all groups assigned to the selected customers will be removed.';
$lang['customer_attachments_show_in_customers_area'] = 'Show to customers area';
$lang['customer_attachments_show_notice']            = 'Only files uploaded from customer profile have ability to show/hide in customers area.';
$lang['customer_profile_files']                      = 'Αρχεία';
$lang['no_files_found']                              = 'No Files Found';
$lang['survey_customers_all']                        = 'All Customers';
$lang['custom_field_column']                         = 'Grid (Bootstrap Column eq. 12) - Max is 12';
$lang['task_status']                                 = 'Κατάσταση';
$lang['task_status_1']                               = 'Δεν ξεκίνησε';
$lang['task_status_2']                               = 'Αναμονή απάντησης';
$lang['task_status_3']                               = 'Δοκιμάζεται';
$lang['task_status_4']                               = 'Σε εξέλιξη';
$lang['task_status_5']                               = 'Ολοκληρωμένο';
$lang['task_mark_as']                                = 'Σήμανση ως %s';
$lang['task_marked_as_success']                      = 'Task marked as %s successfully';
$lang['search_tasks']                                = 'Αναζήτηση Εργασιών';
$lang['tasks_kanban_limit']                          = 'Limit tasks kan ban rows per status';
$lang['show_on_invoice_on_pdf']                      = 'Show %s on Invoice PDF';
$lang['show_pay_link_to_invoice_pdf']                = 'Show Pay Invoice link to PDF (Not applied if invoice status is Cancelled)';
$lang['no_leads_found']                              = 'No Leads Found';
$lang['created_today']                               = 'Created Today';
$lang['total_tasks_deleted']                         = 'Total Tasks Deleted: %s';
$lang['total_tickets_delete']                        = 'Total Tickets Deleted: %s';
$lang['total_tickets_delete']                        = 'Total Tickets Deleted: %s';
$lang['format_letter_portrait']                      = 'Letter Portrait';
$lang['format_letter_landscape']                     = 'Letter Landscape';
$lang['period_datepicker']                           = 'Period';
$lang['total_by_hourly_rate']                        = 'Total By Hourly Rate';
$lang['staff_hourly_rate']                           = $lang['task_hourly_rate'];
// &#37; is for % so the system can confuse for the original fields like %s
$lang['remove_tax_name_from_item_table_help'] = 'eq. Item TAX 15&#37; will be shown as 15&#37; without the tax name (Not applied if multiple taxes with the same name and tax percent found for item)';
$lang['back_to_project']                      = 'Επιστροφή στο έργο';
$lang['view_kanban']                          = 'View Kan Ban';
$lang['invoice_is_overdue']                   = 'This invoice is overdue by %s days';

# Version 1.2.9
$lang['time_decimal']                            = 'Time (decimal)';
$lang['time_h']                                  = 'Time (h)';
$lang['proposal_number_prefix']                  = 'Proposal Number Prefix';
$lang['settings_number_padding_prefix']          = 'Number padding zero\'s for prefix formats <br /> <small>eq. If this value is 3 the number will be formatted: 005 or 025</small>';
$lang['this_week_payments']                      = 'This Week Payments';
$lang['last_week_payments']                      = 'Last Week Payments';
$lang['not_published_new_post']                  = 'published new post';
$lang['expense_name']                            = 'Name';
$lang['expense_name_help']                       = 'For personal usage';
$lang['adjustments']                             = 'Adjustments';
$lang['payments_received']                       = 'Payments Received';
$lang['not_lead_activity_created_proposal']      = 'Created new proposal - %s';
$lang['lead_title']                              = 'Θέση';
$lang['lead_address']                            = 'Διεύθυνση';
$lang['lead_city']                               = 'Πόλη';
$lang['lead_state']                              = 'Νομός';
$lang['lead_country']                            = 'Χώρα';
$lang['lead_zip']                                = 'Τ.Κ.';
$lang['lead_is_public_yes']                      = 'Ναι';
$lang['lead_is_public_no']                       = 'Όχι';
$lang['lead_info']                               = 'Πληροφορίες επαφής';
$lang['lead_general_info']                       = 'Γενικές πληροφορίες';
$lang['lead_latest_activity']                    = 'Τελευταία Δραστηριότητα';
$lang['item_description_new_lines_notice']       = 'New lines are not supported for item description. Use the item long description instead.';
$lang['estimates_report']                        = 'Estimates Report';
$lang['confirm']                                 = 'Confirm';
$lang['delete_staff']                            = 'Delete Staff Member';
$lang['delete_staff_info']                       = 'Some data for this staff member needs to be transferred to another user. Please select user where you want to transfer the data.';
$lang['estimate_items']                          = 'Estimate Items';
$lang['no_proposals_found']                      = 'Δεν βρέθηκαν προσφορές';
$lang['no_actions_found']                       = 'Δεν βρέθηκαν κινήσεις';
$lang['no_estimates_found']                      = 'No Estimates Found';
$lang['pipeline_limit_status']                   = 'Pipeline limit per status';
$lang['settings_update']                         = 'System Update';
$lang['purchase_key']                            = 'Purchase Key';
$lang['update_now']                              = 'Update Now';
$lang['update_available']                        = 'An update is available';
$lang['latest_version']                          = 'Latest Version';
$lang['your_version']                            = 'Your Version';
$lang['using_latest_version']                    = 'You are using the latest version';
$lang['mark_as_active']                          = 'Mark as active';
$lang['customer_inactive_message']               = 'This is inactive customer profile and some features may be disabled';
$lang['active_customers']                        = 'Ενεργοί Πελάτες';
$lang['active_suppliers']                        = 'Ενεργοί Προμηθευτές';
$lang['inactive_active_customers']               = 'Ανενεργοί Πελάτες';
$lang['inactive_active_suppliers']               = 'Ανενεργοί Προμηθευτές';
$lang['include_proposal_items_merge_field_help'] = 'Include proposal items with merge field anywhere in proposal content as %s';
$lang['all_data_synced_successfully']            = 'All data synced successfully';
$lang['sync_now']                                = 'Sync Now';
$lang['sync_data']                               = 'Sync Data';
$lang['sync_proposals_up_to_date']               = 'All proposals are up to date, nothing to sync';
$lang['proposal_sync_1_info']                    = 'All proposal data is stored separately for each proposal after creation. Updating the %s info won\'t affect previous created proposals for this %s.';
$lang['proposal_sync_2_info']                    = 'If you recently updated your %s info you can sync all new data to associated proposals. Here is a list of fields you can sync.';

# Version 1.3.0
$lang['expense_include_additional_data_on_convert'] = 'Include additional details to item long description taken from this expense.';

# Version 1.4.0
$lang['calendar_events_limit']                              = 'Calendar Events Limit (Month and Week View)';
$lang['show_page_number_on_pdf']                            = 'Show page number on PDF';
$lang['customer_active_inactive_help']                      = 'Δεν θα εμφανίζεται στις λίστες κατά την δημιουργία νέων εγγραφών';
$lang['item_groups']                                        = 'Ομάδες';
$lang['item_group']                                         = 'Ομάδα';
$lang['item_group_name']                                    = 'Όνομα Ομάδας';
$lang['new_item_group']                                     = 'Νέα Ομάδα';
$lang['show_setup_menu_item_only_on_hover']                 = 'Show setup menu item only when hover with mouse on main sidebar area';
$lang['internal_article']                                   = 'Internal Article';
$lang['expenses_created_from_this_recurring_expense']       = 'Created expenses from this recurring expense';
$lang['convert_to_task']                                    = 'Convert To Task';
$lang['next_invoice_date']                                  = 'Next Invoice Date: %s';
$lang['next_expense_date']                                  = 'Next Expense Date: %s';
$lang['invoice_recurring_from']                             = 'This invoice is created from recurring invoice with number: %s';
$lang['expense_recurring_from']                             = 'This expense is created from the following recurring expense: %s';
$lang['child_invoices']                                     = 'Child Invoices';
$lang['child_expenses']                                     = 'Child Expenses';
$lang['no_announcements']                                   = 'No Announcements';
$lang['unit']                                               = 'Μονάδα';
$lang['permission_view_own']                                = 'View (Own)';
$lang['permission_global']                                  = 'Global';
$lang['permission_customers_based_on_admins']               = 'Based on customer admins';
$lang['permission_payments_based_on_invoices']              = 'Based on invoices VIEW (Own) permission';
$lang['permission_projects_based_on_assignee']              = 'If staff member do not have permission VIEW (Global) will be shown only projects where staff member is added as project member.';
$lang['permission_tasks_based_on_assignee']                 = 'If staff member do not have permission VIEW (Global) will be shown only tasks where staff member is follower,assigned, task is public or in Setup->Settings->Tasks-> Allow all staff to see all tasks related to projects is set to YES when task is linked to project.';
$lang['settings_paymentmethod_default_selected_on_invoice'] = 'Selected by default on invoice';
$lang['paymentmethod_braintree_merchant_id']                = 'Merchant ID';
$lang['paymentmethod_braintree_private_key']                = 'Private Key';
$lang['paymentmethod_braintree_public_key']                 = 'Public Key';
$lang['company_requires_vat_number_field']                  = 'Company requires the usage of the VAT Number field';
$lang['no_company_view_profile']                            = 'Person - View Profile';
$lang['company_is_required']                                = 'Company field is required?';
$lang['estimate_invoiced']                                  = 'Invoiced';
$lang['file_date_uploaded']                                 = 'Ημερομηνία';
$lang['allow_contact_to_delete_files']                      = 'Allow contacts to delete own files uploaded from customers area';
$lang['file']                                               = 'Αρχείο';
$lang['customer_contact_person_only_one_allowed']           = 'Only 1 contact is allowed when the company field is not filled. The system will cast this customer as person';
$lang['web_to_lead']                                        = 'Web to Lead';
$lang['web_to_lead_form']                                   = 'Web to Lead Form';
$lang['new_form']                                           = 'New Form';
$lang['form_name']                                          = 'Form Name';
$lang['cf_option_in_use']                                   = 'An option you removed is in use and cant be removed. The option is auto appended to the existing options.';
$lang['form_builder']                                       = 'Form Builder';
$lang['form_information']                                   = 'Form Information & Setup';
$lang['form_builder_create_form_first']                     = 'Create form first to be able to use the form builder.';
$lang['notify_assigned_user']                               = 'Responsible person';
$lang['form_recaptcha']                                     = 'Use Google Recaptcha';
$lang['form_lang_validation']                               = 'Language';
$lang['form_lang_validation_help']                          = 'For validation messages';
$lang['form_btn_submit_text']                               = 'Submit button text';
$lang['form_success_submit_msg']                            = 'Message to show after form is succcesfully submitted';
$lang['total_submissions']                                  = 'Total Submissions';
$lang['form_integration_code']                              = 'Integration Code';
$lang['not_lead_imported_from_form']                        = 'New Lead Imported from Web to Lead Form - %s';
$lang['not_lead_activity_log_attachment']                   = 'Attachment Imported from form - %s';
$lang['form_integration_code_help']                         = 'Copy & Paste the code anywhere in your site to show the form, additionally you can adjust the width and height px to fit for your website.';
$lang['invoice_not_found']                                  = 'Invoice not found';
$lang['estimate_not_found']                                 = 'Estimate not found';
$lang['expense_not_found']                                  = 'Expense not found';
$lang['proposal_not_found']                                 = 'Η προσφορά δεν βρέθηκε';
$lang['action_not_found']                                 = 'Η κίνηση δεν βρέθηκε';
$lang['new_task_assigned_non_user']                         = 'New task is assigned to you - %s';
$lang['no_child_found']                                     = 'No Child %s Found';
$lang['company_vat_number']                                 = 'VAT Number';
$lang['not_lead_assigned_from_form']                        = 'New lead is assigned to you';
$lang['not_lead_activity_assigned_from_form']               = 'Lead assigned to %s';
$lang['form_allow_duplicate']                               = 'Allow duplicate %s to be inserted into database?';
$lang['track_duplicate_by_field']                           = 'Prevent duplicate on field';
$lang['and_track_duplicate_by_field']                       = '+ field (leave blank to track duplicates only by 1 field)';
$lang['create_the_duplicate_form_data_as_task']             = 'Create duplicate %s data as task and assign to responsible staff member';
$lang['create_the_duplicate_form_data_as_task_help']        = 'Used for further review on the submission and take the necessary action';
$lang['currently_selected']                                 = 'Currently Selected';
$lang['search_ajax_empty']                                  = 'Επιλέξτε και είσάγετε';
$lang['search_ajax_placeholder']                            = 'Αναζήτηση...';
$lang['search_ajax_searching']                              = 'Searching...';
$lang['search_ajax_initialized']                            = 'Εισάγετε κείμενο για αναζήτηση';
$lang['lead_description']                                   = 'Περιγραφή';
$lang['lead_website']                                       = 'Ιστοσελίδα';
$lang['invoice_activity_auto_converted_from_estimate']      = 'Invoice auto created from estimate with number %s';
$lang['hour_of_day_perform_auto_operations']                = 'Hour of day to perform automatic operations';
$lang['hour_of_day_perform_auto_operations_format']         = '24 hours format eq. 9 for 9am or 15 for 3pm.';
$lang['inv_hour_of_day_perform_auto_operations_help']       = 'Used for recurring invoices, overdue notices etc..';
$lang['use_minified_files']                                 = 'Use minified files version for css and js (only system files)';

# Version 1.5.0
$lang['logo_favicon_changed_notice']       = 'Logo or Favicon change detected. If you still see the original CRM logo try to clear your browser cache';
$lang['kb_search_articles']                = 'Search Knowledge Base Articles';
$lang['kb_search']                         = 'Search';
$lang['have_a_question']                   = 'Have a question?';
$lang['card_expiration_year']              = 'Expiration Year';
$lang['card_expiration_month']             = 'Expiration Month';
$lang['client_website']                    = 'Website';
$lang['search_project_members']            = 'Search Project Members...';
$lang['cf_translate_input_link_title']     = 'Title';
$lang['cf_translate_input_link_url']       = 'URL';
$lang['cf_translate_input_link_tip']       = 'Click here to add link';
$lang['task_edit_delete_timesheet_notice'] = 'Timesheet task is %s, you cant %s the timesheet.';
$lang['department_username']               = 'IMAP Username';
$lang['department_username_help']          = 'Only fill this field if your IMAP server use username to login instead email address. Note that you will still need to add email address.';
$lang['total_tickets_deleted']             = 'Total tickets deleted: %s';

# Version 1.5.1
$lang['ticket_linked_to_project']                                = 'This ticket is linked to project: %s';
$lang['only_own_files_contacts']                                 = 'Contacts see only own files uploaded in customer area (files uploaded in customer profile)';
$lang['only_own_files_contacts_help']                            = 'If you share the file manually from customer profile to other contacts they wil be able to see the file.';
$lang['share_file_with']                                         = 'Share File With';
$lang['file_share_visibility_notice']                            = 'This file is not shared with contacts, toggle visibility again to reload';
$lang['share_file_with_show']                                    = 'This file is shared with: %s';
$lang['allow_primary_contact_to_view_edit_billing_and_shipping'] = 'Allow primary contact to view/edit billing & shipping details';
$lang['estimate_due_after']                                      = 'Estimate Due After (days)';

# Version 1.6.0
$lang['my_timesheets']                                   = 'My Timesheets';
$lang['today']                                           = 'Σήμερα';
$lang['open_in_dropbox']                                 = 'Open In Dropbox';
$lang['show_primary_contact']                            = 'Εμφάνιση ονόματος κύριας επαφής σε %s';
$lang['view_members_timesheets']                         = 'View all timesheets';
$lang['priority']                                        = 'Προτεραιότητα';
$lang['fetch_from_google']                               = 'Fetch from google';
$lang['customer_fetch_lat_lng_usage']                    = 'Fill address, city and country before fetching to get best result.';
$lang['g_search_address_not_found']                      = 'The address couldn\'t be found, please try again';
$lang['proposals_report']                                = 'Αναφορά προσφορών';
$lang['actions_report']                                = 'Αναφορά κινήσεων';
$lang['staff_members_open_tickets_to_all_contacts_help'] = 'If staff member don\'t have permission for customers VIEW only will be able to create new tickets from admin area to customer contacts where is assigned as customer admin.';
$lang['staff_members_open_tickets_to_all_contacts']      = 'Allow staff members to open tickets to all contacts?';
$lang['charts_based_report']                             = 'Charts Based Report';
$lang['delete_backups_older_then']                       = 'Auto delete backups older then X days (set 0 to disable)';
$lang['responsible_admin']                               = 'Responsible admin';
$lang['tags']                                            = 'Ετικέτες';
$lang['tag']                                             = 'Ετικέτα';
$lang['customer_map_notice']                             = 'Add longitude and latitude in the customer profile to show the map here';
$lang['default_view']                                    = 'Προεπιλεγμένη Προβολή';
$lang['day']                                             = 'Ημέρα';
$lang['agenda']                                          = 'Agenda';
$lang['timesheet_tags']                                  = 'Timesheet Tags';
$lang['show_more']                                       = 'Περισσότερα';
$lang['show_less']                                       = 'Λιγότερα';
$lang['project_completed_date']                          = 'Completed Date';
$lang['language']                                        = 'Γλώσσα';
$lang['this_week']                                       = 'Τρέχουσα Εβδομάδα';
$lang['last_week']                                       = 'Περασμένη Εβδομάδα';
$lang['this_month']                                      = 'Τρέχον Μήνας';
$lang['last_month']                                      = 'Περασμένος Μήνας';
$lang['no_description_project']                          = 'Καμία Περιγραφή';
$lang['sales_string']                                    = 'Πωλήσεις';
$lang['no_project_members']                              = 'Κανένα μέλος στο έργο';
$lang['search_by_tags']                                  = 'Use # + tagname to search by tags';


# Version 1.7.0
$lang['not_activity_new_reminder_created']        = 'set a new reminder for %s with date %s';
$lang['not_activity_new_task_created']            = 'Created new task - %s';
$lang['recurring_invoice_draft_notice']           = 'This invoice is with status draft, you need to mark this invoice as sent. Recurring invoices with status draft won\'t be recreated by cron job.';
$lang['recurring_recreate_hour_notice']           = '%s will be recreated on specific hour of the day based from the setting located at Setup->Settings-Cron Job';
$lang['invoice_project_include_timesheets_notes'] = 'Include each timesheet note in item description';
$lang['events']                                   = 'Events';
$lang['clear']                                    = 'Clear';
$lang['auto_mark_as_public']                      = 'Auto mark as public';
$lang['time_format']                              = 'Time Format';
$lang['time_format_24']                           = '24 hours';
$lang['time_format_12']                           = '12 hours';
$lang['delete_activity_log_older_then']           = 'Delete system activity log older then X months';
$lang['mark_as_read']                             = 'Mark as Read';
$lang['mark_all_as_read']                         = 'Mark all as read';
$lang['tax_1']                                    = 'Tax 1';
$lang['tax_2']                                    = 'Tax 2';
$lang['total_with_tax']                           = 'Total with tax';
$lang['new_task_auto_assign_current_member']      = 'Auto assign task creator when new task is created';
$lang['new_task_auto_assign_current_member_help'] = 'Not applied if task is linked to project and the creator is not project member';
$lang['copy_project_tasks_status']                = 'Κατάσταση Εργασιών';
$lang['tasks_summary']                            = 'Εργασίες';
$lang['vault']                                    = 'Vault';
$lang['new_vault_entry']                          = 'New Vault Entry';
$lang['server_address']                           = 'Server Address';
$lang['port']                                     = 'Port';
$lang['vault_username']                           = 'Username';
$lang['vault_password']                           = 'Password';
$lang['vault_description']                        = 'Σύντομη Περιγραφή';
$lang['vault_entry']                              = 'Vault Entry';
$lang['no_port_provided']                         = 'Not provided';
$lang['view_password']                            = 'View Password';
$lang['security_reasons_re_enter_password']       = 'For security reasons please enter your password below';
$lang['password_change_fill_notice']              = 'Only fill password field if you want to change the password';
$lang['vault_password_user_not_correct']          = 'Your password is not correct, please try again';
$lang['no_vault_entries']                         = 'Vault entries not found for this customer.';
$lang['vault_entry_created_from']                 = 'This vault entry is created by %s';
$lang['vault_entry_last_update']                  = 'Last updated by %s';
$lang['vault_entry_visible_to_all']               = 'Visible to all staff member who have access to this customer';
$lang['vault_entry_visible_creator']              = 'Visible only to me (administrator are not excluded)';
$lang['vault_entry_visible_administrators']       = 'Visible only to administrators';
$lang['my_reminders']                             = 'My Reminders';
$lang['reminder_related']                         = 'Σχετίζεται με';
$lang['event_notification']                       = 'Notification';
$lang['days']                                     = 'Days';
$lang['reminder_notification_placeholder']        = 'Eq. 30 minutes before';
$lang['event_color']                              = 'Event Color';
$lang['group_by_task']                            = 'Group by Task';
$lang['save']                                     = 'Save';
$lang['disable_languages']                        = 'Disable Languages';

# Version 1.8.0
$lang['not_customer_viewed_invoice']                         = 'An invoice with number %s has been viewed';
$lang['not_customer_viewed_estimate']                        = 'An estimate with number %s has been viewed';
$lang['not_customer_viewed_proposal']                        = 'An proposal with number %s has been viewed';
$lang['display_inline']                                      = 'Display Inline';
$lang['email_header']                                        = 'Predefined Header';
$lang['email_footer']                                        = 'Predefined Footer';
$lang['exclude_proposal_from_client_area_with_draft_status'] = 'Exclude proposals with draft status from customers area';
$lang['pusher_app_key']                                      = 'APP Key';
$lang['pusher_app_secret']                                   = 'APP Secret';
$lang['pusher_app_id']                                       = 'APP ID';
$lang['pusher_cluster_notice']                               = 'Leave blank to use default pusher cluster.';
$lang['pusher_enable_realtime_notifications']                = 'Enable Real Time Notifications';
$lang['task_is_overdue']                                     = 'This task is overdue';
$lang['this_year']                                           = 'Τρέχον Έτος';
$lang['last_year']                                           = 'Προηγούμενο Έτος';
$lang['customer_statement']                                  = 'Λογαριασμός';
$lang['customer_statement_for']                              = 'Κατάσταση Λογαριασμού του πελάτη %s';
$lang['account_summary']                                     = 'Σύνοψη λογαριασμού';
$lang['statement_beginning_balance']                         = 'Αρχικό Υπόλοιπο';
$lang['invoiced_amount']                                     = 'Τιμολογημένο Ποσό';
$lang['amount_paid']                                         = 'Αποπληρωμένο Ποσό';
$lang['statement_from_to']                                   = '%s To %s';
$lang['customer_statement_info']                             = 'Εμφάνισή όλων των Τιμολογίων και των Πληρωμών από %s μέχρι %s';
$lang['balance_due']                                         = 'Υπόλοιπο';
$lang['statement_heading_date']                              = 'Ημερομηνία';
$lang['statement_heading_details']                           = 'Λεπτομέρειες';
$lang['statement_heading_amount']                            = 'Ποσό';
$lang['statement_heading_payments']                          = 'Πληρωμές';
$lang['statement_heading_balance']                           = 'Υπόλοιπο';
$lang['statement_invoice_details']                           = 'Τιμολόγιο %s - due on %s';
$lang['statement_payment_details']                           = 'Πληρωμή (%s) Τιμολογίου %s';
$lang['statement_bill_to']                                   = 'Προς';
$lang['send_to_email']                                       = 'Απόστολή με Email';
$lang['statement_sent_to_client_success']                    = 'The statement is sent successfully to the client';
$lang['statement_sent_to_client_fail']                       = 'Problem while sending the statement';
$lang['view_account_statement']                              = 'View Account Statement';
$lang['text_not_recommended_for_servers_limited_resources']  = 'Not recommended if the server have limited resources. Eq shared hosting';
$lang['tasks_bull_actions_assign_notice']                    = 'If the task is linked to project and the staff member you are assigning the task to is not project member this staff will be auto added as member.';
$lang['company_information']                                 = 'Company Information';
$lang['ticket_form']                                         = 'Ticket Form';
$lang['ticket_form_subject']                                 = 'Subject';
$lang['ticket_form_name']                                    = 'Your name';
$lang['ticket_form_email']                                   = 'Email Address';
$lang['ticket_form_department']                              = 'Department';
$lang['ticket_form_message']                                 = 'Μήνυμα';
$lang['ticket_form_priority']                                = 'Προτεραιότητα';
$lang['ticket_form_service']                                 = 'Service';
$lang['ticket_form_submit']                                  = 'Υποβολή';
$lang['ticket_form_attachments']                             = 'Συνημμένα';
$lang['success_submit_msg']                                  = 'Thank you for contacting us. We will get back to you shortly.';
$lang['vault_entry_share_on_projects']                       = 'Share this vault entry in projects with project members';
$lang['project_shared_vault_entry_login_details']            = 'View Customer Site Login Details';
$lang['iso_code']                                            = 'ISO Code';
$lang['estimates_not_invoiced']                              = 'Not Invoiced';
$lang['show_on_ticket_form']                                 = 'Show on ticket form';
$lang['cancel_upload']                                       = 'Cancel Upload';
$lang['show_table_export_button']                            = 'Show table export button';
$lang['show_table_export_all']                               = 'To all staff members';
$lang['show_table_export_admins']                            = 'Only to administrators';
$lang['show_table_export_hide']                              = 'Hide export button for all staff members';
$lang['task_created_by']                                     = 'Created by %s';
$lang['validation_extension_not_allowed']                    = 'File extension not allowed';
$lang['allow_staff_view_proposals_assigned']                 = 'Allow staff members to view proposals where they are assigned to';
$lang['task_users_working_on_tasks_multiple']                = 'Currently %s are working on this task';
$lang['task_users_working_on_tasks_single']                  = 'Currently %s is working on this task';

# Version 1.9.0
$lang['estimated_hours']                               = 'Εκτιμώμενες ώρες';
$lang['two_factor_auth_failed_to_send_code']           = 'Failed to send two step authentication code to email, SMTP settings may not be configured properly';
$lang['two_factor_auth_code_sent_successfully']        = 'An email has been sent to %s with verification code to verify your login';
$lang['enable_two_factor_authentication']              = 'Enable Two Factor Authentication';
$lang['two_factor_authentication_info']                = 'Two factor authentication is provided by email, before enable two factor authentication make sure that your SMTP settings are properly configured and the system is able to send an email. Unique authentication key will be sent to email upon login.';
$lang['timesheets_overview_all_members_notice_admins'] = 'Timesheets overview for all staff members is only available for administrators.';
$lang['two_factor_authentication']                     = 'Two Factor Authentication';
$lang['two_factor_authentication_code']                = 'Code';
$lang['admin_two_factor_auth_heading']                 = 'Authentication Code';
$lang['two_factor_code_not_valid']                     = 'Authentication code not valid';
$lang['back_to_login']                                 = 'Go back to login';
$lang['enter_activity']                                = 'Enter Activity';
$lang['attach_files']                                  = 'Επισύναψη αρχείων';
$lang['no_tags_used']                                  = 'No tags used by the system';
$lang['exclude_completed_tasks']                       = 'Exclude Completed Tasks';
$lang['modal_width_class']                             = 'Modal Width Class';
$lang['contract_copy']                                 = 'Copy';
$lang['contract_copied_successfully']                  = 'Contract copied successfully';
$lang['contract_copied_fail']                          = 'Failed to copy contract';
$lang['project_marked_as_finished_to_contacts']        = 'Send <b>Project Marked as Finished</b> email to customer contacts';
$lang['only_admins']                                   = 'Only administrators';
$lang['new_notification']                              = 'New Notification!';
$lang['enable_desktop_notifications']                  = 'Enable Desktop Notifications';
$lang['save_and_send']                                 = 'Αποθήκευση και αποστολή';
$lang['private']                                       = 'Private';
$lang['task_created_at']                               = 'Δημιουργήθηκε %s';
$lang['hide_notified_reminders_from_calendar']         = 'Hide notified reminders from calendar';
$lang['last_active']                                   = 'Last Active';
$lang['open_ticket']                                   = 'Open Ticket';
$lang['task_add_description']                          = 'Προσθήκη Περιγραφής';
$lang['project_setting_create_tasks']                  = 'create tasks';
$lang['project_setting_edit_tasks']                    = 'edit tasks (only tasks created from contact)';

# Version 1.9.2
$lang['items_report']                            = 'Αναφορά Ειδών';
$lang['reports_item']                            = 'Είδος';
$lang['quantity_sold']                           = 'Ποσότητα που πωλήθηκε';
$lang['total_amount']                            = 'Συνολικό ποσό';
$lang['avg_price']                               = 'Μέση Τιμή';
$lang['item_report_paid_invoices_notice']        = 'Items report is generated only from paid invoices before discounts and taxes.';
$lang['overview']                                = 'Overview';
$lang['timer_started_change_status_in_progress'] = 'Change task status to In Progress on timer started (valid only if task status is Not Started)';
$lang['company_info_format']                     = 'Company Information Format (PDF and HTML)';
$lang['customer_info_format']                    = 'Customer Information Format (PDF and HTML)';
$lang['custom_field_info_format_embed_info']     = 'Custom fields for %s can be easily embedded into PDF and HTML documents by adding the merge fields into the page format at the following page: %s';
$lang['transfer_lead_notes_to_customer']         = 'Transfer lead notes to customer profile';
$lang['authorized_signature_text']               = 'Authorized Signature';
$lang['show_pdf_signature_invoice']              = 'Show PDF Signature on Invoice';
$lang['show_pdf_signature_estimate']             = 'Show PDF Signature on Estimate';
$lang['signature']                               = 'Signature';
$lang['signature_image']                         = 'Signature Image';
$lang['insert_checklist_templates']              = 'Insert Checklist Templates';
$lang['save_as_template']                        = 'Save as Template';
$lang['scroll_responsive_tables_help']           = 'Tables with large amount of data will have horizontal scroll instead rows wrapped in + icon.';
$lang['scroll_responsive_tables']                = 'Activate Scroll Responsive Tables';
$lang['invoice_item_add_edit_rate_currency']     = 'Rate - %s';
$lang['total_files_deleted']                     = 'Total files deleted: %s';
$lang['invalid_transaction']                     = 'Invalid Transaction. Please try again.';
$lang['payment_gateway_payu_money_key']          = 'PayU Money Key';
$lang['payment_gateway_payu_money_salt']         = 'PayU Money Salt';
$lang['settings_paymentmethod_description']      = 'Gateway Dashbord Payment Description';

# Version 1.9.3
$lang['default_ticket_reply_status']          = 'Default status selected when replying to ticket';
$lang['ticket_add_response_and_back_to_list'] = 'Return to ticket list after response is submitted';

# Version 1.9.4
$lang['default_task_status']                               = 'Default status when new task is created';
$lang['custom_field_pdf_html_help']                        = 'Make sure you check ' . $lang['custom_field_show_on_client_portal'] . ' field if you want the custom fields to be visible to customers area and when customer download PDF or receive PDF via email.';
$lang['auto']                                              = 'Auto';
$lang['email_queue']                                       = 'Email Queue';
$lang['email_queue_enabled']                               = 'Enable Email Queue';
$lang['email_queue_skip_attachments']                      = 'Do not add emails with attachments in the queue?';
$lang['disable']                                           = 'Απενεργοποίηση';
$lang['enable']                                            = 'Ενεργοποίηση';
$lang['auto_dismiss_desktop_notifications_after']          = 'Auto Dismiss Desktop Notifications After X Seconds (0 to disable)';
$lang['proposal_info_format']                              = 'Proposal Info Format (PDF and HTML)';
$lang['hide_tasks_on_main_tasks_table']                    = 'Hide project tasks on main tasks table (admin area)';
$lang['ticket_replies_order']                              = 'Ticket Replies Order';
$lang['ticket_replies_order_notice']                       = 'The initial ticket message will be always shown as first.';
$lang['invoice_cancelled_email_disabled']                  = 'Invoice is cancelled. Unmark as cancelled to enable email to client';
$lang['email_notifications']                               = 'Email Notifications';
$lang['invoice_activity_record_payment_email_to_customer'] = 'Payment recorded, email sent to: %s';
$lang['exclude_inactive']                                  = 'Εξαίρεση Ανενεργών';
$lang['disable_all']                                       = 'Απενεργοποίηση Όλων';
$lang['enable_all']                                        = 'Enable All';
$lang['reccuring_invoice_option_gen_and_send']             = 'Generate and autosend the renewed invoice to the customer';
$lang['reccuring_invoice_option_gen_unpaid']               = 'Generate a Unpaid Invoice';
$lang['reccuring_invoice_option_gen_draft']                = 'Generate a Draft Invoice';
$lang['event_created_by']                                  = 'This event is created by %s';

# Version 1.9.5
$lang['customers_assigned_to_me']              = 'Πελάτες που μου έχουν ανατεθεί';
$lang['bcc_all_emails']                        = 'BCC All Emails To';
$lang['confirmation_of_identity']              = 'Confirmation Of Identity';
$lang['accept_identity_confirmation']          = 'Require identity confirmation on accept';
$lang['accepted_identity_info']                = 'This %s is accepted by %s on %s from IP address %s';
$lang['clear_this_information']                = 'Clear This Information';
$lang['new_task_auto_follower_current_member'] = 'Auto add task creator as task follower when new task is created';
$lang['expenses_report_net']                   = 'Net Amount (Subtotal)';
$lang['expense_field_billable_help']           = 'If billable, %s can be added to invoice long description.';
$lang['task_biillable_checked_on_creation']    = 'Billable option is by default checked when new task is created?';
$lang['pause_overdue_reminders']               = 'Pause Overdue Reminders';
$lang['resume_overdue_reminders']              = 'Resume Overdue Reminders';
# Credit Notes
$lang['credit_notes']                                            = 'Credit Notes';
$lang['credit_note']                                             = 'Credit Note';
$lang['credit_note_lowercase']                                   = 'credit note';
$lang['credit_note_not_found']                                   = 'Credit note not found';
$lang['credit_note_date']                                        = 'Credit Note Date';
$lang['credit_date']                                             = 'Date';
$lang['settings_sales_next_credit_note_number']                  = 'Next Credit Note Number';
$lang['credit_note_number_prefix']                               = 'Credit Note Number Prefix';
$lang['credit_note_number']                                      = 'Credit Note #';
$lang['credit_note_number_exists']                               = 'Credit note number already exists';
$lang['show_shipping_on_credit_note']                            = 'Show shipping details on credit note';
$lang['credit_note_number_decrement_on_delete']                  = 'Decrement credit note number on delete.';
$lang['credit_note_number_decrement_on_delete_help']             = 'Number will be decremented only if is last credit note created.';
$lang['credit_note_status']                                      = 'Κατάσταση';
$lang['credit_note_status_open']                                 = 'Open';
$lang['credit_note_status_closed']                               = 'Closed';
$lang['credit_note_status_void']                                 = 'Void';
$lang['credit_note_mark_as_open']                                = 'Mark as Open';
$lang['new_credit_note']                                         = 'New Credit Note';
$lang['credit_note_amount']                                      = 'Amount';
$lang['credit_note_remaining_credits']                           = 'Remaining Amount';
$lang['credit_note_client_note']                                 = 'Note';
$lang['invoices_credited']                                       = 'Invoices Credited';
$lang['apply_credits']                                           = 'Apply Credits';
$lang['x_credits_available']                                     = '%s credits available.';
$lang['credit_amount']                                           = 'Credit Amount';
$lang['credits_available']                                       = 'Credits Available';
$lang['amount_to_credit']                                        = 'Amount to Credit';
$lang['invoice_credits_applied']                                 = 'Credits successfully applied to invoice';
$lang['applied_credits']                                         = 'Applied Credits';
$lang['credit_amount_bigger_then_invoice_balance']               = 'Total credits amount is bigger then invoice balance due';
$lang['credit_amount_bigger_then_credit_note_remaining_credits'] = 'Total credits amount is bigger then remaining credits';
$lang['credited_invoices_not_found']                             = 'Credited Invoices Not Found';
$lang['credit_invoice_number']                                   = 'Invoice Number';
$lang['credits_used']                                            = 'Credits Used';
$lang['credits_remaining']                                       = 'Credits Remaining';
$lang['amount_credited']                                         = 'Amount Credited';
$lang['credits_applied_cant_delete_status_closed']               = 'This credit note is with status Closed, you need first to delete the credits in order to delete the credit note.';
$lang['credits_applied_cant_delete_credit_note']                 = 'This credit note has applied credits, you need first to delete the credits in order to delete the credit note.';
$lang['credit_note_pdf_heading']                                 = 'CREDIT NOTE';
$lang['show_status_on_pdf']                                      = 'Show %s status on PDF documents';
$lang['show_pdf_signature_credit_note']                          = 'Show PDF Signature on Credit Note';
$lang['calendar_credit_note_reminder']                           = 'Credit Note Reminder';
$lang['show_credit_note_reminders_on_calendar']                  = 'Credit Note Reminders';
$lang['reminders']                                               = 'Υπενθυμίσεις';
$lang['invoice_activity_applied_credits']                        = 'applied credits of %s from %s';
$lang['create_credit_note']                                      = 'Create Credit Note';
$lang['confirm_invoice_credits_from_credit_note']                = 'When creating credit note from non paid invoice, the credit note amount will get applied for this invoice. Are you sure that you want to create the credit note?';
$lang['credit_invoice_date']                                     = 'Invoice Date';
$lang['apply_to_invoice']                                        = 'Apply to invoice';
$lang['apply_credits_from']                                      = 'Apply Credits From %s';
$lang['credits_successfully_applied_to_invoices']                = 'Invoices credits successfully applied';
$lang['credit_note_send_to_client_modal_heading']                = 'Send Credit Note To Customer';
$lang['credit_note_sent_to_client_success']                      = 'Credit note is sent successfully to the client';
$lang['credit_note_sent_to_client_fail']                         = 'Problem while sending credit note to email';
$lang['credit_note_no_invoices_available']                       = 'There are no available invoices for this customer.';
$lang['show_total_paid_on_invoice']                              = 'Show Total Paid On Invoice';
$lang['show_credits_applied_on_invoice']                         = 'Show Credits Applied On Invoice';
$lang['show_amount_due_on_invoice']                              = 'Show Amount Due On Invoice';
$lang['customer_profile_update_credit_notes']                    = 'Update the shipping/billing info on all previous credit notes (Closed credit notes not affected)';
$lang['zip_credit_notes']                                        = 'Zip Credit Notes';
$lang['statement_credit_note_details']                           = 'Credit Note %s';
$lang['statement_credits_applied_details']                       = 'Credits Applied from Credit Note %s - %s for payment of %s';
$lang['credit_note_files']                                       = 'Credit Note Files';
$lang['credit_notes_report']                                     = 'Credit Notes Report';

$lang['credit_note_set_reminder_title']     = 'Set Credit Note Reminder';
$lang['credit_note_add_edit_client_note']   = $lang['invoice_add_edit_client_note'];
$lang['credit_note_bill_to']                = $lang['invoice_bill_to'];
$lang['credit_note_prefix']                 = $lang['settings_sales_invoice_prefix'];
$lang['credit_note_admin_note']             = $lang['invoice_add_edit_admin_note'];
$lang['credit_note_total']                  = $lang['invoice_total'];
$lang['credit_note_adjustment']             = $lang['invoice_adjustment'];
$lang['credit_note_discount']               = $lang['invoice_discount'];
$lang['credit_note_subtotal']               = $lang['invoice_subtotal'];
$lang['credit_note_table_quantity_heading'] = $lang['invoice_table_quantity_heading'];
$lang['credit_note_table_hours_heading']    = $lang['invoice_table_hours_heading'];
$lang['credit_note_table_item_heading']     = $lang['invoice_table_item_heading'];
$lang['credit_note_table_item_description'] = $lang['invoice_table_item_description'];
$lang['credit_note_table_rate_heading']     = $lang['invoice_table_rate_heading'];
$lang['credit_note_table_tax_heading']      = $lang['invoice_table_tax_heading'];
$lang['credit_note_table_amount_heading']   = $lang['invoice_table_amount_heading'];
$lang['credit_notes_list_all']              = $lang['invoices_list_all'];

# Version 1.9.7
$lang['ticket_assigned']                          = 'Ανατέθηκε';
$lang['dashboard_options']                        = 'Dashboard Options';
$lang['reset_dashboard']                          = 'Reset Dashboard';
$lang['widgets']                                  = 'Widgets';
$lang['s_chart']                                  = '%s Chart';
$lang['quick_stats']                              = 'Quick Statistics';
$lang['user_widget']                              = 'User Widget';
$lang['widgets_visibility_help_text']             = 'Widgets that are shown only if they have enough data do not have options to be hidden or shown.';
$lang['show_project_on_estimate']                 = 'Show Project Name On Estimate';
$lang['show_project_on_invoice']                  = 'Show Project Name On Invoice';
$lang['show_project_on_credit_note']              = 'Show Project Name On Credit Note';
$lang['visible_tabs']                             = 'Καρτέλες';
$lang['all']                                      = 'Όλα';
$lang['view_widgetable_area']                     = 'View Widgetable Area';
$lang['hide_widgetable_area']                     = 'Hide Widgetable Area';
$lang['no_items_warning']                         = 'Enter at least one item.';
$lang['item_forgotten_in_preview']                = 'Have you forgotten to add this item?';
$lang['not_task_status_changed']                  = '%s - task status changed to %s';
$lang['not_project_activity_task_status_changed'] = 'Task Κατάσταση Changed';
$lang['reset']                                    = 'Reset';
$lang['save_message_as_predefined_reply']         = 'Save Message as Predefined Reply';
$lang['inline_create_option']                     = 'Allow non-admin staff members to create %s in %s create/edit area?';
$lang['inline_create']                            = 'Inline Create';
$lang['inline_create_option_predefined_replies']  = 'Allow non-admin staff members to save predefined replies from ticket message';
$lang['reminders_view_none_admin']                = 'Showing your reminders and reminders created by you.';
$lang['show_tabs_and_options']                    = 'Show Tabs & Options';
$lang['no_milestones_found']                      = 'This project has no milestones';
$lang['lead_is_contact_create_task']              = 'Create task if email sender is already customer and assign to responsible staff member.';
$lang['existing_customer']                        = 'Existing Customer';
$lang['use_company_name_instead']                 = 'Χρησιμοποιήστε το όνομα της εταιρίας';
$lang['customer_delete_transactions_warning']     = 'This customer has transactions, %s, you must delete the transactions or move to another customer in order to perform this action.';

# Version 1.9.8
$lang['sending_email_contact_permissions_warning'] = 'Failed to auto select customer contacts. Make sure that the customer has active contacts and associated contacts with email notifications for %s enabled.';
$lang['help_leads_create_permission']              = 'All staff can create leads, except members marked as not staff members';
$lang['help_leads_edit_permission']                = 'Everyone who has access to specific lead can edit most of the lead information';
$lang['triggers']                                  = 'Triggers';
$lang['notice_only_one_active_sms_gateway']        = 'Only 1 active SMS gateway is allowed';
$lang['sms_trigger_disable_tip']                   = 'Leave contents blank to disable specific trigger.';
$lang['tables']                                    = 'Tables';
$lang['only_project_tasks']                        = 'Only project related tasks';
$lang['download_all']                              = 'Download All';
$lang['settings_sales_credit_note_number_format']  = 'Credit Note Number Format';
$lang['sms_reminder_sent_to']                      = 'SMS reminder sent to %s';
$lang['ideal_customer_statement_descriptor']       = 'Statement Descriptor (shown in customer bank statement)';
$lang['payment_received_awaiting_confirmation']    = 'Your payment was received and is awaiting confirmation.';
$lang['discount_fixed_amount']                     = 'Fixed Amount';
$lang['timesheet_duration_instead']                = 'Enter time duration instead';
$lang['timesheet_date_instead']                    = 'Set start and end time instead';
$lang['allow_non_admin_members_to_import_leads']   = 'Allow non-admin staff members to import leads';
$lang['project_hide_tasks_settings_info']          = 'Tasks are excluded from the main tasks table for this project, you can view the project tasks only in this area.';

# Version 1.9.9
$lang['ticket_create_no_contact']            = 'Ticket without contact';
$lang['ticket_create_to_contact']            = 'Ticket to contact';
$lang['showing_billable_tasks_from_project'] = 'Showing billable tasks from project';
$lang['no_billable_tasks_found']             = 'Billable tasks not found';
$lang['help_leads_permission_view']          = 'If this permission is not checked, a staff member will be only able to view leads to where is assigned, leads created by the staff member and leads that are marked as public';

# Version 2.0.0

$lang['customers']                                              = $lang['clients'];
$lang['knowledge_base']                                         = $lang['kb_string'];
$lang['staff']                                                  = $lang['als_staff'];
$lang['checklist_templates']                                    = 'Task Checklist Templates';
$lang['emails_tracking']                                        = 'Emails Tracking';
$lang['no_tracked_emails_sent']                                 = 'No tracked emails sent';
$lang['tracked_emails_sent']                                    = 'Tracked Emails Sent';
$lang['tracked_email_date']                                     = 'Date';
$lang['tracked_email_subject']                                  = 'Subject';
$lang['tracked_email_to']                                       = 'To';
$lang['tracked_email_opened']                                   = 'Opened';
$lang['tracked_email_not_opened']                               = 'Not Opened';
$lang['not_viewed_yet']                                         = 'This %s is not viewed yet by the customer';
$lang['undo']                                                   = 'Undo';
$lang['sign_document_validation']                               = 'Please sign the document.';
$lang['document_customer_signature_text']                       = 'Signature (Customer)';
$lang['accept_identity_confirmation_and_signature_sign']        = 'Require digital signature and identity confirmation on accept';
$lang['legal_bound_text']                                       = 'Legal Bound Text';
$lang['e_signature_sign']                                       = 'Sign';
$lang['is_signed']                                              = 'Signed';
$lang['is_not_signed']                                          = 'Not Signed';
$lang['download']                                               = 'Αποθήκευση';
$lang['view_pdf_in_new_window']                                 = 'Προβολή PDF σε νέο Tab';
$lang['show_pdf_signature_contract']                            = 'Show PDF Signature on Contract';
$lang['document_signed_successfully']                           = 'You have successfully signed this document';
$lang['document_signed_info']                                   = 'This document is signed by %s on %s from IP address %s';
$lang['keep_signature']                                         = 'Keep Customer Signature';
$lang['view_contract']                                          = 'View Contract';
$lang['summary']                                                = 'Σύνοψη';
$lang['discussion']                                             = 'Discussion';
$lang['general_information']                                    = 'Γενικές Πληροφορίες';
$lang['proposal_information']                                   = 'Στοιχεία Προσφοράς';
$lang['action_information']                                   = 'Στοιχεία Κίνησης';
$lang['contract_comments']                                      = 'Comments';
$lang['not_contract_comment_from_client']                       = 'New comment from customer on contract %s ...';
$lang['contract_files']                                         = 'Contract Files';
$lang['date_signed']                                            = 'Date Signed';
$lang['clear_signature']                                        = 'Clear Signature';
$lang['recurring_has_ended']                                    = 'This recurring %s has ended.';
$lang['cycles_remaining']                                       = 'Cycles Remaining';
$lang['cycles_infinity']                                        = 'Infinity';
$lang['recurring_total_cycles']                                 = 'Total Cycles';
$lang['cycles_passed']                                          = 'Passed %s';
$lang['api_key_not_set_error_message']                          = 'API key not configured, click on the following link to configure API key: %s';
$lang['subscription']                                           = 'Subscription';
$lang['subscription_lowercase']                                 = 'subscription';
$lang['subscriptions']                                          = 'Subscriptions';
$lang['tax_is_used_in_subscriptions_warning']                   = 'You can\'t update this tax because is used by subscriptions.';
$lang['credit_card']                                            = 'Credit Card';
$lang['update_credit_card']                                     = 'Update Credit Card';
$lang['credit_card_update_info']                                = 'Want to update the credit card that we have on file? Provide the new details here. Your card information will never directly touch our server.';
$lang['update_card_details']                                    = 'Update Card Details';
$lang['update_card_btn']                                        = 'Update Card';
$lang['subscription_name']                                      = 'Subscription Name';
$lang['subscriptions_description']                              = 'Περιγραφή';
$lang['subscribe']                                              = 'Subscribe';
$lang['subscription_date']                                      = 'Date';
$lang['first_billing_date']                                     = 'First Billing Date';
$lang['allow_primary_contact_to_update_credit_card']            = 'Allow primary contact to update stored credit card token?';
$lang['show_subscriptions_in_customers_area']                   = 'Show subscriptions in customers area?';
$lang['show_subscriptions_in_customers_area_help']              = 'This option is valid only for the customer primary contact.';
$lang['subscription_sent_to_email_success']                     = 'Subscription sent to email successfully';
$lang['subscription_sent_to_email_fail']                        = 'Failed to sent subscription to email';
$lang['new_subscription']                                       = 'New Subscription';
$lang['subscription_status']                                    = 'Κατάσταση';
$lang['next_billing_cycle']                                     = 'Next Billing Cycle';
$lang['subscription_not_subscribed']                            = 'Not Subscribed';
$lang['send_subscription']                                      = 'Send Subscription';
$lang['subscription_will_send_to_primary_contact']              = 'The subscription will be sent to the primary contact.';
$lang['subscription_resumed']                                   = 'Subscription is set to active successfully';
$lang['subscription_canceled']                                  = 'Subscription Cancelled Successfully';
$lang['no_credit_card_found']                                   = 'No Credit Card Found';
$lang['cancel_immediately']                                     = 'Cancel Immediately';
$lang['cancel_at_end_of_billing_period']                        = 'Cancel At The End Of Billing Period';
$lang['view_subscription']                                      = 'View Subscription';
$lang['subscription_future']                                    = 'Future';
$lang['subscription_active']                                    = 'Active';
$lang['subscription_past_due']                                  = 'Past Due';
$lang['subscription_canceled']                                  = 'Canceled';
$lang['subscription_unpaid']                                    = 'Unpaid';
$lang['billing_plan']                                           = 'Billing Plan';
$lang['upcoming_invoice']                                       = 'Upcoming Invoice';
$lang['resume_now']                                             = 'Resume Now';
$lang['subscription_not_yet_subscribed']                        = 'Customer is not yet subscribed to this subscription.';
$lang['subscription_is_canceled_no_resume']                     = 'This subscription is canceled and cannot be resumed.';
$lang['subscription_will_be_canceled_at_end_of_billing_period'] = 'This subscription will be canceled at the end of billing period.';
$lang['customer_successfully_subscribed_to_subscription']       = 'Thank you for subscribing to %s';
$lang['date_subscribed']                                        = 'Date Subscribed';
$lang['reports']                                                = 'Reports';
$lang['subscriptions_summary']                                  = 'Subscriptions Summary';
$lang['calendar_only_assigned_tasks']                           = 'Show only tasks assigned to the logged in staff member';
$lang['invoice_activity_subscription_payment_succeeded']        = 'Subscription Payment Succeeded, email sent to: %s';
$lang['mail_engine']                                            = 'Mail Engine';
$lang['settings_require_client_logged_in_to_view_contract']     = 'Require client to be logged in to view contract';
$lang['privacy_policy']                                         = 'Privacy Policy';
$lang['gdpr_terms_agree']                                       = 'I agree to the <a href="%s" target="_blank">Terms & Conditions</a>';
$lang['terms_and_conditions_validation']                        = 'You must accept the Terms & Conditions in order to continue.';
$lang['gdpr']                                                   = 'General Data Protection Regulation (GDPR)';
$lang['data_removal_request_sent']                              = 'Data removal request successfully sent';
$lang['gdpr_consents']                                          = 'Consents';
$lang['gdpr_consent']                                           = 'Consent';
$lang['gdpr_consent_purpose']                                   = 'Purpose';
$lang['gdpr_consent_opt_in']                                    = 'Opt In';
$lang['gdpr_consent_opt_out']                                   = 'Opt Out';
$lang['gdpr_consent_agree']                                     = 'I agree';
$lang['gdpr_consent_disagree']                                  = 'I disagree';
$lang['view_consent']                                           = 'View Consent';
$lang['transfer_consent']                                       = 'Transfer Consent';
$lang['view_public_form']                                       = 'View Public Form';
$lang['update_consent']                                         = 'Update Consent';
$lang['update_consent']                                         = 'Update Consent';
$lang['consent_last_updated']                                   = 'Last Updated: %s';
$lang['showing_search_result']                                  = 'Showing search results for: %s';
$lang['per_page']                                               = 'Per Page';
$lang['allow_staff_view_invoices_assigned']                     = 'Allow staff members to view invoices where they are assigned to';
$lang['allow_staff_view_estimates_assigned']                    = 'Allow staff members to view estimates where they are assigned to';
$lang['gdpr_right_to_be_informed']                              = 'Right to be informed';
$lang['gdpr_right_of_access']                                   = 'Right of access';
$lang['gdpr_right_to_data_portability']                         = 'Right to data portability';
$lang['gdpr_right_to_erasure']                                  = 'Right to erasure';
$lang['edit_my_information']                                    = 'Edit my information';
$lang['export_my_data']                                         = 'Export my data';
$lang['request_data_removal']                                   = 'Request data removal';
$lang['explanation_for_data_removal']                           = 'Explanation for data removal';
$lang['briefly_describe_why_remove_data']                       = 'Briefly describe why you want to remove the data';
$lang['date_published']                                         = 'Date Published';
$lang['view']                                                   = 'Προβολή';
$lang['customer_is_subscribed_to_subscription_info']            = 'The customer is subscribed to this subscription';
$lang['save_last_order_for_tables']                             = 'Save last order for tables';
$lang['date_created']                                           = 'Ημερομηνία Δημιουργίας';

# Version 2.0.1
$lang['company_logo_dark']                                      = 'Company Logo Dark';
$lang['customers_register_require_confirmation']                = 'Require registration confirmation from administrator after customer register';
$lang['customer_requires_registration_confirmation']            = 'Requires Registration Confirmation';
$lang['confirm_registration']                                   = 'Confirm Registration';
$lang['customer_registration_successfully_confirmed']           = 'Customer registration successfully confirmed';
$lang['customer_register_account_confirmation_approval_notice'] = 'Thank you for registering, your account is pending approval and will be confirmed soon.';
$lang['after_subscription_payment_succeeded']                   = 'After subscription payment is succeeded';
$lang['subscription_option_send_invoice']                       = 'Send Invoice';
$lang['subscription_option_send_payment_receipt']               = 'Send Payment Receipt';
$lang['subscription_option_send_payment_receipt_and_invoice']   = 'Send Invoice and Payment Receipt';
$lang['subscription_option_do_nothing']                         = 'Do Nothing';
$lang['gdpr_not_enabled']                                       = 'GDPR not enabled';
$lang['enable_gdpr']                                            = 'Enable GDPR';
$lang['gdpr_right_to_rectification']                            = 'Right to rectification';
$lang['test_sms_config']                                        = 'Test SMS Config';
$lang['test_sms_message']                                       = 'Test Message';
$lang['send_test_sms']                                          = 'Send Test SMS';
$lang['gdpr_short']                                             = 'GDPR';
$lang['allow_non_admin_staff_to_delete_ticket_attachments']     = 'Allow non-admin staff members to delete ticket attachments';

# Version 2.1.0
$lang['contract_number']                                = 'Contract Number';
$lang['project_changing_status_recurring_tasks_notice'] = 'You are changing the status to {0}, all recurring tasks will be cancelled';
$lang['not_contract_signed']                            = 'Contract with subject %s has been signed by the customer';
$lang['the_number_sign']                                = '#';
$lang['not_new_ticket_reply']                           = 'Customer replied to ticket - %s';
$lang['receive_notification_on_new_ticket_replies']     = 'Receive notification when customer reply to a ticket';
$lang['receive_notification_on_new_ticket_reply_help']  = 'All staff members which belong to the ticket department will receive notification when customer reply to a ticket';
$lang['payment_gateway_enable_paypal']                  = 'Enable PayPal Payments';
$lang['project_member']                                 = 'Project Member';
$lang['contract_notes']                                 = 'Notes';
$lang['contract_add_note']                              = 'Add Note';
