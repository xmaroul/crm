<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Check if action hash is equal
 * @param  mixed $id   action id
 * @param  string $hash action hash
 * @return void
 */
function check_action_restrictions($id, $hash)
{
    $CI = & get_instance();
    $CI->load->model('actions_model');
    if (!$hash || !$id) {
        show_404();
    }
    $action = $CI->actions_model->get($id);
    if (!$action || ($action->hash != $hash)) {
        show_404();
    }
}

/**
 * Check if action email template for expiry reminders is enabled
 * @return boolean
 */
function is_actions_email_expiry_reminder_enabled()
{
    return total_rows('tblemailtemplates', ['slug' => 'action-expiry-reminder', 'active' => 1]) > 0;
}

/**
 * Check if there are sources for sending action expiry reminders
 * Will be either email or SMS
 * @return boolean
 */
function is_actions_expiry_reminders_enabled()
{
    return is_actions_email_expiry_reminder_enabled() || is_sms_trigger_active(SMS_TRIGGER_ACTION_EXP_REMINDER);
}

/**
 * Return action status color class based on twitter bootstrap
 * @param  mixed  $id
 * @param  boolean $replace_default_by_muted
 * @return string
 */
function action_status_color_class($id, $replace_default_by_muted = false)
{
    if ($id == 1) {
        $class = 'default';
    } elseif ($id == 2) {
        $class = 'danger';
    } elseif ($id == 3) {
        $class = 'success';
    } elseif ($id == 4 || $id == 5) {
        // status sent and revised
        $class = 'info';
    } elseif ($id == 6) {
        $class = 'default';
    }
    if ($class == 'default') {
        if ($replace_default_by_muted == true) {
            $class = 'muted';
        }
    }

    return $class;
}
/**
 * Format action status with label or not
 * @param  mixed  $status  action status id
 * @param  string  $classes additional label classes
 * @param  boolean $label   to include the label or return just translated text
 * @return string
 */
function format_action_status($status, $classes = '', $label = true)
{
    $id = $status;
    if ($status == 1) {
        $status      = _l('action_status_waiting');
        $label_class = 'default';
    } elseif ($status == 2) {
        $status      = _l('action_status_to_produce');
        $label_class = 'danger';
    } elseif ($status == 3) {
        $status      = _l('action_status_completed');
        $label_class = 'success';
    } elseif ($status == 4) {
        $status      = _l('action_status_to_delivery');
        $label_class = 'info';
//    } elseif ($status == 5) {
//        $status      = _l('action_status_revised');
//        $label_class = 'info';
//    } elseif ($status == 6) {
//        $status      = _l('action_status_draft');
//        $label_class = 'default';
    }

    if ($label == true) {
        return '<span class="label label-' . $label_class . ' ' . $classes . ' s-status action-status-' . $id . '">' . $status . '</span>';
    }

    return $status;
}

/**
 * Function that format action number based on the prefix option and the action id
 * @param  mixed $id action id
 * @return string
 */
function format_action_number($id)
{
    return get_option('action_number_prefix') . str_pad($id, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT);
}


/**
 * Function that return action item taxes based on passed item id
 * @param  mixed $itemid
 * @return array
 */
function get_action_item_taxes($itemid)
{
    $CI = & get_instance();
    $CI->db->where('itemid', $itemid);
    $CI->db->where('rel_type', 'action');
    $taxes = $CI->db->get('tblitemstax')->result_array();
    $i     = 0;
    foreach ($taxes as $tax) {
        $taxes[$i]['taxname'] = $tax['taxname'] . '|' . $tax['taxrate'];
        $i++;
    }

    return $taxes;
}


/**
 * Calculate action percent by status
 * @param  mixed $status          action status
 * @param  mixed $total_estimates in case the total is calculated in other place
 * @return array
 */
function get_actions_percent_by_status($status, $total_actions = '')
{
    $has_permission_view                 = has_permission('actions', '', 'view');
    $has_permission_view_own             = has_permission('actions', '', 'view_own');
    $allow_staff_view_actions_assigned = get_option('allow_staff_view_actions_assigned');
    $staffId                             = get_staff_user_id();

    $whereUser = '';
    if (!$has_permission_view) {
        if ($has_permission_view_own) {
            $whereUser = '(addedfrom=' . $staffId;
            if ($allow_staff_view_actions_assigned == 1) {
                $whereUser .= ' OR assigned=' . $staffId;
            }
            $whereUser .= ')';
        } else {
            $whereUser .= 'assigned=' . $staffId;
        }
    }

    if (!is_numeric($total_actions)) {
        $total_actions = total_rows('tblactions', $whereUser);
    }

    $data            = [];
    $total_by_status = 0;
    $where           = 'status=' . $status;
    if (!$has_permission_view) {
        $where .= ' AND (' . $whereUser . ')';
    }

    $total_by_status = total_rows('tblactions', $where);
    $percent         = ($total_actions > 0 ? number_format(($total_by_status * 100) / $total_actions, 2) : 0);

    $data['total_by_status'] = $total_by_status;
    $data['percent']         = $percent;
    $data['total']           = $total_actions;

    return $data;
}

/**
 * Function that will search possible action templates in applicaion/views/admin/action/templates
 * Will return any found files and user will be able to add new template
 * @return array
 */
function get_action_templates()
{
    $action_templates = [];
    if (is_dir(VIEWPATH . 'admin/actions/templates')) {
        foreach (list_files(VIEWPATH . 'admin/actions/templates') as $template) {
            $action_templates[] = $template;
        }
    }

    return $action_templates;
}
/**
 * Check if staff member can view action
 * @param  mixed $id action id
 * @param  mixed $staff_id
 * @return boolean
 */
function user_can_view_action($id, $staff_id = false)
{
    $CI = &get_instance();

    $staff_id = $staff_id ? $staff_id : get_staff_user_id();

    if (has_permission('actions', $staff_id, 'view')) {
        return true;
    }

    $CI->db->select('id, addedfrom, assigned');
    $CI->db->from('tblactions');
    $CI->db->where('id', $id);
    $action = $CI->db->get()->row();

    if ((has_permission('actions', $staff_id, 'view_own') && $action->addedfrom == $staff_id)
            || ($action->assigned == $staff_id && get_option('allow_staff_view_actions_assigned') == 1)) {
        return true;
    }

    return false;
}
function parse_action_content_merge_fields($action)
{
    $id           = is_array($action) ? $action['id'] : $action->id;
    $merge_fields = [];
    $merge_fields = array_merge($merge_fields, get_action_merge_fields($id));
    $merge_fields = array_merge($merge_fields, get_other_merge_fields());
    foreach ($merge_fields as $key => $val) {
        $content = is_array($action) ? $action['content'] : $action->content;

        if (stripos($content, $key) !== false) {
            if (is_array($action)) {
                $action['content'] = str_ireplace($key, $val, $content);
            } else {
                $action->content = str_ireplace($key, $val, $content);
            }
        } else {
            if (is_array($action)) {
                $action['content'] = str_ireplace($key, '', $content);
            } else {
                $action->content = str_ireplace($key, '', $content);
            }
        }
    }

    return $action;
}

/**
 * Check if staff member have assigned actions / added as sale agent
 * @param  mixed $staff_id staff id to check
 * @return boolean
 */
function staff_has_assigned_actions($staff_id = '')
{
    $CI         = &get_instance();
    $staff_id = is_numeric($staff_id) ? $staff_id : get_staff_user_id();
    $cache    = $CI->object_cache->get('staff-total-assigned-actions-' . $staff_id);
    if (is_numeric($cache)) {
        $result = $cache;

    } else {
        $result = total_rows('tblactions', ['assigned' => $staff_id]);
        $CI->object_cache->add('staff-total-assigned-actions-' . $staff_id, $result);
    }

    return $result > 0 ? true : false;
}

function get_action_sql_where_staff($staff_id)
{
    $has_permission_view_own            = has_permission('actions', '', 'view_own');
    $allow_staff_view_invoices_assigned = get_option('allow_staff_view_actions_assigned');
    $whereUser                          = '';
    if ($has_permission_view_own) {
        $whereUser = '((tblactions.addedfrom=' . $staff_id . ' AND tblactions.addedfrom IN (SELECT staffid FROM tblstaffpermissions JOIN tblpermissions ON tblpermissions.permissionid=tblstaffpermissions.permissionid WHERE tblpermissions.name = "actions" AND can_view_own=1))';
        if ($allow_staff_view_invoices_assigned == 1) {
            $whereUser .= ' OR assigned=' . $staff_id;
        }
        $whereUser .= ')';
    } else {
        $whereUser .= 'assigned=' . $staff_id;
    }

    return $whereUser;
}

function prepare_actions_for_export($rel_id, $rel_type)
{
    // $readProposalsDir = '';
    // $tmpDir           = get_temp_dir();

    $CI               = &get_instance();

    if (!class_exists('actions_model')) {
        $CI->load->model('actions_model');
    }

    $CI->db->where('rel_id', $rel_id);
    $CI->db->where('rel_type', $rel_type);

    $actions = $CI->db->get('tblactions')->result_array();

    $CI->db->where('show_on_client_portal', 1);
    $CI->db->where('fieldto', 'action');
    $CI->db->order_by('field_order', 'asc');
    $custom_fields = $CI->db->get('tblcustomfields')->result_array();
/*
    if (count($actions) > 0) {
        $uniqueIdentifier = $tmpDir . $rel_id . time() . '-actions';
        $readProposalsDir = $uniqueIdentifier;
    }*/
    $CI->load->model('currencies_model');
    foreach ($actions as $proposaArrayKey => $action) {

        // $action['attachments'] = _prepare_attachments_array_for_export($CI->actions_model->get_attachments($action['id']));

       // $actions[$proposaArrayKey] = parse_action_content_merge_fields($action);

        $actions[$proposaArrayKey]['country'] = get_country($action['country']);

        $actions[$proposaArrayKey]['currency'] = $CI->currencies_model->get($action['currency']);

        $actions[$proposaArrayKey]['items'] = _prepare_items_array_for_export(get_items_by_type('action', $action['id']), 'action');

        $actions[$proposaArrayKey]['comments'] = $CI->actions_model->get_comments($action['id']);

        $actions[$proposaArrayKey]['views'] = get_views_tracking('action', $action['id']);

        $actions[$proposaArrayKey]['tracked_emails'] = get_tracked_emails($action['id'], 'action');

        $actions[$proposaArrayKey]['additional_fields'] = [];
        foreach ($custom_fields as $cf) {
            $actions[$proposaArrayKey]['additional_fields'][] = [
                    'name'  => $cf['name'],
                    'value' => get_custom_field_value($action['id'], $cf['id'], 'action'),
                ];
        }

      /*  $tmpProposalsDirName = $uniqueIdentifier;
        if (!is_dir($tmpProposalsDirName)) {
            mkdir($tmpProposalsDirName, 0755);
        }

        $tmpProposalsDirName = $tmpProposalsDirName . '/' . $action['id'];

        mkdir($tmpProposalsDirName, 0755);*/

/*        if (count($action['attachments']) > 0 || !empty($action['signature'])) {
            $attachmentsDir = $tmpProposalsDirName . '/attachments';
            mkdir($attachmentsDir, 0755);

            foreach ($action['attachments'] as $att) {
                xcopy(get_upload_path_by_type('action') . $action['id'] . '/' . $att['file_name'], $attachmentsDir . '/' . $att['file_name']);
            }

            if (!empty($action['signature'])) {
                xcopy(get_upload_path_by_type('action') . $action['id'] . '/' . $action['signature'], $attachmentsDir . '/' . $action['signature']);
            }
        }*/

        // unset($action['id']);

        // $fp = fopen($tmpProposalsDirName . '/action.json', 'w');
        // fwrite($fp, json_encode($action, JSON_PRETTY_PRINT));
        // fclose($fp);
    }

    return $actions;
}
