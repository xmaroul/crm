<?php

/**
 * Check if supplier id is used in the system
 * @param  mixed  $id supplier id
 * @return boolean
 */
function is_supplier_id_used($id)
{
    $total = 0;

    $total += total_rows('tblcontracts', [
        'supplier' => $id,
    ]);

    $total += total_rows('tblestimates', [
        'supplierid' => $id,
    ]);

    $total += total_rows('tblexpenses', [
        'supplierid' => $id,
    ]);

    $total += total_rows('tblinvoices', [
        'supplierid' => $id,
    ]);

    $total += total_rows('tblproposals', [
        'rel_id'   => $id,
        'rel_type' => 'supplier',
    ]);
	
	$total += total_rows('tblactions', [
        'rel_id'   => $id,
        'rel_type' => 'supplier',
    ]);

    $total += total_rows('tbltickets', [
        'userid' => $id,
    ]);

    $total += total_rows('tblprojects', [
        'supplierid' => $id,
    ]);

    $total += total_rows('tblstafftasks', [
        'rel_id'   => $id,
        'rel_type' => 'supplier',
    ]);

    $total += total_rows('tblcreditnotes', [
        'supplierid' => $id,
    ]);

    $total += total_rows('tblsubscriptions', [
        'supplierid' => $id,
    ]);

    if ($total > 0) {
        return true;
    }

    return false;
}
/**
 * Check if supplier has subscriptions
 * @param  mixed $id supplier id
 * @return boolean
 */
function supplier_has_subscriptions($id)
{
    return total_rows('tblsubscriptions', ['supplierid' => $id]) > 0;
}
/**
 * Get predefined tabs array, used in supplier profile
 * @param  mixed $supplier_id supplier id to prepare the urls
 * @return array
 */
function get_supplier_profile_tabs($supplier_id)
{
    $supplier_tabs = [
      [
        'name'    => 'profile',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=profile'),
        'icon'    => 'fa fa-user-circle',
        'lang'    => _l('supplier_add_edit_profile'),
        'visible' => true,
        'order'   => 1,
    ],
	[
        'name'    => 'actions',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=actions'),
        'icon'    => 'fa fa-arrows-h',
        'lang'    => _l('actions'),
        'visible' => (has_permission('actions', '', 'view') || has_permission('actions', '', 'view_own') || (get_option('allow_staff_view_actions_assigned') == 1 && staff_has_assigned_actions())),
        'order'   => 7,
    ],
    [
        'name'    => 'contacts',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=contacts'),
        'icon'    => 'fa fa-users',
        'lang'    => !is_empty_supplier_company($supplier_id) || empty($supplier_id) ? _l('supplier_contacts') : _l('contact'),
        'visible' => true,
        'order'   => 2,
    ],
      [
        'name'    => 'notes',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=notes'),
        'icon'    => 'fa fa-sticky-note-o',
        'lang'    => _l('contracts_notes_tab'),
        'visible' => true,
        'order'   => 3,
    ],
      [
        'name'    => 'attachments',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=attachments'),
        'icon'    => 'fa fa-paperclip',
        'lang'    => _l('supplier_attachments'),
        'visible' => true,
        'order'   => 16,
    ],
	[
        'name'    => 'map',
        'url'     => admin_url('suppliers/supplier/' . $supplier_id . '?group=map'),
        'icon'    => 'fa fa-map-marker',
        'lang'    => _l('supplier_map'),
        'visible' => true,
        'order'   => 19,
    ],

  ];

    $hook_data     = do_action('supplier_profile_tabs', ['tabs' => $supplier_tabs, 'supplier_id' => $supplier_id]);
    $supplier_tabs = $hook_data['tabs'];

    usort($supplier_tabs, function ($a, $b) {
        return $a['order'] - $b['order'];
    });
    return $supplier_tabs;
}

/**
 * Get supplier id by lead id
 * @since  Version 1.0.1
 * @param  mixed $id lead id
 * @return mixed     supplier id
 */
function get_supplier_id_by_lead_id($id)
{
    $CI = & get_instance();
    $CI->db->select('userid')->from('tblsuppliers')->where('leadid', $id);

    return $CI->db->get()->row()->userid;
}

/**
 * Check if supplier have invoices with multiple currencies
 * @return booelan
 */
function is_supplier_using_multiple_currencies($supplierid = '', $table = 'tblinvoices')
{
    $CI = & get_instance();

    $supplierid = $supplierid == '' ? get_supplier_user_id() : $supplierid;
    $CI->load->model('currencies_model');
    $currencies            = $CI->currencies_model->get();
    $total_currencies_used = 0;
    foreach ($currencies as $currency) {
        $CI->db->where('currency', $currency['id']);
        $CI->db->where('supplierid', $supplierid);
        $total = $CI->db->count_all_results($table);
        if ($total > 0) {
            $total_currencies_used++;
        }
    }
    if ($total_currencies_used > 1) {
        return true;
    } elseif ($total_currencies_used == 0 || $total_currencies_used == 1) {
        return false;
    }

    return true;
}


/**
 * Function used to check if is really empty supplier company
 * Can happen user to have selected that the company field is not required and the primary contact name is auto added in the company field
 * @param  mixed  $id
 * @return boolean
 */
function is_empty_supplier_company($id)
{
    $CI = & get_instance();
    $CI->db->select('company');
    $CI->db->from('tblsuppliers');
    $CI->db->where('userid', $id);
    $row = $CI->db->get()->row();
    if ($row) {
        if ($row->company == '') {
            return true;
        }

        return false;
    }

    return true;
}

/**
 * Get ids to check what files with contacts are shared
 * @param  array  $where
 * @return array
 */
function get_supplier_profile_file_sharing($where = [])
{
    $CI = & get_instance();
    $CI->db->where($where);

    return $CI->db->get('tblsupplierfiles_shares')->result_array();
}

/**
 * Get supplier default language
 * @param  mixed $supplierid
 * @return mixed
 */
function get_supplier_default_language($supplierid = '')
{
    if (!is_numeric($supplierid)) {
        $supplierid = get_supplier_user_id();
    }
    $CI = & get_instance();
    $CI->db->select('default_language');
    $CI->db->from('tblsuppliers');
    $CI->db->where('userid', $supplierid);
    $supplier = $CI->db->get()->row();
    if ($supplier) {
        return $supplier->default_language;
    }

    return '';
}

/**
 * Function is supplier admin
 * @param  mixed  $id       supplier id
 * @param  staff_id  $staff_id staff id to check
 * @return boolean
 */
function is_supplier_admin($id, $staff_id = '')
{
    $staff_id = is_numeric($staff_id) ? $staff_id : get_staff_user_id();
    $CI       = &get_instance();
    $cache    = $CI->object_cache->get($id . '-is-supplier-admin-' . $staff_id);

    if ($cache) {
        return $cache['retval'];
    }

    $total = total_rows('tblsupplieradmins', [
        'supplier_id' => $id,
        'staff_id'    => $staff_id,
    ]);

    $retval = $total > 0 ? true : false;
    $CI->object_cache->add($id . '-is-supplier-admin-' . $staff_id, ['retval' => $retval]);

    return $retval;
}
/**
 * Check if staff member have assigned suppliers
 * @param  mixed $staff_id staff id
 * @return boolean
 */
function have_assigned_suppliers($staff_id = '')
{
    $CI       = &get_instance();
    $staff_id = is_numeric($staff_id) ? $staff_id : get_staff_user_id();
    $cache    = $CI->object_cache->get('staff-total-assigned-suppliers-' . $staff_id);

    if (is_numeric($cache)) {
        $result = $cache;
    } else {
        $result = total_rows('tblsupplieradmins', [
            'staff_id' => $staff_id,
        ]);
        $CI->object_cache->add('staff-total-assigned-suppliers-' . $staff_id, $result);
    }

    return $result > 0 ? true : false;
}

/**
 * Load suppliers area language
 * @param  string $supplier_id
 * @return string return loaded language
 */
function load_supplier_language($supplier_id = '')
{
    $CI       = & get_instance();
    $language = get_option('active_language');
    if (is_supplier_logged_in() || $supplier_id != '') {
        $supplier_language = get_supplier_default_language($supplier_id);
        if (!empty($supplier_language)) {
            if (file_exists(APPPATH . 'language/' . $supplier_language)) {
                $language = $supplier_language;
            }
        }
    }

    $CI->lang->is_loaded = [];
    $CI->lang->language  = [];

    $CI->lang->load($language . '_lang', $language);
    if (file_exists(APPPATH . 'language/' . $language . '/custom_lang.php')) {
        $CI->lang->load('custom_lang', $language);
    }

    $language = do_action('after_load_supplier_language', $language);

    return $language;
}
/**
 * Check if supplier have transactions recorded
 * @param  mixed $id supplierid
 * @return boolean
 */
function supplier_have_transactions($id)
{
    $total_transactions = 0;

    $total_transactions += total_rows('tblinvoices', [
        'supplierid' => $id,
    ]);

    $total_transactions += total_rows('tblcreditnotes', [
        'supplierid' => $id,
    ]);

    $total_transactions += total_rows('tblestimates', [
        'supplierid' => $id,
    ]);

    $total_transactions += total_rows('tblexpenses', [
        'supplierid' => $id,
        'billable' => 1,
    ]);

    $total_transactions += total_rows('tblproposals', [
        'rel_id'   => $id,
        'rel_type' => 'supplier',
    ]);
	
	$total_transactions += total_rows('tblactions', [
        'rel_id'   => $id,
        'rel_type' => 'supplier',
    ]);

    if ($total_transactions > 0) {
        return true;
    }

    return false;
}

/**
* With this function staff can login as supplier in the suppliers area
* @param  mixed $id supplier id
*/
function login_as_supplier($id)
{
    $CI = &get_instance();

    $CI->db->select('tblcontacts.id')
    ->where('userid', $id)
    ->where('is_primary', 1);

    $primary = $CI->db->get('tblcontacts')->row();

    if (!$primary) {
        set_alert('danger', _l('no_primary_contact'));
        redirect($_SERVER['HTTP_REFERER']);
    }

    $user_data = [
        'supplier_user_id'      => $id,
        'contact_user_id'     => $primary->id,
        'supplier_logged_in'    => true,
        'logged_in_as_supplier' => true,
    ];

    $CI->session->set_userdata($user_data);
}

function send_supplier_registered_email_to_administrators($supplier_id)
{
    $CI = &get_instance();
    $CI->load->model('staff_model');
    $admins = $CI->staff_model->get('', ['active' => 1, 'admin' => 1]);

    $CI->load->model('emails_model');
    foreach ($admins as $admin) {
        $merge_fields = get_supplier_contact_merge_fields($supplier_id, get_primary_contact_user_id($supplier_id));
        $CI->emails_model->send_email_template('new-supplier-registered-to-admin', $admin['email'], $merge_fields);
    }
}


/**
*  Get supplier attachment
* @param   mixed $id   supplier id
* @return  array
*/
function get_all_supplier_attachments($id)
{
    $CI = &get_instance();

    $attachments                = [];
    $attachments['invoice']     = [];
    $attachments['estimate']    = [];
    $attachments['credit_note'] = [];
    $attachments['proposal']    = [];
	$attachments['action']      = [];
    $attachments['contract']    = [];
    $attachments['lead']        = [];
    $attachments['task']        = [];
    $attachments['supplier']    = [];
    $attachments['ticket']      = [];
    $attachments['expense']     = [];

    $has_permission_expenses_view = has_permission('expenses', '', 'view');
    $has_permission_expenses_own  = has_permission('expenses', '', 'view_own');
    if ($has_permission_expenses_view || $has_permission_expenses_own) {
        // Expenses
        $CI->db->select('supplierid,id');
        $CI->db->where('supplierid', $id);
        if (!$has_permission_expenses_view) {
            $CI->db->where('addedfrom', get_staff_user_id());
        }

        $CI->db->from('tblexpenses');
        $expenses = $CI->db->get()->result_array();
        $ids      = array_column($expenses, 'id');
        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'expense');
            $_attachments = $CI->db->get('tblfiles')->result_array();
            foreach ($_attachments as $_att) {
                array_push($attachments['expense'], $_att);
            }
        }
    }


    $has_permission_invoices_view = has_permission('invoices', '', 'view');
    $has_permission_invoices_own  = has_permission('invoices', '', 'view_own');
    if ($has_permission_invoices_view || $has_permission_invoices_own || get_option('allow_staff_view_invoices_assigned') == 1) {
        // Invoices
        $CI->db->select('supplierid,id');
        $CI->db->where('supplierid', $id);

        if (!$has_permission_invoices_view) {
            $CI->db->where(get_invoices_where_sql_for_staff(get_staff_user_id()));
        }

        $CI->db->from('tblinvoices');
        $invoices = $CI->db->get()->result_array();

        $ids = array_column($invoices, 'id');
        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'invoice');
            $_attachments = $CI->db->get('tblfiles')->result_array();
            foreach ($_attachments as $_att) {
                array_push($attachments['invoice'], $_att);
            }
        }
    }

    $has_permission_credit_notes_view = has_permission('credit_notes', '', 'view');
    $has_permission_credit_notes_own  = has_permission('credit_notes', '', 'view_own');

    if ($has_permission_credit_notes_view || $has_permission_credit_notes_own) {
        // credit_notes
        $CI->db->select('supplierid,id');
        $CI->db->where('supplierid', $id);

        if (!$has_permission_credit_notes_view) {
            $CI->db->where('addedfrom', get_staff_user_id());
        }

        $CI->db->from('tblcreditnotes');
        $credit_notes = $CI->db->get()->result_array();

        $ids = array_column($credit_notes, 'id');
        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'credit_note');
            $_attachments = $CI->db->get('tblfiles')->result_array();
            foreach ($_attachments as $_att) {
                array_push($attachments['credit_note'], $_att);
            }
        }
    }

    $permission_estimates_view = has_permission('estimates', '', 'view');
    $permission_estimates_own  = has_permission('estimates', '', 'view_own');

    if ($permission_estimates_view || $permission_estimates_own || get_option('allow_staff_view_proposals_assigned') == 1 || get_option('allow_staff_view_actions_assigned') == 1) {
        // Estimates
        $CI->db->select('supplierid,id');
        $CI->db->where('supplierid', $id);
        if (!$permission_estimates_view) {
            $CI->db->where(get_estimates_where_sql_for_staff(get_staff_user_id()));
        }
        $CI->db->from('tblestimates');
        $estimates = $CI->db->get()->result_array();

        $ids = array_column($estimates, 'id');
        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'estimate');
            $_attachments = $CI->db->get('tblfiles')->result_array();

            foreach ($_attachments as $_att) {
                array_push($attachments['estimate'], $_att);
            }
        }
    }

    $has_permission_proposals_view = has_permission('proposals', '', 'view');
    $has_permission_proposals_own  = has_permission('proposals', '', 'view_own');

    if ($has_permission_proposals_view || $has_permission_proposals_own || get_option('allow_staff_view_proposals_assigned') == 1) {
        // Proposals
        $CI->db->select('rel_id,id');
        $CI->db->where('rel_id', $id);
        $CI->db->where('rel_type', 'supplier');
        if (!$has_permission_proposals_view) {
            $CI->db->where(get_proposals_sql_where_staff(get_staff_user_id()));
        }
        $CI->db->from('tblproposals');
        $proposals = $CI->db->get()->result_array();

        $ids = array_column($proposals, 'id');

        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'proposal');
            $_attachments = $CI->db->get('tblfiles')->result_array();

            foreach ($_attachments as $_att) {
                array_push($attachments['proposal'], $_att);
            }
        }
    }

	$has_permission_actions_view = has_permission('actions', '', 'view');
    $has_permission_actions_own  = has_permission('actions', '', 'view_own');

    if ($has_permission_actions_view || $has_permission_actions_own || get_option('allow_staff_view_actions_assigned') == 1) {
        // Proposals
        $CI->db->select('rel_id,id');
        $CI->db->where('rel_id', $id);
        $CI->db->where('rel_type', 'supplier');
        if (!$has_permission_actions_view) {
            $CI->db->where(get_actions_sql_where_staff(get_staff_user_id()));
        }
        $CI->db->from('tblactions');
        $actions = $CI->db->get()->result_array();

        $ids = array_column($actions, 'id');

        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'action');
            $_attachments = $CI->db->get('tblfiles')->result_array();

            foreach ($_attachments as $_att) {
                array_push($attachments['action'], $_att);
            }
        }
    }
	
    $permission_contracts_view = has_permission('contracts', '', 'view');
    $permission_contracts_own  = has_permission('contracts', '', 'view_own');
    if ($permission_contracts_view || $permission_contracts_own) {
        // Contracts
        $CI->db->select('supplier,id');
        $CI->db->where('supplier', $id);
        if (!$permission_contracts_view) {
            $CI->db->where('addedfrom', get_staff_user_id());
        }
        $CI->db->from('tblcontracts');
        $contracts = $CI->db->get()->result_array();

        $ids = array_column($contracts, 'id');

        if (count($ids) > 0) {
            $CI->db->where_in('rel_id', $ids);
            $CI->db->where('rel_type', 'contract');
            $_attachments = $CI->db->get('tblfiles')->result_array();

            foreach ($_attachments as $_att) {
                array_push($attachments['contract'], $_att);
            }
        }
    }

    $CI->db->select('leadid')
    ->where('userid', $id);
    $supplier = $CI->db->get('tblsuppliers')->row();

    if ($supplier->leadid != null) {
        $CI->db->where('rel_id', $supplier->leadid);
        $CI->db->where('rel_type', 'lead');
        $_attachments = $CI->db->get('tblfiles')->result_array();
        foreach ($_attachments as $_att) {
            array_push($attachments['lead'], $_att);
        }
    }

    $CI->db->select('ticketid,userid');
    $CI->db->where('userid', $id);
    $CI->db->from('tbltickets');
    $tickets = $CI->db->get()->result_array();

    $ids = array_column($tickets, 'ticketid');

    if (count($ids) > 0) {
        $CI->db->where_in('ticketid', $ids);
        $_attachments = $CI->db->get('tblticketattachments')->result_array();

        foreach ($_attachments as $_att) {
            array_push($attachments['ticket'], $_att);
        }
    }

    $has_permission_tasks_view = has_permission('tasks', '', 'view');
    $CI->db->select('rel_id, id');
    $CI->db->where('rel_id', $id);
    $CI->db->where('rel_type', 'supplier');

    if (!$has_permission_tasks_view) {
        $CI->db->where(get_tasks_where_string(false));
    }

    $CI->db->from('tblstafftasks');
    $tasks = $CI->db->get()->result_array();

    $ids = array_column($tasks, 'ticketid');
    if (count($ids) > 0) {
        $CI->db->where_in('rel_id', $ids);
        $CI->db->where('rel_type', 'task');

        $_attachments = $CI->db->get('tblfiles')->result_array();

        foreach ($_attachments as $_att) {
            array_push($attachments['task'], $_att);
        }
    }

    $CI->db->where('rel_id', $id);
    $CI->db->where('rel_type', 'supplier');
    $supplier_main_attachments = $CI->db->get('tblfiles')->result_array();

    $attachments['supplier'] = $supplier_main_attachments;

    return $attachments;
}



add_action('check_vault_entries_visibility', '_check_vault_entries_visibility');


